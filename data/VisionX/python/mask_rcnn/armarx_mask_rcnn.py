#!/usr/bin/env python3

import time
import logging
import numpy as np
import skimage.transform
import skimage.io

from armarx.parser import ArmarXArgumentParser as ArgumentParser

from armarx.ice_manager import wait_for_proxy
from armarx.ice_manager import is_alive

from armarx.image_processor import ImageProcessor
from visionx import ImageProviderInterfacePrx

from mrcnn import model as modellib

import os

logger = logging.getLogger(__name__)

from mrcnn.config import Config

class BalloonConfig(Config):
    """Configuration for training on the toy  dataset.
    Derives from the base Config class and overrides some values.
    """
    NAME = 'armarx'
    # We use a GPU with 12GB memory, which can fit two images.
    # Adjust down if you use a smaller GPU.
    IMAGES_PER_GPU = 1
    # Skip detections with < 90% confidence
    DETECTION_MIN_CONFIDENCE = 0.70

    GPU_COUNT = 1

    NUM_CLASSES = 1 + 80


def color_splash(image, mask):
    """Apply color splash effect.
    image: RGB image [height, width, 3]
    mask: instance segmentation mask [height, width, instance count]

    Returns result image.
    """
    # Make a grayscale copy of the image. The grayscale copy still
    # has 3 RGB channels, though.
    gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 255

    # We're treating all instances as one, so collapse the mask into one layer
    mask = (np.sum(mask, -1, keepdims=True) >= 1)
    # Copy color pixels from the original color image where mask is set
    if mask.shape[0] > 0:
        splash = np.where(mask, image, gray).astype(np.uint8)
    else:
        # splash = gray
        splash = np.uint8(gray)
    return splash


class MaskRCNNImageProcessor(ImageProcessor):

    def __init__(self, provider_name, data_dir, image_number=0):
        super().__init__(provider_name)
        self.image_number = image_number

        model_dir = os.path.join(data_dir, 'logs')
        weights_path = os.path.join(data_dir, 'mask_rcnn_coco.h5')

        self.model = modellib.MaskRCNN(mode="inference", config=BalloonConfig(), model_dir=model_dir)
        self.model.load_weights(weights_path, by_name=True)
        self.model.keras_model._make_predict_function()

    def process_image(self, images):
        test = images.copy()
        image = images[self.image_number]
        r = self.model.detect([image], verbose=0)[0]
        logger.debug('done')
        splash = color_splash(image, r['masks'])
        test[self.image_number] = splash
        return test


def main():

    parser = ArgumentParser(description='MaskRCNN for ArmarX')
    parser.add_argument('-n', '--component-name', default='MaskRCNN', help='component name of the image provider')
    parser.add_argument('-i', '--input-name', default='KLGImageProvider', help='name of the image provider')
    parser.add_argument('-d', '--data-dir', default='/common/homes/staff/grotz/libs/mask-fusion/deps/Mask_RCNN/data')

    args = parser.parse_args()

    image_number = 0
    provider_name = args.input_name.split(':')[0]
    if ':' in provider_name:
        provider_name, image_number = provider_name.split(':')
        image_number = int(image_number)

    logger.debug('trying to connect to image provider {}:{}'.format(provider_name, image_number))
    wait_for_proxy(ImageProviderInterfacePrx, provider_name)

    image_processor = MaskRCNNImageProcessor(args.input_name, args.data_dir, image_number)
    image_processor.register()

    try:
        while is_alive():
            time.sleep(0.1)
    except KeyboardInterrupt:
        logger.info('shutting down')
    finally:
        image_processor.shutdown()


if __name__ == '__main__':
    main()
