#!/usr/bin/env python2


import logging
import random

random.seed(123)
import sys
import argparse
import pickle

parser = argparse.ArgumentParser(description='Face recognition with deep learning')
parser.add_argument('--record', help='record images', dest="record", default=None)
parser.add_argument('--encodings', help='Encode found faces and store them', action="store_true", dest="encodings")
parser.add_argument('--learn', help='Train the classifier and use it', action="store_true", dest="learn")
parser.add_argument('--input', help='input video file instead of camera', dest="input_file", default=None)
parser.add_argument('--inputdir', help='input directoy with images instead of camera', dest="input_dir", default=None)
parser.add_argument('--classifier', help='load a stored classifier from a picke file', dest="clf_file", default=None, action="store_true")
parser.add_argument('--face-distinguisher', help='trains a face distinguisher', dest="train_distinguisher", default=None, action="store_true")
parser.add_argument('--start-disabled', help='doesnt run the recognition at start up, only when called via Ice', dest="start_disabled", default=None, action="store_true")
parser.add_argument('--show-window', help='shows the results in its own window', dest="show_visu", default=None, action="store_true")
parser.add_argument('--show-confidence', help='shows the confidence in the result window', dest="show_confidence", default=None, action="store_true")
parser.add_argument('--image-provider', help='shows the confidence in the result window', dest="image_provider", default="WebCamImageProvider")
parser.add_argument('--train-gender-clf', help='trains the gender classifier', dest="train_gender_clf", default=None, action="store_true")
parser.add_argument('--split-encodings', help='split the encodings file into a training and test set', dest="train_split_encodings", default=None, action="store_true")
parser.add_argument('--benchmark', help='run the benchmark', dest="benchmark", default=None, action="store_true")
parser.add_argument('--benchmarkDIR', help='run the benchmark with full DIR (Detection and Identification Rate', dest="benchmark_DIR", default=None, action="store_true")
parser.add_argument('--merge-encodings', nargs='*', help='Merge two or more encodings into one file: in, in,..., out', dest="merge_encodings")


args = parser.parse_args()

import cv2
import face_recognition
import numpy as np


from armarx.interface_helper import load_armarx_slice
import time
load_armarx_slice('VisionX', 'core/DataTypes.ice')
load_armarx_slice('VisionX', 'core/ImageProviderInterface.ice')
load_armarx_slice('VisionX', 'core/ImageProcessorInterface.ice')
load_armarx_slice('VisionX', 'components/FaceRecognitionInterface.ice')
load_armarx_slice('VisionX', 'components/Calibration.ice')
load_armarx_slice('RobotAPI', 'speech/SpeechInterface.ice')
load_armarx_slice('ArmarXCore', 'core/SharedMemory.ice')
load_armarx_slice('ArmarXCore', 'core/BasicVectorTypes.ice')
# load_armarx_slice('MemoryX', 'workingmemory/MotionModel.ice')
# load_armarx_slice('MemoryX', 'component/LongtermMemoryInterface.ice')

from visionx import ImageProviderInterfacePrx, MonocularCalibrationCapturingProviderInterfacePrx, StereoCalibrationProviderInterfacePrx
from visionx import FaceRecognitionInterface, FaceLocation
from armarx import Vector2f

from armarx.interface_helper import ice_helper
from armarx.interface_helper import get_proxy, register_object, using_topic

import os
from armarx import ArmarXBuilder
import re
import ResultImageProvider


from threading import Lock


class FaceRecognizer(object):
    unknownFaceLabel = "Unknown"
    similar_face_dict = None
    face_distance_cash = {}
    def __init__(self, threshold = 0.6, resize_factor = 0.5):
        self.trained_face_encodings = []
        self.labels = []
        self.label_weights = {}
        self.resize_factor = resize_factor
        self.threshold=threshold
        self.clf = None
        self.clf_2class = None
        self.gender_clf = None
        self.unknown_face_classes = []
        self.image_upsampling = 1
        self.detection_model = "cnn" # cnn or hog
        self.alpha = threshold # recognition threshold of similarity of 2 descriptors
        self.beta = 0.43 # threshold above which verificationq by verifier network is needed
        self.gamma = 0.85 # 0.85minimum confidence of verifier network and distance confidence
        self.distance_range = (0.4,0.7) # range used for distance confidence. value below lower range -> full confidence

        self.last_descriptor_distance = None
        self.last_clf_same_class_confidence = None
        self.last_decider = None

    def set_faces(self, face_encodings, labels, merge_classes = True, limit_number_of_known_faces = -1):
        tmp_dict = {}
        if merge_classes:
            logging.info("merging face descriptors")
            self.trained_face_encodings = []
            self.labels = []

            for (face_encoding, label) in zip(face_encodings, labels):
                if label in tmp_dict:
                    tmp_dict[label].append(face_encoding)
                else:
                    if limit_number_of_known_faces > 0 and len(tmp_dict) >= limit_number_of_known_faces:
                        continue
                    tmp_dict[label] = [face_encoding]
            for label in tmp_dict:
                array = np.asarray(tmp_dict[label])

                mean_encodings = np.mean(array, axis=0)

                self.labels.append(label)
                # if(len(tmp_dict[label]) > 1):
                #     feature_variance = np.var(array, axis=0)
                #     print("variance for  " + label + ": " + str(feature_variance))
                #     feature_variance = 1.0/feature_variance
                #     feature_weights = np.divide(feature_variance, np.mean(feature_variance, axis=0))
                #     print(
                #     "Feature weights: " + str(feature_weights) + " average weight: " + str(np.average(feature_weights)))
                #     self.label_weights[label] = feature_weights
                # else:
                #     self.label_weights[label] = np.ones(128)
                self.trained_face_encodings.append(mean_encodings)


        else:
            logging.info("NOT merging face descriptors")
            self.trained_face_encodings = face_encodings
            self.labels = labels

    def train_from_pictures(self, package, path):
        files = self.getFiles(package, path)
        known_image_file_name_tuples = self.extractNamePathTuples(files)
        resultPath = "/tmp/face_recognition_training_" + os.getenv("USER") + "/"
        if not os.path.exists(resultPath):
            os.mkdir(resultPath)
        for known_image_tuple in known_image_file_name_tuples:
            name = known_image_tuple[0]
            path = known_image_tuple[1]
            known_image = cv2.imread(path)
            known_image = cv2.cvtColor(known_image,cv2.COLOR_BGR2RGB)
            # known_image = face_recognition.load_image_file(path)
            # known_image = cv2.cvtColor(known_image,cv2.COLOR_BGR2RGB)
            face_locations = face_recognition.face_locations(known_image, self.image_upsampling, model=self.detection_model)
            print("I found {} face(s) in this photograph of '{}' in file {}.".format(len(face_locations), name, path))
            for face_location in face_locations:
                top, right, bottom, left = face_location
                cv2.rectangle(known_image, (left,top), (right,bottom), (255,0,0))
            cv2.imwrite(resultPath + os.path.basename(path), cv2.cvtColor(known_image,cv2.COLOR_RGB2BGR))
            # cv2.imshow(path,known_image)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            if len(face_locations) != 1:
                continue
            self.trained_face_encodings.append(face_recognition.face_encodings(known_image)[0])
            self.labels.append(name)
        self.train()

    def train(self):
        logging.info("Training classifier")
        # from sklearn import feature_selection
        #
        # from sklearn import preprocessing
        #
        # self.le = preprocessing.LabelEncoder()
        # self.le.fit(self.labels)
        # self.label_indices = self.le.fit_transform(self.labels)
        # self.feature_scores = {}
        # for label in self.le.classes_:
        #     indices = np.copy(self.label_indices)
        #     for i in range(len(indices)):
        #         cur_label = self.le.inverse_transform(indices[i])
        #         if cur_label != label:
        #             indices[i] = -1
        #     sel = feature_selection.SelectKBest()
        #
        #     sel.fit_transform(self.trained_face_encodings, indices)
        #     normalized_scores = sel.scores_/np.average(sel.scores_)
        #     self.feature_scores[label] =  normalized_scores


        # feature_order = np.argsort(sel.scores_)
        # sorted_scores = np.sort(sel.scores_)

        # import numpy as np
        # from sklearn.manifold import TSNE
        # X = np.array(self.trained_face_encodings)
        # model = TSNE(n_components=2, random_state=0)
        # np.set_printoptions(suppress=True)
        # Y = model.fit_transform(X)
        #
        # import matplotlib.pyplot as plt
        # from mpl_toolkits.mplot3d import Axes3D
        # from matplotlib.ticker import NullFormatter
        #
        #
        # fig = plt.figure(figsize=(15, 8))
        # plt.suptitle("Manifold Learning with %i points, %i neighbors"% (len(self.labels), 1), fontsize=14)
        # t0 = time.time()
        # t1 = time.time()
        # print("t-SNE: %.2g sec" % (t1 - t0))
        # ax = fig.add_subplot(2, 5, 10)
        # plt.scatter(Y[:, 0], Y[:, 1], cmap=plt.cm.Spectral)
        # plt.title("t-SNE (%.2g sec)" % (t1 - t0))
        # ax.xaxis.set_major_formatter(NullFormatter())
        # ax.yaxis.set_major_formatter(NullFormatter())
        # plt.axis('tight')
        #
        # plt.show()
        logging.info("Training classifier done")

    def detect(self, image, auto_resize = True):
        if self.resize_factor != 1.0:
            small_frame = cv2.resize(image, (0, 0), fx=self.resize_factor, fy=self.resize_factor)
        else:
            small_frame = image

        height, width, channels = small_frame.shape
        resize_factor = 1.0
        if auto_resize and width > 800:
            resize_factor = 800.0 / width
            small_frame = cv2.resize(small_frame, (int(width * resize_factor), int(height * resize_factor)))
            logging.info("Resized to " + str(small_frame.shape[1]) + "x" + str(small_frame.shape[0]))

        t1 = time.time()
        face_locations = face_recognition.face_locations(small_frame, self.image_upsampling, model=self.detection_model)
        height, width, channels = small_frame.shape
        center = np.asarray([height*0.5, width*0.5])

        logging.debug("face_locations took " + str((time.time() - t1) * 1000) + " ms")
        t1 = time.time()
        compare = lambda x,y: -1 if np.linalg.norm(np.array([(x[1]+x[3])*0.5, (x[0]+x[2])*0.5]) - center) < np.linalg.norm(np.array([(y[1]+y[3])*0.5, (y[0]+y[2])*0.5]) - center) else 1
        face_locations.sort(compare)
        face_encodings = face_recognition.face_encodings(small_frame, face_locations,1)
        logging.debug("face_encodings took " + str((time.time() - t1) * 1000) + " ms")
        t1 = time.time()
        i = 0
        # if(len(face_locations)>1):
        #     for x in face_locations:
        #         distance = np.linalg.norm(np.array([(x[1]+x[3])*0.5, (x[0]+x[2])*0.5]) - center)
        #         print ("location: " + str(np.array([(x[1] + x[3]) * 0.5, (x[0] + x[2]) * 0.5])), " center: " + str(center) + " distance: " + str(distance))

        for face_location in face_locations:
            # face_location_list = list(face_location)
            # face_location_list[0] /= self.resize_factor
            # face_location_list[1] /= self.resize_factor
            # face_location_list[2] /= self.resize_factor
            # face_location_list[3] /= self.resize_factor
            face_locations[i] = [coord/self.resize_factor/resize_factor for coord in face_location]
            i+=1
        logging.debug("Face locations: " + str(face_locations))

        confidences = []
        face_names = []
        for face_encoding in face_encodings:
            (face_name, confidence) = self.recognize(face_encoding, True)
            face_names.append(face_name)
            confidences.append(confidence)
            # if face_name == self.unknownFaceLabel:
            #     self.update_unknown_face_classes(face_encoding)
            #     self.get_frequent_unknown_face_classes()


        return zip(face_locations, face_names, confidences)

    def recognize(self, face_encoding, use_open_set_recognizer = True):
        # See if the face is a match for the known face(s)
        t1 = time.time()
        if self.clf is not None:
            probabilities = self.clf.predict_proba(face_encoding.reshape(1, -1))
            max = np.argmax(probabilities)
            label = self.le.inverse_transform(max)
            confidence = probabilities[0][max]

            if confidence > self.threshold:
                if self.clf_2class is not None:
                    # get first face_encoding of that class
                    comparison_face = None
                    i = 0
                    for cur_label in self.labels:
                        if cur_label == label:
                            comparison_face = self.trained_face_encodings[i]
                        i += 1
                    face_diff = comparison_face - face_encoding
                    t1 = time.time()
                    pred = self.clf_2class.predict(face_diff.reshape(1, -1))
                    t2 = time.time()
                    print "pred: ", pred, " svm-prob:", confidence, "label: ", label, " time: ", (t2 - t1) * 1000
                    if pred[0] < 0.8:
                        label = self.unknownFaceLabel
                        confidence = np.clip(1 - pred[0], 0, 1)

                face_name = label

            else:
                face_name = self.unknownFaceLabel
                confidence = (1.0 - confidence)
            indices2 = np.delete(probabilities, max)
            max2 = np.argmax(indices2)
            labels2 = self.le.inverse_transform(max2)
            print label, "max: ", probabilities[0][max], labels2, " 2nd max: ", indices2[max2]
        elif not use_open_set_recognizer:
            distances = face_recognition.face_distance(self.trained_face_encodings, face_encoding)
            t0 = time.time()
            min_distance_index = np.argmin(distances)


            selected_face_encoding = self.trained_face_encodings[min_distance_index]
            logging.info("descriptor diff: %.2f, match: %s",
                         distances[min_distance_index],
                         self.labels[min_distance_index])  # " time: ", (t2-t1)*1000

            if distances[min_distance_index] < self.alpha:
                face_name = self.labels[min_distance_index]
                confidence = min(1.0, 1.0 - (distances[min_distance_index] - 0.3) / (self.alpha + 0.2 - 0.3))
            else:
                face_name = self.unknownFaceLabel
                confidence = 1.0 - min(1.0, 1.0 - (distances[min_distance_index] - 0.3) / (self.alpha + 0.2 - 0.3))
        else:
            (index, confidence) = self.recognize_open_set(face_encoding, self.trained_face_encodings, self.labels)

            if index < 0:
                face_name = self.unknownFaceLabel
            else:
                face_name = self.labels[index]
                # logging.info("Best match: " + face_name + " with confidence: " + str(confidence))



        logging.debug("comparison took " + str((time.time() - t1) * 1000) + " ms")
        return (face_name, confidence)


    def recognize_open_set(self, face_encoding, known_face_encodings, labels = None):

        distances = face_recognition.face_distance(known_face_encodings, face_encoding)
        # distances = []
        #
        # i = 0
        # for trained_face in self.known_face_encodings:
        #     distances.append(np.linalg.norm(np.multiply(trained_face - face_encoding, self.feature_scores[self.labels[i]])))
        #     i+=1
        t0 = time.time()
        min_distance_index = np.argmin(distances)

        # labels = self.le.inverse_transform(min_distance_index)
        # confidences.append(min_distance_index)

        selected_face_encoding = known_face_encodings[min_distance_index]
        confidence = 1.0 - np.clip((distances[min_distance_index] - self.distance_range[0])/(self.distance_range[1]-self.distance_range[0]), 0.0, 1.0) # min(1.0, 1.0 - (distances[min_distance_index]-0.3)/(self.threshold+0.2 -0.3))
        face_diff = selected_face_encoding - face_encoding
        t1 = time.time()
        # input_data = np.concatenate(
        #     (np.asarray(selected_face_encoding), np.asarray(face_encoding), np.absolute(np.asarray(selected_face_encoding) - np.asarray(face_encoding))))
        input_data = np.absolute(np.asarray(selected_face_encoding) - np.asarray(face_encoding))

        pred = self.clf_2class.predict_proba(input_data.reshape(1, -1))
        t2 = time.time()
        self.last_descriptor_distance = distances[min_distance_index]
        self.last_clf_same_class_confidence = pred[0][1]
        self.last_decider = "DescDist"
        if distances[min_distance_index] < self.alpha:#self.threshold:
            index = min_distance_index
            if self.clf_2class is not None:
                # threshold = 0.95 * distances[min_distance_index] / self.threshold
                # threshold = np.clip(threshold, 0.7, 0.99)
                distance_confidence = 1.0 - np.clip((distances[min_distance_index] - self.distance_range[0])/(self.distance_range[1]-self.distance_range[0]), 0.0, 1.0)
                average_confidence =  0.5*(distance_confidence + pred[0][1])
                if distances[min_distance_index] > self.beta:
                    if average_confidence < self.gamma: #pred[0][1] < self.gamma:
                        index = -1
                        confidence = 1.0-np.clip(pred[0][1], 0, 1) ##overwrite confidence
                    self.last_decider = "VeriNet"

                if labels and min_distance_index >= 0:
                    logging.info("descriptor diff: %.2f, same face pred: %.2f, avg conf: %.2f, match: %s",
                                 distances[min_distance_index], pred[0][1], average_confidence,
                                 labels[min_distance_index])  # " time: ", (t2-t1)*1000
                else:
                    logging.info("descriptor diff: %.2f, same face pred: %.2f", distances[min_distance_index],
                                 pred[0][1])  # " time: ", (t2-t1)*1000

            selected_index = index


        else:
            selected_index = -1
            confidence = max(confidence, 1.0-pred[0][1])
            logging.info("Best candidate: " + labels[min_distance_index] + " diff: " + str(distances[min_distance_index]))
        # logging.info("confidence: " + str(confidence))
        # logging.info("Matching of " + str(len(known_face_encodings)) + " took " + str(time.time()-t0) + " seconds")
        return (selected_index, confidence)


    def update_unknown_face_classes(self, face_encoding):
        bestindex = -1
        bestConfidence = 0.0
        classIndex = 0
        for unknown_class in self.unknown_face_classes:
            (index, confidence) = self.recognize_open_set(face_encoding, unknown_class)
            if index >= 0 and confidence > bestConfidence:
                bestIndex = classIndex
                bestConfidence = confidence
            classIndex +=1
        if bestIndex >= 0:
            self.unknown_face_classes[bestIndex].append(face_encoding)
        else:
            self.unknown_face_classes.append([face_encoding])

    def get_frequent_unknown_face_classes(self, min_repetition_count = 10):
        result = []
        for unknown_class in self.unknown_face_classes:
            if len(unknown_class) > min_repetition_count:
                encoding_sum = np.ndarray((128))
                for encoding in unknown_class:
                    encoding_sum += encoding
                encoding_avg = encoding_sum / len(unknown_class)
                result.append(encoding_avg)
                logging.info("New face was " + str(len(unknown_class)) + " times seen")
        logging.info(str(len(result)) + " new faces seen so far")
        return result

    @classmethod
    def get_similar_faces(cls,train_face_dict, selected_face_label, number_of_similar_faces, max_distance = 0.5):
        keys = train_face_dict.keys()
        face_class = train_face_dict[selected_face_label]
        rand_index = random.randint(0, len(face_class)-1)
        selected_face_instance = face_class[rand_index][0]
        i=0
        differences= []
        labels = []
        face_pairs= []
        # print selected_face_label
        if cls.similar_face_dict is None:
            cls.similar_face_dict = {}
        if selected_face_label in cls.similar_face_dict:
            return cls.similar_face_dict[selected_face_label]
        for face_label in train_face_dict:
            if(face_label == selected_face_label):
                continue
            if (face_label, selected_face_label) in cls.face_distance_cash:
                diff_norm = cls.face_distance_cash[(face_label, selected_face_label)]
            else:
                class_face_encodings = train_face_dict[face_label]
                compared_face = class_face_encodings[random.randint(0, len(class_face_encodings)-1)]
                diff = np.asarray(compared_face[0]) - np.asarray(selected_face_instance)
                diff_norm = np.linalg.norm(diff)
                if(diff_norm > max_distance):
                    continue
                    # cls.face_distance_cash[(face_label, selected_face_label)] = diff_norm
            differences.append(diff_norm)
            face_pairs.append((compared_face, face_class[rand_index], 0.0, face_label, selected_face_label))
            labels.append(face_label)

        max_indices = np.array(differences).argsort()[0:min(len(differences),number_of_similar_faces)]
        # print max_indices
        similar_face_pairs = []
        for i in max_indices:
            # print differences[i], labels[i]
            similar_face_pairs.append(face_pairs[i])
        cls.similar_face_dict[selected_face_label] = similar_face_pairs
        return similar_face_pairs

        # selected_labels.append((face1[1], face2[1]))
        # if face1[1] != face2[1]:
        #     same_class = 0.0
        # else:
        #     same_class = 1.0
        #
        # face_pairs.append((face1, face2, same_class))
        # i += 1


    @classmethod
    def get_equal_amount_of_face_pairs(cls, train_face_dict, maxSamples = -1, use_similar_faces_for_different_faces_perc = 1.0):
        face_pairs = []
        keys = train_face_dict.keys()
        while True:
            # face_label = keys[random.randint(0, len(keys))]
            # print face_label
            if maxSamples > 0 and len(face_pairs) > maxSamples:
                break

            key1_index = random.randrange(0, len(keys))
            key1 = keys[key1_index]
            face_class = train_face_dict[key1]
            face1 = face_class[random.randrange(0, len(face_class))]
            face2 = face_class[random.randrange(0, len(face_class))]
            face_pairs.append((face1, face2, 1.0, key1, key1))

            # for face1 in train_face_dict[face_label]:
            #     for face2 in train_face_dict[face_label]:
            #         face_pairs.append((face1, face2, 1.0))
        print "creating rest of face pairs (" + str(len(face_pairs)) + ")"
        same_face_samples = len(face_pairs)
        i = 0
        selected_labels = []
        similar_face_number = max(len(face_pairs)/len(keys), 1)
        print(str(similar_face_number) + " other faces per face")
        t = time.time()
        similar_face_count = 0
        random_face_count = 0
        growth = 1
        while i <= same_face_samples:
            if random.uniform(0.0, 1.0) <= use_similar_faces_for_different_faces_perc:
                key1 = random.randrange(0, len(keys))
                new_faces = cls.get_similar_faces(train_face_dict, keys[key1], similar_face_number)
                face_pairs += new_faces
                similar_face_count += len(new_faces)
                growth = len(new_faces)
            else:
                j = 0
                while j <= growth:
                    key1 = random.randrange(0, len(keys))
                    key2 = random.randrange(0, len(keys))

                    face_class1 = train_face_dict[keys[key1]]
                    face_class2 = train_face_dict[keys[key2]]

                    face1 = face_class1[random.randrange(0, len(face_class1))]
                    face2 = face_class2[random.randrange(0, len(face_class2))]
                    if face1[1] != face2[1]:
                        same_class = 0.0
                    else:
                        continue
                    j += 1
                    random_face_count += 1
                    selected_labels.append((face1[1], face2[1]))
                    face_pairs.append((face1, face2, same_class, key1, key2))
            if time.time() - t > 10:
                print(i, len(cls.similar_face_dict))
                t = time.time()
            i+=growth
        random.shuffle(face_pairs)
        print("similar_face_count: " + str(similar_face_count) + " random_face_count: " + str(random_face_count))

        print selected_labels[:200]
        print "created "+ str(len(face_pairs)) + " labeled face pairs"
        return face_pairs



    @classmethod
    def train_face_distinguisher(cls, face_encodings, labels):

        # first create  pairs of faces and label if they are the same or not
        face_pairs = []
        face_dict = {}
        print "Zipping faces"
        labeled_faces = zip(face_encodings, labels)
        print "creating all face pairs of same class, samples: " + str(len(labels))
        for face in labeled_faces:
            if face_dict.has_key(face[1]):
                face_dict[face[1]].append(face)
            else:
                face_dict[face[1]] = [face]
        print "splitting up " + str(len(face_dict)) + " classes/faces"
        test_face_dict = {}
        train_face_dict = {}
        test_percentage = 0.0
        for face_label in face_dict:
            if random.random() <= test_percentage:
                test_face_dict[face_label] = face_dict[face_label]
            else:
                train_face_dict[face_label] = face_dict[face_label]

        dataDir = ArmarXBuilder.ArmarXBuilder.GetArmarXPackageVariable("VisionX", "DATA_DIR")[0]
        (face_encodings, labels) = pickle.load(open(dataDir + "/VisionX/facerecognition/test_known_encodings.pickle", "r"))
        (face_encodings_unique, labels_unique) = pickle.load(open(dataDir + "/VisionX/facerecognition/test_unknown_encodings.pickle", "r"))
        face_encodings += face_encodings_unique
        labels += labels_unique
        labeled_test_faces = zip(face_encodings, labels)
        for face in labeled_test_faces:
            if test_face_dict.has_key(face[1]):
                test_face_dict[face[1]].append(face)
            else:
                test_face_dict[face[1]] = [face]

        from sklearn.neural_network import MLPRegressor, MLPClassifier
        # clf = svm.SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
        #     decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
        #     max_iter=-1, probability=False, random_state=None, shrinking=True,
        #     tol=0.001, verbose=True)
        layers = (1024,256,128)
        print("layers " + str(layers))
        activations = ["relu"] #, "logistic", "tanh"]
        for activation in activations:
            for i in range(1):


                clf = MLPClassifier(solver='adam', alpha=1e-4, hidden_layer_sizes=layers, activation=activation, random_state=1, verbose=True, max_iter=100, batch_size=40000, early_stopping=True, warm_start=True)

                # clf = MLPRegressor(verbose=True, hidden_layer_sizes=layers, batch_size=20000, activation=activation, early_stopping=True, warm_start=True)
                print "getting face pairs of " + str(len(train_face_dict)) + " classes"
                numberTrainingSamples = 600000
                face_pairs=cls.get_equal_amount_of_face_pairs(train_face_dict, numberTrainingSamples, 1)
                print("first: ",str(face_pairs[0]))
                print("last: ", str(face_pairs[-1]))
                pickle.dump(face_pairs, open("/tmp/face_pairs_" + str(numberTrainingSamples) +"_samples.pickle", "wb"))

                # clf = svm.SVC()
                X_train = []
                y_train = []
                for pair in face_pairs:
                    #input_data = np.concatenate((np.asarray(pair[0][0]), np.asarray(pair[1][0]), np.absolute(np.asarray(pair[0][0]) - np.asarray(pair[1][0]))))
                    input_data = np.absolute(np.asarray(pair[0][0]) - np.asarray(pair[1][0]))
                    X_train.append(input_data)
                    y_train.append(pair[2])
                print "starting learning"
                # X_train, X_test, y_train, y_test = train_test_split (X, Y, test_size=0.00, random_state=42)
                print "training classifier"
                clf.fit(X_train,y_train)
                print "trained classifier"
                X_test = []
                y_test = []
                test_face_pairs=cls.get_equal_amount_of_face_pairs(test_face_dict, 30000, 1)
                for pair in test_face_pairs:
                    #input_data = np.concatenate((np.asarray(pair[0][0]), np.asarray(pair[1][0]), np.absolute(np.asarray(pair[0][0]) - np.asarray(pair[1][0]))))
                    input_data = np.absolute(np.asarray(pair[0][0]) - np.asarray(pair[1][0]))
                    X_test.append(input_data)
                    y_test.append(pair[2])


                i = 0
                correct_classification = 0
                classifications = 0
                same_class_counter = 0
                diff_class_counter = 0
                correct_same_class_pred = 0
                correct_different_class_pred  = 0
                for x in X_test:
                    x = np.asarray(x)
                    Y_pred = clf.predict(x.reshape(1,-1))
                    Y_pred_proba = clf.predict_proba(x.reshape(1, -1))
                    if y_test[i] == 1:
                        same_class_counter +=1
                    else:
                        diff_class_counter +=1
                    diff = float(Y_pred[0])-float(y_test[i])
                    if abs(diff) < 0.5:
                        correct_classification+=1
                        if y_test[i] == 1:
                            correct_same_class_pred += 1
                        else:
                            correct_different_class_pred += 1
                    else:
                        pass
                        # logging.info("Failed for %s<->%s, prediction: %s" %(test_face_pairs[i][3],test_face_pairs[i][4], str(Y_pred_proba)))
                    classifications +=1
                    if i % 1000 == 0:
                        print "accuracy: " + str(100*float(correct_classification)/classifications) + "%"
                        # print str(100*i/len(X_test)) + ": difference: " + str(diff) + " pred: " + str(Y_pred)
                        print test_face_pairs[i][0][1], test_face_pairs[i][1][1], "pred: ", Y_pred, " pred_proba: ", Y_pred_proba
                    i+=1
                accuracy = 100.0 * float(correct_classification) / classifications
                print "final accuracy: " + str(accuracy) + "%"
                clf_path = "/tmp/face-distinguisher-clf-" + activation + "-" + str(layers)+"-"+str("{:.1f}".format(accuracy))+ "perc.pickle"
                print ("Saving to " + clf_path)
                pickle.dump(clf, open(clf_path, "wb"))
                print "same classes accuracy: ", 100.0*correct_same_class_pred/same_class_counter, " different classes: ", 100.0*correct_different_class_pred /diff_class_counter

        return clf


    @classmethod
    def select_biggest_face(cls, face_locations):
        biggest_area = 0
        winner_index = -1
        i = 0
        for face_location in face_locations:
            top, right, bottom, left = face_location
            area = (bottom-right)^2 + (right-left)^2
            if area > biggest_area:
                biggest_area = area
                winner_index = i
            i+=1
        return face_locations[winner_index]

    @classmethod
    def increaseBrightness(cls, image):
        image_copy = np.copy(image)
        print (image_copy.flat().shape())


    def write_face_encodings(self, package, path, write_path, single_dir = False):
        files = self.getFiles(package, path, (".png", ".jpeg", ".gif", ".jpg", ".bmp"))
        print "found " + str(len(files))
        # random.shuffle(files)
        # files = files[0:int(0.5*len(files))]
        known_image_file_name_tuples = self.extractNamePathTuples(files, single_dir=single_dir)
        resultPath = "/tmp/face_recognition_learning_" + os.getenv("USER") + "/"
        croppedFaces_dir = "/tmp/cropped_faces_" + os.getenv("USER") + "/"
        if not os.path.exists(resultPath):
            os.mkdir(resultPath)
        if not os.path.exists(croppedFaces_dir):
            os.mkdir(croppedFaces_dir)
        trained_face_encodings = []
        labels = []
        image_counter = 0

        variations = 0
        print ("Getting encodings for " +  str(len(known_image_file_name_tuples)) + " image files")

        names = set()
        for known_image_tuple in known_image_file_name_tuples:
            name = known_image_tuple[0]
            names.add(name)
        print("Different names: " + str(len(names)))

        import difflib
        import Levenshtein

        for label in names:
            for label2 in names:
                if(label == label2):
                    continue
                ratio = Levenshtein.ratio(label, label2)
                # print(label, label2, str(ratio))
                if ratio > 0.9:
                    # if difflib.SequenceMatcher(None, label, label2).ratio() > 0.9 :
                    print("Names are very similar: " + label + " and " +  label2)


        for known_image_tuple in known_image_file_name_tuples:
            image_counter += 1
            name = known_image_tuple[0]
            path = known_image_tuple[1]

            try:
                for l in range(variations+1):
                    known_imageBGR = cv2.imread(path)
                    # known_imageBGR += 50
                    value = np.uint8(random.randint(0, 100))
                    if l > 0:
                        if random.random() > 0.5:
                            # logging.info("inc by: " + str(value))
                            known_imageBGR = np.where((255 - known_imageBGR) < value, 255, known_imageBGR + value)
                        else:
                            # logging.info("dec by: " + str(value))
                            known_imageBGR = np.where(known_imageBGR < abs(value), 0, known_imageBGR - value)
                    # known_image = face_recognition.load_image_file(path)
                    known_image = cv2.cvtColor(known_imageBGR,cv2.COLOR_BGR2RGB)
                    height, width, channels = known_image.shape
                    if width > 800:
                        resize_factor = 800.0/width
                        known_image = cv2.resize(known_image, (int(width * resize_factor), int(height * resize_factor)))


                    # known_image = cv2.cvtColor(known_image_orig, cv2.COLOR_RGB2HSV)
                    # value = random.randint(-20, 20)
                    # known_image = self.increaseBrightness(known_image_orig)
                    # print "loaded image"
                    face_locations = face_recognition.face_locations(known_image, self.image_upsampling, model=self.detection_model)
                    print("{:.2f}% :I found {} face(s) in this photograph of {} in file {}.".format(float(image_counter*100)/len(known_image_file_name_tuples),len(face_locations), name, path))

                    if len(face_locations) != 1:
                        print "found " + str(len(face_locations)) + " faces in " + path + " - skipping"
                        continue

                    (height, width, bytes_per_pixel) = known_image.shape
                    i = 0
                    for face_location in face_locations:
                        top, right, bottom, left = face_location
                        cv2.rectangle(known_imageBGR, (left,top), (right,bottom), (255,0,0))
                        top = max(0, top - 30)
                        right = min(width, right  +30)
                        bottom = min(height, bottom + 30)
                        left = max(0, left - 30)
                        roi = known_image[top:bottom, left:right]
                        splitted = os.path.splitext(os.path.basename(path))
                        if len(face_locations) > 1:
                            roi_filename = croppedFaces_dir + splitted[0] + "_" + str(i) + "_" + str(l) + splitted[1]
                        else:
                            roi_filename = croppedFaces_dir +  splitted[0] + "_" + str(l) + splitted[1]
                            # logging.info(roi_filename)
                            # cv2.imwrite(roi_filename, cv2.cvtColor(roi,cv2.COLOR_RGB2BGR))
                    # print "Saving to " + resultPath + os.path.basename(path)
                    splitted = os.path.splitext(os.path.basename(path))
                    #cv2.imwrite(resultPath + splitted[0] + "_" + str(l) + splitted[1], cv2.cvtColor(known_image,cv2.COLOR_RGB2BGR))
                    # cv2.imshow(path,known_image)
                    # if cv2.waitKey(1) & 0xFF == ord('q'):
                    #     break
                    if len(face_locations) == 0:
                        print "found " + str(len(face_locations)) + " face_encodings in " + path + " - skipping"
                        continue
                    face_location = self.select_biggest_face(face_locations)
                    for k in range(variations+1):
                        jittered_face_location = list(face_location)
                        if k > 0:
                            for i in range(4):
                                jittered_face_location[i] += random.randint(-5, 5)
                                jittered_face_location[i] = np.clip(jittered_face_location[i], 0, jittered_face_location[i])
                        face_encodings = face_recognition.face_encodings(known_image, [jittered_face_location], 2)

                        trained_face_encodings.append(face_encodings[0])
                        labels.append(name)
            except Exception as e:
                print "Getting encodings of file " + path + " failed - skipping: " + str(e)

        pickle.dump((trained_face_encodings,labels), open( write_path,  "wb" ))
        print("Wrote face " + str(len(trained_face_encodings)) + " encodings to " + write_path)




    def train_classifier(self, face_encodings, labels, clf_store_path = None):
        print "training classifier"
        from sklearn import svm
        from sklearn import preprocessing
        self.trained_face_encodings = face_encodings

        self.le = preprocessing.LabelEncoder()
        self.le.fit(labels)
        print "found {} classes: {}".format(len(self.le.classes_), self.le.classes_)

        self.labels = labels
        self.label_indices = self.le.transform(labels)

        # list(le.inverse_transform([2, 2, 1]))


        # features = zip(feature_order, sorted_scores)
        # self.clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(128, len(self.le.classes_)), random_state=1)

        # self.clf = LogisticRegression()
        # self.clf = GPC()
        self.clf = svm.SVC(probability=True, tol=1e-4)# decision_function_shape='ovr', class_weight='balanced'
        # self.clf = KNeighborsClassifier()
        # self.clf = OneVsRestClassifier(self.clf)
        self.clf.fit(face_encodings, self.label_indices)
        from sklearn.model_selection import cross_val_score
        scores = cross_val_score(self.clf, face_encodings, self.label_indices, cv=5)
        print "training done- scores: ", scores
        if clf_store_path is not None:
            pickle.dump((self.clf, self.le), open(clf_store_path,"wb"))
            print "wrote classifier to ", clf_store_path
            # import sklearn.metrics as metrics

            #
            # import matplotlib.pyplot as plt
            # from mpl_toolkits.mplot3d import Axes3Dindices
            # from matplotlib.ticker import NullFormatter
            #
            # from sklearn import manifold, datasets
            #
            # # Next line to silence pyflakes. This import is needed.
            # Axes3D
            #
            # n_points = len(face_encodings)
            # X, color = datasets.samples_generator.make_s_curve(n_points, random_state=0)
            # n_neighbors = 10
            # n_components = 2
            #
            #
            # fig = plt.figure(figsize=(15, 8))
            # plt.suptitle("Manifold Learning with %i points, %i neighbors"
            #              % (n_points, n_neighbors), fontsize=14)
            #
            # try:
            #     # compatibility matplotlib < 1.0
            #     ax = fig.add_subplot(251, projection='3d')
            #     # ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=color, cmap=plt.cm.Spectral)
            #     ax.view_init(4, -72)
            # except:
            #     ax = fig.add_subplot(251, projection='3d')
            #     plt.scatter(X[:, 0], X[:, 2], c=color, cmap=plt.cm.Spectral)
            #
            # methods = ['standard', 'ltsa', 'hessian', 'modified']
            # labels = ['LLE', 'LTSA', 'Hessian LLE', 'Modified LLE']
            #
            # t0 = time.time()
            # tsne = manifold.TSNE(n_components=n_components, init='pca', random_state=0)
            # Y = tsne.fit_transform(face_encodings, self.label_indices)
            # color = self.label_indices
            # t1 = time.time()
            # print("t-SNE: %.2g sec" % (t1 - t0))
            # ax = fig.add_subplot(2, 5, 10)
            # plt.scatter(Y[:, 0], Y[:, 1], c=color, cmap=plt.cm.Spectral)
            # plt.title("t-SNE (%.2g sec)" % (t1 - t0))
            # ax.xaxis.set_major_formatter(NullFormatter())
            # ax.yaxis.set_major_formatter(NullFormatter())
            # plt.axis('tight')
            #
            # plt.show()

    def train_gender_classifier2(self, encodings, labels, male_names, female_names):
        X = []
        Y = []
        for i in range(len(male_names)):
            male_names[i] = self.extractName(male_names[i])
        for i in range(len(female_names)):
            female_names[i] = self.extractName(female_names[i])

        for (encoding, label) in zip(encodings, labels):
            if label in male_names:
                gender = 0.0
            elif label in female_names:
                gender = 1.0
            else:
                logging.info(label + ": gender could not be determined")
                continue
            X.append(encoding)
            Y.append(gender)
        logging.info(str(len(X)) + " training samples")
        from sklearn.neural_network import MLPRegressor
        # clf = MLPClassifier(solver='adam', alpha=1e-4, hidden_layer_sizes=(256), activation='logistic', random_state=1, verbose=True, max_iter=300)
        clf = MLPRegressor(verbose=True, hidden_layer_sizes=(256, 128, 128), batch_size=3000, early_stopping=True, warm_start=True)
        for i in range(10):
            clf.batch_size = random.randint(300,3000)
            logging.info("Batchsize: " + clf.batch_size)
            clf.fit(X,Y)
        return clf

    def train_gender_classifier(self,male_encodings, female_encodings, male_names, female_names):
        X = []
        Y = []
        for encoding in male_encodings:
            X.append(encoding)
            Y.append(0.0)
        for encoding in female_encodings:
            X.append(encoding)
            Y.append(1.0)

        from sklearn.neural_network import MLPRegressor
        # clf = MLPClassifier(solver='adam', alpha=1e-4, hidden_layer_sizes=(256), activation='logistic', random_state=1, verbose=True, max_iter=300)
        clf = MLPRegressor(verbose=True, hidden_layer_sizes=(256, 128), batch_size=300, early_stopping=True, warm_start=True)
        clf.fit(X,Y)
        return clf

    def visualize(self, image, detections, show_confidence = False):
        face_locations = []

        for (top, right, bottom, left), name, error_value in detections:
            # # Scale back up face locations since the frame we detected in was scaled to 1/4 size
            # top /= self.resize_factor
            # right /= self.resize_factor
            # bottom /= self.resize_factor
            # left /= self.resize_factor
            face_locations.append((top, right, bottom, left))
            top = int(top)
            right = int(right)
            bottom = int(bottom)
            left = int(left)
            # Draw a box around the face
            normalized_confidence = error_value

            color = (np.clip((1.0-normalized_confidence) * 255,0, 255), min((normalized_confidence) * 255,255), 0)

            # normalized_error = (error_value-self.threshold*0.5) /(self.threshold*0.5)
            #
            # color = (0, np.clip((1-normalized_error) * 255,0, 255), min((normalized_error) * 255,255))
            cv2.rectangle(image, (left, top), (right, bottom), color)

            # Draw a label with a name below the face
            cv2.rectangle(image, (left, bottom), (right, bottom + 30 ), color, thickness=-1)
            font = cv2.FONT_HERSHEY_DUPLEX
            label = name
            if(show_confidence):
                label += " - " + "{:.2f}".format(float(error_value))
            cv2.putText(image, label.title(), (left + 6, bottom + 24), font, 0.7, (255, 255, 255), 1)

    @classmethod
    def extractName(cls, string, single_dir = False):
        regexDirName = u"([a-zA-Z_\-]+)/([0-9/]*)([A-Za-z\.0-9_\-]*)\.([a-zA-Z]+)$"
        regexFileName = u"([A-Za-z\.0-9_\-]*)\.([a-zA-Z]+)$"
        if single_dir:
            regex = regexFileName
        else:
            regex = regexDirName
        #
        regex_result = re.search(regex, string)
        if regex_result is None or regex_result.group(1) is None:
            print "not a name in '" + string + "' - skipping"
            return None
        if regex_result.group(1) == "face":
            return None
        name_of_person = regex_result.group(1)

        if name_of_person is not None and not "random" in string:
            name_of_person = name_of_person.replace("_", " ")
            name_of_person = name_of_person.replace("-", " ")
            name_of_person = ''.join([i for i in name_of_person if not i.isdigit()])
            name_of_person = name_of_person.strip()

        if len(name_of_person) <= 1:
            name_of_person = cls.extractName(string, False)

        if name_of_person is not None and len(name_of_person) <= 1:
            return None
        return name_of_person

    @classmethod
    def extractNamePathTuples(cls, filepaths, addFailedParsing = False, single_dir = False):
        result = []
        for file in filepaths:
            filename = os.path.basename(file)
            # print filename
            name_of_person = cls.extractName(file, single_dir)
            if name_of_person is not None:
                # print group
                result.append((name_of_person, file))
            elif addFailedParsing:
                result.append((cls.unknownFaceLabel, file))
        return result

    @classmethod
    def getFiles(cls, package, path, allowedFileTypes = None):
        # builder = ArmarXBuilder.ArmarXBuilder(None,None,None,None)
        # data = builder.getArmarXPackageData(package)
        # datadir = builder.getArmarXPackageDataValue(data, "DATA_DIR")
        datadir = ArmarXBuilder.ArmarXBuilder.GetArmarXPackageVariable(package, "DATA_DIR")
        path = os.path.join(datadir[0],path)
        print path
        files = []
        for root, directories, filenames in os.walk(path, followlinks=True):
            # for directory in directories:
            if len(directories) >0:
                print os.path.join(root, directories[0])
            for filename in filenames:
                # print os.path.join(root,filename)
                if allowedFileTypes is not None and not filename.lower().endswith((allowedFileTypes)):
                    continue

                files.append(os.path.join(root,filename))
        return files

class FaceRecognizerComponent(FaceRecognitionInterface):
    def __init__(self, recognizer, imageProviderName = "WebCamImageProvider", enableAutoImageRetrieval = True, visualizeResultsInOwnWindow = True):
        super(FaceRecognitionInterface, self).__init__()

        self.recognizer = recognizer
        self.autoImageRetrievalEnabled = enableAutoImageRetrieval
        self.visualizeResultsInOwnWindow = visualizeResultsInOwnWindow
        self.provider = None
        self.busy = False
        self.image = None
        self.mutex = Lock()
        self.timestamp = 0
        self.calibration = None
        while True:
            try:
                self.provider = get_proxy(ImageProviderInterfacePrx, imageProviderName)
                break
            except:
                time.sleep(1)
                print "waiting for image provider " + imageProviderName

        try:
            calibProvider = StereoCalibrationProviderInterfacePrx.checkedCast(self.provider)
            self.calibration = calibProvider.getStereoCalibration().left.calibrationLeft
        except:
            pass

        try:
            calibProvider = MonocularCalibrationCapturingProviderInterfacePrx.checkedCast(self.provider)
            self.calibration = calibProvider.getCalibration()
        except:
            pass

        self.format = self.provider.getImageFormat()
        self.numberOfImages = self.provider.getNumberImages()
        self.resultProvider = ResultImageProvider.ResultImageProvider("FaceRecognitionResultProvider", self.format)
        register_object(self.resultProvider, self.resultProvider.name)

        self.proxy = register_object(self, "FaceRecognition")
        self.topic = using_topic(self.proxy, imageProviderName + ".ImageListener")



    def destroy(self):
        self.topic.unsubscribe(self.proxy)

    def retrieveImage(self):
        # self.timestamp = time.time() * 1e6
        (imageBlob, _, self.timestamp, _) = self.provider.getImagesAndInfo()


        self.image = np.fromstring(imageBlob,dtype='uint8')
        self.image = self.image.reshape((self.numberOfImages, self.format.dimension.height,self.format.dimension.width,self.format.bytesPerPixel))
        self.image = self.image[0]
        # self.image = cv2.cvtColor(self.image,cv2.COLOR_RGB2BGR)


    def reportImageAvailable(self, providerName, current = None):
        # print "auto image update: ", self.autoImageRetrievalEnabled
        if self.busy or not self.autoImageRetrievalEnabled:
            return
        self.mutex.acquire()
        self.busy = True
        try:
            self.retrieveImage()
        except Exception as e:
            print("Image retrieval failed: " + str(e))
        self.busy = False
        self.mutex.release()
        #
        #
        #     if cv2.waitKey(1) & 0xFF == ord('q'):
        #         break

    def calculateFaceLocationsForRGBImage(self, image):
        t0 = time.time()
        results = self.recognizer.detect(image)
        t1 = time.time()
        logging.debug("face detection and localization took: " + str((t1-t0)*1000) + " ms")


        # logging.debug(results)
        result_image = image

        # print "releasing mutex"
        logging.debug("visualizing")
        self.recognizer.visualize(result_image, results, True)

        # Display the resulting image
        if (self.visualizeResultsInOwnWindow):
            # print("Visualizing")
            logging.debug("showing window")
            cv2.imshow('Video', cv2.cvtColor(result_image, cv2.COLOR_RGB2BGR))
        # result_image = cv2.cvtColor(result_image,cv2.COLOR_BGR2RGB)
        logging.debug("providing result image")
        self.resultProvider.provideImages(result_image)

        logging.debug("Returning results")
        return [FaceLocation(r[1], Vector2f(r[0][3], r[0][0]), Vector2f(r[0][1], r[0][2]), r[2], self.timestamp) for
                r in results]

    def calculateFaceLocations(self, current = None):
        # print "getting mutex"
        # self.mutex.acquire()
        t0 = time.time()
        self.retrieveImage()
        t1 = time.time()
        # print "image retrieval took: " + str((t1-t0)*1000) + " ms"
        t0 = time.time()

        result = self.calculateFaceLocationsForRGBImage(self.image)

        # self.mutex.release()
        # print "return results: ", len(result)
        return result


    def calculateFaceLocationsForImage(self, imageBlob, imageFormat, numberOfImages, current=None):

        image = np.fromstring(imageBlob, dtype='uint8')
        image = image.reshape((numberOfImages, imageFormat.dimension.height, imageFormat.dimension.width, imageFormat.bytesPerPixel))
        image = image[0]

        return self.calculateFaceLocationsForRGBImage(image)

    def record_images(self, number_of_pictures_to_record, save_path, file_prefix = "face",fps = 3):
        t_start = time.time()
        i = 0
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        while i < number_of_pictures_to_record:
            self.mutex.acquire()
            self.retrieveImage()
            image = self.image
            self.mutex.release()
            face_locations = face_recognition.face_locations(image)
            if len(face_locations) == 1:
                filepath = save_path + "/" + file_prefix + "_" + str(i).zfill(4) + ".jpg"
                print "found 1 face in camera image - writing file to " + filepath
                cv2.imwrite(filepath, cv2.cvtColor(image,cv2.COLOR_BGR2RGB))
                i+=1
            time.sleep(1.0/fps)

def main():
    global parser
    args = parser.parse_args()

    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(format=FORMAT)
    logging.root.setLevel(logging.INFO)

    print "starting"


    f = FaceRecognizer(0.6, 1)
    dataDir = ArmarXBuilder.ArmarXBuilder.GetArmarXPackageVariable("VisionX", "DATA_DIR")[0]
    if args.record is not None:
        comp = FaceRecognizerComponent(f, enableAutoImageRetrieval=not args.start_disabled, visualizeResultsInOwnWindow=True, imageProviderName=args.image_provider)
        comp.record_images(30, "/tmp/recordings", args.record)
        return
    elif args.encodings:

        path = "VisionX/facerecognition/face_dbs"
        if args.input_dir:
            path = args.input_dir
        f.write_face_encodings("VisionX", path, dataDir +"/VisionX/facerecognition/face_encodings.pickle", True)
        return
    elif args.merge_encodings:
        print("merging encoding files: " + str(args.merge_encodings))
        merged_encodings = []
        merged_labels = []
        for encoding_file in args.merge_encodings:
            print ("Loading: " + encoding_file)
            (face_encodings, labels) = pickle.load(
                open(encoding_file, "r"))
            if(len(face_encodings) != len(labels)):
                print("Label and face encoding size does not match - skipping this file! {} vs. {}".format(len(face_encodings), len(labels)))
                continue
            merged_encodings += face_encodings
            merged_labels += labels
        out_path = "/tmp/merged_encodings.pickle"
        print("Writing " + str(len(merged_encodings)) + " encodings+labels to " + out_path)
        pickle.dump((merged_encodings, merged_labels), open(out_path, "wb"))
        return

    elif args.learn:
        (encodings, labels) = pickle.load(open(dataDir +"/VisionX/facerecognition/face_encodings.pickle", "r"))
        f.train_classifier(encodings, labels, "/tmp/classifier.pickle")
        f.clf_2class = pickle.load(open(ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX", "VisionX/facerecognition/face-distinguisher-clf.pickle")))
    elif args.clf_file:
        (clf, le) = pickle.load(open("/tmp/classifier.pickle"))
        f.clf = clf
        f.le = le
    elif args.train_distinguisher:
        face_encodings_path = ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX", "VisionX/facerecognition/train_encodings.pickle")
        (f.trained_face_encodings, f.labels) = pickle.load(open(face_encodings_path, "r"))
        # face_encodings_path = ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX", "VisionX/facerecognition/face_encodings-faces94.pickle")
        # (trained_face_encodings, labels) = pickle.load(open(face_encodings_path, "r"))
        # f.trained_face_encodings += trained_face_encodings
        # f.labels += labels
        f.train_face_distinguisher(f.trained_face_encodings, f.labels)
        return
    elif args.train_gender_clf:
        # male_face_encodings_path = ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX",
        #                                                                            "VisionX/facerecognition/male_encodings.pickle")
        # (male_encodings, _) = pickle.load(open(male_face_encodings_path, "r"))
        # female_face_encodings_path = ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX",
        #                                                                            "VisionX/facerecognition/female_encodings.pickle")
        # (female_encodings, _) = pickle.load(open(female_face_encodings_path, "r"))
        # clf = f.train_gender_classifier(male_encodings, female_encodings)

        male_labels_path = ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX",
                                                                                    "VisionX/facerecognition/LFWgenders/male_names.txt")
        with open(male_labels_path) as file:
            content = file.readlines()
        male_labels = [x.strip() for x in content]
        female_labels_path = ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX",
                                                                                    "VisionX/facerecognition/LFWgenders/female_names.txt")
        with open(female_labels_path) as file:
            content = file.readlines()
        female_labels = [x.strip() for x in content]

        (trained_face_encodings, labels) = pickle.load(
            open(dataDir + "/VisionX/facerecognition/face_encodings-all.pickle", "r"))

        clf = f.train_gender_classifier2(trained_face_encodings, labels, male_labels, female_labels)
        pickle.dump(clf, open("/tmp/gender_clf.pickle", "wb"))
        return
    elif args.train_split_encodings:
        unique_labels_in_test_set_amount = 0.2
        unique_labels_in_test_set = 0
        known_face_amount = 0.1
        (face_encodings, labels) = pickle.load(
            open(dataDir + "/VisionX/facerecognition/face_encodings-3DBs.pickle", "r"))
        train_face_encodings = []
        test_face_encodings = []
        unique_test_encodings = []
        train_labels = []
        test_labels = []
        unique_test_labels = []
        train_dict = {}
        test_dict = {}
        unique_test_dict = {}

        pairs = zip(face_encodings, labels)



        labelset = set(labels)

        face_dict = {}
        for (face_encoding, label) in pairs:
            if label in face_dict:
                face_dict[label].append(face_encoding)
            else:
                face_dict[label] = [face_encoding]

        # move faces with only a few images to the front, so that they are used for testing
        pairs_list = face_dict.items()
        compare = lambda x,y: -1 if len(x[1]) < len(y[1]) else 1
        pairs_list.sort(compare)

        for item in pairs_list:
            label = item[0]
            face_encodings = item[1]
            for face_encoding in face_encodings:
                if (float(unique_labels_in_test_set)/len(labelset) < unique_labels_in_test_set_amount or label in unique_test_dict) and label not in train_dict:
                    if label in unique_test_dict:
                        unique_test_dict[label].append(face_encoding)
                    else:
                        unique_test_dict[label] = [face_encoding]
                    unique_labels_in_test_set +=1
                elif random.uniform(0.0, 1.0) <= 0.8 and label not in unique_test_dict:
                    if label in train_dict:
                        train_dict[label].append(face_encoding)
                    else:
                        train_dict[label] = [face_encoding]
                else:
                    if label in test_dict:
                        test_dict[label].append(face_encoding)
                    else:
                        test_dict[label] = [face_encoding]

        for label in train_dict:
            for encoding in train_dict[label]:
                train_face_encodings.append(encoding)
                train_labels.append(label)
        test_set_only_labels = 0
        for label in test_dict:
            for encoding in test_dict[label]:
                test_face_encodings.append(encoding)
                test_labels.append(label)
            if label not in train_dict:
                test_set_only_labels+=1
        for label in unique_test_dict:
            for encoding in unique_test_dict[label]:
                unique_test_encodings.append(encoding)
                unique_test_labels.append(label)
        logging.info("all samples: %d" %(len(labels)))
        logging.info("labels: train: %d, test: %d, unique test: %d" %(len(train_dict), len(test_dict), len(unique_test_dict)))
        logging.info("samples: train: %d, test: %d, unique test: %d" %(len(train_face_encodings), len(test_face_encodings), len(unique_test_encodings)))
        logging.info("Test label count: %d, test only labels: %d" %(len(test_labels), test_set_only_labels))
        pickle.dump((train_face_encodings, train_labels), open(dataDir + "/VisionX/facerecognition/train_encodings.pickle", "w"))
        pickle.dump((test_face_encodings, test_labels), open(dataDir + "/VisionX/facerecognition/test_known_encodings.pickle", "w"))
        pickle.dump((unique_test_encodings, unique_test_labels), open(dataDir + "/VisionX/facerecognition/test_unknown_encodings.pickle", "w"))
        return
    elif args.benchmark:
        (known_face_encodings, known_labels) = pickle.load(open(dataDir +"/VisionX/facerecognition/train_encodings.pickle", "r"))
        # face_dict = {}
        # for (face_encoding, label) in zip(face_encodings, labels):
        #     if label in face_dict:
        #         face_dict[label].append(face_encoding)
        #     else:
        #         face_dict[label] = [face_encoding]
        # known_face_encodings = []
        # known_labels = []
        # for label in face_dict:
        #     known_face_encodings.apend(face_dict)
        #     known_labels.append(label)
        f.set_faces(known_face_encodings, known_labels)#, limit_number_of_known_faces=596)
        f.clf_2class = pickle.load(open(ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX", "VisionX/facerecognition/face-distinguisher-clf-relu-(1024, 256, 128)-92.8perc.pickle")))
        face_encodings = []
        labels = []
        (face_encodings, labels) = pickle.load(open(dataDir + "/VisionX/facerecognition/test_known_encodings.pickle", "r"))
        (face_encodings_unique, labels_unique) = pickle.load(open(dataDir + "/VisionX/facerecognition/test_unknown_encodings.pickle", "r"))
        face_encodings += face_encodings_unique
        labels += labels_unique

        known_set = set(f.labels)
        intersection = known_set.intersection(set(labels))
        intersection_size = len(intersection)
        logging.info("Intersection size : " + str(intersection_size)+ " unknown faces: " + str(len(set(labels)) - intersection_size) + " classifier labels: " + str(len(known_set)))

        f.alpha = 0.6
        f.beta = 0.43
        fps = []
        tps = []
        accuracies = []
        for i in range(1):
            tn = 0
            tp = 0
            fp = 0
            fn = 0
            fp_acc= 0
            tp_acc= 0
            fn_acc= 0
            tn_acc= 0
            correct_pred_verinet = 0
            correct_pred_descdist = 0
            wrong_pred_verinet = 0
            wrong_pred_descdist = 0
            known_face_count = 0
            unknown_face_count = 0
            predictions = []
            ground_truth_labels = []
            for (face_encoding, label) in zip(face_encodings, labels):
                if label in known_labels and label not in f.labels:
                    # face was used during training, but is not in the set of the classifier used now -> invalid sample to test with
                    continue
                known_face = label in f.labels

                ground_truth_labels.append(label if known_face else f.unknownFaceLabel)
                (face_name, confidence) = f.recognize(face_encoding, use_open_set_recognizer=True)


                predictions.append(face_name)
                if known_face:
                    known_face_count +=1
                    if face_name == label:
                        tp += 1
                        if f.last_decider == "VeriNet":
                            correct_pred_verinet+=1
                        else:
                            correct_pred_descdist +=1

                    else:
                        fn +=1
                        if f.last_decider == "VeriNet":
                            wrong_pred_verinet+=1
                        else:
                            wrong_pred_descdist +=1


                else:

                    unknown_face_count += 1
                    if face_name == f.unknownFaceLabel:
                        tn += 1
                        if f.last_decider == "VeriNet":
                            correct_pred_verinet+=1
                        else:
                            correct_pred_descdist +=1
                    else:
                        fp += 1
                        if f.last_decider == "VeriNet":
                            wrong_pred_verinet+=1
                        else:
                            wrong_pred_descdist +=1
                if known_face_count > 0:
                    tp_acc = (100.*tp/known_face_count)
                if unknown_face_count > 0:
                    tn_acc = (100.*tn/unknown_face_count)
                if unknown_face_count > 0:
                    fp_acc = (100.*fp/unknown_face_count)
                if known_face_count > 0:
                    fn_acc = (100.*fn/known_face_count)

                if not known_face:
                    logging.info(
                        "Known face: %r, label: %s, recognized label: %s, tp: %.2f, tn: %.2f, fn: %.2f, fp: %.2f" % (
                            known_face, label, face_name, tp_acc, tn_acc, fn_acc, fp_acc))

            logging.info("#TestFaces: %d, #known faces in classifier: %d"%(unknown_face_count+known_face_count, len(set(f.labels))))
            import sklearn as sk
            logging.info("known face queries: %d, known face perc: %.2f, unknown face queries: %d" %(known_face_count, 100.*known_face_count/(unknown_face_count+known_face_count), unknown_face_count))
            accuracy = 100.0*(tp+tn)/(known_face_count+unknown_face_count)
            logging.info("My Accuracy: " + str(accuracy))
            logging.info("Accuracy: " + str(sk.metrics.accuracy_score(ground_truth_labels, predictions)))
            logging.info("Precision: " + str(sk.metrics.precision_score(ground_truth_labels, predictions, average='micro')))
            logging.info("My Precision: " + str(float(tp)/(fp+tp)))
            logging.info("Recall: " + str(sk.metrics.recall_score(ground_truth_labels, predictions, average='micro')))
            logging.info("My Recall: " + str(float(tp)/(fn+tp)))

            logging.info("F1: " + str(sk.metrics.f1_score(ground_truth_labels, predictions, average='micro')))

            logging.info("VeriNet correct: {} wrong: {}".format(correct_pred_verinet, wrong_pred_verinet))
            logging.info("DestDesc correct: {} wrong: {}".format(correct_pred_descdist, wrong_pred_descdist))

            fps.append(fp_acc)
            tps.append(tp_acc)
            accuracies.append(accuracy)
            f.alpha += 0.03
            f.beta += 0.02

        print("False Alarm Rate,Correct Classification,Accuracy")
        for (tp, fp, acc) in zip(tps, fps, accuracies):
            print("%.2f,%.2f,%.2f"% (fp, tp, acc))
        # logging.info("Results: tp: %.2f, tn: %.2f, fp: %.2f, fn: %.2f" % (tp_acc, tn_acc, (100.*fp/known_face_count), (100.*fn/unknown_face_count)))
        return
    elif args.benchmark_DIR:
        # (known_face_encodings, known_labels) = pickle.load(open(dataDir +"/VisionX/facerecognition/train_encodings.pickle", "r"))
        (known_face_encodings, known_labels) = pickle.load(open(dataDir +"/VisionX/facerecognition/train_encodings.pickle", "r"))
        # face_dict = {}
        # for (face_encoding, label) in zip(face_encodings, labels):
        #     if label in face_dict:
        #         face_dict[label].append(face_encoding)
        #     else:
        #         face_dict[label] = [face_encoding]
        # known_face_encodings = []
        # known_labels = []
        # for label in face_dict:
        #     known_face_encodings.apend(face_dict)
        #     known_labels.append(label)
        f.set_faces(known_face_encodings, known_labels, limit_number_of_known_faces=596)
        f.clf_2class = pickle.load(open(ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX", "VisionX/facerecognition/face-distinguisher-clf-relu-(1024, 256, 128)-92.8perc.pickle")))
        face_encodings = []
        labels = []
        (face_encodings, labels) = pickle.load(open(dataDir + "/VisionX/facerecognition/test_known_encodings.pickle", "r"))
        (face_encodings_unique, labels_unique) = pickle.load(open(dataDir + "/VisionX/facerecognition/test_unknown_encodings.pickle", "r"))
        face_encodings += face_encodings_unique
        labels += labels_unique

        known_set = set(f.labels)
        intersection = known_set.intersection(set(labels))
        intersection_size = len(intersection)
        logging.info("Intersection size : " + str(intersection_size)+ " unknown faces: " + str(len(set(labels)) - intersection_size) + " classifier labels: " + str(len(known_set)))


        tn = 0
        tp = 0
        fp = 0
        fn = 0
        fp_acc= 0
        tp_acc= 0
        fn_acc= 0
        tn_acc= 0
        known_face_count = 0
        unknown_face_count = 0
        no_face_found_count = 0
        predictions = []
        ground_truth_labels = []

        files = f.getFiles("VisionX", "VisionX/facerecognition/face_dbs")
        print "found " + str(len(files))
        # random.shuffle(files)
        # files = files[0:int(0.5*len(files))]
        known_image_file_name_tuples = f.extractNamePathTuples(files)
        resultPath = "/tmp/face_recognition_learning_" + os.getenv("USER") + "/"
        croppedFaces_dir = "/tmp/cropped_faces_" + os.getenv("USER") + "/"
        if not os.path.exists(resultPath):
            os.mkdir(resultPath)
        if not os.path.exists(croppedFaces_dir):
            os.mkdir(croppedFaces_dir)
        trained_face_encodings = []
        labels = []
        image_counter = 0

        variations = 0
        print ("Getting encodings for " + str(len(known_image_file_name_tuples)) + " image files")
        random.shuffle(known_image_file_name_tuples)
        for known_image_tuple in known_image_file_name_tuples:
            image_counter += 1
            label = known_image_tuple[0]
            path = known_image_tuple[1]
            if label in known_labels and label not in f.labels:
                # face was used during training, but is not in the set of the classifier used now -> invalid sample to test with
                continue

            try:
                for l in range(variations + 1):
                    known_imageBGR = cv2.imread(path)
                    # known_imageBGR += 50
                    value = np.uint8(random.randint(0, 100))
                    if l > 0:
                        if random.random() > 0.5:
                            # logging.info("inc by: " + str(value))
                            known_imageBGR = np.where((255 - known_imageBGR) < value, 255, known_imageBGR + value)
                        else:
                            # logging.info("dec by: " + str(value))
                            known_imageBGR = np.where(known_imageBGR < abs(value), 0, known_imageBGR - value)
                    # known_image = face_recognition.load_image_file(path)
                    known_image = cv2.cvtColor(known_imageBGR, cv2.COLOR_BGR2RGB)
                    height, width, channels = known_image.shape
                    if width > 800:
                        resize_factor = 800.0 / width
                        known_image = cv2.resize(known_image, (int(width * resize_factor), int(height * resize_factor)))

                    # known_image = cv2.cvtColor(known_image_orig, cv2.COLOR_RGB2HSV)
                    # value = random.randint(-20, 20)
                    # known_image = self.increaseBrightness(known_image_orig)
                    # print "loaded image"
                    face_locations = face_recognition.face_locations(known_image, f.image_upsampling,
                                                                     model="hog")
                    if len(face_locations) == 0:
                        face_locations = face_recognition.face_locations(known_image, f.image_upsampling,
                                                                         model="cnn")

                    print("{:.2f}% :I found {} face(s) in this photograph of '{}' in file {}.".format(
                        float(image_counter * 100) / len(known_image_file_name_tuples), len(face_locations), label,
                        path))

                    (height, width, bytes_per_pixel) = known_image.shape
                    i = 0

                    known_face = label in f.labels


                    # if known_face and known_face_count > unknown_face_count:
                    #     continue


                    centerX = width/2
                    centerY = height/2
                    center_face_location = None

                    if len(face_locations) == 0:
                        known_face_count += 1
                        no_face_found_count += 1
                        fn += 1
                        logging.warn("No face found for " + label)
                        head, tail = os.path.split(path)
                        cv2.imwrite("/tmp/failed_images/" + tail, cv2.cvtColor(known_image, cv2.COLOR_RGB2BGR))

                        continue
                    elif len(face_locations) == 1:
                        center_face_location = face_locations[0]
                    else:
                        minDistance = 1000000
                        bestindex = -1
                        i = 0
                        for face_location in face_locations:
                            top, right, bottom, left = face_location
                            if top < centerY and right > centerX and bottom > centerY and left < centerX:
                                center_face_location = face_location
                                break
                            distanceX = min(abs(left-centerX), abs(right-centerX))
                            distanceY = min(abs(top - centerY), abs(bottom - centerY))
                            distance = min(distanceX, distanceY)
                            if distance < minDistance:
                                minDistance = distance
                                bestIndex = i
                            i+=1
                        if not center_face_location:
                            center_face_location = face_locations[bestIndex]

                    if center_face_location is None:
                        logging.warn("No center face")
                        # count as false negative
                        known_face_count += 1
                        no_face_found_count += 1
                        fn += 1
                        head, tail = os.path.split(path)
                        cv2.imwrite("/tmp/failed_images/" + tail, cv2.cvtColor(known_image, cv2.COLOR_RGB2BGR))

                        continue
                    face_encoding = face_recognition.face_encodings(known_image, [center_face_location])[0]




                    ground_truth_labels.append(label if known_face else f.unknownFaceLabel)
                    (face_name, confidence) = f.recognize(face_encoding, False)

                    predictions.append(face_name)
                    if known_face:
                        known_face_count += 1
                        if face_name == label:
                            tp += 1
                        else:
                            fn += 1

                    else:

                        unknown_face_count += 1
                        if face_name == f.unknownFaceLabel:
                            tn += 1
                        else:
                            fp += 1
                    if known_face_count > 0:
                        tp_acc = (100. * tp / known_face_count)
                    if unknown_face_count > 0:
                        tn_acc = (100. * tn / unknown_face_count)
                    if unknown_face_count > 0:
                        fp_acc = (100. * fp / unknown_face_count)
                    if known_face_count > 0:
                        fn_acc = (100. * fn / known_face_count)

                    # if not known_face:
                    logging.info(
                            "Known face: %r, label: %s, recognized label: %s, tp: %.2f, tn: %.2f, fn: %.2f, fp: %.2f no face found: %2.5f " % (
                                known_face, label, face_name, tp_acc, tn_acc, fn_acc, fp_acc, no_face_found_count))

            except Exception as e:
                print "Getting encodings of file " + path + " failed - skipping: " + str(e)
                # head, tail = os.path.split(path)
                # cv2.imwrite("/tmp/failed_images/" + tail, cv2.cvtColor(known_image, cv2.COLOR_RGB2BGR))



        logging.info("#TestFaces: %d, #known faces in classifier: %d"%(unknown_face_count+known_face_count, len(set(f.labels))))
        import sklearn as sk
        logging.info("known face queries: %d, known face perc: %.2f, unknown face queries: %d" %(known_face_count, 100.*known_face_count/(unknown_face_count+known_face_count), unknown_face_count))
        logging.info("My Accuracy: " + str(100.0*(tp+tn)/(known_face_count+unknown_face_count)))
        logging.info("Accuracy: " + str(sk.metrics.accuracy_score(ground_truth_labels, predictions)))
        logging.info("Precision: " + str(sk.metrics.precision_score(ground_truth_labels, predictions)))
        logging.info("Recall: " + str(sk.metrics.recall_score(ground_truth_labels, predictions)))

        logging.info("F1: " + str(sk.metrics.f1_score(ground_truth_labels, predictions, average='micro')))
        # logging.info("Results: tp: %.2f, tn: %.2f, fp: %.2f, fn: %.2f" % (tp_acc, tn_acc, (100.*fp/known_face_count), (100.*fn/unknown_face_count)))
        return
    else:
        # f.train_from_pictures("VisionX", "VisionX/facerecognition/faces")
        #(face_encodings, labels) = pickle.load(open(dataDir +"/VisionX/facerecognition/face_encodings-facescrub.pickle", "r"))
        (face_encodings, labels) = pickle.load(open(dataDir +"/VisionX/facerecognition/face_encodings.pickle", "r"))
        # print("Known faces: " + str(zip(face_encodings, labels)))
        f.set_faces(face_encodings, labels)
        # f.train()
        # f.clf_2class = pickle.load(open(ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX", "VisionX/facerecognition/face-distinguisher-clf-relu-(1024, 256, 128)-92.8perc.pickle")))
        f.clf_2class = pickle.load(open(ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX", "VisionX/facerecognition/face-distinguisher-clf-relu-128-91.8perc.pickle")))
        f.gender_clf = pickle.load(open(ArmarXBuilder.ArmarXBuilder.GetArmarXPackageDataFile("VisionX", "VisionX/facerecognition/gender_clf.pickle")))



    comp = None
    files = []
    if args.input_file:
        try:
            vidFile = cv2.VideoCapture(args.input_file)
        except:
            print "problem opening input stream"
            sys.exit(1)
        if not vidFile.isOpened():
            print "capture stream not open"
            sys.exit(1)
        nFrames = int(vidFile.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)) # one good way of namespacing legacy openCV: cv2.cv.*
        print "frame number: %s" %nFrames
        fps = vidFile.get(cv2.cv.CV_CAP_PROP_FPS)
        print "FPS value: %s" %fps
    elif args.input_dir:
        print "Searching for files in " + os.path.abspath(args.input_dir)
        for root, directories, filenames in os.walk(args.input_dir):
            for filename in filenames:
            # print os.path.join(root,filename)
                files.append(os.path.join(root,filename))
        random.shuffle(files)
        files = files[int(0.5*len(files)):]
    else:
        comp = FaceRecognizerComponent(f, enableAutoImageRetrieval=not args.start_disabled, visualizeResultsInOwnWindow=args.show_visu, imageProviderName=args.image_provider)
    frame_skip = 10
    file_counter = 0
    # format = provider.getImageFormat()
    current_correct_label = None
    correct_label_counter = 0
    classification_counter = 0
    unknown_persons_queries = 0
    correct_unknown_counter = 0
    file_wrong_classifications = None
    if not args.start_disabled:
        while True:
            t_total = time.time()
            if comp and comp.image is None:
                time.sleep(1)
                continue
            if args.input_file or args.input_dir or comp.autoImageRetrievalEnabled:
                image = None
                if args.input_file:
                    for i in range(0, frame_skip):
                        ret, image = vidFile.read()
                    if not ret:
                        break

                    image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
                    width = int(vidFile.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
                    height= int(vidFile.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
                    if width > 800:
                        resize_factor = 800.0/width
                        image = cv2.resize(image, (int(width * resize_factor), int(height * resize_factor)))
                    # # cv2.imshow("frameWindow", frame)
                    # cv2.waitKey(int(1/fps*1000)) # time to wait between frames, in mSec
                elif args.input_dir:
                    if file_counter == len(files):
                        break

                    file_wrong_classifications = open("/tmp/wrong_classifications.log", "w")
                    file = files[file_counter]
                    logging.debug("reading file " + file + " [" + str(float(file_counter*100)/len(files)) +"%]")
                    #time.sleep(0.3)
                    image = cv2.imread(file)
                    file_counter += 1
                    if image is not None and image.shape[2] == 3:

                        shape = image.shape
                        (height, width, bytes_per_pixel) = shape
                        # if width < height:
                        #
                        #     image = np.transpose(image, [0,1])
                        if width > 1300:
                            resize_factor = 1300.0/width
                            image = cv2.resize(image, (int(width * resize_factor), int(height * resize_factor)))
                        name_tuples = FaceRecognizer.extractNamePathTuples([file])
                        if len(name_tuples) == 0:
                            continue
                        (current_correct_label, path) = name_tuples[0]
                        image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
                    else:
                        continue
                else:
                    t1 = time.time()
                    comp.mutex.acquire()
                    image = comp.image
                    comp.mutex.release()
                    logging.debug( "Getting image took " + str((time.time() - t1) * 1000) + " ms")
                t1 = time.time()
                # logging.info("\n\n")
                # if current_correct_label:
                #     logging.info("{:.3f}%".format(100.*float(file_counter)/len(files)) + "%: current_correct_label: " + current_correct_label + " Person known: " + str(current_correct_label in f.labels) +"\npath: " + path)
                results = f.detect(image, auto_resize=True)
                logging.debug("Detecting took " + str((time.time() - t1) * 1000) + " ms")
                if current_correct_label:
                    if len(results) > 0:
                        if len(results) > 0 and results[0][1] == current_correct_label:
                            correct_label_counter+=1
                            # logging.info ("Correct classification")
                        elif current_correct_label not in f.labels:
                            classification_counter +=-1
                            unknown_persons_queries+=1
                            if len(results) > 0:
                                if results[0][1] == FaceRecognizer.unknownFaceLabel:
                                    # logging.info ("Correct unknown label")
                                    correct_unknown_counter+= 1
                                else:
                                    file_wrong_classifications.write("fp," + current_correct_label + "," + results[0][1])
                                    logging.info("{:.3f}%".format(100. * float(file_counter) / len(
                                        files)) + "%: current_correct_label: " + current_correct_label + " Person known: " + str(
                                        current_correct_label in f.labels) + "\npath: " + path)
                                    logging.warn("Wrong unknown classification: " + results[0][1])

                            else:
                                raise "Should not get here!"
                        else:
                            file_wrong_classifications.write("fn," + current_correct_label + "," + results[0][1])
                            logging.info("{:.3f}%".format(100. * float(file_counter) / len(
                                files)) + "%: current_correct_label: " + current_correct_label + " Person known: " + str(
                                current_correct_label in f.labels) + "\npath: " + path)
                            logging.info("Wrong classification:" + current_correct_label + " <->" + results[0][1])


                        classification_counter +=1
                    else:
                        logging.info("did not find any faces")
                else:
                    logging.info("No ground truth label!?")
                logging.debug(results)
                f.visualize(image, results, args.show_confidence)

                if unknown_persons_queries > 0:
                    logging.info("correct classifications of unknown persons: " + "{:.3f}%".format(
                        float(correct_unknown_counter) / unknown_persons_queries * 100))
                if classification_counter > 0:
                    logging.info("correct classifications of known persons: " + "{:.3f}%".format(
                    float(correct_label_counter) / classification_counter * 100))
                if classification_counter > 0 or unknown_persons_queries > 0:
                    logging.info("correct classifications: " + "{:.3f}%".format(
                        float(correct_label_counter+correct_unknown_counter) / (classification_counter+unknown_persons_queries) * 100))

                #
                # # Display the resulting image
                if args.show_visu:
                    bgr_image = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
                    cv2.imwrite("/tmp/video.png", bgr_image)
                    cv2.imshow('Video', bgr_image)

                # comp.image = cv2.cvtColor(comp.image,cv2.COLOR_BGR2RGB)
                if comp:
                    t1 = time.time()
                    comp.resultProvider.provideImages(image)
                    logging.debug("Result providing took " + str((time.time() - t1) * 1000) + " ms")

        # cv2.waitKey(0)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            # time.sleep(0.1)
            logging.debug("Total loop time took " + str((time.time() - t_total) * 1000) + " ms")
    else:
        while True:
            if cv2.waitKey(100) & 0xFF == ord('q'):
                break
    if classification_counter > 0:
        print "correct classifications: " + ("{:.3f}%".format(float(correct_label_counter)/classification_counter*100))
    if unknown_persons_queries > 0:
        print "correct classifications of unknown persons: " + "{:.3f}%".format(float(correct_unknown_counter)/unknown_persons_queries*100)
    if file_wrong_classifications is not None:
        file_wrong_classifications.close()

    if comp:
        comp.destroy()
    time.sleep(0.5)
    # try:
    #     while not ice_helper.iceCommunicator.isShutdown():
    #
    #         time.sleep(1)
    # except KeyboardInterrupt:
    #     print('shutting down')
    #     ice_helper.iceCommunicator.shutdown()

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
    ice_helper.iceCommunicator.destroy()
