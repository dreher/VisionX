from armarx.interface_helper import load_armarx_slice
import SharedMemoryProvider
import time
load_armarx_slice('VisionX', 'core/DataTypes.ice')
load_armarx_slice('VisionX', 'core/ImageProviderInterface.ice')
load_armarx_slice('VisionX', 'core/ImageProcessorInterface.ice')

from visionx import ImageProviderInterfacePrx, ImageProviderInterface, ImageFormatInfo, ImageDimension, ImageType, BayerPatternType

from armarx.interface_helper import get_proxy, register_object, using_topic, get_topic
from visionx import ImageProcessorInterface, ImageProcessorInterfacePrx


class ResultImageProvider(ImageProviderInterface):
    def __init__(self, name, format):
        super(ImageProviderInterface, self).__init__()
        self.name = name
        self.resultImage = []
        # self.format = ImageFormatInfo()
        # self.format.bytesPerPixel = 3
        # self.format.dimension.height = 480
        # self.format.dimension.width = 640
        # self.format.type = ImageType.eRgb
        self.format = format
        self.numberOfImages = 1
        self.topicName = name + ".ImageListener"
        self.topic = get_topic(ImageProcessorInterfacePrx, self.topicName)

        self.sharedMemoryProvider = SharedMemoryProvider.SharedMemoryProvider(self.calcSize())

        register_object(self.sharedMemoryProvider, name + "MemoryImageProvider")

    def calcSize(self):
        return self.format.dimension.height * self.format.dimension.width * self.format.bytesPerPixel

    def provideImages(self, image):
        size = self.calcSize()
        self.resultImage = image.reshape((size)).tolist()
        # print "providing result image"
        self.sharedMemoryProvider.data = self.resultImage
        timestamp = time.time()
        self.sharedMemoryProvider.timestamp = int(round(time.time() * 1e6))
        self.topic.reportImageAvailable(self.name)

    def getImages(self, current = None):
        # print "Returning image"

        return self.resultImage

    def getImageFormat(self, current = None):
        # print ("Returning format: " + str(self.format.dimension.width) + "x"+ str(self.format.dimension.height) + "x" + str(self.format.bytesPerPixel))

        return self.format

    def getNumberImages(self, current = None):
        return self.numberOfImages

    def hasSharedMemorySupport(self, current = None):
        return False