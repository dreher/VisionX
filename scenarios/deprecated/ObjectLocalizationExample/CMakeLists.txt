# Add your components below as shown in the following example:
#
set(SCENARIO_COMPONENTS
    CommonStorage
    PriorKnowledge
    ImageSequenceProvider
#    TexturedObjectRecognition
    SegmentableObjectRecognition
    WorkingMemory
    ObjectMemoryObserver
    ConditionHandler
    ObjectLocalizationExampleApp)


# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario("WhatCanYouSeeNow" "${SCENARIO_COMPONENTS}" "config/global.cfg")

#set(SCENARIO_CONFIGS
#    config/ComponentName.optionalString.cfg
#    )
# optional 3rd parameter: "path/to/global/config.cfg"
#armarx_scenario_from_configs("WhatCanYouSeeNow" "${SCENARIO_CONFIGS}")
