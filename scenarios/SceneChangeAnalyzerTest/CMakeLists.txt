# Add your components below as shown in the following example:
#
set(SCENARIO_COMPONENTS
    SceneChangeAnalyzer
)


# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario("SceneChangeAnalyzerScenario" "${SCENARIO_COMPONENTS}" "config/global.cfg")


