/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ImageProvider.h"

#include <string>

using namespace armarx;
using namespace visionx;

// ================================================================== //
// == ImageProvider ice interface =================================== //
// ================================================================== //
armarx::Blob ImageProvider::getImages(const Ice::Current& c)
{
    if (numberImages == 0)
    {
        return armarx::Blob();
    }

    return sharedMemoryProvider->getData();
}


ImageFormatInfo ImageProvider::getImageFormat(const Ice::Current& c)
{
    return imageFormat;
}

int ImageProvider::getNumberImages(const Ice::Current& c)
{
    return numberImages;
}

// ================================================================== //
// == Component implementation =============================== //
// ================================================================== //
void ImageProvider::onInitComponent()
{
    // init members
    exiting = false;

    // default image format (640x480, bayerpattern).
    // call from within init to change!
    setImageFormat(ImageDimension(640, 480), eBayerPattern, eBayerPatternRg);
    setNumberImages(0);

    // call setup of image provider implementation to setup image size
    onInitImageProvider();
}


void ImageProvider::onConnectComponent()
{
    // init shared memory
    int imageSize = getImageFormat().dimension.width *  getImageFormat().dimension.height * getImageFormat().bytesPerPixel;

    if (numberImages != 0)
    {
        MetaInfoSizeBasePtr info(new MetaInfoSizeBase(getNumberImages() * imageSize, getNumberImages() * imageSize, 0));
        if (sharedMemoryProvider)
        {
            sharedMemoryProvider->stop();
        }
        sharedMemoryProvider = new IceSharedMemoryProvider<unsigned char>(this, info, "ImageProvider");

        // reference to shared memory
        imageBuffers = (void**) new unsigned char* [getNumberImages()];

        for (int i = 0 ; i < getNumberImages() ; i++)
        {
            imageBuffers[i] = sharedMemoryProvider->getBuffer() + i * imageSize;
        }

        // offer topic for image events
        offeringTopic(getName() + ".ImageListener");

        // retrieve storm topic proxy
        imageProcessorProxy = getTopic<ImageProcessorInterfacePrx>(getName() + ".ImageListener");

        // start icesharedmemory provider
        sharedMemoryProvider->start();
    }

    onConnectImageProvider();
}

void ImageProvider::onDisconnectComponent()
{
    onDisconnectImageProvider();
    if (sharedMemoryProvider)
    {
        sharedMemoryProvider->stop();
        sharedMemoryProvider = nullptr;
    }
}

void ImageProvider::onExitComponent()
{
    exiting = true;

    onExitImageProvider();

    if (numberImages != 0)
    {
        delete [] imageBuffers;
    }
}

void ImageProvider::updateTimestamp(Ice::Long timestamp, bool threadSafe)
{
    auto sharedMemoryProvider = this->sharedMemoryProvider; // preserve from deleting
    if (!sharedMemoryProvider)
    {
        ARMARX_INFO << "Shared memory provider is null!";
        return;
    }
    MetaInfoSizeBasePtr info = this->sharedMemoryProvider->getMetaInfo(threadSafe);
    if (info)
    {
        info->timeProvided = timestamp;
    }
    else
    {
        info = new MetaInfoSizeBase(imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel,
                                    imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel,
                                    timestamp);
    }

    this->sharedMemoryProvider->setMetaInfo(info, threadSafe);
}


// ================================================================== //
// == Utility methods for ImageProviders ============================ //
// ================================================================== //
void ImageProvider::setImageFormat(ImageDimension imageDimension,
                                   ImageType imageType,
                                   BayerPatternType bayerPatternType)
{
    imageFormat.dimension  = imageDimension;
    imageFormat.type = imageType;
    imageFormat.bpType = bayerPatternType;

    switch (imageType)
    {
        case eGrayScale:
        case eBayerPattern:
            imageFormat.bytesPerPixel = 1;
            break;

        case eRgb:
            imageFormat.bytesPerPixel = 3;
            break;

        case eFloat1Channel:
            imageFormat.bytesPerPixel = 4;
            break;

        case eFloat3Channels:
            imageFormat.bytesPerPixel = 12;
            break;

        case ePointsScan:
            imageFormat.bytesPerPixel = 12;
            break;

        case eColoredPointsScan:
            imageFormat.bytesPerPixel = 16;
            break;
    }
}

void ImageProvider::setNumberImages(int numberImages)
{
    this->numberImages = numberImages;
}

void ImageProvider::provideImages(void** inputBuffers)
{
    if (numberImages == 0)
    {
        ARMARX_INFO << "Number of images is 0 - thus none can be provided";
        return;
    }

    int imageSize = imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel;

    // copy
    {
        // lock memory access
        armarx::SharedMemoryScopedWriteLockPtr lock = getScopedWriteLock();
        if (!lock) // already shutdown
        {
            return;
        }
        for (int i = 0 ; i < numberImages ; i++)
        {
            memcpy(imageBuffers[i], inputBuffers[i], imageSize);
        }
    }

    // notify processors
    if (imageProcessorProxy)
    {
        ARMARX_DEBUG << "Notifying ImageProcessorProxy";
        imageProcessorProxy->reportImageAvailable(getName());
    }
    else
    {
        ARMARX_ERROR << deactivateSpam(4) << "imageProcessorProxy is NULL - could not report Image available";
    }
}

void ImageProvider::provideImages(CByteImage** images)
{
    if (numberImages == 0)
    {
        ARMARX_INFO << "Number of images is 0 - thus none can be provided";
        return;
    }

    //ISO C++ forbids variable length array [-Werror=vla] => use a vector (the array will be on the heap anyways)
    std::vector<void*> imageBuffers(numberImages);

    for (int i = 0 ; i < numberImages ; i++)
    {
        //ARMARX_VERBOSE << i;
        imageBuffers[i] = images[i]->pixels;
    }

    provideImages(imageBuffers.data());
}

void ImageProvider::provideImages(CFloatImage** images)
{
    if (numberImages == 0)
    {
        ARMARX_INFO << "Number of images is 0 - thus none can be provided";
        return;
    }

    //ISO C++ forbids variable length array [-Werror=vla] => use a vector (the array will be on the heap anyways)
    std::vector<void*> imageBuffers(numberImages);

    for (int i = 0 ; i < numberImages ; i++)
    {
        imageBuffers[i] = images[i]->pixels;
    }

    provideImages(imageBuffers.data());
}



