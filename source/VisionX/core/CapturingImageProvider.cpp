/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CapturingImageProvider.h"

#include <string>

#include <ArmarXCore/core/time/TimeUtil.h>

using namespace armarx;
using namespace visionx;

// ================================================================== //
// == ImageProvider ice interface =================================== //
// ================================================================== //
void CapturingImageProvider::startCapture(float framesPerSecond,
        const Ice::Current& ctx)
{
    ScopedLock lock(captureMutex);
    onStartCapture(framesPerSecond);

    captureEnabled = true;
    frameRate = framesPerSecond;

    ARMARX_INFO << "Starting image capture with " << frameRate << "fps";
}


void CapturingImageProvider::stopCapture(const Ice::Current& ctx)
{
    ScopedLock lock(captureMutex);
    captureEnabled = false;
    onStopCapture();
}

// ================================================================== //
// == Component implementation =============================== //
// ================================================================== //
void CapturingImageProvider::onInitImageProvider()
{
    // init members
    frameRate = hasProperty("FPS") ? getProperty<float>("FPS").getValue() : 30;
    captureEnabled = false;

    // default sync mode
    setImageSyncMode(eFpsSynchronization);

    // call setup of image provider implementation to setup image size
    onInitCapturingImageProvider();

    // capature task
    captureTask = new RunningTask<CapturingImageProvider>(this, &CapturingImageProvider::capture);
}


void CapturingImageProvider::onConnectImageProvider()
{
    onStartCapturingImageProvider();

    captureTask->start();

    // TODO: hack start capturing
    startCapture(frameRate);
}


void CapturingImageProvider::onExitImageProvider()
{
    // TODO: hack stop capturing
    stopCapture();

    captureTask->stop();

    onExitCapturingImageProvider();
}


void CapturingImageProvider::capture()
{
    ARMARX_INFO << "Starting Image Provider: " << getName();

    // main loop of component
    boost::posix_time::milliseconds td(1);

    while (!captureTask->isStopped() && !isExiting() && sharedMemoryProvider)
    {
        if (captureEnabled)
        {

            MetaInfoSizeBasePtr info = this->sharedMemoryProvider->getMetaInfo();
            long oldTimestamp = info ? info->timeProvided : 0;
            bool succeeded = capture(imageBuffers);

            if (succeeded)
            {
                MetaInfoSizeBasePtr info = this->sharedMemoryProvider->getMetaInfo();
                if (!info || info->timeProvided == oldTimestamp)
                {
                    ARMARX_WARNING << deactivateSpam(10000000) << "The image provider did not set a timestamp - measuring the timestamp now. The ImageProvider implementation should call updateTimestamp()!";
                    updateTimestamp(armarx::TimeUtil::GetTime().toMicroSeconds());
                }

                imageProcessorProxy->reportImageAvailable(getName());
            }

            if (imageSyncMode == eFpsSynchronization)
            {
                fpsCounter.assureFPS(frameRate);
            }
        }
        else
        {
            boost::this_thread::sleep(td);
        }
    }

    ARMARX_INFO << "Stopping ImageProvider";
}

// ================================================================== //
// == Utility methods for CapturingImageProviders =================== //
// ================================================================== //
void CapturingImageProvider::setImageSyncMode(ImageSyncMode imageSyncMode)
{
    this->imageSyncMode = imageSyncMode;
}

ImageSyncMode CapturingImageProvider::getImageSyncMode()
{
    return imageSyncMode;
}
