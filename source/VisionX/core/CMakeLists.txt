armarx_set_target("Vision Core Library: VisionXCore")

find_package(IVT QUIET)
find_package(Eigen3 QUIET)

armarx_build_if(IVT_FOUND "IVT library not found")
armarx_build_if(Eigen3_FOUND "Eigen3 not available")

if (IVT_FOUND AND Eigen3_FOUND)
    include_directories(${IVT_INCLUDE_DIRS})
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

# Dependencies
set(LIBS VisionXInterfaces VisionXTools ArmarXCore RobotAPIInterfaces)

# Source files
set(LIB_FILES
    ImageProvider.cpp
    CapturingImageProvider.cpp
    ImageProcessor.cpp
)

# Header files
set(LIB_HEADERS
    VisionXApplication.h
    ImageProvider.h
    CapturingImageProvider.h
    ImageProcessor.h

    exceptions/user/FrameRateNotSupportedException.h
    exceptions/user/StartingCaptureFailedException.h
)

# Define target
armarx_add_library(VisionXCore "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")
