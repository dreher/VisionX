/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::playback
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/playback/public_api.h>


// Boost
#include <boost/algorithm/string.hpp>
#include <boost/filesystem/operations.hpp>  // TODO: As of C++17 (or C++14) use std::filesystem (or std::experimental::filesystem) instead

// VisionX
#include <VisionX/libraries/playback/strats/ChunkedImageSequencePlaybackStrategy.h>
#include <VisionX/libraries/playback/strats/ImageSequencePlaybackStrategy.h>
#include <VisionX/libraries/playback/strats/VideoPlaybackStrategy.h>


visionx::playback::Playback
visionx::playback::newPlayback(const boost::filesystem::path& path)
{
    // String representation just for string operations, so to lower case
    const std::string pathstr = boost::algorithm::to_lower_copy(path.string());

    // Checks against metadata.csv to identify a chunked image sequence as used by the ImageMonitor (individual image recordings)
    if (boost::algorithm::ends_with(pathstr, "metadata.csv") or (boost::filesystem::is_directory(path) and boost::filesystem::exists(path / "metadata.csv")))
    {
        return std::make_shared<visionx::playback::strats::ChunkedImageSequencePlaybackStrategy>(path);
    }

    // If the path is an image file, contains a wilcard, or is a simple directory, try to make sense of the input when interpreted as image sequence
    const bool isImage = boost::algorithm::ends_with(pathstr, ".png") or boost::algorithm::ends_with(pathstr, ".jpg")
                         or boost::algorithm::ends_with(pathstr, ".jpeg") or boost::algorithm::ends_with(pathstr, ".bmp");
    if (isImage or boost::algorithm::contains(pathstr, "*") or boost::filesystem::is_directory(path))
    {
        return std::make_shared<visionx::playback::strats::ImageSequencePlaybackStrategy>(path);
    }

    // Common OpenCV-supported video file extensions
    if (boost::algorithm::ends_with(pathstr, ".avi") or boost::algorithm::ends_with(pathstr, ".mp4"))
    {
        return std::make_shared<visionx::playback::strats::VideoPlaybackStrategy>(path);
    }

    return nullptr;
}
