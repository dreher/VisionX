/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// OpenCV 2
#include <opencv2/core/core.hpp>

// IVT
#include <Image/ByteImage.h>


namespace visionx
{
    namespace record
    {

        /**
         * @brief Converts an IVT CByteImage to OpenCV's Mat
         * @param in CByteImage
         * @param out Output parameter
         */
        void convert(const CByteImage& in, cv::Mat& out);

        /**
         * @brief Converts an OpenCV Mat to IVT's CByteImage
         * @param in cv::Mat
         * @param out Output parameter
         */
        void convert(const cv::Mat& in, CByteImage& out);

    }
}
