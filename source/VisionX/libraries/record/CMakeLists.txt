armarx_set_target("Vision Core Library: record")

find_package(IVT QUIET)
find_package(OpenCV 2 QUIET)

# Only required for legacy X264 support for Ubuntu 14.04 trusty, since ffmpeg is not pacakged
# with it and OpenCV depends on it for X264 support. Ubuntu 18.04 bionic should work though.
if(NOT VISIONX_NO_LEGACY_X264)
    find_package(x264 QUIET)
    find_package(swscale QUIET)
endif()

armarx_build_if(IVT_FOUND "IVT library not found")
armarx_build_if(OpenCV_FOUND "OpenCV 2 not available")

if (IVT_FOUND AND OpenCV_FOUND)
    include_directories(${IVT_INCLUDE_DIRS})
    include_directories(SYSTEM ${OpenCV_INCLUDE_DIR})
endif()

# Set defaults for X264 handling and override them if legacy mode is enabled
set(LEGACY_X264_LIBS "")
set(X264_RECORDING_STRATEGY "H264RecordingStrategy")
if(NOT VISIONX_NO_LEGACY_X264 AND x264_FOUND AND swscale_FOUND)
    include_directories(SYSTEM ${swscale_INCLUDE_DIRS})
    include_directories(SYSTEM ${x264_INCLUDE_DIRS})
    message(WARNING "Using legacy X264 video recording support for use on Ubuntu 14.04 trusty. To use OpenCV (ffmpeg) for Ubuntu 18.04 bionic re-run CMake with -DVISIONX_NO_LEGACY_X264")
    set(LEGACY_X264_LIBS ${x264_LIBRARIES} ${swscale_LIBRARIES})
    set(X264_RECORDING_STRATEGY "H264LegacyRecordingStrategy")
    add_definitions("-DLEGACY_X264_SUPPORT")
endif()

# Dependencies
set(LIBS
    ArmarXCore
    ${IVT_LIBRARIES}
    ${OpenCV_LIBS}
    ${LEGACY_X264_LIBS}
)

# Source files
set(LIB_FILES
    ./AbstractRecordingStrategy.cpp
    ./AbstractSequencedRecordingStrategy.cpp
    ./RecordingMethod.cpp
    ./RecordingMethodRegistry.cpp
    ./public_api.cpp
    ./helper.cpp
    ./strats/AVIRecordingStrategy.cpp
    ./strats/BMPRecordingStrategy.cpp
    ./strats/${X264_RECORDING_STRATEGY}.cpp
    ./strats/JPGRecordingStrategy.cpp
    ./strats/PNGRecordingStrategy.cpp
)

# Header files
set(LIB_HEADERS
    ../record.h
    ./AbstractRecordingStrategy.h
    ./AbstractSequencedRecordingStrategy.h
    ./RecordingMethod.h
    ./RecordingMethodRegistry.h
    ./public_api.h
    ./helper.h
    ./strats/AVIRecordingStrategy.h
    ./strats/BMPRecordingStrategy.h
    ./strats/${X264_RECORDING_STRATEGY}.h
    ./strats/JPGRecordingStrategy.h
    ./strats/PNGRecordingStrategy.h
)

# Define target
armarx_add_library(visionx-record "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")
