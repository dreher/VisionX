/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKitArmarX
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <AffordanceKit/BimanualAffordance.h>

#include <VirtualRobot/Visualization/ColorMap.h>

#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoLineSet.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>

#include <VirtualRobot/Visualization/ColorMap.h>

namespace AffordanceKitArmarX
{

    class BimanualAffordanceArmarX
    {
    public:
        BimanualAffordanceArmarX(const AffordanceKit::BimanualAffordancePtr& bimanualAffordance);
        ~BimanualAffordanceArmarX();

        void visualize(const armarx::DebugDrawerInterfacePrx& debugDrawer, const std::string& layerName, const std::string& id, float minExpectedProbability, const AffordanceKit::PrimitivePtr& primitive);

        void reset();

    protected:
        AffordanceKit::BimanualAffordancePtr affordance;

        SoCoordinate3* visualizationCoordinateNode;
        SoMaterial* visualizationMaterialNode;
        SoLineSet* visualizationLineSetNode;
        SoSeparator* visualizationNode;
    };

    typedef boost::shared_ptr<BimanualAffordanceArmarX> BimanualAffordanceArmarXPtr;


}



