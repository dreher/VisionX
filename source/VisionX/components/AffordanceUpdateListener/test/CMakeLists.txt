
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore AffordanceUpdateListener)
 
armarx_add_test(AffordanceUpdateListenerTest AffordanceUpdateListenerTest.cpp "${LIBS}")