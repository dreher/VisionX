/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once
class CByteImage;
class CStereoCalibration;

#include <vector>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

namespace visionx
{
    namespace depthfromstereo
    {
        void GetPointsFromDisparity(const CByteImage* pImageLeftColor, const CByteImage* pImageRightColor, const CByteImage* pImageLeftGrey, const CByteImage* pImageRightGrey,
                                    const int nDisparityPointDistance, pcl::PointCloud<pcl::PointXYZRGBA>& aPointsFromDisparity, CByteImage* pDisparityImage,
                                    const int imageWidth, const int imageHeight, CStereoCalibration* pStereoCalibration, bool imagesAreUndistorted, const bool smoothDisparities = true);

        void FillHolesGray(const CByteImage* pInputImage, CByteImage* pOutputImage, const int imageWidth, const int imageHeight, const int nRadius = 1);

        void CalculateSmoothedDisparityImage(float* pInputDisparity, float* pSmoothedDisparity, const int imageWidth, const int imageHeight, const int nRadius = 1);

    }
}
