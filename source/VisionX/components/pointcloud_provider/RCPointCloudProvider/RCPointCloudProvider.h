/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RCPointCloudProvider
 * @author     Fabian Paus ( paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>


#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>
#include <VisionX/interface/components/RGBDImageProvider.h>

#include <Image/ImageProcessor.h>
#include <Calibration/Undistortion.h>


#include <rc_genicam_api/device.h>
#include <rc_genicam_api/stream.h>
#include <rc_genicam_api/imagelist.h>
#include <memory>

namespace rcg
{

    typedef std::shared_ptr<rcg::Device> DevicePtr;
    typedef std::shared_ptr<rcg::Stream>  StreamPtr;
}

namespace visionx
{


    /**
     * @class RCPointCloudProviderPropertyDefinitions
     * @brief
     */
    class RCPointCloudProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RCPointCloudProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {

            defineOptionalProperty<std::string>("DeviceId", "00_14_2d_2c_6e_ed", "");
            defineOptionalProperty<float>("ScaleFactor", 2.0, "Image down scale factor");
            defineOptionalProperty<float>("framerate", 25.0f, "Frames per second")
            .setMatchRegex("\\d+(.\\d*)?")
            .setMin(0.0f)
            .setMax(25.0f);
            defineOptionalProperty<bool>("isEnabled", true, "enable the capturing process immediately");

            defineOptionalProperty<std::string>("ReferenceFrameName", "Roboception_LeftCamera", "Optional reference frame name.");

        }
    };

    /**
     * @defgroup Component-RCPointCloudProvider RCPointCloudProvider
     * @ingroup VisionX-Components
     * A description of the component RCPointCloudProvider.
     *
     * @class RCPointCloudProvider
     * @ingroup Component-RCPointCloudProvider
     * @brief Brief description of class RCPointCloudProvider.
     *
     * Detailed description of class RCPointCloudProvider.
     */
    class RCPointCloudProvider :
        virtual public RGBDPointCloudProviderInterface,
        virtual public CapturingPointCloudProvider,
        virtual public ImageProvider
    {
    public:
        RCPointCloudProvider();

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RCPointCloudProvider";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

    protected:
        void onInitCapturingPointCloudProvider() override;

        void onExitCapturingPointCloudProvider() override;

        void onStartCapture(float frameRate) override;

        void onStopCapture() override;

        bool doCapture() override;

        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = ::Ice::Current()) override
        {
            return stereoCalibration;
        }

        bool getImagesAreUndistorted(const Ice::Current& c = ::Ice::Current()) override
        {
            return true;
        }

        std::string getReferenceFrame(const Ice::Current& c = ::Ice::Current()) override
        {
            return getProperty<std::string>("ReferenceFrameName").getValue();
        }


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        visionx::StereoCalibration stereoCalibration;

    private:

        size_t numImages;

        float scaleFactor;

        CByteImage** cameraImages;
        CByteImage** smallImages;
        rcg::DevicePtr dev;
        rcg::StreamPtr stream;

        double focalLengthFactor = 1.0;
        double baseline = 1.0;
        double scan3dCoordinateScale = 1.0;

        rcg::ImageList intensityBuffer;
        rcg::ImageList disparityBuffer;

        // CapturingPointCloudProviderInterface interface
    public:
        void startCaptureForNumFrames(Ice::Int, const Ice::Current&) override;

        // ImageProvider interface
    protected:
        void onInitImageProvider();
        void onExitImageProvider();
    };
}

