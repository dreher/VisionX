/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RCPointCloudProvider
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RCPointCloudProvider.h"


#include <rc_genicam_api/system.h>
#include <rc_genicam_api/interface.h>
#include <rc_genicam_api/buffer.h>
#include <rc_genicam_api/image.h>
#include <rc_genicam_api/config.h>
#include <rc_genicam_api/pixel_formats.h>

#include <VisionX/tools/TypeMapping.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>


#include <Calibration/Calibration.h>
#include <Calibration/StereoCalibration.h>
#include <Image/ImageProcessor.h>
#include <stdlib.h>

using namespace visionx;
using namespace armarx;

typedef pcl::PointXYZRGBA Point;
typedef pcl::PointCloud<Point> PointCloud;

// Our installed version is missing the getColor method
// From: https://github.com/roboception/rc_genicam_api/blob/f7f1621fc3ac5d638292e3d5d1ea9393959ecd9d/rc_genicam_api/image.cc
static void rcg_getColor(uint8_t rgb[3], const std::shared_ptr<const rcg::Image>& img,
                         uint32_t ds, uint32_t i, uint32_t k)
{
    i *= ds;
    k *= ds;

    if (img->getPixelFormat() == Mono8) // convert from monochrome
    {
        size_t lstep = img->getWidth() + img->getXPadding();
        const uint8_t* p = img->getPixels() + k * lstep + i;

        uint32_t g = 0, n = 0;

        for (uint32_t kk = 0; kk < ds; kk++)
        {
            for (uint32_t ii = 0; ii < ds; ii++)
            {
                g += p[ii];
                n++;
            }

            p += lstep;
        }

        rgb[2] = rgb[1] = rgb[0] = static_cast<uint8_t>(g / n);
    }
    else if (img->getPixelFormat() == YCbCr411_8) // convert from YUV
    {
        size_t lstep = (img->getWidth() >> 2) * 6 + img->getXPadding();
        const uint8_t* p = img->getPixels() + k * lstep;

        uint32_t r = 0;
        uint32_t g = 0;
        uint32_t b = 0;
        uint32_t n = 0;

        for (uint32_t kk = 0; kk < ds; kk++)
        {
            for (uint32_t ii = 0; ii < ds; ii++)
            {
                uint8_t v[3];
                rcg::convYCbCr411toRGB(v, p, static_cast<int>(i + ii));

                r += v[0];
                g += v[1];
                b += v[2];
                n++;
            }

            p += lstep;
        }

        rgb[0] = static_cast<uint8_t>(r / n);
        rgb[1] = static_cast<uint8_t>(g / n);
        rgb[2] = static_cast<uint8_t>(b / n);
    }
}

// Adapted from: https://github.com/roboception/rc_genicam_api/blob/master/tools/gc_pointcloud.cc
static void storePointCloud(double f, double t, double scale,
                            std::shared_ptr<const rcg::Image> intensityCombined,
                            std::shared_ptr<const rcg::Image> disp,
                            PointCloud* outPoints)
{
    // intensityCombined: Top half is the left image, bottom half the right image
    // get size and scale factor between left image and disparity image

    size_t width = disp->getWidth();
    size_t height = disp->getHeight();
    bool bigendian = disp->isBigEndian();
    size_t leftWidth = intensityCombined->getWidth();
    size_t ds = (leftWidth + disp->getWidth() - 1) / disp->getWidth();

    // convert focal length factor into focal length in (disparity) pixels

    f *= width;

    // get pointer to disparity data and size of row in bytes

    const uint8_t* dps = disp->getPixels();
    size_t dstep = disp->getWidth() * sizeof(uint16_t) + disp->getXPadding();

    // count number of valid disparities

    int n = 0;
    for (size_t k = 0; k < height; k++)
    {
        int j = 0;
        for (size_t i = 0; i < width; i++)
        {
            if ((dps[j] | dps[j + 1]) != 0)
            {
                n++;
            }
            j += 2;
        }

        dps += dstep;
    }

    dps = disp->getPixels();

    outPoints->clear();
    outPoints->reserve(height * width);

    // create point cloud

    for (size_t k = 0; k < height; k++)
    {
        for (size_t i = 0; i < width; i++)
        {
            // convert disparity from fixed comma 16 bit integer into float value

            double d;
            if (bigendian)
            {
                size_t j = i << 1;
                d = scale * ((dps[j] << 8) | dps[j + 1]);
            }
            else
            {
                size_t j = i << 1;
                d = scale * ((dps[j + 1] << 8) | dps[j]);
            }

            // if disparity is valid and color can be obtained

            if (d > 0)
            {
                // reconstruct 3D point from disparity value

                double x = (i + 0.5 - 0.5 * width) * t / d;
                double y = (k + 0.5 - 0.5 * height) * t / d;
                double z = f * t / d;

                // compute size of reconstructed point

                // double x2=(i-0.5*width)*t/d;
                // double size=2*1.4*std::abs(x2-x); // unused

                // get corresponding color value

                uint8_t rgb[3] = {0, 0, 0 };
                // FIXME: Replace with rcg::getColor once available
                ::rcg_getColor(rgb, intensityCombined, static_cast<uint32_t>(ds), static_cast<uint32_t>(i),
                               static_cast<uint32_t>(k));

                // store colored point
                Point point;
                const double meterToMillimeter = 1000.0f;
                point.x = x * meterToMillimeter;
                point.y = y * meterToMillimeter;
                point.z = z * meterToMillimeter;
                point.r = rgb[0];
                point.g = rgb[1];
                point.b = rgb[2];
                point.a = 255;

                outPoints->push_back(point);
            }
        }

        dps += dstep;
    }
}

RCPointCloudProvider::RCPointCloudProvider()
    : intensityBuffer(50)
    , disparityBuffer(25)
{

}

void RCPointCloudProvider::onInitComponent()
{
    ImageProvider::onInitComponent();
    CapturingPointCloudProvider::onInitComponent();
}

void RCPointCloudProvider::onConnectComponent()
{
    ImageProvider::onConnectComponent();
    CapturingPointCloudProvider::onConnectComponent();
}

void RCPointCloudProvider::onDisconnectComponent()
{
    ImageProvider::onDisconnectComponent();
    CapturingPointCloudProvider::onDisconnectComponent();
}

void RCPointCloudProvider::onExitComponent()
{
    CapturingPointCloudProvider::onExitComponent();
    ImageProvider::onExitComponent();
}

void RCPointCloudProvider::onInitCapturingPointCloudProvider()
{
    setenv("GENICAM_GENTL64_PATH", "/usr/lib/rc_genicam_api/", 0);
    dev = rcg::getDevice(getProperty<std::string>("DeviceId").getValue().c_str());

    if (dev)
    {
        ARMARX_INFO << "Connecting to device: " << getProperty<std::string>("DeviceId").getValue();
        dev->open(rcg::Device::EXCLUSIVE);
    }
    else
    {
        ARMARX_WARNING << "Unable to connect to device: " << getProperty<std::string>("DeviceId").getValue();

        std::vector<std::shared_ptr<rcg::Device> > devices = rcg::getDevices();
        ARMARX_INFO << "found " << devices.size();
        for (const rcg::DevicePtr& d : devices)
        {
            ARMARX_INFO << "Device:" << d->getID();
            ARMARX_INFO << "Vendor:" << d->getVendor();
            ARMARX_INFO << "Model:" << d->getModel();
            ARMARX_INFO << "TL type:" << d->getTLType();
            ARMARX_INFO << "Display name: " << d->getDisplayName();
            ARMARX_INFO << "Access status:" << d->getAccessStatus();
            ARMARX_INFO << "User name:" << d->getUserDefinedName();
            ARMARX_INFO << "Serial number:" << d->getSerialNumber();
            ARMARX_INFO << "Version:" << d->getVersion();
            ARMARX_INFO << "TS Frequency:" << d->getTimestampFrequency();
        }

        ARMARX_ERROR << "Please specify the device id. Aborting";

        getArmarXManager()->asyncShutdown();

        return;
    }

    std::shared_ptr<GenApi::CNodeMapRef> nodemap = dev->getRemoteNodeMap();

    numImages = 2;

    if (!rcg::setBoolean(nodemap, "GevIEEE1588", true))
    {
        ARMARX_WARNING << "Could not activate Precision Time Protocol (PTP) client!";
    }

    visionx::ImageDimension dimensions;
    dimensions.height = rcg::getInteger(nodemap, "Height") / 2.0;
    dimensions.width = rcg::getInteger(nodemap, "Width");
    ARMARX_INFO << VAROUT(dimensions.height) << VAROUT(dimensions.width);

    rcg::setString(nodemap, "ComponentSelector", "Intensity", true);
    rcg::setString(nodemap, "ComponentEnable", "0", true);
    rcg::setString(nodemap, "ComponentSelector", "IntensityCombined", true);
    rcg::setString(nodemap, "ComponentEnable", "1", true);
    rcg::setString(nodemap, "ComponentSelector", "Disparity", true);
    rcg::setString(nodemap, "ComponentEnable", "1", true);

    // Color format

    if (rcg::setEnum(nodemap, "PixelFormat", "YCbCr411_8", false))
    {
        ARMARX_INFO << "Enabling color mode";
    }
    else
    {
        ARMARX_INFO << "Falling back to monochrome mode";
    }


    focalLengthFactor = rcg::getFloat(nodemap, "FocalLengthFactor", 0, 0, false);
    baseline = rcg::getFloat(nodemap, "Baseline", 0, 0, true);
    scan3dCoordinateScale = rcg::getFloat(nodemap, "Scan3dCoordinateScale", 0, 0, true);
    float focalLength = focalLengthFactor * dimensions.width;

    std::vector<rcg::StreamPtr> availableStreams = dev->getStreams();

    for (const rcg::StreamPtr s : availableStreams)
    {
        ARMARX_INFO << "Stream: " << s->getID();
    }

    if (!availableStreams.size())
    {
        ARMARX_FATAL << "Unable to open stream";
        return;
    }
    else
    {
        this->stream = availableStreams[0];
    }


    cameraImages = new CByteImage*[numImages];
    for (size_t i = 0 ; i < numImages; i++)
    {
        cameraImages[i] = new CByteImage(dimensions.width, dimensions.height, CByteImage::eRGB24);
    }


    scaleFactor = getProperty<float>("ScaleFactor");
    if (scaleFactor > 1.0)
    {
        dimensions.height /= scaleFactor;
        dimensions.width /= scaleFactor;
        focalLength /= scaleFactor;
    }

    smallImages = new CByteImage*[numImages];
    for (size_t i = 0 ; i < numImages; i++)
    {
        smallImages[i] = new CByteImage(dimensions.width, dimensions.height, CByteImage::eRGB24);
    }



    frameRate = getProperty<float>("framerate").getValue();

    setNumberImages(numImages);
    setImageFormat(dimensions, visionx::eRgb, visionx::eBayerPatternRg);
    setPointCloudSyncMode(visionx::eCaptureSynchronization);

    CCalibration c;
    c.SetCameraParameters(focalLength, focalLength, dimensions.width / 2.0, dimensions.height / 2.0,
                          0.0, 0.0, 0.0, 0.0,
                          Math3d::unit_mat, Math3d::zero_vec, dimensions.width, dimensions.height);

    CStereoCalibration ivtStereoCalibration;
    ivtStereoCalibration.SetSingleCalibrations(c, c);
    ivtStereoCalibration.rectificationHomographyRight = Math3d::unit_mat;
    ivtStereoCalibration.rectificationHomographyLeft = Math3d::unit_mat;


    Vec3d right_translation;
    right_translation.x = baseline * -1000.0;
    right_translation.y = 0.0;
    right_translation.z = 0.0;
    ivtStereoCalibration.SetExtrinsicParameters(Math3d::unit_mat, Math3d::zero_vec, Math3d::unit_mat, right_translation, false);


    stereoCalibration = visionx::tools::convert(ivtStereoCalibration);

}

void RCPointCloudProvider::onExitCapturingPointCloudProvider()
{
    if (dev)
    {
        dev->close();
    }

    for (size_t i = 0; i < numImages; i++)
    {
        delete cameraImages[i];
        delete smallImages[i];
    }

    delete [] cameraImages;
    delete [] smallImages;
}

void RCPointCloudProvider::onStartCapture(float framesPerSecond)
{

    // rcg::setString(nodemap, "AcquisitionFrameRate", std::to_string(framesPerSecond), true);
    stream->open();
    stream->startStreaming();
}

void RCPointCloudProvider::onStopCapture()
{
    stream->stopStreaming();
    stream->close();
}

bool RCPointCloudProvider::doCapture()
{
    const rcg::Buffer* buffer = stream->grab(10000);
    if (!buffer)
    {
        ARMARX_WARNING << deactivateSpam(10) <<  "Unable to get image";
        return false;
    }

    // check for a complete image in the buffer

    if (buffer->getIsIncomplete() || !buffer->getImagePresent())
    {
        ARMARX_INFO << deactivateSpam(1) << "Waiting for image";
        return false;
    }

    // store image in the corresponding list

    uint64_t pixelformat = buffer->getPixelFormat();
    if (pixelformat == Mono8 || pixelformat == YCbCr411_8)
    {
        intensityBuffer.add(buffer);
    }
    else if (pixelformat == Coord3D_C16)
    {
        disparityBuffer.add(buffer);
    }
    else
    {
        ARMARX_WARNING << "Unexpected pixel format: " << pixelformat;
        return false;
    }
    // check if both lists contain an image with the current timestamp

    uint64_t timestamp = buffer->getTimestampNS();

    std::shared_ptr<const rcg::Image> intensityCombined = intensityBuffer.find(timestamp);
    std::shared_ptr<const rcg::Image> disparity = disparityBuffer.find(timestamp);

    if (!intensityCombined || !disparity)
    {
        // Waiting for second synchronized image
        return false;
    }
    updateTimestamp(buffer->getTimestamp());

    // compute and store point cloud from synchronized image pair

    PointCloud::Ptr pointCloud(new PointCloud());
    storePointCloud(focalLengthFactor, baseline, scan3dCoordinateScale, intensityCombined, disparity, pointCloud.get());


    // Provide images
    {
        visionx::ImageFormatInfo imageFormat = getImageFormat();

        // ARMARX_LOG << "grabbing image " << imageFormat.dimension.width << "x" << imageFormat.dimension.height << " vs " <<  buffer->getWidth() << "x" << buffer->getHeight();


        const uint8_t* p = intensityCombined->getPixels();

        size_t px = intensityCombined->getXPadding();
        uint64_t format = intensityCombined->getPixelFormat();

        int width = imageFormat.dimension.width;
        int height = imageFormat.dimension.height;
        if (scaleFactor > 1.0)
        {
            width *= scaleFactor;
            height *= scaleFactor;
        }

        //const int imageSize = imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel;

        for (size_t n = 0; n < numImages; n++)
        {
            auto outPixels = cameraImages[n]->pixels;
            switch (format)
            {
                case Mono8:
                case Confidence8:
                case Error8:
                {
                    unsigned char const* row = p + (n * height) * (width + px);
                    unsigned char* outRow = outPixels;
                    for (int j = 0; j < height; j++)
                    {
                        for (int i = 0; i < width; i++)
                        {
                            const unsigned char c = row[i];
                            outRow[3 * i + 0] = c;
                            outRow[3 * i + 1] = c;
                            outRow[3 * i + 2] = c;
                        }

                        p += px;
                        outRow += width * 3;
                        row += width + px;
                    }
                }
                break;
                case YCbCr411_8:
                {
                    // Compression: 4 pixel are encoded in 6 bytes
                    size_t inStride = width * 6 / 4 + px;
                    unsigned char const* row = p + (n * height) * inStride;
                    unsigned char* outRow = outPixels;
                    uint8_t rgb[3];
                    for (int j = 0; j < height; j++)
                    {
                        for (int i = 0; i < width; i++)
                        {
                            rcg::convYCbCr411toRGB(rgb, row, i);
                            outRow[i * 3 + 0] = rgb[0];
                            outRow[i * 3 + 1] = rgb[1];
                            outRow[i * 3 + 2] = rgb[2];
                        }

                        row += inStride;
                        outRow += width * 3;
                    }
                }
                break;
            }
        }


        if (scaleFactor > 1.0)
        {
            ::ImageProcessor::Resize(cameraImages[0], smallImages[0]);
            ::ImageProcessor::Resize(cameraImages[1], smallImages[1]);
            provideImages(smallImages);

            //            armarx::SharedMemoryScopedWriteLockPtr lock(this->ImageProvider::sharedMemoryProvider->getScopedWriteLock());

            //            for (size_t i = 0; i < numImages; i++)
            //            {
            //                memcpy(ppImageBuffers[i], smallImages[i]->pixels, imageSize);
            //            }
        }
        else
        {
            provideImages(cameraImages);
            //            for (size_t i = 0; i < numImages; i++)
            //            {
            //                memcpy(ppImageBuffers[i], cameraImages[i]->pixels, imageSize);
            //            }
        }
    }

    providePointCloud<Point>(pointCloud);

    // remove all images from the buffer with the current or an older time stamp
    intensityBuffer.removeOld(timestamp);
    disparityBuffer.removeOld(timestamp);

    return true;
}


armarx::PropertyDefinitionsPtr RCPointCloudProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new RCPointCloudProviderPropertyDefinitions(
            getConfigIdentifier()));
}



void visionx::RCPointCloudProvider::startCaptureForNumFrames(Ice::Int, const Ice::Current&)
{
    throw std::runtime_error("Not yet implemented");
}


void visionx::RCPointCloudProvider::onInitImageProvider()
{
}

void visionx::RCPointCloudProvider::onExitImageProvider()
{
}
