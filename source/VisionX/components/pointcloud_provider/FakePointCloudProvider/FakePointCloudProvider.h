/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::FakePointCloudProvider
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <VisionX/interface/components/FakePointCloudProviderInterface.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>
#include <VisionX/core/ImageProvider.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/filter.h>

#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/range/algorithm.hpp>

#include <vector>
#include <cmath>

class CByteImage;

namespace visionx
{
    /**
      * @class FakePointCloudProviderPropertyDefinitions
      * @brief
      */
    class FakePointCloudProviderPropertyDefinitions:
        public CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        FakePointCloudProviderPropertyDefinitions(std::string prefix):
            CapturingPointCloudProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("pointCloudFileName", "VisionX/examples/point_clouds/", "the point cloud to read. If directory, all *.pcd files of this and all subdirectories are read. All point clouds are read in before the pointcloud providing starts.");
            defineOptionalProperty<bool>("rewind", true, "loop through the point clouds");
            defineOptionalProperty<float>("scaleFactor", -1.0f, "scale point cloud. only applied if value is larger than zero");
            defineOptionalProperty<Eigen::Vector2i>("dimensions", Eigen::Vector2i(640, 480), "")
            .map("320x240", Eigen::Vector2i(320, 240))
            .map("640x480", Eigen::Vector2i(640, 480))
            .map("320x240",     Eigen::Vector2i(320,  240))
            .map("640x480",     Eigen::Vector2i(640,  480))
            .map("800x600",     Eigen::Vector2i(800,  600))
            .map("768x576",     Eigen::Vector2i(768,  576))
            .map("1024x768",    Eigen::Vector2i(1024, 768))
            .map("1024x1024",   Eigen::Vector2i(1024, 1024))
            .map("1280x960",    Eigen::Vector2i(1280, 960))
            .map("1600x1200",   Eigen::Vector2i(1600, 1200));
            defineOptionalProperty<std::string>("depthCameraCalibrationFile", "", "Camera depth calibration file (YAML)");
            defineOptionalProperty<std::string>("RGBCameraCalibrationFile", "", "Camera RGB calibration file (YAML)");
            defineOptionalProperty<std::string>("sourceFrameName", "", "the robot node to use as source coordinate system for the point clouds");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<bool>("RemoveNAN", true, "Remove NAN from point cloud");
            defineOptionalProperty<std::string>("ProvidedPointCloudFormat", "XYZRGBA", "Format of the provided point cloud (XYZRGBA, XYZL, XYZRGBL)");
        }
    };

    /**
      * @class FakePointCloudProvider
      * @ingroup VisionX-Components
      * @brief A brief description
      *
      * Detailed Description
      */
    class FakePointCloudProvider :
        virtual public FakePointCloudProviderInterface,
        virtual public CapturingPointCloudProvider,
        virtual public ImageProvider
    {
    public:
        /**
          * @see armarx::ManagedIceObject::getDefaultName()
          */
        std::string getDefaultName() const override
        {
            return "FakePointCloudProvider";
        }

    protected:
        /**
          * @see visionx::PointCloudProviderBase::onInitPointCloudProvider()
          */
        void onInitCapturingPointCloudProvider() override;

        /**
          * @see visionx::PointCloudProviderBase::onExitPointCloudProvider()
          */
        void onExitCapturingPointCloudProvider() override;

        /**
          * @see visionx::PointCloudProviderBase::onStartCapture()
          */
        void onStartCapture(float frameRate) override;

        /**
          * @see visionx::PointCloudProviderBase::onStopCapture()
          */
        void onStopCapture() override;

        /**
          * @see visionx::PointCloudProviderBase::doCapture()
          */
        bool doCapture() override;

        MetaPointCloudFormatPtr getDefaultPointCloudFormat() override;

        /**
          * @see visionx::CapturingImageProvider::onInitImageProvider()
          */
        void onInitImageProvider() override;

        /**
          * @see visionx::CapturingImageProvider::onExitImageProvider()
          */
        void onExitImageProvider() override;

        StereoCalibration getStereoCalibration(const Ice::Current& c  = ::Ice::Current())
        {
            return calibration;
        }



        bool getImagesAreUndistorted(const Ice::Current& c = Ice::Current()) override
        {
            return true;
        }

        std::string getReferenceFrame(const Ice::Current& c = Ice::Current()) override
        {
            return getProperty<std::string>("sourceFrameName");
        }


        // mixed inherited stuff
        void onInitComponent() override
        {
            ImageProvider::onInitComponent();
            CapturingPointCloudProvider::onInitComponent();
        }
        void onConnectComponent() override
        {
            ImageProvider::onConnectComponent();
            CapturingPointCloudProvider::onConnectComponent();
        }
        void onDisconnectComponent() override
        {
            ImageProvider::onDisconnectComponent();
            CapturingPointCloudProvider::onDisconnectComponent();
        }

        void onExitComponent() override
        {
            ImageProvider::onExitComponent();
            CapturingPointCloudProvider::onExitComponent();
        }

        /**
          * @see PropertyUser::createPropertyDefinitions()
          */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void setPointCloudFilename(const std::string& filename, const ::Ice::Current& c = ::Ice::Current()) override;

    private:
        bool loadPointCloud(const std::string& fileName);
        bool loadPointCloudDirectory(const std::string& filename);
        bool loadCalibrationFile(std::string fileName, visionx::CameraParameters& calibration);

        template<typename PointT> bool processAndProvide(const pcl::PCLPointCloud2Ptr& pointCloud);
        bool processRGBImage(const pcl::PCLPointCloud2Ptr& pointCloud);

    private:
        std::string pointCloudFileName;
        std::string sourceFrameName;
        std::string providedPointCloudFormat;
        std::string robotStateComponentName;

        std::vector<pcl::PCLPointCloud2Ptr> pointClouds;

        unsigned int counter;

        bool rewind;
        float scaleFactor;
        bool removeNAN;

        StereoCalibration calibration;
        CByteImage** rgbImages;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        armarx::SharedRobotInterfacePrx synchronizedRobot;

        boost::mutex pointCloudMutex;
    };
}
