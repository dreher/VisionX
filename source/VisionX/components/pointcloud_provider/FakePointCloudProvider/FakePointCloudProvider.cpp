/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::FakePointCloudProvider
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FakePointCloudProvider.h"

#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <RobotAPI/libraries/core/FramedPose.h>

#include <opencv2/opencv.hpp>

// IVT
#include <Image/ByteImage.h>
#include <Image/ImageProcessor.h>
#include <Calibration/Calibration.h>


using namespace armarx;
using namespace pcl;
using namespace boost::filesystem;

namespace visionx
{
    void FakePointCloudProvider::onInitCapturingPointCloudProvider()
    {
        rewind = getProperty<bool>("rewind").getValue();
        pointCloudFileName = getProperty<std::string>("pointCloudFileName").getValue();
        scaleFactor = getProperty<float>("scaleFactor").getValue();
        sourceFrameName = getProperty<std::string>("sourceFrameName").getValue();
        removeNAN = getProperty<bool>("RemoveNAN").getValue();

        providedPointCloudFormat = getProperty<std::string>("ProvidedPointCloudFormat").getValue();
        ARMARX_INFO << "Providing point clouds in format " << providedPointCloudFormat;

        robotStateComponentName = getProperty<std::string>("RobotStateComponentName").getValue();
        if (robotStateComponentName != "")
        {
            usingProxy(robotStateComponentName);
        }

        if (!ArmarXDataPath::getAbsolutePath(pointCloudFileName, pointCloudFileName))
        {
            ARMARX_ERROR << "Could not find point cloud file in ArmarXDataPath: " << pointCloudFileName;
        }

        // loading each file
        if (is_directory(pointCloudFileName))
        {
            loadPointCloudDirectory(pointCloudFileName);
        }
        else
        {
            loadPointCloud(pointCloudFileName);
        }

        if (pointClouds.size() == 0)
        {
            ARMARX_FATAL << "unable to load point cloud: " << pointCloudFileName;
            throw LocalException("unable to load point cloud");
        }

        ARMARX_INFO << "loaded " << pointClouds.size() << " point clouds";


        visionx::CameraParameters RGBCameraIntrinsics;
        RGBCameraIntrinsics.focalLength.resize(2);
        RGBCameraIntrinsics.principalPoint.resize(2);

        if (getProperty<std::string>("RGBCameraCalibrationFile").getValue() != "" && !loadCalibrationFile(getProperty<std::string>("RGBCameraCalibrationFile").getValue(), RGBCameraIntrinsics))
        {
            ARMARX_WARNING << "Could not load rgb camera parameter file " << getProperty<std::string>("RGBCameraCalibrationFile").getValue() << " - using camera uncalibrated";
        }

        visionx::CameraParameters depthCameraIntrinsics;
        depthCameraIntrinsics.focalLength.resize(2);
        depthCameraIntrinsics.principalPoint.resize(2);

        if (getProperty<std::string>("depthCameraCalibrationFile").getValue() != "" && !loadCalibrationFile(getProperty<std::string>("depthCameraCalibrationFile").getValue(), depthCameraIntrinsics))
        {
            ARMARX_WARNING << "Could not load depth camera parameter file " << getProperty<std::string>("depthCameraCalibrationFile").getValue() << " - using camera uncalibrated";
        }

        calibration.calibrationLeft = visionx::tools::createDefaultMonocularCalibration();
        calibration.calibrationRight = visionx::tools::createDefaultMonocularCalibration();
        calibration.calibrationLeft.cameraParam = RGBCameraIntrinsics;
        calibration.calibrationRight.cameraParam = depthCameraIntrinsics;
    }


    bool FakePointCloudProvider::loadPointCloud(const std::string& fileName)
    {
        if (boost::algorithm::ends_with(fileName, ".pcd") && is_regular_file(fileName))
        {
            pcl::PCLPointCloud2Ptr cloudptr(new pcl::PCLPointCloud2());
            ARMARX_INFO << "Loading: " << fileName;

            if (io::loadPCDFile(fileName.c_str(), *cloudptr) == -1)
            {
                ARMARX_WARNING << "unable to load point cloud from file " << fileName;
                return false;
            }
            else
            {
                pointClouds.push_back(cloudptr);
                return true;
            }
        }

        return false;
    }

    bool FakePointCloudProvider::loadPointCloudDirectory(const std::string& filename)
    {
        // reading file names
        std::vector<std::string> fileNames;

        for (boost::filesystem::recursive_directory_iterator end, dir(pointCloudFileName);
             dir != end && getState() < eManagedIceObjectExiting; ++dir)
        {
            // search for all *.pcd files
            if (dir->path().extension() == ".pcd")
            {
                fileNames.push_back(dir->path().string());
            }
        }

        ARMARX_INFO << "In total " << fileNames.size() << " point clouds found!";

        // sorting file names and loading them
        std::sort(fileNames.begin(),  fileNames.end());

        for (size_t f = 0; f < fileNames.size(); f++)
        {
            ARMARX_INFO << " file " << f   << " / " << fileNames.size() << " is being loaded... " << fileNames[f];
            try
            {
                loadPointCloud(fileNames[f]);
            }
            catch (...)
            {
                ARMARX_WARNING << "Loading pointcloud '" << fileNames[f] << "' failed: " << armarx::GetHandledExceptionString();
            }
        }

        return true;
    }


    void FakePointCloudProvider::onExitCapturingPointCloudProvider()
    {
        pointClouds.clear();
    }


    MetaPointCloudFormatPtr FakePointCloudProvider::getDefaultPointCloudFormat()
    {
        MetaPointCloudFormatPtr format = new MetaPointCloudFormat();

        if (providedPointCloudFormat == "XYZL")
        {
            format->type = PointContentType::eLabeledPoints;
        }
        else if (providedPointCloudFormat == "XYZRGBL")
        {
            format->type = PointContentType::eColoredLabeledPoints;
        }
        else
        {
            format->type = PointContentType::eColoredPoints;
        }

        format->capacity = 640 * 480 * visionx::tools::getBytesPerPoint(format->type) * 50;
        format->size = format->capacity;
        return format;
    }


    void FakePointCloudProvider::onInitImageProvider()
    {
        Eigen::Vector2i dimensions = getProperty<Eigen::Vector2i>("dimensions").getValue();
        setImageFormat(visionx::ImageDimension(dimensions(0), dimensions(1)), visionx::eRgb);
        setNumberImages(1);
        rgbImages = new CByteImage*[1];
        rgbImages[0] = new CByteImage(dimensions(0), dimensions(1), CByteImage::eRGB24);
        ::ImageProcessor::Zero(rgbImages[0]);

        counter = 0;
    }


    void FakePointCloudProvider::onStartCapture(float frameRate)
    {
        if (robotStateComponentName != "")
        {
            robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(robotStateComponentName);
            if (!robotStateComponent)
            {
                ARMARX_ERROR << "Failed to obtain robot state component";
                return;
            }

            synchronizedRobot = robotStateComponent->getSynchronizedRobot();
        }
    }


    void FakePointCloudProvider::onStopCapture()
    {
    }


    bool FakePointCloudProvider::doCapture()
    {
        pcl::PCLPointCloud2Ptr cloudptr;

        {
            boost::mutex::scoped_lock lock(pointCloudMutex);

            if (rewind && counter >= pointClouds.size())
            {
                counter = 0;
            }

            cloudptr = pointClouds.at(counter);
        }

        // Provide point cloud in configured format
        if (providedPointCloudFormat == "XYZRGBA")
        {
            return processAndProvide<pcl::PointXYZRGBA>(cloudptr);
        }
        else if (providedPointCloudFormat == "XYZL")
        {
            return processAndProvide<pcl::PointXYZL>(cloudptr);
        }
        else if (providedPointCloudFormat == "XYZRGBL")
        {
            return processAndProvide<pcl::PointXYZRGBL>(cloudptr);
        }
        else
        {
            ARMARX_ERROR << "Could not provide point cloud, because format '" << providedPointCloudFormat << "' is unknown";
            return false;
        }
    }


    bool FakePointCloudProvider::loadCalibrationFile(std::string fileName, visionx::CameraParameters& calibration)
    {
        ArmarXDataPath::getAbsolutePath(fileName, fileName, Ice::StringSeq(), false);
        cv::FileStorage fs(fileName, cv::FileStorage::READ);

        if (!fs.isOpened())
        {
            return false;
        }

        std::string cameraName = fs["camera_name"];
        ARMARX_LOG << "loading calibration file for " << cameraName;


        Eigen::Vector2i dimensions = getProperty<Eigen::Vector2i>("dimensions").getValue();
        int imageWidth = fs["image_width"];
        int imageHeight = fs["image_height"];

        if (imageWidth != dimensions(0) || imageHeight != dimensions(1))
        {
            ARMARX_ERROR << "invalid camera size";
            return false;
        }

        cv::Mat cameraMatrix, distCoeffs;

        fs["camera_matrix"] >> cameraMatrix;
        fs["distortion_coefficients:"] >> distCoeffs;

        for (int i = 0; i < 5; i++)
        {
            calibration.distortion[i] = distCoeffs.at<float>(0, i);
        }

        calibration.focalLength[0] = cameraMatrix.at<float>(0, 0);
        calibration.focalLength[1] = cameraMatrix.at<float>(1, 1);

        calibration.principalPoint[0] = cameraMatrix.at<float>(2, 1);
        calibration.principalPoint[1] = cameraMatrix.at<float>(2, 2);

        return true;
    }


    PropertyDefinitionsPtr FakePointCloudProvider::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new FakePointCloudProviderPropertyDefinitions(getConfigIdentifier()));
    }

    void FakePointCloudProvider::setPointCloudFilename(const std::string& filename, const Ice::Current& c)
    {
        boost::mutex::scoped_lock lock(pointCloudMutex);

        ARMARX_INFO << "Changing point cloud filename to: " << filename;

        pointClouds.clear();
        loadPointCloud(filename);
    }

    template<typename PointT>
    bool FakePointCloudProvider::processAndProvide(const PCLPointCloud2Ptr& pointCloud)
    {
        typename pcl::PointCloud<PointT>::Ptr cloudptr(new pcl::PointCloud<PointT>());
        pcl::fromPCLPointCloud2(*pointCloud, *cloudptr);

        if (cloudptr->isOrganized())
        {
            processRGBImage(pointCloud);
        }


        typename pcl::PointCloud<PointT>::Ptr resultCloudPtr(new pcl::PointCloud<PointT>());

        if (scaleFactor > 0)
        {
            Eigen::Affine3f transform(Eigen::Scaling(scaleFactor));
            pcl::transformPointCloud(*cloudptr, *resultCloudPtr, transform);
        }
        else
        {
            // Copy initial point cloud for possibly applying a second transformation
            pcl::copyPointCloud(*cloudptr, *resultCloudPtr);
        }

        if (sourceFrameName != "")
        {
            if (synchronizedRobot)
            {
                Eigen::Matrix4f sourceFramePose = armarx::FramedPosePtr::dynamicCast(synchronizedRobot->getRobotNode(sourceFrameName)->getGlobalPose())->toEigen();

                ARMARX_INFO << deactivateSpam() << "Transforming point cloud: " << sourceFramePose;
                pcl::transformPointCloud(*resultCloudPtr, *resultCloudPtr, sourceFramePose);
            }
            else
            {
                ARMARX_ERROR << "Source frame specified, but no RobotStateComponent available";
            }
        }

        if (removeNAN)
        {
            std::vector<int> indices;
            pcl::removeNaNFromPointCloud(*resultCloudPtr, *resultCloudPtr, indices);
        }

        ARMARX_VERBOSE << deactivateSpam() << "Providing point cloud of size " << resultCloudPtr->points.size();
        providePointCloud<PointT>(resultCloudPtr);
        provideImages(rgbImages);

        counter++;
        return true;
    }

    bool FakePointCloudProvider::processRGBImage(const PCLPointCloud2Ptr& pointCloud)
    {
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudptr(new pcl::PointCloud<pcl::PointXYZRGB>());
        pcl::fromPCLPointCloud2(*pointCloud, *cloudptr);

        if (!cloudptr->isOrganized())
        {
            ARMARX_WARNING << deactivateSpam(10) << "The point cloud is not organized, so no RGB image is generated";
            return false;
        }

        if ((int)cloudptr->width != rgbImages[0]->width || (int)cloudptr->height != rgbImages[0]->height)
        {
            ARMARX_WARNING << "Dimensions of point cloud and image do not match, so no RGB image is generated";
            return false;
        }

        ::ImageProcessor::Zero(rgbImages[0]);
        const int width = cloudptr->width;
        const int height = cloudptr->height;

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                pcl::PointXYZRGB& point = cloudptr->at(i * width + j);
                rgbImages[0]->pixels[3 * (i * width + j) + 0] = point.r;
                rgbImages[0]->pixels[3 * (i * width + j) + 1] = point.g;
                rgbImages[0]->pixels[3 * (i * width + j) + 2] = point.b;
            }
        }

        return true;
    }


    void FakePointCloudProvider::onExitImageProvider()
    {


        for (size_t i = 0; i < 1; i++)
        {
            delete rgbImages[i];
        }

        delete [] rgbImages;
    }

}



