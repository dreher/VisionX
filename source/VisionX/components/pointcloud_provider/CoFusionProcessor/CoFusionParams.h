#ifndef COFUSIONPARAMS_H
#define COFUSIONPARAMS_H

#ifdef USE_MASKFUSION
#include <Core/MaskFusion.h>
#else
#include <Core/CoFusion.h>
#endif

namespace armarx
{




    class CoFusionParams
    {
    public:
        CoFusionParams();

#ifdef USE_MASKFUSION
        std::unique_ptr<MaskFusion> makeCoFusion() const;
#else
        std::unique_ptr<CoFusion> makeCoFusion() const;
#endif

        int timeDelta = 200;
        int countThresh = 35000;
        float errThresh = 5e-05;
        float covThresh = 1e-05;
        bool closeLoops = true;
        bool iclnuim = false;
        bool reloc = false;
        float photoThresh = 115;
        float initConfidenceGlobal = 4;
        float initConfidenceObject = 2;
        float depthCut = 3;
        float icpThresh = 10;
        bool fastOdom = false;
        float fernThresh = 0.3095;
        bool so3 = true;
        bool frameToFrameRGB = false;
        unsigned modelSpawnOffset = 20;
        Model::MatchingType matchingType = Model::MatchingType::Drost;
        //Segmentation::Method segmentationMethod = Segmentation::Method::SUPERPIXEL_CRF;
        std::string exportDirectory = "";
        bool exportSegmentationResults = false;

        int width;
        int height;
        float fx;
        float fy;
        float cx;
        float cy;

#ifdef USE_MASKFUSION

        Segmentation::Method segmentationMethod = Segmentation::Method::BIFOLD;

        bool usePrecomputedMasksOnly = false;
        int frameQueueSize = -1;
        int deviceMaskRCNN = -1;

#endif


    };

}

#endif // COFUSIONPARAMS_H
