#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <chrono>
#include <map>
#include <ostream>

namespace stopwatch
{
    using Clock = std::chrono::system_clock;
    using Duration = std::chrono::duration<float>; // seconds
    using TimePoint = std::chrono::time_point<Clock>;

    class Stats
    {
    public:
        Stats() : numExecutions(0) { }

        std::size_t numExecutions;
        Duration totalExecutionTime;

        Duration averageExecutionTime;
        Duration latestExecutionTime;

        friend std::ostream& operator<<(std::ostream& os, const Stats& stats);

    };


    class Stopwatch
    {

    public:
        Stopwatch();

        void reset();
        void start(const std::string& tag);
        void stop(const std::string& tag);

        const Stats& getStats(const std::string& tag);

        friend std::ostream& operator<<(std::ostream& os, const Stopwatch& stats);

    private:

        std::map<std::string, Stats> statsMap;
        std::map<std::string, TimePoint> currentStarts;

    };

}

#endif // STOPWATCH_H
