/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     David Gonzalez Aguirre (david dot gonzalez at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudProvider.h"

#include <string>

using namespace armarx;
using namespace visionx;

// ================================================================== //
// == PointCloudProvider ice interface ============================== //
// ================================================================== //
armarx::Blob PointCloudProvider::getPointCloud(MetaPointCloudFormatPtr& info, const Ice::Current& c)
{
    MetaInfoSizeBasePtr infoBase = info;
    return sharedMemoryProvider->getData(infoBase);
}

MetaPointCloudFormatPtr PointCloudProvider::getPointCloudFormat(const Ice::Current& c)
{
    //    armarx::SharedMemoryScopedWriteLockPtr lock(sharedMemoryProvider->getScopedWriteLock());

    return sharedMemoryProvider->getMetaInfo();
}




// ================================================================== //
// == Component implementation ====================================== //
// ================================================================== //
void PointCloudProvider::onInitComponent()
{
    // init members
    exiting = false;

    // call setup of derived poitcloud provider implementation to setup Point Clouds type and size
    onInitPointCloudProvider();

    MetaPointCloudFormatPtr info = getDefaultPointCloudFormat();

    sharedMemoryProvider = new IceSharedMemoryProvider<unsigned char, MetaPointCloudFormat>(this, info, "PointCloudProvider");

    // Offer topic for point cloud events
    offeringTopic(getName() + ".PointCloudListener");
}


void PointCloudProvider::onConnectComponent()
{
    pointCloudProcessorProxy = getTopic<PointCloudProcessorInterfacePrx>(getName() + ".PointCloudListener");

    sharedMemoryProvider->start();

    onConnectPointCloudProvider();
}

void PointCloudProvider::onDisconnectComponent()
{
    if (sharedMemoryProvider)
    {
        sharedMemoryProvider->stop();
        sharedMemoryProvider = nullptr;
    }
}


void PointCloudProvider::onExitComponent()
{
    exiting = true;
    onExitPointCloudProvider();
}



MetaPointCloudFormatPtr PointCloudProvider::getDefaultPointCloudFormat()
{
    MetaPointCloudFormatPtr info = new MetaPointCloudFormat();
    //info->frameId = getProperty<std::string>("frameId").getValue();
    info->type = PointContentType::eColoredPoints;
    info->capacity = 640 * 480 * sizeof(ColoredPoint3D);// + info->frameId.size();
    info->size = info->capacity;
    return info;
}


