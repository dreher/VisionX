/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <cassert>
#include <iostream>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <VisionX/interface/core/DataTypes.h>


namespace visionx
{

    namespace tools
    {

        template<typename PointT>
        visionx::PointContentType getPointContentType()
        {
            if (std::is_same<PointT, pcl::PointXYZ>::value)
            {
                return visionx::ePoints;
            }
            else if (std::is_same<PointT, pcl::PointXYZL>::value)
            {
                return visionx::eLabeledPoints;
            }
            else if (std::is_same<PointT, pcl::PointXYZI>::value)
            {
                return visionx::eIntensity;
            }
            else if (std::is_same<PointT, pcl::PointXYZRGBA>::value)
            {
                return visionx::eColoredPoints;
            }
            else if (std::is_same<PointT, pcl::PointXYZRGBL>::value)
            {
                return visionx::eColoredLabeledPoints;
            }
            else if (std::is_same<PointT, pcl::PointXYZRGBNormal>::value)
            {
                return visionx::eColoredOrientedPoints;
            }
            else
            {
                return visionx::ePoints;
            }
        }


        size_t getBytesPerPoint(visionx::PointContentType pointContent);


        /**
         * @param sourcePtr point to the source pcl point cloud
         */
        //template<typename PointT>
        // void convertFromPCL(typename pcl::PointCloud<PointT>::Ptr sourcePtr, void **target)
        void convertFromPCL(pcl::PointCloud<pcl::PointXYZ>::Ptr sourcePtr, void** target);


        /**
         * @param targetPtr point to the pcl point cloud
         */
        //template<typename PointT>
        //void convertToPCL(void **source, visionx::MetaPointCloudFormatPtr pointCloudFormat, typename pcl::PointCloud<PointT>::Ptr targetPtr)
        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZ>::Ptr targetPtr);

        void convertToPCL(const visionx::ColoredPointCloud& source, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr targetPtr);
        void convertFromPCL(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sourcePtr, visionx::ColoredPointCloud& target);

        void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sourcePtr, void** target);
        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr targetPtr);

        void convertFromPCL(pcl::PointCloud<pcl::PointXYZL>::Ptr sourcePtr, void** target);
        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZL>::Ptr targetPtr);

        void convertFromPCL(pcl::PointCloud<pcl::PointXYZI>::Ptr sourcePtr, void** target);
        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZI>::Ptr targetPtr);


        void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGBL>::Ptr sourcePtr, void** target);
        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGBL>::Ptr targetPtr);

        void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& sourcePtr, void** target);
        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr targetPtr);

        enum PCLToDebugDrawerConversionMode
        {
            eConvertAsPoints,
            eConvertAsColors,
            eConvertAsLabels,
        };

        void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode = eConvertAsPoints, float pointSize = 3);
        void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZL>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode = eConvertAsPoints, float pointSize = 3);
        void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZ>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode = eConvertAsPoints, float pointSize = 3);
        void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZRGBL>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode = eConvertAsPoints, float pointSize = 3);
    }
}

