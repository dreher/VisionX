/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     David Gonzalez Aguirre (david dot gonzalez at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudProcessor.h"


#include "PointCloudConversions.h"

#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>

#include <chrono>
#include <thread>

using namespace armarx;
using namespace boost::interprocess;

namespace visionx
{
    void PointCloudProcessor::onInitComponent()
    {
        onInitPointCloudProcessor();
    }

    void PointCloudProcessor::onConnectComponent()
    {
        processorTask = new RunningTask<PointCloudProcessor>(this, &PointCloudProcessor::runProcessor);

        for (auto& provider : usedPointCloudProviders)
        {
            getPointCloudProvider(provider.first);
        }

        onConnectPointCloudProcessor();
        processorTask->start();
    }

    void PointCloudProcessor::onDisconnectComponent()
    {
        onDisconnectPointCloudProcessor();
        processorTask->stop();
    }

    void PointCloudProcessor::onExitComponent()
    {
        ARMARX_VERBOSE << "PointCloudProcessor::onExitComponent()";
        onExitPointCloudProcessor();

        boost::shared_lock<boost::shared_mutex> lock(pointCloudProviderInfoMutex);

        for (auto& resultProvider : resultPointCloudProviders)
        {
            getArmarXManager()->removeObjectBlocking(resultProvider.first);
        }
    }

    void PointCloudProcessor::runProcessor()
    {
        ARMARX_INFO << "Starting PointCloud Processor";

        // main loop of component
        while (!processorTask->isStopped())
        {
            // call process method of sub class
            process();

            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }

        ARMARX_INFO << "Stopping PointCloud Processor";
    }

    void PointCloudProcessor::usingPointCloudProvider(std::string providerName)
    {

        boost::unique_lock<boost::shared_mutex> lock(pointCloudProviderInfoMutex);

        // use PointCloud event topic
        usingTopic(providerName + ".PointCloudListener");

        // create shared memory consumer
        IceSharedMemoryConsumer<unsigned char, MetaPointCloudFormat>::pointer_type consumer = new IceSharedMemoryConsumer<unsigned char, MetaPointCloudFormat>(this, providerName, "PointCloudProvider");

        usedPointCloudProviders.insert(std::make_pair(providerName, consumer));
    }

    void PointCloudProcessor::releasePointCloudProvider(std::string providerName)
    {

        boost::upgrade_lock<boost::shared_mutex> lock(pointCloudProviderInfoMutex);

        if (usedPointCloudProviders.count(providerName))
        {
            usedPointCloudProviders.erase(providerName);
        }

        {
            boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);

            if (pointCloudProviderInfoMap.count(providerName))
            {
                pointCloudProviderInfoMap.erase(providerName);
            }
        }

        //Following calls take care of existence of parameter themself, no need to check.
        //If they return false, we can still be sure the dependency is removed and the
        //topic unsubscribed.

        //Unsubscribe topic
        this->unsubscribeFromTopic(providerName + ".PointCloudListener");

        //Remove proxy dependencies
        std::string memoryName = providerName + "Memory" + "PointCloudProvider";
        this->removeProxyDependency(providerName);
        this->removeProxyDependency(memoryName);
    }

    PointCloudProviderInfo PointCloudProcessor::getPointCloudProvider(std::string providerName, bool waitForProxy)
    {

        boost::upgrade_lock<boost::shared_mutex> lock(pointCloudProviderInfoMutex);

        if (pointCloudProviderInfoMap.count(providerName))
        {
            // return pointCloudProviderInfoMap[providerName];

            ARMARX_WARNING << "point cloud provider already started:  " << providerName;
        }

        PointCloudProviderInfo providerInfo;

        // get proxy for PointCloud polling
        providerInfo.proxy = getProxy<PointCloudProviderInterfacePrx>(providerName, waitForProxy);
        providerInfo.pointCloudFormat = providerInfo.proxy->getPointCloudFormat();

        providerInfo.pointCloudAvailableEvent.reset(new boost::condition_variable);
        providerInfo.pointCloudAvailable = false;

        size_t pointCloudBufferSize = providerInfo.pointCloudFormat->size;
        providerInfo.buffer.resize(pointCloudBufferSize);


        {
            boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);

            pointCloudProviderInfoMap.insert(std::make_pair(providerName, providerInfo));
        }


        boost::mutex::scoped_lock lock2(statisticsMutex);

        statistics[providerName].pollingFPS.reset();
        statistics[providerName].pointCloudProviderFPS.reset();

        usedPointCloudProviders[providerName]->start();

        return providerInfo;
    }


    void PointCloudProcessor::enableResultPointClouds(std::string resultProviderName, size_t shmCapacity, PointContentType pointContentType)
    {
        if (resultProviderName == "")
        {
            resultProviderName = getName() + "Result";
        }

        boost::upgrade_lock<boost::shared_mutex> lock(resultProviderMutex);

        if (resultPointCloudProviders.count(resultProviderName))
        {
            ARMARX_WARNING << "result point cloud provider already exists: " << resultProviderName;
        }
        else
        {
            IceInternal::Handle<ResultPointCloudProvider> resultProvider = Component::create<ResultPointCloudProvider>();
            resultProvider->setName(resultProviderName);
            resultProvider->setShmCapacity(shmCapacity);
            resultProvider->setPointContentType(pointContentType);

            getArmarXManager()->addObject(resultProvider);

            {
                boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);

                resultPointCloudProviders.insert(std::make_pair(resultProvider->getName(), resultProvider));
            }

            lock.unlock();

            resultProvider->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted);

        }
    }



    bool PointCloudProcessor::waitForPointClouds(int milliseconds)
    {
        boost::shared_lock<boost::shared_mutex> lock(pointCloudProviderInfoMutex);

        if (pointCloudProviderInfoMap.size() > 1)
        {
            ARMARX_ERROR << "Calling waitForPointClouds without PointCloudProvider name but using multiple PointCloudProviders";
            return false;
        }

        std::string providerName = pointCloudProviderInfoMap.begin()->first;

        return waitForPointClouds(providerName, milliseconds);
    }

    bool PointCloudProcessor::waitForPointClouds(std::string providerName, int milliseconds)
    {
        boost::shared_lock<boost::shared_mutex> lock(pointCloudProviderInfoMutex);


        // find PointCloud provider by name
        std::map<std::string, PointCloudProviderInfo>::iterator iter = pointCloudProviderInfoMap.find(providerName);

        if (iter == pointCloudProviderInfoMap.end())
        {
            ARMARX_ERROR << deactivateSpam(5) << "Trying to wait for PointClouds from unknown PointCloud provider. Call usingPointCloudProvider before";

            return false;
        }

        if (iter->second.pointCloudAvailable)
        {
            return true;
        }

        boost::shared_ptr<boost::condition_variable> cond = iter->second.pointCloudAvailableEvent;

        lock.unlock();

        // wait for conditionale
        boost::mutex mut;
        boost::unique_lock<boost::mutex> lock2(mut);
        boost::posix_time::time_duration td = boost::posix_time::milliseconds(milliseconds);


        return cond->timed_wait(lock2, td);
    }

    bool PointCloudProcessor::pointCloudHasNewData(std::string providerName)
    {
        boost::shared_lock<boost::shared_mutex> lock(pointCloudProviderInfoMutex);

        // find PointCloud provider by name
        std::map<std::string, PointCloudProviderInfo>::iterator iter = pointCloudProviderInfoMap.find(providerName);

        if (iter == pointCloudProviderInfoMap.end())
        {
            ARMARX_ERROR  << deactivateSpam(5) << "Trying to wait for PointClouds from unknown PointCloud provider. Call usingPointCloudProvider before";

            return false;
        }

        return iter->second.pointCloudAvailable;
    }


    MetaPointCloudFormatPtr PointCloudProcessor::getPointCloudFormat(std::string providerName)
    {
        boost::shared_lock<boost::shared_mutex> lock(pointCloudProviderInfoMutex);

        MetaPointCloudFormatPtr format;

        // find PointCloud provider
        std::map<std::string, PointCloudProviderInfo>::iterator iter = pointCloudProviderInfoMap.find(providerName);

        if (iter == pointCloudProviderInfoMap.end())
        {
            ARMARX_ERROR << "Trying to retrieve PointClouds from unknown PointCloud provider. Call usingPointCloudProvider before";
        }
        else
        {
            format = iter->second.proxy->getPointCloudFormat();
        }

        return format;
    }


    PointCloudTransferStats PointCloudProcessor::getPointCloudTransferStats(std::string providerName, bool resetStats)
    {
        boost::mutex::scoped_lock lock(statisticsMutex);

        std::map<std::string, PointCloudTransferStats>::iterator iter = statistics.find(providerName);

        if (iter == statistics.end())
        {
            ARMARX_ERROR << "Requesting statistics for unknown PointCloud provider (" << providerName << ")";
            return PointCloudTransferStats();
        }

        PointCloudTransferStats stats = iter->second;

        if (resetStats)
        {
            iter->second.pointCloudProviderFPS.reset();
            iter->second.pollingFPS.reset();
        }
        else
        {
            iter->second.pointCloudProviderFPS.recalculate();
            iter->second.pollingFPS.recalculate();
        }

        return stats;
    }

    void PointCloudProcessor::reportPointCloudAvailable(const std::string& providerName, const Ice::Current& c)
    {
        boost::shared_lock<boost::shared_mutex> lock(pointCloudProviderInfoMutex);

        // find provider
        std::map<std::string, PointCloudProviderInfo>::iterator iter = pointCloudProviderInfoMap.find(providerName);

        if (iter == pointCloudProviderInfoMap.end())
        {
            ARMARX_ERROR << "Received notification from unknown PointCloudProvider (" << providerName << ")";
            return;
        }

        iter->second.pointCloudAvailable = true;
        iter->second.pointCloudAvailableEvent->notify_all();

        // update statistics
        boost::mutex::scoped_lock lock2(statisticsMutex);
        statistics[providerName].pointCloudProviderFPS.update();
    }
}
