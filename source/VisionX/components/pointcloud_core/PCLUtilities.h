/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <map>
#include <tuple>
#include <iostream>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/io.h>
#include <pcl/io/pcd_io.h>

#ifdef PCL_COMMON_COLORS_H
#include <pcl/common/colors.h>
#endif

namespace visionx
{

    namespace tools
    {


        template<typename PointT>
        std::tuple<uint8_t, uint8_t, uint8_t> colorizeSegment(typename pcl::PointCloud<PointT>::Ptr& segment)
        {
            const uint8_t r = rand() % 255;
            const uint8_t g = rand() % 255;
            const uint8_t b = rand() % 255;

            for (auto& p : segment->points)
            {
                p.r = r;
                p.g = g;
                p.b = b;
            }

            return std::make_tuple(r, g, b);
        }


        void colorizeLabeledPointCloud(pcl::PointCloud<pcl::PointXYZL>::Ptr sourceCloudPtr, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr targetCloudPtr)
        {
            pcl::copyPointCloud(*sourceCloudPtr, *targetCloudPtr);

            std::map<uint32_t, float> colorMap;

            for (size_t i = 0; i < sourceCloudPtr->points.size(); i++)
            {
                pcl::PointXYZL p = sourceCloudPtr->points[i];

                if (!colorMap.count(p.label))
                {
#ifdef PCL_COMMON_COLORS_H
                    pcl::RGB c = pcl::GlasbeyLUT::at(p.label % pcl::GlasbeyLUT::size());
                    colorMap.insert(std::make_pair(p.label, c.rgb));
#else
                    const uint8_t r = rand() % 255;
                    const uint8_t g = rand() % 255;
                    const uint8_t b = rand() % 255;
                    float color = r << 16 | g << 8 | b;
                    colorMap.insert(std::make_pair(p.label, color));
#endif
                }

                targetCloudPtr->points[i].rgb = colorMap[p.label];
            }
        }

        template <typename PointCloudType>
        void fillLabelMap(PointCloudType labeledCloudPtr, std::map<uint32_t, pcl::PointIndices>& labeledPointMap)
        {
            for (size_t i = 0; i < labeledCloudPtr->points.size(); i++)
            {
                uint32_t currentLabel = labeledCloudPtr->points[i].label;

                if (!currentLabel)
                {
                    continue;
                }
                else if (labeledPointMap.count(currentLabel))
                {
                    labeledPointMap[currentLabel].indices.push_back(i);
                }
                else
                {
                    pcl::PointIndices labelIndices;
                    labelIndices.indices.push_back(i);
                    labeledPointMap.insert(std::make_pair(currentLabel, labelIndices));
                }
            }
        }




    }
}

