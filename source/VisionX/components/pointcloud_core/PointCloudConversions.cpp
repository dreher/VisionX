/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudConversions.h"

#include <pcl/io/pcd_io.h>


#ifdef PCL_COMMON_COLORS_H
#include <pcl/common/colors.h>
#endif


namespace visionx
{
    namespace tools
    {
        size_t getBytesPerPoint(visionx::PointContentType pointContent)
        {
            switch (pointContent)
            {
                case visionx::ePoints:
                    return sizeof(visionx::Point3D);

                case visionx::eColoredPoints:
                    return sizeof(visionx::ColoredPoint3D);

                case visionx::eOrientedPoints:
                    return sizeof(visionx::OrientedPoint3D);

                case visionx::eColoredOrientedPoints:
                    return sizeof(visionx::ColoredOrientedPoint3D);

                case visionx::eLabeledPoints:
                    return sizeof(visionx::LabeledPoint3D);

                case visionx::eColoredLabeledPoints:
                    return sizeof(visionx::ColoredLabeledPoint3D);

                case visionx::eIntensity:
                    return sizeof(visionx::IntensityPoint3D);

                default:
                    return 0;
                    //ARMARX_FATAL_S << "invalid point cloud content " << pointContent;
            }
        }


        void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sourcePtr, void** target)
        {
            const unsigned int Area = sourcePtr->width * sourcePtr->height;
            visionx::ColoredPoint3D* pBuffer = new visionx::ColoredPoint3D[Area];

            size_t size = sourcePtr->points.size();
            auto& points = sourcePtr->points;
            for (size_t i = 0; i < size; i++)
            {
                auto& p = points[i];
                auto& pB = pBuffer[i];
                pB.point.x = p.x;
                pB.point.y = p.y;
                pB.point.z = p.z;
                pB.color.r = p.r;
                pB.color.g = p.g;
                pB.color.b = p.b;
                pB.color.a = p.a;
            }

            memcpy(target[0], pBuffer, Area * sizeof(visionx::ColoredPoint3D));
            delete[] pBuffer;
        }


        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr targetPtr)
        {
            targetPtr->width = pointCloudFormat->width;
            targetPtr->height = pointCloudFormat->height;
            targetPtr->points.resize(targetPtr->width * targetPtr->height);
            visionx::ColoredPoint3D* pBuffer = reinterpret_cast<visionx::ColoredPoint3D*>(*source);
            size_t size = targetPtr->points.size();
            auto& points = targetPtr->points;
            for (size_t i = 0; i < size; i++)
            {
                auto& p = points[i];
                auto& pB = pBuffer[i];
                p.x = pB.point.x;
                p.y = pB.point.y;
                p.z = pB.point.z;
                p.r = pB.color.r;
                p.g = pB.color.g;
                p.b = pB.color.b;
                p.a = pB.color.a;
            }
        }

        void convertFromPCL(pcl::PointCloud<pcl::PointXYZL>::Ptr sourcePtr, void** target)
        {
            const unsigned int Area = sourcePtr->width * sourcePtr->height;
            visionx::LabeledPoint3D* pBuffer = new visionx::LabeledPoint3D[Area];

            for (size_t i = 0; i < sourcePtr->points.size(); i++)
            {
                pBuffer[i].point.x = sourcePtr->points[i].x;
                pBuffer[i].point.y = sourcePtr->points[i].y;
                pBuffer[i].point.z = sourcePtr->points[i].z;
                pBuffer[i].label = sourcePtr->points[i].label;
            }

            memcpy(target[0], pBuffer, Area * sizeof(visionx::LabeledPoint3D));
            delete[] pBuffer;
        }


        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZL>::Ptr targetPtr)
        {
            targetPtr->width = pointCloudFormat->width;
            targetPtr->height = pointCloudFormat->height;
            targetPtr->points.resize(targetPtr->width * targetPtr->height);
            visionx::LabeledPoint3D* pBuffer = reinterpret_cast<visionx::LabeledPoint3D*>(*source);

            for (size_t i = 0; i < targetPtr->points.size(); i++)
            {
                targetPtr->points[i].x = pBuffer[i].point.x;
                targetPtr->points[i].y = pBuffer[i].point.y;
                targetPtr->points[i].z = pBuffer[i].point.z;
                targetPtr->points[i].label = pBuffer[i].label;
            }
        }



        void convertFromPCL(pcl::PointCloud<pcl::PointXYZI>::Ptr sourcePtr, void** target)
        {
            const unsigned int Area = sourcePtr->width * sourcePtr->height;
            visionx::IntensityPoint3D* pBuffer = new visionx::IntensityPoint3D[Area];

            for (size_t i = 0; i < sourcePtr->points.size(); i++)
            {
                pBuffer[i].point.x = sourcePtr->points[i].x;
                pBuffer[i].point.y = sourcePtr->points[i].y;
                pBuffer[i].point.z = sourcePtr->points[i].z;
                pBuffer[i].intensity = sourcePtr->points[i].intensity;
            }

            memcpy(target[0], pBuffer, Area * sizeof(visionx::IntensityPoint3D));
            delete[] pBuffer;
        }


        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZI>::Ptr targetPtr)
        {
            targetPtr->width = pointCloudFormat->width;
            targetPtr->height = pointCloudFormat->height;
            targetPtr->points.resize(targetPtr->width * targetPtr->height);
            visionx::IntensityPoint3D* pBuffer = reinterpret_cast<visionx::IntensityPoint3D*>(*source);

            for (size_t i = 0; i < targetPtr->points.size(); i++)
            {
                targetPtr->points[i].x = pBuffer[i].point.x;
                targetPtr->points[i].y = pBuffer[i].point.y;
                targetPtr->points[i].z = pBuffer[i].point.z;
                targetPtr->points[i].intensity = pBuffer[i].intensity;
            }
        }



        void convertFromPCL(pcl::PointCloud<pcl::PointXYZ>::Ptr sourcePtr, void** target)
        {
            const unsigned int Area = sourcePtr->width * sourcePtr->height;
            visionx::Point3D* pBuffer = new visionx::Point3D[Area];

            for (size_t i = 0; i < sourcePtr->points.size(); i++)
            {
                pBuffer[i].x = sourcePtr->points[i].x;
                pBuffer[i].y = sourcePtr->points[i].y;
                pBuffer[i].z = sourcePtr->points[i].z;
            }

            memcpy(target[0], pBuffer, Area * sizeof(visionx::Point3D));
            delete[] pBuffer;

        }

        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZ>::Ptr targetPtr)
        {
            targetPtr->width = pointCloudFormat->width;
            targetPtr->height = pointCloudFormat->height;
            targetPtr->points.resize(targetPtr->width * targetPtr->height);
            visionx::Point3D* pBuffer = reinterpret_cast<visionx::Point3D*>(*source);

            for (size_t i = 0; i < targetPtr->points.size(); i++)
            {
                targetPtr->points[i].x = pBuffer[i].x;
                targetPtr->points[i].y = pBuffer[i].y;
                targetPtr->points[i].z = pBuffer[i].z;
            }
        }

        void convertToPCL(const ColoredPointCloud& source, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr targetPtr)
        {
            if (!targetPtr)
            {
                targetPtr = pcl::PointCloud<pcl::PointXYZRGBA>::Ptr(new pcl::PointCloud<pcl::PointXYZRGBA>);
            }

            targetPtr->resize(source.size());

            for (size_t i = 0; i < targetPtr->points.size(); i++)
            {
                targetPtr->points[i].x = source.at(i).point.x;
                targetPtr->points[i].y = source.at(i).point.y;
                targetPtr->points[i].z = source.at(i).point.z;
                targetPtr->points[i].r = source.at(i).color.r;
                targetPtr->points[i].g = source.at(i).color.g;
                targetPtr->points[i].b = source.at(i).color.b;
                targetPtr->points[i].a = source.at(i).color.a;
            }
        }

        void convertFromPCL(const  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sourcePtr, ColoredPointCloud& target)
        {
            target.resize(sourcePtr->size());

            for (size_t i = 0; i < target.size(); i++)
            {
                target.at(i).point.x = sourcePtr->points[i].x;
                target.at(i).point.y = sourcePtr->points[i].y;
                target.at(i).point.z = sourcePtr->points[i].z;
                target.at(i).color.r = sourcePtr->points[i].r;
                target.at(i).color.g = sourcePtr->points[i].g;
                target.at(i).color.b = sourcePtr->points[i].b;
                target.at(i).color.a = sourcePtr->points[i].a;
            }
        }


        void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGBL>::Ptr sourcePtr, void** target)
        {
            const unsigned int Area = sourcePtr->width * sourcePtr->height;
            visionx::ColoredLabeledPoint3D* pBuffer = new visionx::ColoredLabeledPoint3D[Area];

            for (size_t i = 0; i < sourcePtr->points.size(); i++)
            {
                pBuffer[i].point.x = sourcePtr->points[i].x;
                pBuffer[i].point.y = sourcePtr->points[i].y;
                pBuffer[i].point.z = sourcePtr->points[i].z;
                pBuffer[i].color.r = sourcePtr->points[i].r;
                pBuffer[i].color.g = sourcePtr->points[i].g;
                pBuffer[i].color.b = sourcePtr->points[i].b;
                pBuffer[i].color.a = sourcePtr->points[i].a;
                pBuffer[i].label = sourcePtr->points[i].label;
            }

            memcpy(target[0], pBuffer, Area * sizeof(visionx::ColoredLabeledPoint3D));
            delete[] pBuffer;

        }

        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGBL>::Ptr targetPtr)
        {
            targetPtr->width = pointCloudFormat->width;
            targetPtr->height = pointCloudFormat->height;
            targetPtr->points.resize(targetPtr->width * targetPtr->height);
            visionx::ColoredLabeledPoint3D* pBuffer = reinterpret_cast<visionx::ColoredLabeledPoint3D*>(*source);

            for (size_t i = 0; i < targetPtr->points.size(); i++)
            {
                targetPtr->points[i].x = pBuffer[i].point.x;
                targetPtr->points[i].y = pBuffer[i].point.y;
                targetPtr->points[i].z = pBuffer[i].point.z;
                targetPtr->points[i].r = pBuffer[i].color.r;
                targetPtr->points[i].g = pBuffer[i].color.g;
                targetPtr->points[i].b = pBuffer[i].color.b;
                targetPtr->points[i].a = pBuffer[i].color.a;
                targetPtr->points[i].label = pBuffer[i].label;

            }
        }

        void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& sourcePtr, void** target)
        {
            const unsigned int area = sourcePtr->width * sourcePtr->height;
            std::vector<visionx::ColoredOrientedPoint3D> buffer(area);

            for (size_t i = 0; i < sourcePtr->points.size(); i++)
            {
                buffer[i].point.x = sourcePtr->points[i].x;
                buffer[i].point.y = sourcePtr->points[i].y;
                buffer[i].point.z = sourcePtr->points[i].z;
                buffer[i].color.r = sourcePtr->points[i].r;
                buffer[i].color.g = sourcePtr->points[i].g;
                buffer[i].color.b = sourcePtr->points[i].b;
                buffer[i].normal.nx = sourcePtr->points[i].normal_x;
                buffer[i].normal.ny = sourcePtr->points[i].normal_y;
                buffer[i].normal.nz = sourcePtr->points[i].normal_z;
            }

            copy(buffer.begin(), buffer.end(), static_cast<visionx::ColoredOrientedPoint3D*>(target[0]));
        }

        void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr targetPtr)
        {
            targetPtr->width = pointCloudFormat->width;
            targetPtr->height = pointCloudFormat->height;
            targetPtr->points.resize(targetPtr->width * targetPtr->height);
            visionx::ColoredOrientedPoint3D* pBuffer = static_cast<visionx::ColoredOrientedPoint3D*>(*source);

            for (size_t i = 0; i < targetPtr->points.size(); i++)
            {
                targetPtr->points[i].x = pBuffer[i].point.x;
                targetPtr->points[i].y = pBuffer[i].point.y;
                targetPtr->points[i].z = pBuffer[i].point.z;
                targetPtr->points[i].r = pBuffer[i].color.r;
                targetPtr->points[i].g = pBuffer[i].color.g;
                targetPtr->points[i].b = pBuffer[i].color.b;
                targetPtr->points[i].normal_x = pBuffer[i].normal.nx;
                targetPtr->points[i].normal_y = pBuffer[i].normal.ny;
                targetPtr->points[i].normal_z = pBuffer[i].normal.nz;
            }
        }

        void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode, float pointSize)
        {
            target.pointSize = pointSize;

            int numPoints = sourcePtr->width * sourcePtr->height;
            target.points.reserve(numPoints);

            armarx::DebugDrawer24BitColoredPointCloudElement e;
            for (const auto & p : sourcePtr->points)
            {
                if (pcl_isfinite(p.x) && pcl_isfinite(p.y) && pcl_isfinite(p.z))
                {
                    e.x = p.x;
                    e.y = p.y;
                    e.z = p.z;

                    if (mode == eConvertAsColors)
                    {
                        e.color.r = p.r;
                        e.color.g = p.g;
                        e.color.b = p.b;
                    }
                    else
                    {
                        // Simple rainbow coloring
                        float value = ((int)e.z / 10) % 256;

                        e.color.r = (value > 128) ? (value - 128) * 2 : 0;
                        e.color.g = (value < 128) ? 2 * value : 255 - ((value - 128) * 2);
                        e.color.b = (value < 128) ? 255 - (2 * value) : 0;
                    }

                    target.points.push_back(e);
                }
            }
        }

        void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZL>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode, float pointSize)
        {
            target.pointSize = pointSize;

            int numPoints = sourcePtr->width * sourcePtr->height;
            target.points.reserve(numPoints);
            std::map<uint32_t, float> colorMap;

            armarx::DebugDrawer24BitColoredPointCloudElement e;
            for (const auto & p : sourcePtr->points)
            {
                if (pcl_isfinite(p.x) && pcl_isfinite(p.y) && pcl_isfinite(p.z))
                {
                    e.x = p.x;
                    e.y = p.y;
                    e.z = p.z;

                    if (mode == eConvertAsLabels)
                    {

                        if (!colorMap.count(p.label))
                        {
#ifdef PCL_COMMON_COLORS_H
                            pcl::RGB c = pcl::GlasbeyLUT::at(p.label % pcl::GlasbeyLUT::size());
                            colorMap.insert(std::make_pair(p.label, c.rgb));
#else
                            const uint8_t r = rand() % 255;
                            const uint8_t g = rand() % 255;
                            const uint8_t b = rand() % 255;
                            float color = r << 16 | g << 8 | b;
                            colorMap.insert(std::make_pair(p.label, color));
#endif
                        }



                        const uint8_t rgb = colorMap[p.label];

                        e.color.r = (rgb >> 16) % 255;
                        e.color.g = (rgb >> 8) % 255;
                        e.color.b = rgb % 255;
                    }
                    else
                    {
                        // Simple rainbow coloring
                        float value = ((int)e.z / 10) % 256;

                        e.color.r = (value > 128) ? (value - 128) * 2 : 0;
                        e.color.g = (value < 128) ? 2 * value : 255 - ((value - 128) * 2);
                        e.color.b = (value < 128) ? 255 - (2 * value) : 0;
                    }

                    target.points.push_back(e);
                }
            }
        }

        void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZ>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode, float pointSize)
        {
            target.pointSize = pointSize;

            int numPoints = sourcePtr->width * sourcePtr->height;
            target.points.reserve(numPoints);

            armarx::DebugDrawer24BitColoredPointCloudElement e;
            for (const auto & p : sourcePtr->points)
            {
                if (pcl_isfinite(p.x) && pcl_isfinite(p.y) && pcl_isfinite(p.z))
                {
                    e.x = p.x;
                    e.y = p.y;
                    e.z = p.z;

                    // Simple rainbow coloring
                    float value = ((int)e.z / 10) % 256;

                    e.color.r = (value > 128) ? (value - 128) * 2 : 0;
                    e.color.g = (value < 128) ? 2 * value : 255 - ((value - 128) * 2);
                    e.color.b = (value < 128) ? 255 - (2 * value) : 0;

                    target.points.push_back(e);
                }
            }
        }

        void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZRGBL>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode, float pointSize)
        {
            target.pointSize = pointSize;

            int numPoints = sourcePtr->width * sourcePtr->height;
            target.points.reserve(numPoints);
            std::map<uint32_t, float> colorMap;

            armarx::DebugDrawer24BitColoredPointCloudElement e;
            for (const auto & p : sourcePtr->points)
            {
                if (pcl_isfinite(p.x) && pcl_isfinite(p.y) && pcl_isfinite(p.z))
                {
                    e.x = p.x;
                    e.y = p.y;
                    e.z = p.z;

                    if (mode == eConvertAsColors)
                    {
                        e.color.r = p.r;
                        e.color.g = p.g;
                        e.color.b = p.b;
                    }
                    else if (mode == eConvertAsLabels)
                    {


                        if (!colorMap.count(p.label))
                        {
#ifdef PCL_COMMON_COLORS_H
                            pcl::RGB c = pcl::GlasbeyLUT::at(p.label % pcl::GlasbeyLUT::size());
                            colorMap.insert(std::make_pair(p.label, c.rgb));
#else
                            const uint8_t r = rand() % 255;
                            const uint8_t g = rand() % 255;
                            const uint8_t b = rand() % 255;
                            float color = r << 16 | g << 8 | b;
                            colorMap.insert(std::make_pair(p.label, color));
#endif
                        }


                        const uint8_t rgb = colorMap[p.label];

                        e.color.r = (rgb >> 16) % 255;
                        e.color.g = (rgb >> 8) % 255;
                        e.color.b = rgb % 255;


                    }
                    else
                    {
                        // Simple rainbow coloring
                        float value = ((int)e.z / 10) % 256;

                        e.color.r = (value > 128) ? (value - 128) * 2 : 0;
                        e.color.g = (value < 128) ? 2 * value : 255 - ((value - 128) * 2);
                        e.color.b = (value < 128) ? 255 - (2 * value) : 0;
                    }

                    target.points.push_back(e);
                }
            }
        }
    }
}
