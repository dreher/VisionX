/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Component
* @author    Kai Welke <welke at kit dot edu>
* @copyright  2013 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// boost
#include <boost/shared_ptr.hpp>

// VisionX
#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>



#include <ArmarXCore/observers/DebugObserver.h>

// IVT
#include <Color/ColorParameterSet.h>
#include <SegmentableRecognition/SegmentableRecognition.h>
#include <Visualizer/OpenGLVisualizer.h>
#include <Eigen/Core>

// global forward declarations
class CByteImage;
class CGLContext;

namespace visionx
{

    static Eigen::Vector3f stringToVector3f(std::string propertyValue)
    {
        Eigen::Vector3f vec;
        sscanf(propertyValue.c_str(), "%f, %f, %f", &vec.data()[0], &vec.data()[1], &vec.data()[2]);
        return vec;
    }


    // shared pointer type definitions
    typedef boost::shared_ptr<CSegmentableRecognition>      CSegmentableRecognitionPtr;
    typedef boost::shared_ptr<CColorParameterSet>      CColorParameterSetPtr;

    // properties of SegmentableObjectRecognition
    class SegmentableObjectRecognitionPropertyDefinitions:
        public ObjectLocalizerProcessorPropertyDefinitions
    {
    public:
        SegmentableObjectRecognitionPropertyDefinitions(std::string prefix):
            ObjectLocalizerProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ColorParameterFile", "VisionX/examples/colors.txt", "The color parameter file configures the colors used for segmentable recognition (usually colors.txt)");
            defineOptionalProperty<float>("MinPixelsPerRegion", 200, "Minimum number of pixels per region for detecting a uniformly colored object");
            defineOptionalProperty<float>("MaxEpipolarDistance", 6, "Maximum epipolar line distance allowed for a valid 3D recognition result");


            armarx::PropertyDefinition<Eigen::Vector3f>::PropertyFactoryFunction f = &stringToVector3f;

            defineOptionalProperty<Eigen::Vector3f>("MinPoint", Eigen::Vector3f(-3000.0f, -3000.0f, 100.0f), "min point for valid result bounding box").setFactory(f);
            defineOptionalProperty<Eigen::Vector3f>("MaxPoint", Eigen::Vector3f(3000.0f, 3000.0f, 3500.0f), "max point for valid result bounding box").setFactory(f);

            defineOptionalProperty<float>("SizeRatioThreshold", 0.7f, "");
            defineOptionalProperty<float>("CorrelationThreshold", 0.7f, "");
        }
    };

    /**
     * SegmentableObjectRecognition uses CSegmentableRecognition from IVT to recognize and localize single-colored objects based on their color and shape.
     * The object data is read from PriorKnowledge and CommonStorage via MemoryX.
     * The object localization is invoked automatically by the working memory if the object has been requested there.
     *
     */
    class SegmentableObjectRecognition:
        virtual public ObjectLocalizerProcessor
    {
    public:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new SegmentableObjectRecognitionPropertyDefinitions(getConfigIdentifier()));
        }

        void reportStereoCalibrationChanged(const StereoCalibration& stereoCalibration, bool x, const std::string& referenceName, const Ice::Current& c = Ice::Current()) override
        {
            ObjectLocalizerProcessor::reportStereoCalibrationChanged(stereoCalibration, x, referenceName, c);

            initRecognizer();
        }

        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "SegmentableObjectRecognition";
        }

        SegmentableObjectRecognition();
        ~SegmentableObjectRecognition() override;

    protected:
        /**
         * @see ObjectLocalizerProcessor::onInitObjectLocalizerProcessor()
         */
        void onInitObjectLocalizerProcessor() override
        {

            offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
        }

        /**
         * Initializes the CSegmentableRecognition
         *
         * @see ObjectLocalizerProcessor::onConnectObjectLocalizerProcessor()
         */
        void onConnectObjectLocalizerProcessor() override
        {

            debugObserver = getTopic<armarx::DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
        }

        /**
         * @see ObjectLocalizerProcessor::onExitObjectLocalizerProcessor()
         */
        void onExitObjectLocalizerProcessor() override;

        /**
         * Initializes segmentable recognition
         *
         * @return success
         */
        bool initRecognizer() override;

        /**
         * Add object class to segmentable object recognition.
         *
         * @param objectClassEntity entity containing all information available for the object class
         * @param fileManager GridFileManager required to read files associated to prior knowledge from the database.
         *
         * @return success of adding this entity to the TexturedObjectDatabase
         */
        bool addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager) override;

        /**
         * localizes segmentable object instances
         *
         * @param objectClassNames names of the class to localize
         * @param cameraImages the two input images
         * @param resultImages the two result images. are provided if result images are enabled.
         *
         * @return list of object instances
         */
        virtual memoryx::ObjectLocalizationResultList localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages);

    private:
        // calculates recognition certainty
        float calculateRecognitionCertainty(const std::string& objectClassName, const Object3DEntry& entry);
        void visualizeResults(const Object3DList& objectList, CByteImage** resultImages);

        // pointer to IVT segmentable recognition
        CSegmentableRecognitionPtr segmentableRecognition;
        boost::shared_ptr<CGLContext> contextGL;
        boost::shared_ptr<COpenGLVisualizer> m_pOpenGLVisualizer;

        // settings
        float minPixelsPerRegion;
        float maxEpipolarDistance;
        CColorParameterSetPtr colorParameters;

        // remember colors of objects (not stored in IVT viewdatabase)
        std::map<std::string, ObjectColor> objectColors;

        Vec3d validResultBoundingBoxMin, validResultBoundingBoxMax;


        std::map<std::string, int> seq;


        armarx::DebugObserverInterfacePrx debugObserver;

    };
}
