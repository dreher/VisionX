/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// VisionX
#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>
#include <Image/StereoMatcher.h>

#include <VirtualRobot/VirtualRobot.h>
// RobotAPI
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

// forward declarations
class CByteImage;
class CVisualTargetLocator;
class CColorParameterSet;

namespace visionx
{
    class BigBowlLocalizationPropertyDefinitions:
        public ObjectLocalizerProcessorPropertyDefinitions
    {
    public:
        BigBowlLocalizationPropertyDefinitions(std::string prefix):
            ObjectLocalizerProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ColorParameterFile", "VisionX/examples/colors.txt", "The color parameter file configures the colors used for segmentable recognition (usually colors.txt)");
            defineOptionalProperty<bool>("ShowSegmentedResultImages", true, "Decide whether to calculate color-segmented images with the marker color and show them as result images. Causes small additional computational effort.");
            defineOptionalProperty<bool>("UseVision", true, "You can switch off the use of the camera images. The returned pose will then just be taken from the forward kinematics.");

            defineOptionalProperty<ObjectColor>("BlobColor", eOrange, "")
            .map("Blue", eBlue)
            .map("Blue2", eBlue2)
            .map("Blue3", eBlue3)
            .map("Colored", eColored)
            .map("Green", eGreen)
            .map("Green2", eGreen2)
            .map("Green3", eGreen3)
            .map("Orange", eOrange)
            .map("Orange2", eOrange2)
            .map("Orange3", eOrange3)
            .map("Red", eRed)
            .map("Red2", eRed2)
            .map("Red3", eRed3)
            .map("Skin", eSkin)
            .map("White", eWhite)
            .map("Yellow", eYellow)
            .map("Yellow2", eYellow2)
            .map("Yellor3", eYellow3);

            defineOptionalProperty<int>("nMinPixelsPerRegion", 450, "");
            defineOptionalProperty<double>("dMaxEpipolarDiff", 10, "");
            defineOptionalProperty<double>("dMinFilledRatio", 0.55, "");
            defineOptionalProperty<double>("dMaxSideRatio", 1.4, "");
            defineOptionalProperty<double>("dMinSize", 1150, "");
            defineOptionalProperty<double>("dMaxSize", 2200, "");

        }
    };


    /**
     * BigBowlLocalization uses the CVisualTargetLocator of IVT in order to recognize and localize the marker balls at the hands of the robot.
     * The returned hand pose takes the offset from marker to TCP into account, which is defined in the robot model file.
     * The marker color is read from PriorKnowledge and CommonStorage via MemoryX.
     * The hand localization is invoked automatically by the working memory if the object has been requested there.
     */
    class BigBowlLocalization:
        virtual public ObjectLocalizerProcessor
    {
    public:

        BigBowlLocalization();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new BigBowlLocalizationPropertyDefinitions(getConfigIdentifier()));
        }

        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "BigBowlLocalization";
        }

    protected:
        /**
         * @see ObjectLocalizerProcessor::onInitObjectLocalizerProcessor()
         */
        void onInitObjectLocalizerProcessor() override;

        /**
         * Initialize stuff here?
         *
         * @see ObjectLocalizerProcessor::onConnectObjectLocalizerProcessor()
         */
        void onConnectObjectLocalizerProcessor() override;

        /**
         * @see ObjectLocalizerProcessor::onExitObjectLocalizerProcessor()
         */
        void onExitObjectLocalizerProcessor() override;

        /**
         * Initializes segmentable recognition
         *
         * @return success
         */
        bool initRecognizer() override;

        bool addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager) override
        {
            return true;
        }

        /**
         * localize one or both hand markers
         *
         * @param objectClassNames is ignored here
         * @param cameraImages the two input images
         * @param resultImages the two result images. are provided if result images are enabled.
         *
         * @return list of object instances
         */
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages) override;


    private:

        void drawCrossInImage(CByteImage* image, Eigen::Vector3f point, bool leftCamera);

        int locateBowl(CByteImage** cameraImages, Vec3d& bowlPosition, CByteImage** resultImages);

        // decides whether images or forward kinematic will be used
        bool useVision;

        // locator for the tracking balls
        CVisualTargetLocator* visualTargetLocator;
        ObjectColor objectColor;

        // remote robot for the orientation
        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr localRobot;

        // visualization
        bool showSegmentedResultImages;
        CColorParameterSet* colorParameterSet;
        int imageCounter;


        CStereoMatcher* stereoMatcher;
    protected:
        armarx::DebugDrawerInterfacePrx debugDrawerPrx;

    };
}

