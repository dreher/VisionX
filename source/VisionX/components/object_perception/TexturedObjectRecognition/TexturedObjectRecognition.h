/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// VisionX
#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>

// forward declarations
class CByteImage;


#include <TexturedRecognition/TexturedRecognition.h>
#include <TexturedRecognition/TexturedObjectDatabase.h>


#include <ArmarXCore/observers/DebugObserver.h>


#include <Eigen/Core>

namespace visionx
{


    static Eigen::Vector3f stringToVector3f(std::string propertyValue)
    {
        Eigen::Vector3f vec;
        sscanf(propertyValue.c_str(), "%f, %f, %f", &vec.data()[0], &vec.data()[1], &vec.data()[2]);
        return vec;
    }
    class TexturedObjectRecognitionPropertyDefinitions:
        public ObjectLocalizerProcessorPropertyDefinitions
    {
    public:
        TexturedObjectRecognitionPropertyDefinitions(std::string prefix):
            ObjectLocalizerProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("SIFTThreshold", 0.01, "A threshold for detecting SIFT descriptors in the image. Smaller value -> more features found");

            defineOptionalProperty<float>("QualityThreshold", 0.0001, "Set quality threshold for Harris interest point calculation. Smaller means more features. See CTexturedRecognition");
            defineOptionalProperty<int>("nMinValidFeatures", 10, "minimum number of features in Houghspace. See CTexturedRecognition");
            defineOptionalProperty<float>("MaxError", 3.3f, "maximum error after application of homography on valid features. See CTexturedRecognition");

            armarx::PropertyDefinition<Eigen::Vector3f>::PropertyFactoryFunction f = &stringToVector3f;

            defineOptionalProperty<Eigen::Vector3f>("MinPoint", Eigen::Vector3f(-3000.0f, -3000.0f, 100.0f), "min point for valid result bounding box").setFactory(f);
            defineOptionalProperty<Eigen::Vector3f>("MaxPoint", Eigen::Vector3f(3000.0f, 3000.0f, 3500.0f), "max point for valid result bounding box").setFactory(f);

            defineOptionalProperty<int>("StereoCorrelationWindowSize", 19, "Correlation size for stereo matching");
            defineOptionalProperty<float>("StereoCorrelationThreshold", 0.7f, "The threshold for the Zero Mean-Normalized Cross Correlation (ZNCC, range: 0.0 - 1.0).");
        }
    };


    /**
     * TexturedObjectRecognition uses CTexturedRecognition of IVTRecognition in order to recognize and localize objects.
     * The object data is read from PriorKnowledge and CommonStorage via MemoryX.
     * The object localization is invoked automatically by the working memory if the object has been requested there.
     */
    class TexturedObjectRecognition:
        virtual public ObjectLocalizerProcessor
    {
    public:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new TexturedObjectRecognitionPropertyDefinitions(getConfigIdentifier()));
        }

        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "TexturedObjectRecognition";
        }

        void reportStereoCalibrationChanged(const StereoCalibration& stereoCalibration, bool x, const std::string& referenceFrame, const Ice::Current& c = Ice::Current()) override
        {
            ObjectLocalizerProcessor::reportStereoCalibrationChanged(stereoCalibration, x, referenceFrame, c);

            texturedRecognition->GetObjectDatabase()->InitCameraParameters(getStereoCalibration(), true);
            texturedRecognition->GetObjectDatabase()->SetCorrelationParameters(19, 400, 3500, 0.7f);
        }

    protected:
        /**
         * @see ObjectLocalizerProcessor::onInitObjectLocalizerProcessor()
         */
        void onInitObjectLocalizerProcessor() override
        {

            offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
        }

        /**
         * Initializes the CTexturedRecognition
         *
         * @see ObjectLocalizerProcessor::onConnectObjectLocalizerProcessor()
         */
        void onConnectObjectLocalizerProcessor() override
        {
            debugObserver = getTopic<armarx::DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
        }

        /**
         * @see ObjectLocalizerProcessor::onExitObjectLocalizerProcessor()
         */
        void onExitObjectLocalizerProcessor() override {}

        /**
         * Initializes textured recognition
         *
         * @return success
         */
        bool initRecognizer() override;

        /**
         * Add object class to textured object recognition.
         *
         * @param objectClassEntity entity containing all information available for the object class
         * @param fileManager GridFileManager required to read files associated to prior knowledge from the database.
         *
         * @return success of adding this entity to the TexturedObjectDatabase
         */
        bool addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager) override;

        /**
         * localizes textured object instances
         *
         * @param objectClassNames names of the class to localize
         * @param cameraImages the two input images
         * @param resultImages the two result images. are provided if result images are enabled.
         *
         * @return list of object instances
         */
        virtual memoryx::ObjectLocalizationResultList localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages);


    private:
        // calculates recognition certainty
        float calculateRecognitionCertainty(const std::string& objectClassName, const Object3DEntry& entry);
        Vec3d validResultBoundingBoxMin, validResultBoundingBoxMax;

        // textured recognition
        CTexturedRecognition* texturedRecognition = nullptr;
        int correlationWindowSize = 0;
        float correlationThreshold = 0.0f;

        int seq;


        armarx::DebugObserverInterfacePrx debugObserver;


    };
}

