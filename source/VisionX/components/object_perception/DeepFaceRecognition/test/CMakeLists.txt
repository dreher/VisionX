
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore DeepFaceRecognition)
 
armarx_add_test(DeepFaceRecognitionTest DeepFaceRecognitionTest.cpp "${LIBS}")