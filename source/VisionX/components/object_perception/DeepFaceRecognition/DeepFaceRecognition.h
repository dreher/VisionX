/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::DeepFaceRecognition
 * @author     Mirko Waechter (waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <MemoryX/interface/components/LongtermMemoryInterface.h>
#include <RobotAPI/interface/speech/SpeechInterface.h>
#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>
#include <VisionX/interface/components/FaceRecognitionInterface.h>
#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>
#include <VisionX/tools/TypeMapping.h>

//#include <Python.h>


namespace visionx
{
    /**
     * @class DeepFaceRecognitionPropertyDefinitions
     * @brief
     */
    class DeepFaceRecognitionPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DeepFaceRecognitionPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("AgentName", "Armar3", "Name of the robot that does the localization");
            defineOptionalProperty<std::string>("CameraFrameName", "EyeLeftCamera", "Name of the camera frame of the robot that does the localization");
            defineOptionalProperty<float>("GreetAgainDelay",  60 * 60 * 12, "Time in seconds between that a person is not greeted again");
            //            defineOptionalProperty<int>("FaceMatchCounter",  3, "How many times a face must be seen until a greeting is send to the TTS");
            defineOptionalProperty<float>("GreetThreshold",  3.0, "The sum of all gaussians of the recognized faces over time.", armarx::PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<std::string>("ImageProviderName",  "OpenNIPointCloudProvider", "Name of the Imageprovider to be used");

        }
    };

    /**
     * @defgroup Component-DeepFaceRecognition DeepFaceRecognition
     * @ingroup VisionX-Components
     * A description of the component DeepFaceRecognition.
     *
     * @class DeepFaceRecognition
     * @ingroup Component-DeepFaceRecognition
     * @brief Brief description of class DeepFaceRecognition.
     *
     * Detailed description of class DeepFaceRecognition.
     */
    class DeepFaceRecognition :
        public armarx::Component,
        virtual public memoryx::ObjectLocalizerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "DeepFaceRecognition";
        }

    protected:
        memoryx::ObjectLocalizationResultList localizeAndIdentifyFaces(CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages);
        //        /**
        //         * @see ObjectLocalizerProcessor::onInitObjectLocalizerProcessor()
        //         */
        //        void onInitObjectLocalizerProcessor();

        //        /**
        //         * @see ObjectLocalizerProcessor::onConnectObjectLocalizerProcessor()
        //         */
        //        void onConnectObjectLocalizerProcessor();

        //        /**
        //         * @see ObjectLocalizerProcessor::onExitObjectLocalizerProcessor()
        //         */
        //        void onExitObjectLocalizerProcessor();

        //        /**
        //         * Add a memory entity representing the hand marker in order to set its properties
        //         *
        //         * @param objectClassEntity entity containing all information available for the object class
        //         * @param fileManager GridFileManager required to read files associated to prior knowledge from the database.
        //         *
        //         * @return success of adding this entity
        //         */
        //        memoryx::ObjectLocalizationResultList localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages) override;

        //        /**
        //         * Initializes segmentable recognition
        //         *
        //         * @return success
        //         */
        //        bool initRecognizer();

        //        /**
        //         * Add a memory entity representing the hand marker in order to set its properties
        //         *
        //         * @param objectClassEntity entity containing all information available for the object class
        //         * @param fileManager GridFileManager required to read files associated to prior knowledge from the database.
        //         *
        //         * @return success of adding this entity
        //         */
        //        bool addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager);

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
        typedef std::map<std::string, std::list<std::pair<IceUtil::Time, double> > > FaceConfidenceHistory;
    private:
        visionx::FaceRecognitionInterfacePrx faceReg;
        memoryx::LongtermMemoryInterfacePrx ltm;
        ::memoryx::EntityMemorySegmentInterfacePrx faceSegmentPrx;
        armarx::TextListenerInterfacePrx tts;
        visionx::CapturingPointCloudAndImageAndCalibrationProviderInterfacePrx imageProvider;
        const std::string faceSegmentName = "faceMemory";
        std::shared_ptr<CCalibration> calibration;
        FaceConfidenceHistory faceConfidenceHistory;
        //        PyObject* pModule, *py_main;

        float updateAndCheckFaceExistenceProbability(const visionx::FaceLocation& faceLocation);
        // ObjectLocalizerInterface interface
    public:
        memoryx::ObjectLocalizationResultList localizeObjectClasses(const memoryx::ObjectClassNameList& classes, const Ice::Current&) override;

        // ManagedIceObject interface
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;

    };
}

