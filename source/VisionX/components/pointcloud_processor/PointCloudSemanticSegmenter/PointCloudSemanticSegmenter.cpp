/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudSemanticSegmenter.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <Image/ImageProcessor.h>

#ifdef USE_DARKNET
#include "DarkNet.h"
#endif

using namespace visionx;
using namespace armarx;

PointCloudSemanticSegmenter::PointCloudSemanticSegmenter() :
    inputRawPCPtr(new pcl::PointCloud<PointO>),
    inputSegmentedPCPtr(new pcl::PointCloud<PointRGBL>),
    semanticSegmentCloudPtr(new pcl::PointCloud<PointRGBL>),
    unorganizedSemanticCloudPtr(new pcl::PointCloud<PointRGBL>),
    visualizeSourcePointCloud(false),
    visualizeSegmentedPointCloud(false),
    visualizationIteration(0),
    currentRawPointCloud(new pcl::PointCloud<PointO>),
    currentSegmentedPointCloud(new pcl::PointCloud<PointRGBL>)
{
}

void PointCloudSemanticSegmenter::onInitPointCloudProcessor()
{
    lastProcessedTimestamp = 0;

    // Get point cloud provider name
    rawPCProviderName = getProperty<std::string>("rawPointCloudProviderName").getValue();
    ARMARX_INFO << "Using point cloud provider " << rawPCProviderName << flush;

    usingPointCloudProvider(rawPCProviderName);

    mergeWithSegmentationResults = getProperty<bool>("mergeWithSegmentationResults").getValue();

    if (mergeWithSegmentationResults)
    {
        // Get point cloud segmenter name
        segmentedPCProviderName = getProperty<std::string>("segmentedPointCloudProviderName").getValue();
        ARMARX_INFO << "Using point cloud segmenter " << segmentedPCProviderName << flush;
        usingPointCloudProvider(segmentedPCProviderName);
    }

    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());

    enabled = getProperty<bool>("StartEnabled").getValue();
    ARMARX_INFO << "Starting " << (enabled ? "Enabled" : "Disabled");


    // Get input/ouput parameters
    minSegSize = getProperty<int>("minimumSegmentSize").getValue();
    maxSegSize = getProperty<int>("maximumSegmentSize").getValue();
    deltaROI = getProperty<int>("deltaROI").getValue();
    returnUnorganizedCloud = getProperty<bool>("returnUnorganizedCloud").getValue();

    std::string tempString = getProperty<std::string>("DesiredObjects").getValue();
    if (tempString != "")
    {
        boost::split(desiredObjecs, tempString, boost::is_any_of(","));
    }

    // Get the list of all known object names
    knownObjectNamesFile = getProperty<std::string>("knownObjectNamesFile").getValue();
    if (!ArmarXDataPath::getAbsolutePath(knownObjectNamesFile, knownObjectNamesFile))
    {
        ARMARX_ERROR << "Could not find known object name file in ArmarXDataPath: " << knownObjectNamesFile;
    }
    ARMARX_INFO << "knownObjectNamesFile config path: " << knownObjectNamesFile << flush;
    parseKnownObjectList();

    // Get DarkNet path
    darkNetConfigPath = getProperty<std::string>("darkNetConfigPath").getValue();
    if (!ArmarXDataPath::getAbsolutePath(darkNetConfigPath, darkNetConfigPath))
    {
        ARMARX_ERROR << "Could not find dark net config file in ArmarXDataPath: " << darkNetConfigPath;
    }
    ARMARX_INFO << "darkNet config path: " << darkNetConfigPath << flush;

    // create a color vector
    maxObjectSize = 1000;
    createColorVector(maxObjectSize);

    // create data storage for darknet outputs
    objectNumber = 0;
    int initSize = 1000;
    objectNames = (char**) malloc(initSize * sizeof(char*));
    objectBB = (int**) malloc(initSize * sizeof(int*));

    if (objectNames && objectBB)
    {
        for (int val = 0; val < initSize; val++)
        {
            objectNames[val] = (char*) malloc(sizeof * objectNames[val] * 100);
            objectBB[val] = (int*) malloc(sizeof * objectBB[val] * 4);
        }
    }

}

void PointCloudSemanticSegmenter::onConnectPointCloudProcessor()
{
    debugDrawerTopic = getTopic<armarx::DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());
    if (!debugDrawerTopic)
    {
        ARMARX_WARNING << "Failed to obtain debug drawer proxy";
    }

    enableResultPointClouds("SemanticSegmentationResult", 999999999, eColoredLabeledPoints);
}

void PointCloudSemanticSegmenter::onDisconnectPointCloudProcessor()
{
    if (objectNames)
    {
        delete objectNames;
    }

    if (objectBB)
    {
        delete objectBB;
    }
}

void PointCloudSemanticSegmenter::onExitPointCloudProcessor()
{

}

void PointCloudSemanticSegmenter::process()
{
    {
        ScopedLock lock(enableMutex);

        if (!enabled)
        {
            return;
        }
    }

    if (!waitForPointClouds(rawPCProviderName, 10000))
    {
        ARMARX_WARNING << "Timeout or error while waiting for point cloud data from " << rawPCProviderName << armarx::flush;
        return;
    }
    else
    {
        getPointClouds<PointO>(rawPCProviderName, inputRawPCPtr);
        ARMARX_INFO << "Raw point cloud received (size: " << inputRawPCPtr->points.size() << ", providerName: " << rawPCProviderName << ")";
    }

    if (mergeWithSegmentationResults)
    {
        if (!waitForPointClouds(segmentedPCProviderName, 10000))
        {
            ARMARX_WARNING << "Timeout or error while waiting for segmented point cloud data from " << segmentedPCProviderName << armarx::flush;
            return;
        }
        else
        {
            getPointClouds<PointRGBL>(segmentedPCProviderName, inputSegmentedPCPtr);
            ARMARX_INFO << "Segmented point cloud received (size: " << inputSegmentedPCPtr->points.size() << ", providerName: " << segmentedPCProviderName << ")";
        }
    }


    {
        ScopedLock lock(pointCloudStorageMutex);
        pcl::copyPointCloud(*inputRawPCPtr, *currentRawPointCloud);
        if (mergeWithSegmentationResults)
        {
            pcl::copyPointCloud(*inputSegmentedPCPtr, *currentSegmentedPointCloud);
        }
        else
        {
            pcl::copyPointCloud(*inputRawPCPtr, *currentSegmentedPointCloud);
        }
    }

    MetaPointCloudFormatPtr info = getPointCloudFormat(rawPCProviderName);
    IceUtil::Int64 originalTimestamp = info->timeProvided;

    IceUtil::Time ts = IceUtil::Time::microSeconds(originalTimestamp);
    std::string timestampString = ts.toDateTime().substr(ts.toDateTime().find(' ') + 1);

    ARMARX_INFO << "Point cloud received (timestamp: " << timestampString << ", size: " << inputRawPCPtr->points.size() << ", providerName: " << rawPCProviderName << ")";

    if (inputRawPCPtr->points.size() == 0)
    {
        return;
    }

    if (mergeWithSegmentationResults && inputSegmentedPCPtr->points.size() == 0)
    {
        return;
    }


    if (!inputRawPCPtr->isOrganized())
    {
        ARMARX_INFO << " Point Cloud from " <<  rawPCProviderName << " is organized : " << inputRawPCPtr->isOrganized();
        return;
    }


    if (mergeWithSegmentationResults && !inputSegmentedPCPtr->isOrganized())
    {
        ARMARX_INFO << " Point Cloud from " << segmentedPCProviderName  << " is organized : " << inputSegmentedPCPtr->isOrganized();
        return;
    }


    // add a timestamp
    pcl::copyPointCloud(*currentSegmentedPointCloud, *semanticSegmentCloudPtr);
    semanticSegmentCloudPtr->header.stamp = originalTimestamp;

    // first detect objects
    darkNetObjectDetector();

    if (mergeWithSegmentationResults)
    {
        // now merge objects with segmented cloud
        computeSemanticSegmentCloud();
    }
    else
    {
        computeRawSemanticSegmentCloud();
    }

    if (returnUnorganizedCloud)
    {
        pcl::copyPointCloud(*semanticSegmentCloudPtr, objectPointIndices, *unorganizedSemanticCloudPtr);
        pcl::copyPointCloud(*unorganizedSemanticCloudPtr, *semanticSegmentCloudPtr);
    }
    ARMARX_IMPORTANT << "deltaROI " << semanticSegmentCloudPtr->points.size();


    {
        ScopedLock lock(enableMutex);

        // Do not provide result if segmenter has been switched off in the meantime
        if (enabled)
        {
            provideResultPointClouds<PointRGBL>(semanticSegmentCloudPtr);

            {
                ScopedLock lock(pointCloudStorageMutex);
            }

            {
                ScopedLock lock(timestampMutex);
                lastProcessedTimestamp = originalTimestamp;
            }
        }
    }

    updateVisualization();

    if (getProperty<bool>("SingleRun").getValue())
    {
        ARMARX_WARNING << "PointCloudSemanticSegmenter configured to run only once";
        enabled = false;
    }
}

PropertyDefinitionsPtr PointCloudSemanticSegmenter::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new PointCloudSemanticSegmenterPropertyDefinitions(getConfigIdentifier()));
}

void PointCloudSemanticSegmenter::enablePipelineStep(const Ice::Current& c)
{
    ScopedLock lock(enableMutex);
    enabled = true;

    ARMARX_INFO << "Enabling segmentation";
}

void PointCloudSemanticSegmenter::disablePipelineStep(const Ice::Current& c)
{
    ScopedLock lock(enableMutex);
    enabled = false;

    ARMARX_INFO << "Disabling segmentation";
}

void PointCloudSemanticSegmenter::enablePointCloudVisualization(bool enableSource, bool enableSegmented, const Ice::Current& c)
{
    visualizeSourcePointCloud = enableSource;
    visualizeSegmentedPointCloud = enableSegmented;

    updateVisualization();
}

void PointCloudSemanticSegmenter::changeRawPointCloudProvider(const std::string& providerName, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(providerMutex);

    ARMARX_INFO << "Changing point cloud provider to '" << providerName << "'";

    onDisconnectComponent();

    releasePointCloudProvider(this->rawPCProviderName);
    this->rawPCProviderName = providerName;
    usingPointCloudProvider(this->rawPCProviderName);

    onConnectComponent();
}

void PointCloudSemanticSegmenter::convertFromXYZRGBAtoXYZL(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& sourceCloudPtr, pcl::PointCloud<pcl::PointXYZL>::Ptr& targetCloudPtr)
{
    targetCloudPtr->resize(sourceCloudPtr->points.size());
    uint32_t newLabel = 1;

    struct rgbValues
    {
        int rVal;
        int gVal;
        int bVal;
    };
    std::map<uint32_t, rgbValues > colorMap;

    // scan the source cloud
    for (size_t i = 0; i < sourceCloudPtr->points.size(); i++)
    {
        rgbValues currRGB;
        currRGB.rVal = (int) sourceCloudPtr->points[i].r;
        currRGB.gVal = (int) sourceCloudPtr->points[i].g;
        currRGB.bVal = (int) sourceCloudPtr->points[i].b;

        bool isFound = false;
        uint32_t foundLabel = 0;

        // search in the map
        std::map<uint32_t, rgbValues >::iterator iter_i;

        for (iter_i = colorMap.begin(); iter_i != colorMap.end(); iter_i++)
        {
            if (currRGB.rVal == iter_i->second.rVal && currRGB.gVal == iter_i->second.gVal && currRGB.bVal == iter_i->second.bVal)
            {
                foundLabel = iter_i->first;
                isFound = true;
                break;
            }
        }

        if (!isFound)
        {
            colorMap[newLabel] = currRGB;
            foundLabel = newLabel;
            newLabel++;
        }

        targetCloudPtr->points[i].x = sourceCloudPtr->points[i].x;
        targetCloudPtr->points[i].y = sourceCloudPtr->points[i].y;
        targetCloudPtr->points[i].z = sourceCloudPtr->points[i].z;
        targetCloudPtr->points[i].label = foundLabel;
    }
}

void PointCloudSemanticSegmenter::updateVisualization()
{
    if (!debugDrawerTopic)
    {
        return;
    }

    {
        ScopedLock lock(pointCloudStorageMutex);

        std::string oldLayerName = "PointCloudSemanticSegmenter_" + boost::lexical_cast<std::string>((visualizationIteration + 1) % 2);
        std::string currentLayerName = "PointCloudSemanticSegmenter_" + boost::lexical_cast<std::string>(visualizationIteration % 2);

        if (visualizeSourcePointCloud)
        {
            visualizePointCloud(currentRawPointCloud, currentLayerName, "SourcePointCloud_" + boost::lexical_cast<std::string>(visualizationIteration % 2));
        }

        if (visualizeSegmentedPointCloud)
        {
            visualizeLabeledPointCloud(semanticSegmentCloudPtr, currentLayerName, "SegmentedPointCloud_" + boost::lexical_cast<std::string>(visualizationIteration % 2));
        }

        // Wait for 100ms to give the DebugDrawer enough time to process the new commands
        // TODO: There should be a smarter way to avoid flickering
        usleep(100000);

        debugDrawerTopic->clearLayer(oldLayerName);

        visualizationIteration++;
    }
}

void PointCloudSemanticSegmenter::visualizePointCloud(const PointOPtr& pointCloud, const std::string& layer, const std::string& name)
{
    DebugDrawer24BitColoredPointCloud dpc;
    dpc.pointSize = 3;

    int numPoints = pointCloud->width * pointCloud->height;
    dpc.points.reserve(numPoints);

    DebugDrawer24BitColoredPointCloudElement e;

    for (const PointO& p : pointCloud->points)
    {
        if (pcl_isfinite(p.x) && pcl_isfinite(p.y) && pcl_isfinite(p.z))
        {
            e.x = p.x;
            e.y = p.y;
            e.z = p.z;

            e.color.r = p.r;
            e.color.g = p.g;
            e.color.b = p.b;

            dpc.points.push_back(e);
        }
    }

    debugDrawerTopic->set24BitColoredPointCloudVisu(layer, name, dpc);
}

void PointCloudSemanticSegmenter::visualizeLabeledPointCloud(const PointRGBLPtr& pointCloud, const std::string& layer, const std::string& name)
{
    DebugDrawer24BitColoredPointCloud dpc;
    dpc.pointSize = 3;

    int numPoints = pointCloud->width * pointCloud->height;
    dpc.points.reserve(numPoints);


    DebugDrawer24BitColoredPointCloudElement e;
    for (const PointRGBL& p : pointCloud->points)
    {
        if (pcl_isfinite(p.x) && pcl_isfinite(p.y) && pcl_isfinite(p.z))
        {
            e.x = p.x;
            e.y = p.y;
            e.z = p.z;

            pcl::RGB rgb = pcl::GlasbeyLUT::at(p.label);
            e.color.r = rgb.r;
            e.color.g = rgb.g;
            e.color.b = rgb.b;

            dpc.points.push_back(e);
        }
    }


    debugDrawerTopic->set24BitColoredPointCloudVisu(layer, name, dpc);
}

bool visionx::PointCloudSemanticSegmenter::isPipelineStepEnabled(const Ice::Current& c)
{
    ScopedLock lock(enableMutex);
    return enabled;
}

armarx::TimestampBasePtr visionx::PointCloudSemanticSegmenter::getLastProcessedTimestamp(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(timestampMutex);
    return new TimestampVariant(lastProcessedTimestamp);
}

void visionx::PointCloudSemanticSegmenter::createColorVector(int VectorSize)
{
    int currSize = colorVector.size();
    // creating for the first time
    if (currSize == 0)
    {
        for (int i = 0; i < VectorSize; i++)
        {
            rgbValues currRGB;
            currRGB.rVal = rand() % 255;
            currRGB.gVal = rand() % 255;
            currRGB.bVal = rand() % 255;

            colorVector.push_back(currRGB);
        }
    }
    else
    {
        // updating the vector
        for (int i = currSize; i < VectorSize; i++)
        {
            rgbValues currRGB;
            currRGB.rVal = rand() % 255;
            currRGB.gVal = rand() % 255;
            currRGB.bVal = rand() % 255;

            colorVector.push_back(currRGB);
        }
    }
}

void PointCloudSemanticSegmenter::parseKnownObjectList()
{
    knownObjectList.clear();

    std::string currName;
    std::ifstream infile;
    infile.open(knownObjectNamesFile);
    while (getline(infile, currName))
    {
        knownObjectList.push_back(currName);
    }

    infile.close();
    ARMARX_INFO << " knownObjectList size: "  << knownObjectList.size();

}

void PointCloudSemanticSegmenter::darkNetObjectDetector()
{
#ifndef USE_DARKNET
    return;
#endif

    ARMARX_INFO << " DarkNet received image with width: " << currentRawPointCloud->width << " height: " << currentRawPointCloud->height ;

    IplImage* iplImage;
    iplImage = cvCreateImage(cvSize(currentRawPointCloud->width, currentRawPointCloud->height), 8, 3);

    int h = 0, w = 0;
    // convert from point cloud to iplImage
    for (size_t i = 0; i < currentRawPointCloud->points.size(); i++, h++)
    {
        if (h == iplImage->width)
        {
            w++;
            h = 0;
        }

        iplImage->imageData[w * iplImage->widthStep + h * iplImage->nChannels + 0] =  currentRawPointCloud->points[i].r;
        iplImage->imageData[w * iplImage->widthStep + h * iplImage->nChannels + 1] =  currentRawPointCloud->points[i].g;
        iplImage->imageData[w * iplImage->widthStep + h * iplImage->nChannels + 2] =  currentRawPointCloud->points[i].b;
    }


#ifdef USE_DARKNET
    char* baseFolderPath = &darkNetConfigPath[0];
    processIplImage(iplImage, objectNames, objectBB, &objectNumber, baseFolderPath);
#endif

    // just update the color vector
    if (objectNumber > maxObjectSize)
    {
        createColorVector(objectNumber);
    }

    if (true)
    {
        ARMARX_INFO << " DarkNet detected " <<  objectNumber << " objects!...";
        for (int objectIndex = 0; objectIndex < objectNumber; ++objectIndex)
            ARMARX_INFO << objectNames[objectIndex] << " ( " << objectBB[objectIndex][0] << " , " << objectBB[objectIndex][1]
                        << " , " << objectBB[objectIndex][2] << " , " << objectBB[objectIndex][3] << " ) " ;
    }
}

void PointCloudSemanticSegmenter::computeSemanticSegmentCloud()
{
    // get a list of raw segments
    getLabelMap();

    // find the match between raw segments and object labels
    assignObjectLabelsToSegments();

}

void PointCloudSemanticSegmenter::computeRawSemanticSegmentCloud()
{
    objectPointIndices.clear();

    for (size_t currPoint = 0; currPoint < currentSegmentedPointCloud->points.size(); currPoint++)
    {
        currentSegmentedPointCloud->points[currPoint].label = 0;

        std::div_t posXY = std::div((int) currPoint, currentSegmentedPointCloud->width);
        int posX = posXY.rem;
        int posY = posXY.quot;
        std::string bestObjName = getMatchedObjectName(posX, posY);


        auto objExists = std::find(knownObjectList.begin(), knownObjectList.end(), bestObjName);
        if (objExists == knownObjectList.end())
        {
            semanticSegmentCloudPtr->points[currPoint].label = 0;
            if (bestObjName != "")
            {
                ARMARX_ERROR << "ERROR: An Unknown Object detected: not existing in the knownObjectList " << bestObjName;
            }

        }
        else
        {
            size_t objPosition = std::distance(knownObjectList.begin(), objExists);

            // check if the user requires a set of desired objects
            if (desiredObjecs.size() == 0)
            {
                semanticSegmentCloudPtr->points[currPoint].label = objPosition + 1;
                objectPointIndices.push_back(currPoint);
            }
            else
            {
                auto isDesiredObj = std::find(desiredObjecs.begin(), desiredObjecs.end(), bestObjName);
                if (isDesiredObj == desiredObjecs.end())
                {
                    semanticSegmentCloudPtr->points[currPoint].label = 0;
                }
                else
                {
                    semanticSegmentCloudPtr->points[currPoint].label = objPosition + 1;
                    objectPointIndices.push_back(currPoint);
                }
            }
        }
    }

}

void PointCloudSemanticSegmenter::getLabelMap()
{
    labelMap.clear();
    std::map<uint32_t, pcl::PointIndices >::iterator iter_i;

    for (size_t i = 0; i < currentSegmentedPointCloud->points.size(); i++)
    {
        uint32_t currLabel = currentSegmentedPointCloud->points[i].label;

        if (currLabel)
        {
            iter_i = labelMap.find(currLabel);

            if (iter_i == labelMap.end())
            {
                pcl::PointIndices label_indices;
                label_indices.indices.push_back(i);
                labelMap[currLabel] = label_indices;
            }
            else
            {
                iter_i->second.indices.push_back(i);
            }
        }
    }

    ARMARX_INFO << " Detected segment number " << labelMap.size() ;
}

void PointCloudSemanticSegmenter::assignObjectLabelsToSegments()
{
    objectPointIndices.clear();
    // for each segment in the segmented cloud, find the best object label

    for (auto segment = labelMap.begin(); segment != labelMap.end(); segment++)
    {

        size_t segmentSize = segment->second.indices.size();

        if (segmentSize > minSegSize  && segmentSize < maxSegSize)
        {
            std::map<std::string, int> objNameMap;
            std::map<std::string, int >::iterator iter_i;

            // get the object names matching with the current segment
            for (size_t index = 0; index < segmentSize; index++)
            {
                int currPoint = segment->second.indices[index];
                std::div_t posXY = std::div(currPoint, currentSegmentedPointCloud->width);
                int posX = posXY.rem;
                int posY = posXY.quot;
                std::string currObjName = getMatchedObjectName(posX, posY);

                iter_i = objNameMap.find(currObjName);

                if (iter_i == objNameMap.end())
                {
                    objNameMap[currObjName] = 1;
                }
                else
                {
                    iter_i->second++ ;
                }
            }

            // get the best objectName
            std::string bestObjName = "";
            int bestScore = 0;
            for (iter_i = objNameMap.begin(); iter_i != objNameMap.end(); iter_i++)
            {
                if (iter_i->second >= bestScore && iter_i->first != "")
                {
                    bestObjName = iter_i->first;
                    bestScore = iter_i->second;
                }
            }

            // apply a threshold
            if (bestScore < segmentSize * 0.3)
            {
                bestObjName = "";
            }

            // assign the best matched object label to those segment indices
            for (size_t index = 0; index < segmentSize; index++)
            {
                int currPoint = segment->second.indices[index];

                auto objExists = std::find(knownObjectList.begin(), knownObjectList.end(), bestObjName);
                if (objExists == knownObjectList.end())
                {
                    semanticSegmentCloudPtr->points[currPoint].label = 0;
                    if (bestObjName != "")
                    {
                        ARMARX_ERROR << "ERROR: An Unknown Object detected: not existing in the knownObjectList " << bestObjName;
                    }

                }
                else
                {
                    size_t objPosition = std::distance(knownObjectList.begin(), objExists);

                    // check if the user requires a set of desired objects
                    if (desiredObjecs.size() == 0)
                    {
                        semanticSegmentCloudPtr->points[currPoint].label = objPosition + 1;
                        objectPointIndices.push_back(currPoint);
                    }
                    else
                    {
                        auto isDesiredObj = std::find(desiredObjecs.begin(), desiredObjecs.end(), bestObjName);
                        if (isDesiredObj == desiredObjecs.end())
                        {
                            semanticSegmentCloudPtr->points[currPoint].label = 0;
                        }
                        else
                        {
                            semanticSegmentCloudPtr->points[currPoint].label = objPosition + 1;
                            objectPointIndices.push_back(currPoint);
                        }
                    }
                }
            }

        }
        else
        {
            // assign zero
            for (size_t index = 0; index < segmentSize; index++)
            {
                int currPoint = segment->second.indices[index];
                semanticSegmentCloudPtr->points[currPoint].label = 0;
            }
        }
    }

}


std::string PointCloudSemanticSegmenter::getMatchedObjectName(int posX, int posY)
{
    std::string matchedObjName = ""; // for unknown objects

    // go through all detected objects and find the matched obj ID
    for (int objectIndex = 0; objectIndex < objectNumber; ++objectIndex)
    {

        int limitROIX = (objectBB[objectIndex][1] - objectBB[objectIndex][0]) / 2;
        int limitROIY = (objectBB[objectIndex][3] - objectBB[objectIndex][2]) / 2;
        if (limitROIX < 0 || deltaROI >= static_cast<uint32_t>(limitROIX) ||
            limitROIY < 0 || deltaROI >= static_cast<uint32_t>(limitROIY))
        {
            deltaROI = std::min(limitROIX, limitROIY);
        }

        int startX = objectBB[objectIndex][0] + deltaROI;
        int endX =  objectBB[objectIndex][1] - deltaROI;
        int startY = objectBB[objectIndex][2] + deltaROI;
        int endY =  objectBB[objectIndex][3] - deltaROI;
        std::string objName(objectNames[objectIndex]);

        if (posX >= startX && posX <= endX && posY >= startY && posY <= endY)
        {
            matchedObjName = objName;
            break;
        }
    }

    return matchedObjName;
}
