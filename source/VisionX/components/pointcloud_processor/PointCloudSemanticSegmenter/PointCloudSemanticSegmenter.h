/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <VisionX/interface/components/DeepSegmenterInterface.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/interface/components/PointCloudSemanticSegmenter.h>

#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/search/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/correspondence.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#include <pcl/common/colors.h>
#include <pcl/filters/crop_box.h>

// boost
#include <boost/thread.hpp>
#include <boost/circular_buffer.hpp>

#include <Image/ByteImage.h>

#include <opencv2/opencv.hpp>
#include <mutex>

namespace visionx
{
    typedef pcl::PointXYZRGBA PointO;
    typedef pcl::PointCloud<PointO>::Ptr PointOPtr;
    typedef pcl::PointXYZL PointL;
    typedef pcl::PointCloud<PointL>::Ptr PointLPtr;
    typedef pcl::PointXYZRGBL PointRGBL;
    typedef pcl::PointCloud<PointRGBL>::Ptr PointRGBLPtr;

    /**
     * @class PointCloudSemanticSegmenterPropertyDefinitions
     * @brief
    */
    class PointCloudSemanticSegmenterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PointCloudSemanticSegmenterPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            // General parameters
            defineOptionalProperty<std::string>("rawPointCloudProviderName", "OpenNIPointCloudProvider", "Name of the point cloud provider");
            defineOptionalProperty<std::string>("segmentedPointCloudProviderName", "PointCloudSegmenterResult", "Name of the point cloud segmenter");
            defineOptionalProperty<bool>("mergeWithSegmentationResults", false, "DarkNet result should be combined with LCCP like segmentation output");
            defineOptionalProperty<std::string>("darkNetConfigPath", "VisionX/darknet", "Path to the folder that contains darknet weights");
            defineOptionalProperty<std::string>("knownObjectNamesFile", "VisionX/darknet/data/coco.names", "Path to the file that contains all possible object names");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic");
            defineOptionalProperty<bool>("StartEnabled", true, "Enable or disable the segmentation by default");
            defineOptionalProperty<bool>("SingleRun", false, "Segment only one scene after each enable call");

            // input/output params
            defineOptionalProperty<int>("minimumSegmentSize",  500, "Minimum segment size required for the semantic segmentation");
            defineOptionalProperty<int>("maximumSegmentSize",  100000, "Minimum segment size required for the semantic segmentation");
            defineOptionalProperty<bool>("returnUnorganizedCloud", false, "Return only detected object clouds");
            defineOptionalProperty<std::string>("DesiredObjects", "", "Only evaluate objects from this list (use short names in comma separated list)");
            defineOptionalProperty<int>("deltaROI", 0, "To decrease the bounding box size coming from the object detector");

            // Debug parameters
            defineOptionalProperty<bool>("VisualizeCropBox", false, "Visualize crop box via debug drawer");
        }
    };

    /**
    * @class PointCloudSemanticSegmenter
    *
    * @ingroup VisionX-Components
    * @brief A brief description
    *
    *
    * Detailed Description
    */
    class PointCloudSemanticSegmenter :
        virtual public PointCloudSemanticSegmenterInterface,
        virtual public PointCloudProcessor
    {
    public:
        PointCloudSemanticSegmenter();

        /**
        * @see armarx::ManagedIceObject::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "PointCloudSemanticSegmenter";
        }

        void enablePointCloudVisualization(bool enableSource, bool enableSegmented, const Ice::Current& c = ::Ice::Current()) override;
        void changeRawPointCloudProvider(const std::string& rawPCProviderName, const Ice::Current& c = ::Ice::Current()) override;

    protected:
        /**
        * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        */
        void onInitPointCloudProcessor() override;

        /**
        * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        */
        void onConnectPointCloudProcessor() override;

        /**
        * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        */
        void onDisconnectPointCloudProcessor() override;

        /**
        * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        */
        void onExitPointCloudProcessor() override;

        /**
        * @see visionx::PointCloudProcessor::process()
        */
        void process() override;

        /**
        * @see PropertyUser::createPropertyDefinitions()
        */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void enablePipelineStep(const Ice::Current& c = ::Ice::Current()) override;
        void disablePipelineStep(const Ice::Current& c = ::Ice::Current()) override;
        bool isPipelineStepEnabled(const Ice::Current& c = ::Ice::Current()) override;
        armarx::TimestampBasePtr getLastProcessedTimestamp(const Ice::Current& c = ::Ice::Current()) override;

    private:
        void convertFromXYZRGBAtoXYZL(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& sourceCloudPtr, pcl::PointCloud<pcl::PointXYZL>::Ptr& targetCloudPtr);
        void pushColoredLabeledCloudtoBuffer();
        void updateVisualization();
        void visualizePointCloud(const PointOPtr& pointCloud, const std::string& layer, const std::string& name);
        void visualizeLabeledPointCloud(const PointRGBLPtr& pointCloud, const std::string& layer, const std::string& name);



    private:

        PointOPtr inputRawPCPtr;
        PointRGBLPtr inputSegmentedPCPtr;
        PointRGBLPtr semanticSegmentCloudPtr;
        PointRGBLPtr unorganizedSemanticCloudPtr;

        armarx::Mutex timestampMutex;
        long lastProcessedTimestamp;

        armarx::Mutex enableMutex;
        bool enabled;
        bool mergeWithSegmentationResults;

        bool visualizeSourcePointCloud;
        bool visualizeSegmentedPointCloud;
        unsigned int visualizationIteration;

        armarx::Mutex pointCloudStorageMutex;
        PointOPtr currentRawPointCloud;
        PointRGBLPtr currentSegmentedPointCloud;

        armarx::Mutex providerMutex;
        std::string rawPCProviderName;
        std::string segmentedPCProviderName;

        void colorizeLabeledCloud();

        armarx::DebugDrawerInterfacePrx debugDrawerTopic;
        armarx::Blob rgbImage;

        std::string knownObjectNamesFile;
        std::string darkNetConfigPath;
        void darkNetObjectDetector();
        void computeSemanticSegmentCloud();
        void computeRawSemanticSegmentCloud();
        void createColorVector(int VectorSize);
        int maxObjectSize;
        int objectNumber;
        char** objectNames;
        int** objectBB;

        std::map<uint32_t, pcl::PointIndices> labelMap;
        std::vector<std::string> desiredObjecs;
        std::vector<std::string> knownObjectList;
        std::vector<int> objectPointIndices;

        void parseKnownObjectList();
        void getLabelMap();
        void assignObjectLabelsToSegments();
        std::string getMatchedObjectName(int posX, int posY);

        uint32_t minSegSize;
        uint32_t maxSegSize;
        uint32_t deltaROI;
        bool returnUnorganizedCloud;

        struct rgbValues
        {
            uint8_t rVal;
            uint8_t gVal;
            uint8_t bVal;
        };
        std::vector<rgbValues > colorVector;
    };
}

