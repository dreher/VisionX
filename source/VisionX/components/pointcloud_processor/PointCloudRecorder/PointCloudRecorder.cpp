/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudRecorder
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudRecorder.h"


#include <pcl/filters/filter.h>
#include <Eigen/Core>
#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;



armarx::PropertyDefinitionsPtr PointCloudRecorder::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new PointCloudRecorderPropertyDefinitions(
            getConfigIdentifier()));
}


void armarx::PointCloudRecorder::onInitPointCloudProcessor()
{
    offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());

    usingPointCloudProvider(getProperty<std::string>("ProviderName").getValue());
}

void armarx::PointCloudRecorder::onConnectPointCloudProcessor()
{
    debugObserver = getTopic<DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
    debugDrawer = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());

    enableResultPointClouds<PointT>();
}


void armarx::PointCloudRecorder::onDisconnectPointCloudProcessor()
{
}

void armarx::PointCloudRecorder::onExitPointCloudProcessor()
{
}

void armarx::PointCloudRecorder::process()
{
    pcl::PointCloud<PointT>::Ptr inputCloud(new pcl::PointCloud<PointT>());

    if (!waitForPointClouds())
    {
        ARMARX_INFO << "Timeout or error while waiting for point cloud data";
        return;
    }
    else
    {
        getPointClouds<PointT>(inputCloud);
    }
    // StringVariantBaseMap debugValues;
    // debugValues["debug_value"] = new Variant(inputCloud->header.timestamp);
    // debugObserver->setDebugChannel(getName(), debugValues);

    std::string fileName = getProperty<std::string>("Folder").getValue() + "/cloud_" + std::to_string(inputCloud->header.stamp) + ".pcd";

    pcl::io::savePCDFile(fileName, *inputCloud);

    ARMARX_LOG << "writing to file: " << fileName;

    provideResultPointClouds<PointT>(inputCloud);
}
