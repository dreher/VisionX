/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudRecorder
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once




#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

#include <pcl/point_types.h>

#include <pcl/io/pcd_io.h>



namespace armarx
{

    typedef pcl::PointXYZRGBA PointT;

    /**
     * @class PointCloudRecorderPropertyDefinitions
     * @brief
     */
    class PointCloudRecorderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PointCloudRecorderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("Folder", "/tmp", "Path where to store the point clouds.");
            defineOptionalProperty<std::string>("ProviderName", "PointCloudProvider", "Name of the point cloud provider.");
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
        }
    };

    /**
     * @defgroup Component-PointCloudRecorder PointCloudRecorder
     * @ingroup VisionX-Components
     * A description of the component PointCloudRecorder.
     *
     * @class PointCloudRecorder
     * @ingroup Component-PointCloudRecorder
     * @brief Brief description of class PointCloudRecorder.
     *
     * Detailed description of class PointCloudRecorder.
     */

    class PointCloudRecorder :
        virtual public visionx::PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "PointCloudRecorder";
        }

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        DebugDrawerInterfacePrx debugDrawer;
        DebugObserverInterfacePrx debugObserver;
    };
}

