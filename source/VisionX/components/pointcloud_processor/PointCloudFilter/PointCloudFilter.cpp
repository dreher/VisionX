/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudFilter
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudFilter.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <pcl/surface/convex_hull.h>
#include <pcl/common/colors.h>

using namespace armarx;

void PointCloudFilter::setCroppingParameters(const Vector3BasePtr& min, const Vector3BasePtr& max, const std::string& frame, const Ice::Current& c)
{
    ScopedLock lock(croppingMutex);
    croppingEnabled = true;
    minPoint = Vector3Ptr::dynamicCast(min)->toEigen();
    maxPoint = Vector3Ptr::dynamicCast(max)->toEigen();
    croppingFrame = frame;
}

void PointCloudFilter::onInitPointCloudProcessor()
{
    providerName = getProperty<std::string>("providerName").getValue();
    sourceFrameName = getProperty<std::string>("sourceFrameName").getValue();
    pointCloudFormat = getProperty<std::string>("PointCloudFormat").getValue();
    croppingEnabled = getProperty<bool>("EnableCropping").getValue();

    gridLeafSize = getProperty<float>("leafSize").getValue();
    downsamplingEnabled = getProperty<bool>("EnableDownsampling").getValue();

    if (getProperty<std::string>("RobotStateComponentName").getValue() != "")
    {
        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
    }
    else
    {
        ARMARX_INFO << "No RobotStateComponent configured, not performing any point cloud transformations";
    }

    usingPointCloudProvider(providerName);

    maxPoint = getProperty<Eigen::Vector3f>("maxPoint").getValue();
    minPoint = getProperty<Eigen::Vector3f>("minPoint").getValue();
    croppingFrame = getProperty<std::string>("croppingFrame").getValue();

    applyCollisionModelFilter = getProperty<bool>("applyCollisionModelFilter").getValue();
    if (applyCollisionModelFilter && getProperty<std::string>("RobotStateComponentName").getValue() == "")
    {
        ARMARX_WARNING << "Collision model filter activated, but no RobotStateComponent configured, deactivating collision model filtering";
        applyCollisionModelFilter = false;
    }
}

void PointCloudFilter::onConnectPointCloudProcessor()
{
    if (getProperty<std::string>("RobotStateComponentName").getValue() != "")
    {
        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        localRobot = RemoteRobot::createLocalCloneFromFile(robotStateComponent, VirtualRobot::RobotIO::RobotDescription::eFull);
    }

    if (pointCloudFormat == "XYZRGBA")
    {
        enableResultPointClouds<pcl::PointXYZRGBA>();
    }
    else if (pointCloudFormat == "XYZL")
    {
        enableResultPointClouds<pcl::PointXYZL>();
    }
    else if (pointCloudFormat == "XYZRGBL")
    {
        enableResultPointClouds<pcl::PointXYZRGBL>();
    }
    else
    {
        ARMARX_ERROR << "Could not initialize point cloud, because format '" << pointCloudFormat << "' is unknown";
    }
}

void PointCloudFilter::onExitPointCloudProcessor()
{

}

void PointCloudFilter::process()
{
    if (pointCloudFormat == "XYZRGBA")
    {
        processPointCloud<pcl::PointXYZRGBA>();
    }
    else if (pointCloudFormat == "XYZL")
    {
        processPointCloud<pcl::PointXYZL>();
    }
    else if (pointCloudFormat == "XYZRGBL")
    {
        processPointCloud<pcl::PointXYZRGBL>();
    }
    else
    {
        ARMARX_ERROR << "Could not process point cloud, because format '" << pointCloudFormat << "' is unknown";
    }
}

void PointCloudFilter::onDisconnectPointCloudProcessor()
{

}

armarx::PropertyDefinitionsPtr PointCloudFilter::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new PointCloudFilterPropertyDefinitions(
            getConfigIdentifier()));
}

template<typename PointType>
void PointCloudFilter::processPointCloud()
{
    typename pcl::PointCloud<PointType>::Ptr inputCloudPtr(new pcl::PointCloud<PointType>());
    typename pcl::PointCloud<PointType>::Ptr outputCloudPtr(new pcl::PointCloud<PointType>());

    if (!waitForPointClouds(providerName, 10000))
    {
        ARMARX_INFO << "Timeout or error while waiting for point cloud data" << armarx::flush;
        return;
    }
    else
    {
        getPointClouds<PointType>(providerName, inputCloudPtr);
    }

    visionx::MetaPointCloudFormatPtr format = getPointCloudFormat(providerName);

    float original_w = inputCloudPtr->width;
    float original_h = inputCloudPtr->height;

    if (robotStateComponent)
    {
        RemoteRobot::synchronizeLocalCloneToTimestamp(localRobot, robotStateComponent, format->timeProvided);
    }

    bool currentCroppingEnabled;
    Eigen::Vector3f currentCroppingMin, currentCroppingMax;
    std::string currentCroppingFrame;
    {
        ScopedLock lock(croppingMutex);
        currentCroppingEnabled = croppingEnabled;
        currentCroppingMin = minPoint;
        currentCroppingMax = maxPoint;
        currentCroppingFrame = croppingFrame;
    }

    if (downsamplingEnabled)
    {
        pcl::ApproximateVoxelGrid<PointType> grid;
        grid.setLeafSize(gridLeafSize, gridLeafSize, gridLeafSize);
        grid.setInputCloud(inputCloudPtr);
        grid.filter(*outputCloudPtr);
        outputCloudPtr.swap(inputCloudPtr);
    }

    Eigen::Matrix4f sourceFrameGlobalPose = Eigen::Matrix4f::Identity();
    if (robotStateComponent && sourceFrameName != "Global")
    {
        sourceFrameGlobalPose = localRobot->getRobotNode(sourceFrameName)->getGlobalPose();
    }

    Eigen::Matrix4f sourceFrameInRootFrame = Eigen::Matrix4f::Identity();
    if (robotStateComponent && sourceFrameName == "Global")
    {
        sourceFrameInRootFrame = localRobot->getRootNode()->getGlobalPose().inverse();
    }
    else if (robotStateComponent)
    {
        sourceFrameInRootFrame = localRobot->getRobotNode(sourceFrameName)->getPoseInRootFrame();
    }

    Eigen::Matrix4f cameraToCrop, cropToGlobal;
    if (!robotStateComponent)
    {
        cameraToCrop = Eigen::Matrix4f::Identity();
        cropToGlobal = Eigen::Matrix4f::Identity();
    }
    else if (currentCroppingFrame == "Global")
    {
        cameraToCrop = sourceFrameGlobalPose;
        cropToGlobal = Eigen::Matrix4f::Identity();
    }
    else if (currentCroppingFrame == "Root")
    {
        cameraToCrop = sourceFrameInRootFrame;
        cropToGlobal = sourceFrameGlobalPose * cameraToCrop.inverse();
    }
    else
    {
        cameraToCrop = localRobot->getRobotNode(currentCroppingFrame)->getPoseInRootFrame().inverse() * sourceFrameInRootFrame;
        cropToGlobal = sourceFrameGlobalPose * cameraToCrop.inverse();
    }

    // Transform to intermediate cropping frame
    pcl::transformPointCloud(*inputCloudPtr, *outputCloudPtr, cameraToCrop);
    outputCloudPtr.swap(inputCloudPtr);

    if (currentCroppingEnabled)
    {
        // assumes that z axis goes up in robot's root frame
        pcl::PassThrough<PointType> pass;

        pass.setInputCloud(inputCloudPtr);
        pass.setFilterFieldName("z");
        pass.setFilterLimits(currentCroppingMin(2), currentCroppingMax(2));
        pass.filter(*outputCloudPtr);
        outputCloudPtr.swap(inputCloudPtr);

        pass.setInputCloud(inputCloudPtr);
        pass.setFilterFieldName("x");
        pass.setFilterLimits(currentCroppingMin(0), currentCroppingMax(0));
        pass.filter(*outputCloudPtr);
        outputCloudPtr.swap(inputCloudPtr);

        pass.setInputCloud(inputCloudPtr);
        pass.setFilterFieldName("y");
        pass.setFilterLimits(currentCroppingMin(1), currentCroppingMax(1));
        pass.filter(*outputCloudPtr);
        outputCloudPtr.swap(inputCloudPtr);
    }

    // Transform to global frame
    pcl::transformPointCloud(*inputCloudPtr, *outputCloudPtr, cropToGlobal);
    outputCloudPtr.swap(inputCloudPtr);

    if (applyCollisionModelFilter && robotStateComponent)
    {
        typename pcl::PointCloud<PointType>::Ptr collisionCloudPtr(new pcl::PointCloud<PointType>());

        for (VirtualRobot::CollisionModelPtr collisionModel : localRobot->getCollisionModels())
        {
            std::vector<Eigen::Vector3f> vertices = collisionModel->getModelVeticesGlobal();

            typename pcl::PointCloud<PointType>::Ptr hullPtr(new pcl::PointCloud<PointType>());
            hullPtr->width = vertices.size();
            hullPtr->height = 1;
            hullPtr->points.resize(vertices.size());
            for (size_t i = 0; i < vertices.size(); ++i)
            {
                hullPtr->points[i].getVector3fMap() = vertices[i].cast<float>();
            }

            (*collisionCloudPtr) += (*hullPtr);
        }

        pcl::ConvexHull<PointType> hull;
        hull.setInputCloud(collisionCloudPtr);
        hull.setDimension(3);

        std::vector<pcl::Vertices> polygons;
        typename pcl::PointCloud<PointType>::Ptr surfaceHull(new pcl::PointCloud<PointType>);
        hull.reconstruct(*surfaceHull, polygons);

        pcl::CropHull<PointType> cropHull;
        cropHull.setDim(3);
        cropHull.setHullIndices(polygons);
        cropHull.setHullCloud(surfaceHull);
        cropHull.setInputCloud(inputCloudPtr);
        cropHull.filter(*outputCloudPtr);

        outputCloudPtr.swap(inputCloudPtr);
    }

    typename pcl::PointCloud<PointType>::Ptr resultCloud = inputCloudPtr;

    ARMARX_INFO << deactivateSpam(1) << "Input cloud " << original_w << " x " << original_h << ", filtered cloud " << resultCloud->width <<  " x " << resultCloud->height;

    provideResultPointClouds<PointType>(resultCloud);
}
