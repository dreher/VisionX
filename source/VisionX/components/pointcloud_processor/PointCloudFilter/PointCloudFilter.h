/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudFilter
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/interface/components/PointCloudFilter.h>

#include <pcl/point_types.h>

#include <pcl/common/transforms.h>

#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/crop_hull.h>

#include <Eigen/Core>

namespace armarx
{
    static Eigen::Vector3f stringToVector3f(std::string propertyValue)
    {
        Eigen::Vector3f vec;
        sscanf(propertyValue.c_str(), "%f, %f, %f", &vec.data()[0], &vec.data()[1], &vec.data()[2]);
        return vec;
    }

    /**
     * @class PointCloudFilterPropertyDefinitions
     * @brief
     */
    class PointCloudFilterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PointCloudFilterPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<bool>("EnableDownsampling", true, "Enable/Disable downsampling");
            defineOptionalProperty<float>("leafSize", 5.0f, "the voxel grid leaf size");

            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("providerName", "OpenNIPointCloudProvider", "name of the point cloud provider");
            defineOptionalProperty<std::string>("sourceFrameName", "DepthCamera", "The source frame name");

            PropertyDefinition<Eigen::Vector3f>::PropertyFactoryFunction f = &stringToVector3f;

            defineOptionalProperty<bool>("applyCollisionModelFilter", true, "Filter points that appear to belong to the robot itself");

            defineOptionalProperty<Eigen::Vector3f>("minPoint", Eigen::Vector3f(-1000.0f, -1000.0f, 0.0f), "").setFactory(f);
            defineOptionalProperty<Eigen::Vector3f>("maxPoint", Eigen::Vector3f(4800.0f, 11500.0f, 1800.0f), "").setFactory(f);
            defineOptionalProperty<std::string>("croppingFrame", "Global", "The coordinate frame in which cropping is applied");
            defineOptionalProperty<bool>("EnableCropping", true, "Enable/Disable cropping");
            defineOptionalProperty<std::string>("PointCloudFormat", "XYZRGBA", "Format of the input and output point cloud (XYZRGBA, XYZL, XYZRGBL)");
        }
    };

    /**
     * @defgroup Component-PointCloudFilter PointCloudFilter
     * @ingroup VisionX-Components
     * A description of the component PointCloudFilter.
     *
     * @class PointCloudFilter
     * @ingroup Component-PointCloudFilter
     * @brief Brief description of class PointCloudFilter.
     *
     * Detailed description of class PointCloudFilter.
     */
    class PointCloudFilter :
        virtual public armarx::PointCloudFilterInterface,
        virtual public visionx::PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "PointCloudFilter";
        }

        std::string getReferenceFrame(const Ice::Current& c = ::Ice::Current()) override
        {
            return croppingFrame;
        }

        void setCroppingParameters(const armarx::Vector3BasePtr& min, const armarx::Vector3BasePtr& max, const std::string& frame, const Ice::Current& c = ::Ice::Current()) override;

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        template<typename PointType> void processPointCloud();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        armarx::Mutex croppingMutex;
        Eigen::Vector3f minPoint;
        Eigen::Vector3f maxPoint;
        std::string croppingFrame;
        bool croppingEnabled;

        std::string pointCloudFormat;

        VirtualRobot::RobotPtr localRobot;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;

        std::string sourceFrameName;
        std::string providerName;

        float gridLeafSize;
        bool downsamplingEnabled;

        bool applyCollisionModelFilter;
    };
}

