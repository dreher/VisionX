armarx_component_set_name("PointCloudFilter")
find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")
if(Eigen3_FOUND)
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox not available")
if(Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
endif()

find_package(PCL 1.8 QUIET)
armarx_build_if(PCL_FOUND "PCL not available")
if(PCL_FOUND)
    include_directories(SYSTEM ${PCL_INCLUDE_DIRS})
    add_definitions(${PCL_DEFINITIONS})
endif()

set(COMPONENT_LIBS
    VisionXInterfaces
    VisionXPointCloud
    VisionXCore
    MemoryXCore
    MemoryXInterfaces
    ArmarXCoreInterfaces
    ArmarXCore
    RobotAPICore
    ${Simox_LIBRARIES}
    ${PCL_COMMON_LIBRARY}
    ${PCL_KDTREE_LIBRARY}
    ${PCL_SAMPLE_CONSENSUS_LIBRARY}
    ${PCL_SEARCH_LIBRARY}
    ${PCL_FEATURES_LIBRARY}
    ${PCL_FILTERS_LIBRARY}
    ${PCL_SEGMENTATION_LIBRARY}
    ${PCL_SURFACE_LIBRARY}
    ${PCL_TRACKING_LIBRARY}
)

set(SOURCES PointCloudFilter.cpp)
set(HEADERS PointCloudFilter.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
