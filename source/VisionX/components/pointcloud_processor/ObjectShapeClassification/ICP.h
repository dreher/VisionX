/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <Math/Math3d.h>
//Eigen
#include <Eigen/Core>

#include <float.h>

class CKdTree;
namespace visionx
{

    class ICP
    {
    public:
        ICP();
        ~ICP();

        // this is the pointcloud in which we search for an object
        void SetScenePointcloud(const std::vector<Eigen::Vector3f>& aScenePoints);
        // this is the pointcloud of the object we search
        void SetObjectPointcloud(const std::vector<Eigen::Vector3f>& aObjectPoints);

        /* Estimate a transformation that fits the object pointcloud into the scene pointcloud
         * returns the average point distance value (in weighted xyzrgb-space) after the match, and the transformation of the object
         * fBestDistanceUntilNow=n: if the distance of the new transformation is smaller than 1.25*n, the result is refined more by reducing the fConvergenceDelta parameter (see below) */
        float SearchObject(Mat3d& mRotation, Vec3d& vTranslation, const float fBestDistanceUntilNow = FLT_MAX);

        /* Set the parameters:
         * fCutoffDistance=n: if two corresponding points have a distance of more than n, they are ignored. This is helpful when matching pointclouds with partial overlap
         * fConvergenceDelta=n: the algorithm stops when the relative improvement of the distance between the two clouds is less than n
         * nMaxIterations=n: the algorithm stops after at most n iterations */
        void SetParameters(float fCutoffDistance = FLT_MAX, float fConvergenceDelta = 0.001f, int nMaxIterations = 40, int nKdTreeBucketSize = 50); // 50, FLT_MAX, 0.0001f, 50, 50

        // returns the distances of object points to their nearest neighbour in the scene
        void GetPointMatchDistances(std::vector<float>& aPointMatchDistances);
        void GetNearestNeighbors(std::vector<Eigen::Vector3f>& aNeighbors, std::vector<float>& aPointMatchDistances);

    private:

        void FindNNBruteForce(const float* pPoint, float& fSquaredDistance, float*& pNeighbor);

        CKdTree* m_pKdTree;
        int m_nKdTreeBucketSize;
        std::vector<Eigen::Vector3f> m_aScenePoints, m_aObjectPoints;
        float m_fCutoffDistance, m_fConvergenceDelta;
        int m_nMaxIterations;

        int m_nNumScenePoints;
        float** m_pValues;
    };
}
