/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PrimitiveExtractionParameterTuning
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/interface/components/FakePointCloudProviderInterface.h>
#include <VisionX/interface/components/PointCloudSegmenter.h>
#include <VisionX/interface/components/PrimitiveMapper.h>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

namespace armarx
{
    /**
     * @class PrimitiveExtractionParameterTuningPropertyDefinitions
     * @brief
     */
    class PrimitiveExtractionParameterTuningPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PrimitiveExtractionParameterTuningPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("SourcePointCloud", "A XYZRGBA source truth point cloud");
            defineRequiredProperty<std::string>("GroundTruthPointCloud", "A XYZRGBL ground truth point cloud");
            defineOptionalProperty<std::string>("PointCloudProviderName", "FakePointCloudProvider", "The name of the point cloud provider");
            defineOptionalProperty<std::string>("PointCloudSegmenterName", "PointCloudSegmenter", "The name of the point cloud segmenter");
            defineOptionalProperty<std::string>("PrimitiveExtractorName", "PrimitiveExtractor", "The name of the point cloud provider");
            defineOptionalProperty<std::string>("PrimitivesProviderName", "PrimitiveExtractorResult", "The name of the primitive provider");
            defineRequiredProperty<std::string>("ExportDirectory", "The name of the directory to export point clouds and setup files");
        }
    };

    /**
     * @defgroup Component-PrimitiveExtractionParameterTuning PrimitiveExtractionParameterTuning
     * @ingroup VisionX-Components
     * A description of the component PrimitiveExtractionParameterTuning.
     *
     * @class PrimitiveExtractionParameterTuning
     * @ingroup Component-PrimitiveExtractionParameterTuning
     * @brief Brief description of class PrimitiveExtractionParameterTuning.
     *
     * Detailed description of class PrimitiveExtractionParameterTuning.
     */
    class PrimitiveExtractionParameterTuning :
        virtual public visionx::PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "PrimitiveExtractionParameterTuning";
        }

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        float compareLabeledPointClouds(const pcl::PointCloud<pcl::PointXYZL>::Ptr& labeled, const pcl::PointCloud<pcl::PointXYZRGBL>::Ptr& reference);
        void generateNewParameterSet();

        float sample(float current, const Eigen::Vector2f& bounds);
        void exportBestSetup(const std::string& directory, const pcl::PointCloud<pcl::PointXYZL>::Ptr& pointCloud, const visionx::LccpParameters& lccp_prm, const visionx::PrimitiveExtractorParameters& pe_prm);

    protected:
        std::string primitivesProviderName;
        std::string sourcePointCloudFilename;
        std::string referencePointCloudFilename;
        std::string exportDirectory;

        visionx::FakePointCloudProviderInterfacePrx pointCloudProvider;
        visionx::PointCloudSegmenterInterfacePrx pointCloudSegmenter;
        visionx::PrimitiveMapperInterfacePrx primitiveExtractor;

        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr referencePointCloud;

        visionx::PrimitiveExtractorParameters extractionPrm;
        visionx::LccpParameters segmentationPrm;

        visionx::PrimitiveExtractorParameters bestExtractionPrm;
        visionx::LccpParameters bestSegmentationPrm;
        float bestFitness;

        unsigned int iteration = 0;

        Eigen::Vector2f bounds_segmentation_minSegmentSize;
        Eigen::Vector2f bounds_segmentation_maxSegmentSize;
        Eigen::Vector2f bounds_segmentation_euclideanClusteringTolerance;
        Eigen::Vector2f bounds_segmentation_outlierThreshold;
        Eigen::Vector2f bounds_segmentation_planeMaxIterations;
        Eigen::Vector2f bounds_segmentation_planeDistanceThreshold;
        Eigen::Vector2f bounds_segmentation_planeNormalDistance;
        Eigen::Vector2f bounds_segmentation_cylinderMaxIterations;
        Eigen::Vector2f bounds_segmentation_cylinderDistanceThreshold;
        Eigen::Vector2f bounds_segmentation_cylinderRadiusLimit;
        Eigen::Vector2f bounds_segmentation_sphereMaxIterations;
        Eigen::Vector2f bounds_segmentation_sphereDistanceThreshold;
        Eigen::Vector2f bounds_segmentation_sphereNormalDistance;
        Eigen::Vector2f bounds_segmentation_circularDistanceThreshold;
        Eigen::Vector2f bounds_primitives_minSegmentSize;
        Eigen::Vector2f bounds_primitives_voxelResolution;
        Eigen::Vector2f bounds_primitives_seedResolution;
        Eigen::Vector2f bounds_primitives_colorImportance;
        Eigen::Vector2f bounds_primitives_spatialImportance;
        Eigen::Vector2f bounds_primitives_normalImportance;
        Eigen::Vector2f bounds_primitives_concavityThreshold;
        Eigen::Vector2f bounds_primitives_smoothnessThreshold;
    };
}

