/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Deep_Segmenter.h"

DeepSegClass::DeepSegClass()
{
    labeledCloud.reset(new pcl::PointCloud<pcl::PointXYZRGBL>());
}

pcl::PointCloud<pcl::PointXYZRGBL>::Ptr& DeepSegClass::GetLabeledPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& CloudPtr, armarx::Blob segmentImage)
{
    std::cout << " Input point cloud size: " << CloudPtr->points.size() << " width: " << CloudPtr->width << " height: " << CloudPtr->height << std::endl;
    std::cout << " segment mask size: " << segmentImage.size() << std::endl;

    const int width = CloudPtr->width;
    const int height = CloudPtr->height;

    labeledCloud->resize(CloudPtr->points.size());


    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            labeledCloud->points[i * width + j].x = CloudPtr->points[i * width + j].x;
            labeledCloud->points[i * width + j].y = CloudPtr->points[i * width + j].y;
            labeledCloud->points[i * width + j].z = CloudPtr->points[i * width + j].z;
            labeledCloud->points[i * width + j].r = CloudPtr->points[i * width + j].r;
            labeledCloud->points[i * width + j].g = CloudPtr->points[i * width + j].g;
            labeledCloud->points[i * width + j].b = CloudPtr->points[i * width + j].b;
            labeledCloud->points[i * width + j].label = segmentImage.at(i * width + j);
        }
    }

    return labeledCloud;
}

armarx::Blob DeepSegClass::GetImageFromPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& CloudPtr)
{
    const int width = CloudPtr->width;
    const int height = CloudPtr->height;
    rgbImage.clear();

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            pcl::PointXYZRGBA& point = CloudPtr->at(i * width + j);
            rgbImage.push_back(point.r);
            rgbImage.push_back(point.g);
            rgbImage.push_back(point.b);
        }
    }

    return rgbImage;
}



