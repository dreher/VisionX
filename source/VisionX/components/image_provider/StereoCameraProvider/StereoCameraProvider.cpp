/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StereoCameraProvider.h"

#include <iostream>
#include <string>
#include <vector>

// VisionXTools
#include <VisionX/tools/TypeMapping.h>

// IVT
#include <Calibration/Calibration.h>

#include "LoadingCalibrationFileFailedException.h"
#include "StereoCameraSystemRequiredException.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>

void visionx::StereoCameraProvider::onInitCapturingImageProvider()
{
    IEEE1394ImageProvider::onInitCapturingImageProvider();

    using namespace armarx;
    using namespace visionx;
    using namespace visionx::tools;

    // verify number of cameras

    if (ieee1394Capturer->GetNumberOfCameras() != 2)
    {
        throw visionx::exceptions::local::StereoCameraSystemRequiredException(
            ieee1394Capturer->GetNumberOfCameras());
    }

    std::string calibrationFileName = getProperty<std::string>("CalibrationFile") .getValue();
    if (!armarx::ArmarXDataPath::getAbsolutePath(calibrationFileName, calibrationFileName))
    {
        throw visionx::exceptions::local::LoadingCalibrationFileFailedException(calibrationFileName.c_str());
    }
    bool transformLeftCameraToIdentity = getProperty<bool>("LeftCameraAsIdentity") .getValue();

    if (!ivtStereoCalibration.LoadCameraParameters(calibrationFileName.c_str(), transformLeftCameraToIdentity))
    {
        throw visionx::exceptions::local::LoadingCalibrationFileFailedException(calibrationFileName.c_str());
    }

    stereoCalibration = visionx::tools::convert(ivtStereoCalibration);
}


visionx::StereoCalibration visionx::StereoCameraProvider::getStereoCalibration(const Ice::Current& c)
{
    return stereoCalibration;
}
