/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::KLGImageProvider
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <Eigen/Core>

#include <opencv2/opencv.hpp>

#include <VisionX/core/CapturingImageProvider.h>
#include <VisionX/interface/components/RGBDImageProvider.h>


#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>

namespace armarx
{
    /**
     * @class KLGImageProviderPropertyDefinitions
     * @brief
     */
    class KLGImageProviderPropertyDefinitions:
        public visionx::CapturingImageProviderPropertyDefinitions
    {
    public:
        KLGImageProviderPropertyDefinitions(std::string prefix):
            visionx::CapturingImageProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");

            defineOptionalProperty<std::string>("FileName", "VisionX/examples/point_clouds/", "path to the klg log file");
            defineOptionalProperty<bool>("Rewind", true, "loop through the point clouds");

            defineOptionalProperty<Eigen::Vector2i>("Dimensions", Eigen::Vector2i(640, 480), "")
            .map("320x240", Eigen::Vector2i(320, 240))
            .map("640x480", Eigen::Vector2i(640, 480))
            .map("320x240",     Eigen::Vector2i(320,  240))
            .map("640x480",     Eigen::Vector2i(640,  480))
            .map("800x600",     Eigen::Vector2i(800,  600))
            .map("768x576",     Eigen::Vector2i(768,  576))
            .map("1024x768",    Eigen::Vector2i(1024, 768))
            .map("1024x1024",   Eigen::Vector2i(1024, 1024))
            .map("1280x960",    Eigen::Vector2i(1280, 960))
            .map("1600x1200",   Eigen::Vector2i(1600, 1200));


            defineOptionalProperty<std::string>("ReferenceFrameName", "DepthCamera", "Optional reference frame name.");
        }
    };

    /**
     * @defgroup Component-KLGImageProvider KLGImageProvider
     * @ingroup VisionX-Components
     * A description of the component KLGImageProvider.
     *
     * @class KLGImageProvider
     * @ingroup Component-KLGImageProvider
     * @brief Brief description of class KLGImageProvider.
     *
     * Adds support for the klg format used by ElasticFusion
     */
    class KLGImageProvider :
        virtual public visionx::CapturingImageProvider,
        virtual public visionx::RGBDCapturingImageProviderInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "KLGImageProvider";
        }


        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::Current()) override
        {
            visionx::StereoCalibration stereoCalibration;

            Eigen::Vector2i dimensions = getProperty<Eigen::Vector2i>("Dimensions");

            visionx::CameraParameters RGBCameraIntrinsics;
            RGBCameraIntrinsics.distortion = {0, 0, 0};
            RGBCameraIntrinsics.focalLength = {525.0, 525.0};
            RGBCameraIntrinsics.height = dimensions(1);
            RGBCameraIntrinsics.principalPoint = {dimensions(0) / 2.0, dimensions(1) / 2.0};
            RGBCameraIntrinsics.rotation = visionx::tools::convertEigenMatToVisionX(Eigen::Matrix3f::Identity());
            RGBCameraIntrinsics.translation = visionx::tools::convertEigenVecToVisionX(Eigen::Vector3f::Zero());
            RGBCameraIntrinsics.width = dimensions(0);

            visionx::CameraParameters DepthCameraIntrinsics;
            DepthCameraIntrinsics.distortion = {0, 0, 0};
            DepthCameraIntrinsics.focalLength = {525.0, 525.0};
            DepthCameraIntrinsics.height = dimensions(1);
            DepthCameraIntrinsics.principalPoint = {dimensions(0) / 2.0, dimensions(1) / 2.0};
            DepthCameraIntrinsics.rotation =  visionx::tools::convertEigenMatToVisionX(Eigen::Matrix3f::Identity());
            DepthCameraIntrinsics.translation = {0.075, 0, 0};
            DepthCameraIntrinsics.width = dimensions(0);


            stereoCalibration.calibrationLeft = visionx::tools::createDefaultMonocularCalibration();
            stereoCalibration.calibrationRight = visionx::tools::createDefaultMonocularCalibration();
            stereoCalibration.calibrationLeft.cameraParam = RGBCameraIntrinsics;
            stereoCalibration.calibrationRight.cameraParam = DepthCameraIntrinsics;
            stereoCalibration.rectificationHomographyLeft = visionx::tools::convertEigenMatToVisionX(Eigen::Matrix3f::Zero());
            stereoCalibration.rectificationHomographyRight = visionx::tools::convertEigenMatToVisionX(Eigen::Matrix3f::Zero());


            return stereoCalibration;
        }

        bool getImagesAreUndistorted(const Ice::Current& c = Ice::Current()) override
        {
            return true;
        }

        std::string getReferenceFrame(const Ice::Current& c = Ice::Current()) override
        {
            return getProperty<std::string>("ReferenceFrameName");
        }

    protected:

        void onInitCapturingImageProvider() override;

        void onExitCapturingImageProvider() override;

        void onStartCapture(float frameRate) override;

        void onStopCapture() override;

        bool capture(void** ppImageBuffers) override;


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        DebugDrawerInterfacePrx debugDrawer;
        DebugObserverInterfacePrx debugObserver;

        CByteImage** rgbImages;

        int32_t currentFrame;
        int32_t totalFrames;
        FILE* fp;
        bool doRewind;

        cv::Mat depthBuffer;



    };
}

