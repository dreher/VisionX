/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::IntelRealSenseImageProvider
 * @author     Simon Thelen ( urday at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IntelRealSenseProvider.h"


#include <stdio.h>
#include <boost/filesystem.hpp>

#include <Image/ImageProcessor.h>

#include <librealsense2/rs.hpp>


using namespace armarx;

void IntelRealSenseImageProvider::onInitCapturingImageProvider()
{
    offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());
}


void IntelRealSenseImageProvider::onStartCapture(float framesPerSecond)
{
    debugObserver = getTopic<DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());
    debugDrawer = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());

    pipe.start();

    /*
    StringVariantBaseMap debugValues;
    debugValues["debug_value"] = new Variant(1.0);
    debugObserver->setDebugChannel(getName(), debugValues);
    */
}


void IntelRealSenseImageProvider::onStopCapture()
{

}

bool IntelRealSenseImageProvider::capture(void** ppImageBuffers)
{
    rs2::frameset data = pipe.wait_for_frames();

    rs2::frame depth = color_map(data.get_depth_frame());
    rs2::frame color = data.get_color_frame();

    const int dw = depth.as<rs2::video_frame>().get_width();
    const int dh = depth.as<rs2::video_frame>().get_height();
    const int cw = color.as<rs2::video_frame>().get_width();
    const int ch = color.as<rs2::video_frame>().get_height();

    // TODO set the timestamp

    armarx::SharedMemoryScopedWriteLockPtr lock(sharedMemoryProvider->getScopedWriteLock());

    memcpy(ppImageBuffers[0], color.get_data(), cw * ch);
    memcpy(ppImageBuffers[1], depth.get_data(), dw * dh);

    return true;
}



void IntelRealSenseImageProvider::onExitCapturingImageProvider()
{
}


armarx::PropertyDefinitionsPtr IntelRealSenseImageProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new IntelRealSenseImageProviderPropertyDefinitions(
            getConfigIdentifier()));
}

