/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::IntelRealSenseImageProvider
 * @author     Simon Thelen ( urday at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_VisionX_IntelRealSenseImageProvider_H
#define _ARMARX_COMPONENT_VisionX_IntelRealSenseImageProvider_H


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <Eigen/Core>

#include <opencv2/opencv.hpp>

#include <VisionX/core/CapturingImageProvider.h>

#include <librealsense2/rs.hpp>

namespace armarx
{
    /**
     * @class IntelRealSenseImageProviderPropertyDefinitions
     * @brief
     */
    class IntelRealSenseImageProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        IntelRealSenseImageProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");

        }
    };

    /**
     * @defgroup Component-IntelRealSenseImageProvider IntelRealSenseImageProvider
     * @ingroup VisionX-Components
     * A description of the component IntelRealSenseImageProvider.
     *
     * @class IntelRealSenseImageProvider
     * @ingroup Component-IntelRealSenseImageProvider
     * @brief Brief description of class IntelRealSenseImageProvider.
     *
     * Adds support for the klg format used by ElasticFusion
     */
    class IntelRealSenseImageProvider :
        virtual public visionx::CapturingImageProvider
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "IntelRealSenseImageProvider";
        }

    protected:

        virtual void onInitCapturingImageProvider();

        virtual void onExitCapturingImageProvider();

        virtual void onStartCapture(float frameRate);

        virtual void onStopCapture();

        virtual bool capture(void** ppImageBuffers);


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

    private:
        DebugDrawerInterfacePrx debugDrawer;
        DebugObserverInterfacePrx debugObserver;

	rs2::pipeline pipe;
	rs2::colorizer color_map;
    };
}

#endif
