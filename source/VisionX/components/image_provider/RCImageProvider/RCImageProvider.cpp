/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RCImageProvider
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RCImageProvider.h"


using namespace armarx;


#include <rc_genicam_api/system.h>
#include <rc_genicam_api/interface.h>
#include <rc_genicam_api/buffer.h>
#include <rc_genicam_api/image.h>
#include <rc_genicam_api/config.h>

#include <rc_genicam_api/pixel_formats.h>

#include <VisionX/tools/TypeMapping.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>


#include <Calibration/Calibration.h>
#include <Calibration/StereoCalibration.h>
#include <stdlib.h>
using namespace armarx;

namespace
{

    const char* const GC_COMPONENT_ENABLE = "ComponentEnable";
    const char* const GC_COMPONENT_SELECTOR = "ComponentSelector";
    const char* const GC_EXPOSURE_TIME = "ExposureTime";
    const char* const GC_EXPOSURE_AUTO = "ExposureAuto";
    const char* const GC_EXPOSURE_TIME_AUTO_MAX = "ExposureTimeAutoMax";
    const char* const GC_GAIN = "Gain";

}


void RCImageProvider::updateCameraSettings()
{
    bool autoExposure = getProperty<bool>("AutoExposure");
    float exposureTimeMs = getProperty<float>("ExposureTimeMs");
    float gainDb = getProperty<float>("GainDb");

    bool enableColor = getProperty<bool>("EnableColor").getValue();
    std::shared_ptr<GenApi::CNodeMapRef> nodemap = dev->getRemoteNodeMap();

    if (enableColor)
    {
        if (rcg::setEnum(nodemap, "PixelFormat", "YCbCr411_8", false))
        {
            ARMARX_INFO << "Enabling color mode";
        }
        else
        {
            ARMARX_WARNING << "Falling back to monochrome mode";
            enableColor = false;
        }
    }

    if (enableColor)
    {
        if (getProperty<bool>("EnableAutoWhiteBalance").getValue())
        {
            if (rcg::setEnum(nodemap, "BalanceWhiteAuto", "Continuous"))
            {
                ARMARX_INFO << "enabling auto white balance";
            }
            else
            {
                ARMARX_WARNING << "unable to enable auto white balance";
            }
        }
        else
        {
            if (rcg::setEnum(nodemap, "BalanceWhiteAuto", "Off"))
            {
                ARMARX_INFO << "disabling auto white balance";

                rcg::setEnum(nodemap, "BalanceRatioSelector", "Red");
                if (!rcg::setFloat(nodemap, "BalanceRatio", getProperty<float>("WhiteBalanceRatioRed").getValue()))
                {
                    ARMARX_WARNING << "unable to set white balance ratio for red-green";
                }

                rcg::setEnum(nodemap, "BalanceRatioSelector", "Blue");
                if (!rcg::setFloat(nodemap, "BalanceRatio", getProperty<float>("WhiteBalanceRatioBlue").getValue()))
                {
                    ARMARX_WARNING << "unable to set white balance ratio for green-blue";
                }
            }
            else
            {
                ARMARX_WARNING << "unable to disable auto white balance";
            }

        }

        rcg::setEnum(nodemap, "BalanceRatioSelector", "Red");
        float wbRatioRed = rcg::getFloat(nodemap, "BalanceRatio");

        rcg::setEnum(nodemap, "BalanceRatioSelector", "Blue");
        float wbRatioBlue  = rcg::getFloat(nodemap, "BalanceRatio");

        ARMARX_INFO << "white balance ratio is red: " << wbRatioRed << " blue: " << wbRatioBlue;
    }
    else
    {
        if (rcg::setEnum(nodemap, "PixelFormat", "Mono8", false))
        {
            ARMARX_INFO << "Enabling monochrome mode";
        }
        else
        {
            ARMARX_ERROR << "Could not set monochrome mode";
        }
    }

    std::string oldAutoExposure = rcg::getEnum(nodemap, GC_EXPOSURE_AUTO, false);
    ARMARX_INFO << "Setting auto exposure from " << oldAutoExposure << " to " << autoExposure;
    if (!rcg::setEnum(nodemap, GC_EXPOSURE_AUTO, autoExposure ? "Continuous" : "Off", false))
    {
        ARMARX_WARNING << "Could not set auto exposure to: " << autoExposure;
    }
    float oldExposureTime = rcg::getFloat(nodemap, GC_EXPOSURE_TIME) / 1000.0f;
    ARMARX_INFO << "Setting exposure time from " << oldExposureTime << " to " << exposureTimeMs;
    if (!rcg::setFloat(nodemap, GC_EXPOSURE_TIME, exposureTimeMs * 1000.0f, false))
    {
        ARMARX_WARNING << "Could not set exposure time to: " << exposureTimeMs;
    }
    float oldGain = rcg::getFloat(nodemap, GC_GAIN);
    ARMARX_INFO << "Setting gain from " << oldGain << " to " << gainDb;
    if (!rcg::setFloat(nodemap, GC_GAIN, gainDb, false))
    {
        ARMARX_WARNING << "Could not set gain to: " << gainDb;
    }
}

void armarx::RCImageProvider::onInitCapturingImageProvider()
{
    offeringTopic(getProperty<std::string>("RobotHealthTopicName").getValue());

    setenv("GENICAM_GENTL64_PATH", "/usr/lib/rc_genicam_api/", 0);
    dev = rcg::getDevice(getProperty<std::string>("DeviceId").getValue().c_str());

    if (dev)
    {
        ARMARX_INFO << "Connecting to device: " << getProperty<std::string>("DeviceId").getValue();
        dev->open(rcg::Device::EXCLUSIVE);
    }
    else
    {
        ARMARX_WARNING << "Unable to connect to device: " << getProperty<std::string>("DeviceId").getValue();

        std::vector<std::shared_ptr<rcg::Device> > devices = rcg::getDevices();
        ARMARX_INFO << "found " << devices.size();
        for (const rcg::DevicePtr& d : devices)
        {
            ARMARX_INFO << "Device:" << d->getID();
            ARMARX_INFO << "Vendor:" << d->getVendor();
            ARMARX_INFO << "Model:" << d->getModel();
            ARMARX_INFO << "TL type:" << d->getTLType();
            ARMARX_INFO << "Display name: " << d->getDisplayName();
            ARMARX_INFO << "Access status:" << d->getAccessStatus();
            ARMARX_INFO << "User name:" << d->getUserDefinedName();
            ARMARX_INFO << "Serial number:" << d->getSerialNumber();
            ARMARX_INFO << "Version:" << d->getVersion();
            ARMARX_INFO << "TS Frequency:" << d->getTimestampFrequency();
        }

        ARMARX_ERROR << "Please specify the device id. Aborting";

        getArmarXManager()->asyncShutdown();

        return;
    }

    std::shared_ptr<GenApi::CNodeMapRef> nodemap = dev->getRemoteNodeMap();

    numImages = 2;

    visionx::ImageDimension dimensions;
    dimensions.height = rcg::getInteger(nodemap, "Height") / 2.0;
    dimensions.width = rcg::getInteger(nodemap, "Width");
    ARMARX_INFO << VAROUT(dimensions.height) << VAROUT(dimensions.width);

    rcg::setString(nodemap, GC_COMPONENT_SELECTOR, "Intensity", true);
    rcg::setString(nodemap, GC_COMPONENT_ENABLE, "0", true);
    rcg::setString(nodemap, GC_COMPONENT_SELECTOR, "IntensityCombined", true);
    rcg::setString(nodemap, GC_COMPONENT_ENABLE, "1", true);

    float baseline = rcg::getFloat(nodemap, "Baseline");
    float focalLength = rcg::getFloat(nodemap, "FocalLengthFactor") * dimensions.width;


    updateCameraSettings();

    if (!rcg::setBoolean(nodemap, "GevIEEE1588", true))
    {
        ARMARX_WARNING << "Could not activate Precision Time Protocol (PTP) client!";
    }



    std::vector<rcg::StreamPtr> availableStreams = dev->getStreams();

    for (const rcg::StreamPtr s : availableStreams)
    {
        ARMARX_INFO << "Stream: " << s->getID();
    }

    if (!availableStreams.size())
    {
        ARMARX_FATAL << "Unable to open stream";
        return;
    }
    else
    {
        this->stream = availableStreams[0];
    }


    cameraImages = new CByteImage*[numImages];
    for (size_t i = 0 ; i < numImages; i++)
    {
        cameraImages[i] = new CByteImage(dimensions.width, dimensions.height, CByteImage::eRGB24);
    }


    scaleFactor = getProperty<float>("ScaleFactor");
    if (scaleFactor > 1.0)
    {
        dimensions.height /= scaleFactor;
        dimensions.width /= scaleFactor;
        focalLength /= scaleFactor;
    }

    smallImages = new CByteImage*[numImages];
    for (size_t i = 0 ; i < numImages; i++)
    {
        smallImages[i] = new CByteImage(dimensions.width, dimensions.height, CByteImage::eRGB24);
    }



    frameRate = getProperty<float>("FrameRate").getValue();

    setNumberImages(numImages);
    setImageFormat(dimensions, visionx::eRgb, visionx::eBayerPatternRg);
    setImageSyncMode(visionx::eCaptureSynchronization);

    CCalibration c;
    c.SetCameraParameters(focalLength, focalLength, dimensions.width / 2.0, dimensions.height / 2.0,
                          0.0, 0.0, 0.0, 0.0,
                          Math3d::unit_mat, Math3d::zero_vec, dimensions.width, dimensions.height);

    CStereoCalibration ivtStereoCalibration;
    ivtStereoCalibration.SetSingleCalibrations(c, c);
    ivtStereoCalibration.rectificationHomographyRight = Math3d::unit_mat;
    ivtStereoCalibration.rectificationHomographyLeft = Math3d::unit_mat;


    Vec3d right_translation;
    right_translation.x = baseline * -1000.0;
    right_translation.y = 0.0;
    right_translation.z = 0.0;
    ivtStereoCalibration.SetExtrinsicParameters(Math3d::unit_mat, Math3d::zero_vec, Math3d::unit_mat, right_translation, false);

    auto calibSavePath = "/tmp/armar6_rc_color_calibration.txt";
    ARMARX_INFO << "Saving calibration to: " << calibSavePath;
    ivtStereoCalibration.SaveCameraParameters(calibSavePath);
    stereoCalibration = visionx::tools::convert(ivtStereoCalibration);
}

void RCImageProvider::onConnectImageProvider()
{
    robotHealthTopic = getTopic<RobotHealthInterfacePrx>(getProperty<std::string>("RobotHealthTopicName").getValue());
}

void armarx::RCImageProvider::onExitCapturingImageProvider()
{
    if (stream)
    {
        stream->stopStreaming();
        stream->close();
    }

    if (dev)
    {
        dev->close();
    }

    for (size_t i = 0; i < numImages; i++)
    {
        delete cameraImages[i];
        delete smallImages[i];
    }

    delete [] cameraImages;
    delete [] smallImages;
}

void armarx::RCImageProvider::onStartCapture(float framesPerSecond)
{

    // rcg::setString(nodemap, "AcquisitionFrameRate", std::to_string(framesPerSecond), true);
    stream->open();
    stream->startStreaming();
}

void armarx::RCImageProvider::onStopCapture()
{
    stream->stopStreaming();
    stream->close();
}

bool RCImageProvider::capture(void** ppImageBuffers)
{
    visionx::ImageFormatInfo imageFormat = getImageFormat();

    IceUtil::Time timeoutTime = IceUtil::Time::now(IceUtil::Time::Monotonic) + IceUtil::Time::secondsDouble(2);
    while (IceUtil::Time::now(IceUtil::Time::Monotonic) < timeoutTime)
    {
        const rcg::Buffer* buffer = stream->grab((timeoutTime - IceUtil::Time::now(IceUtil::Time::Monotonic)).toMilliSeconds());
        if (!buffer)
        {
            ARMARX_WARNING << deactivateSpam(10) <<  "buffer is NULL - Unable to get image - restarting streaming";
            stream->stopStreaming();
            stream->close();
            ARMARX_WARNING << deactivateSpam(10) <<  "streaming stopped";
            stream->open();
            stream->startStreaming();
            ARMARX_WARNING << deactivateSpam(10) <<  "streaming started again";
            return false;
        }

        if (buffer->getIsIncomplete())
        {
            continue;    // try again
        }
        if (!buffer->getImagePresent())
        {
            ARMARX_WARNING << deactivateSpam(10) <<  "Buffer does not contain an image!";
            return false;
        }


        // ARMARX_LOG << "grabbing image " << imageFormat.dimension.width << "x" << imageFormat.dimension.height << " vs " <<  buffer->getWidth() << "x" << buffer->getHeight();

        const unsigned char* p = static_cast<const unsigned char*>(buffer->getBase()) + buffer->getImageOffset();


        size_t px = buffer->getXPadding();
        uint64_t format = buffer->getPixelFormat();

        int width = imageFormat.dimension.width;
        int height = imageFormat.dimension.height;
        if (scaleFactor > 1.0)
        {
            width *= scaleFactor;
            height *= scaleFactor;
        }

        const int imageSize = imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel;

        for (size_t n = 0; n < numImages; n++)
        {
            auto outPixels = cameraImages[n]->pixels;
            switch (format)
            {
                case Mono8:
                case Confidence8:
                case Error8:
                {
                    unsigned char const* row = p + (n * height) * (width + px);
                    unsigned char* outRow = outPixels;
                    for (int j = 0; j < height; j++)
                    {
                        for (int i = 0; i < width; i++)
                        {
                            const unsigned char c = row[i];
                            outRow[3 * i + 0] = c;
                            outRow[3 * i + 1] = c;
                            outRow[3 * i + 2] = c;
                        }

                        p += px;
                        outRow += width * 3;
                        row += width + px;
                    }
                }
                break;
                case YCbCr411_8:
                {
                    // Compression: 4 pixel are encoded in 6 bytes
                    size_t inStride = width * 6 / 4 + px;
                    unsigned char const* row = p + (n * height) * inStride;
                    unsigned char* outRow = outPixels;
                    uint8_t rgb[3];
                    for (int j = 0; j < height; j++)
                    {
                        for (int i = 0; i < width; i++)
                        {
                            rcg::convYCbCr411toRGB(rgb, row, i);
                            outRow[i * 3 + 0] = rgb[0];
                            outRow[i * 3 + 1] = rgb[1];
                            outRow[i * 3 + 2] = rgb[2];
                        }

                        row += inStride;
                        outRow += width * 3;
                    }
                }
                break;
            }
        }


        armarx::SharedMemoryScopedWriteLockPtr lock(sharedMemoryProvider->getScopedWriteLock());
        updateTimestamp(buffer->getTimestampNS() / 1000, false);
        if (scaleFactor > 1.0)
        {
            ImageProcessor::Resize(cameraImages[0], smallImages[0]);
            ImageProcessor::Resize(cameraImages[1], smallImages[1]);

            for (size_t i = 0; i < numImages; i++)
            {
                memcpy(ppImageBuffers[i], smallImages[i]->pixels, imageSize);
            }
        }
        else
        {

            for (size_t i = 0; i < numImages; i++)
            {
                memcpy(ppImageBuffers[i], cameraImages[i]->pixels, imageSize);
            }
        }

        robotHealthTopic->heartbeat(getName(), RobotHealthHeartbeatArgs());

        return true;
    }

    return false;
}




armarx::PropertyDefinitionsPtr RCImageProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new RCImageProviderPropertyDefinitions(
            getConfigIdentifier()));
}

