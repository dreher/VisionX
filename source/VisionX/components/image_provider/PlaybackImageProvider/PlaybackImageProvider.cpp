/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::components
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/components/image_provider/PlaybackImageProvider/PlaybackImageProvider.h>


// STD/STL
#include <functional>
#include <tuple>

// Boost
#include <boost/algorithm/clamp.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


const std::string
visionx::components::PlaybackImageProvider::DEFAULT_NAME = "PlaybackImageProvider";


visionx::components::PlaybackImageProvider::PlaybackImageProvider()
{
    // pass
}


visionx::components::PlaybackImageProvider::~PlaybackImageProvider()
{
    // pass
}


void
visionx::components::PlaybackImageProvider::onInitCapturingImageProvider()
{
    const boost::filesystem::path basePath = boost::filesystem::path(this->getProperty<std::string>("base_path").getValue());
    const std::vector<boost::filesystem::path> recordingFiles = [](const std::string & filesStr)
    {
        std::vector<boost::filesystem::path> files;
        boost::split(files, filesStr, boost::is_any_of(";"));
        return files;
    }(this->getProperty<std::string>("recording_files"));

    std::function<std::tuple<double, double>(const std::string&, unsigned int)> parse_fps = [](const std::string & setting, double source_fps) -> std::tuple<double, double>
    {
        // This lambda assumes that all operations are safe to call, as fps_setting was checked
        // against a RegEx while configuration, and does therefore not perform any sanity checks

        const unsigned long int prefix_length = std::string("sourceX").size();
        std::string setting_value;
        std::function<double(double, double)> operation;

        // Source FPS
        if (setting == "source")
        {
            return std::make_tuple(source_fps, 1.);
        }
        // Source FPS multiplied by value
        else if (boost::algorithm::starts_with(setting, "source*"))
        {
            // Multiplication
            operation = [](double x, double y) -> double { return x * y; };
            setting_value = setting.substr(prefix_length);
        }
        // Source FPS divided by value
        else if (boost::algorithm::starts_with(setting, "source/"))
        {
            // Division
            operation = [](double x, double y) -> double { return x / y; };
            setting_value = setting.substr(prefix_length);
        }
        // Source FPS is ignored and the value is assumed to be source FPS
        else if (boost::algorithm::starts_with(setting, "source="))
        {
            return std::make_tuple(std::stod(setting.substr(prefix_length)), 1.);
        }
        // Set FPS to a specific value
        else
        {
            // Identity to keep the user defined setting regardless of source FPS
            operation = [](double, double fps) -> double { return fps; };
            setting_value = setting;
        }

        const double derived_fps = operation(source_fps, std::stod(setting_value));
        const double playback_speed_normalisation = source_fps / derived_fps;

        return std::make_tuple(derived_fps, playback_speed_normalisation);
    };

    unsigned int refFrameHeight = 0;
    unsigned int refFrameWidth = 0;
    unsigned int actual_source_fps = 0;

    // Instantiate playbacks
    for (const boost::filesystem::path & recordingFile : recordingFiles)
    {
        const boost::filesystem::path fullPath = basePath != "" ? basePath / recordingFile : recordingFile;
        visionx::playback::Playback playback = visionx::playback::newPlayback(fullPath);
        this->playbacks.push_back(playback);

        if (not playback)
        {
            ARMARX_ERROR << "Could not find a playback strategy for '" << fullPath.string() << "'";
            continue;
        }

        // Sanity checks for image heights and widths
        {
            // If uninitialised, use first playback as reference
            if (refFrameHeight == 0 and refFrameWidth == 0)
            {
                refFrameHeight = playback->getFrameHeight();
                refFrameWidth = playback->getFrameWidth();
            }

            // Check that neither hight nor width are 0 pixels
            ARMARX_CHECK_GREATER_W_HINT(playback->getFrameHeight(), 0, "Image source frame height cannot be 0 pixel");
            ARMARX_CHECK_GREATER_W_HINT(playback->getFrameWidth(), 0, "Image source frame width cannot be 0 pixel");

            // Check that the frame heights and widths from all image sources are equal
            ARMARX_CHECK_EQUAL_W_HINT(playback->getFrameHeight(), refFrameHeight, "Image source frames must have the same dimensions");
            ARMARX_CHECK_EQUAL_W_HINT(playback->getFrameWidth(), refFrameWidth, "Image source frames must have the same dimensions");
        }

        // Determine playback with the highest FPS
        if (playback->getFps() > actual_source_fps)
        {
            actual_source_fps = playback->getFps();
        }
    }

    // Figure out FPS related variables base FPS, normalisations, multiplier
    double playback_speed_normalisation;
    std::tie(this->frameRate, playback_speed_normalisation) = parse_fps(this->getProperty<std::string>("fps"), actual_source_fps);
    this->frameAdvance = playback_speed_normalisation * this->getProperty<double>("playback_speed_multiplier");

    ARMARX_INFO << "Playback at " << this->frameRate << " FPS, advancing " << this->frameAdvance << " per loop";

    // Set up required properties for the image capturer
    this->setNumberImages(static_cast<int>(this->playbacks.size()));
    this->setImageFormat(visionx::ImageDimension(static_cast<int>(refFrameWidth), static_cast<int>(refFrameHeight)), visionx::eRgb, visionx::eBayerPatternGr);
    this->setImageSyncMode(visionx::eFpsSynchronization);
}


void
visionx::components::PlaybackImageProvider::onExitCapturingImageProvider()
{
    for (visionx::playback::Playback playback : this->playbacks)
    {
        playback->stopPlayback();
    }

    this->playbacks.clear();
}


void
visionx::components::PlaybackImageProvider::onStartCapture(float)
{
    // pass
}


void
visionx::components::PlaybackImageProvider::onStopCapture()
{
    // pass
}


bool
visionx::components::PlaybackImageProvider::capture(void** imageBuffer)
{
    bool success = false; // Result is true, if at least one of the playbacks provided a picture

    for (unsigned int i = 0; i < static_cast<unsigned int>(this->getNumberImages()); ++i)
    {
        visionx::playback::Playback playback = this->playbacks[i];

        // If the last frame is reached and loop is enabled, rewind
        if (!playback->hasNextFrame() and this->getProperty<bool>("loop"))
        {
            this->currentFrame = 0;
        }

        // TODO: From C++17 onwards, use std::clamp instead. Also remove boost header then
        unsigned int frameIndex = boost::algorithm::clamp(static_cast<unsigned int>(std::round(this->currentFrame)), 0, playback->getFrameCount() - 1);
        playback->setCurrentFrame(frameIndex);
        success |= playback->getNextFrame(imageBuffer[i]);
    }

    this->currentFrame += this->frameAdvance;

    return success;
}


std::string
visionx::components::PlaybackImageProvider::getDefaultName() const
{
    return visionx::components::PlaybackImageProvider::DEFAULT_NAME;
}


armarx::PropertyDefinitionsPtr
visionx::components::PlaybackImageProvider::createPropertyDefinitions()
{
    armarx::PropertyDefinitionsPtr defs = armarx::PropertyDefinitionsPtr(new armarx::ComponentPropertyDefinitions(this->getConfigIdentifier()));
    defs->defineRequiredProperty<std::string>("recording_files", "List of recording files, separated by semicolons `;` for each channel. For video files, use the filename, for image sequences, use the folder name where the image sequence is located or any frame as pattern.\n"
            "Wildcards (e.g. `frame_left_*.jpg;frame_right_*.jpg`) are supported as well.\n"
            "Files will be interpreted as absolute paths, or relative paths to the current working directory if base_path is not set. If base_path is set, all paths in recording_files are interpreted relative to base_path");
    defs->defineOptionalProperty<std::string>("base_path", "", "Common base path of recording_files (will be prepened if set). To unset, use \"\"");
    defs->defineOptionalProperty<double>("playback_speed_multiplier", 1.0, "Adjust the playback speed with this multiplier, e.g. `2` = double playback speed, `0.5` = half playback speed").setMin(0);
    defs->defineOptionalProperty<std::string>("fps", "source", "Lock the FPS to an absolute value, or a value relative to source FPS. Valid inputs:\n"
            "  1) `source` => Derive FPS from source\n"
            "  2) `source*<X>`, with <X> being a positive decimal or integer => Playback with FPS = source FPS multiplied by <X>\n"
            "  3) `source/<X>`, with <X> being a positive decimal or integer => Playback with FPS = source FPS devided by <X>\n"
            "  4) `<X>`, with <X> being a positive decimal or integer => Playback with FPS at <X>\n"
            "  5) `source=<X>`, with <X> being a positive decimal or integer => Playback with FPS at <X>, ignoring source FPS completely (Assume that <X> is source FPS)\n"
            "With the exception of 5), all settings only have direct effect of the FPS the image provider delivers the frames, but not on the playback speed. "
            "Use `playback_speed_multiplier` to adjust that.\n"
            "5) is only useful if the metadata of the recording is incomplete or incorrect (for example when replaying generic image sequences where the FPS cannot be derived)")
    .setMatchRegex(R"(source(\/|\*|=)(\d+(\.\d*)?|\.\d+)|(\d+(\.\d*)?|\.\d+)|(\d+(\.\d*)?|\.\d+)|source)");
    defs->defineOptionalProperty<bool>("loop", true, "Whether the playback should restart after the last frame was provided");
    return defs;
}
