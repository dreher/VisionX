/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::components
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>

// VisionX
#include <VisionX/core/CapturingImageProvider.h>
#include <VisionX/libraries/playback.h>


namespace visionx
{
    namespace components
    {
        class PlaybackImageProvider;
    }
}


class visionx::components::PlaybackImageProvider :
    public visionx::CapturingImageProvider
{

public:

    static const std::string DEFAULT_NAME;

protected:

    std::vector<visionx::playback::Playback> playbacks;
    double currentFrame;
    double frameAdvance;

public:

    PlaybackImageProvider();
    virtual ~PlaybackImageProvider() override;

    virtual void onInitCapturingImageProvider() override;
    virtual void onExitCapturingImageProvider() override;
    virtual void onStartCapture(float) override;
    virtual void onStopCapture() override;
    virtual bool capture(void** imageBuffer) override;

    virtual std::string getDefaultName() const override;
    virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

};
