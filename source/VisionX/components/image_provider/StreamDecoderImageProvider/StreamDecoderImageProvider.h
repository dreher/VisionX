/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::StreamDecoderImageProvider
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <VisionX/interface/components/StreamProvider.h>
#include <VisionX/core/ImageProvider.h>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
}

class CByteImage;

namespace armarx
{
    /**
     * @class StreamDecoderImageProviderPropertyDefinitions
     * @brief
     */
    class StreamDecoderImageProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        StreamDecoderImageProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("UsedStreamProvider", "StreamProvider", "Names of the StreamProvider that is to be used");
            defineOptionalProperty<std::string>("imageStreamTopicName", "ImageStream", "Name of the image streaming topic");

            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-StreamDecoderImageProvider StreamDecoderImageProvider
     * @ingroup VisionX-Components
     * A description of the component StreamDecoderImageProvider.
     *
     * @class StreamDecoderImageProvider
     * @ingroup Component-StreamDecoderImageProvider
     * @brief Brief description of class StreamDecoderImageProvider.
     *
     * Detailed description of class StreamDecoderImageProvider.
     */
    class StreamDecoderImageProvider :
        virtual public visionx::ImageProvider,
        public Stream::StreamListenerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "StreamDecoderImageProvider";
        }

    protected:
        void onInitImageProvider() override;
        void onConnectComponent() override;
        void onDisconnectImageProvider() override;
        void onExitImageProvider() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
        // StreamListenerInterface interface
        Stream::StreamProviderPrx streamProvider;
        void reportNewStreamData(const Stream::DataChunk& chunk, Ice::Long imageTimestamp, const Ice::Current&) override;
        int numberImages;
        AVCodec* m_decoder;
        AVCodecContext* m_decoderContext;
        int m_got_picture;
        AVFrame* m_picture;
        AVPacket m_packet;
        Mutex decodedImageMutex;
        Mutex streamDecodeMutex;
        SwsContext* m_swsCtx = NULL;
        CByteImage** ppDecodedImages, *pCombinedDecodedImage;
    };
}

