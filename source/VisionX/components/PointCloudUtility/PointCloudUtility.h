/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudUtility
 * @author     Tim Niklas Freudenstein ( uidnv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/icp.h>
#include <pcl/visualization/cloud_viewer.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <pcl/common/transforms.h>
#include <VirtualRobot/MathTools.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <pcl/filters/approximate_voxel_grid.h>
namespace armarx
{
    /**
     * @class PointCloudUtilityPropertyDefinitions
     * @brief
     */
    class PointCloudUtilityPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PointCloudUtilityPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("PointCloud1", "FileName1");
            defineOptionalProperty<std::string>("PointCloud2", "", "FileName2");
            defineOptionalProperty<bool>("Merge", true, "If set the program merges PointCloud1 and PointCloud2");
            defineOptionalProperty<bool>("Transform", true, "If set the program transforms the merged Pointcloud or Pointcloud1");
            defineOptionalProperty<bool>("Show", true, "If set the program shows the merged/transformed Pointcloud or Pointcloud1");
            defineOptionalProperty<std::string>("ResultFileName", "result.pcd", "The filename of the output file");
            defineOptionalProperty<float>("X", 0.0, "");
            defineOptionalProperty<float>("Y", 0.0, "");
            defineOptionalProperty<float>("Z", 0.0, "");
            defineOptionalProperty<float>("Roll", 0.0, "");
            defineOptionalProperty<float>("Pitch", 0.0, "");
            defineOptionalProperty<float>("Yaw", 0.0, "");

        }
    };

    /**
     * @defgroup Component-PointCloudUtility PointCloudUtility
     * @ingroup VisionX-Components
     * A description of the component PointCloudUtility.
     *
     * @class PointCloudUtility
     * @ingroup Component-PointCloudUtility
     * @brief Brief description of class PointCloudUtility.
     *
     * Detailed description of class PointCloudUtility.
     */
    class PointCloudUtility :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "PointCloudUtility";
        }
    private:
        void fusePointclouds(std::string file1, std::string file2, std::string out);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr loadPointCloud(std::string filename);
        void moveOrigin(std::string file, std::string out, Eigen::Vector3f translation);
        void rotatePointCloud(std::string file, std::string out, Eigen::Matrix3f rotation);
        void transformPointcloud(std::string file, std::string out, Eigen::Matrix4f pose);
        void showResult(std::string file);
        RunningTask<PointCloudUtility>::pointer_type processorTask;
        void process();
    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    };
}
