/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OpenPoseEstimation
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "OpenPoseEstimation.h"
#include <VisionX/interface/components/OpenPoseEstimationInterface.h>

#include <VisionX/tools/ImageUtil.h>
#include <VisionX/tools/TypeMapping.h>
#include <VisionX/interface/components/RGBDImageProvider.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/libraries/core/math/ColorUtils.h>

#include <boost/make_shared.hpp>
#include <boost/algorithm/string.hpp>

#include <openpose/pose/poseParameters.hpp>
#include <openpose/pose/poseParametersRender.hpp>
#include <Image/IplImageAdaptor.h>
#include <Image/PrimitivesDrawer.h>

using namespace armarx;

#define MAX_LAYER 5

armarx::PropertyDefinitionsPtr OpenPoseEstimation::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new OpenPoseEstimationPropertyDefinitions(
            getConfigIdentifier()));
}

void OpenPoseEstimation::onInitImageProcessor()
{
    providerName = getProperty<std::string>("ImageProviderName").getValue();

    usingImageProvider(providerName);
    offeringTopic(getProperty<std::string>("DebugDrawerTopicName").getValue());

    depthMedianRadius = getProperty<int>("DepthMedianRadius").getValue();
    useDistortionParameters = getProperty<bool>("UseDistortionParameters").getValue();
    layerName = "OpenPoseEstimation";
    robotLayerName = "OpenPoseEstimation_IKResult";
    stereo = getProperty<bool>("3DConstructionWithStereo").getValue();
    renderThreshold = getProperty<float>("OP_render_threshold").getValue();

    ikPoseEstimationActive = getProperty<bool>("IKPoseEstimationActive").getValue();

    cameraNodeName = getProperty<std::string>("CameraNodeName").getValue();
    minimalValidKeypoints = getProperty<int>("MinimalAmountKeypoints").getValue();
    reportOnlyNearestPerson = getProperty<bool>("ReportOnlyNearestPerson").getValue();

    // Setup workspace-polygon
    std::vector<Polygon2D::Point> points;
    std::vector<std::string> pointStrings;
    std::string input = getProperty<std::string>("WorkspacePolygon").getValue();
    boost::split(pointStrings, input, boost::is_any_of(";"));
    for (auto s : pointStrings)
    {
        ARMARX_INFO << s;
        std::vector<std::string> p;
        boost::split(p, s, boost::is_any_of(","));
        ARMARX_CHECK_EXPRESSION(p.size() == 2);
        Polygon2D::Point point;
        point.x = std::strtof(p.at(0).c_str(), NULL);
        point.y = std::strtof(p.at(1).c_str(), NULL);
        points.push_back(point);
    }
    workspacePolygon = Polygon2D(points);
}

void OpenPoseEstimation::onConnectImageProcessor()
{
    visionx::ImageType imageDisplayType = visionx::tools::typeNameToImageType("rgb");
    imageProviderInfo = getImageProvider(providerName, imageDisplayType);

    numImages = imageProviderInfo.numberImages;
    if (numImages < 1 || numImages > 2)
    {
        ARMARX_FATAL << "invalid number of images. aborting";
        return;
    }
    if (stereo)
    {
        if (numImages != 2)
        {
            ARMARX_FATAL << "invalid number of images. aborting";
            return;
        }
    }

    imageBuffer = new CByteImage*[numImages];
    for (int i = 0 ; i < numImages ; i++)
    {
        imageBuffer[i] = visionx::tools::createByteImage(imageProviderInfo);
    }
    rgbImageBuffer = visionx::tools::createByteImage(imageProviderInfo);
    depthImageBuffer = visionx::tools::createByteImage(imageProviderInfo);
    openPoseResultImage = new CByteImage*[1];
    openPoseResultImage[0] = visionx::tools::createByteImage(imageProviderInfo);
    enableResultImages(1, imageProviderInfo.imageFormat.dimension, imageProviderInfo.imageFormat.type);

    offeringTopic(getProperty<std::string>("OpenPoseEstimation2DTopicName").getValue());
    listener2DPrx = getTopic<OpenPose2DListenerPrx>(getProperty<std::string>("OpenPoseEstimation2DTopicName").getValue());
    offeringTopic(getProperty<std::string>("OpenPoseEstimation3DTopicName").getValue());
    listener3DPrx = getTopic<OpenPose3DListenerPrx>(getProperty<std::string>("OpenPoseEstimation3DTopicName").getValue());

    // Calibration
    stereoCalibration = NULL;
    calibration = NULL;
    {
        visionx::StereoCalibrationInterfacePrx calib = visionx::StereoCalibrationInterfacePrx::checkedCast(imageProviderInfo.proxy);
        if (!calib)
        {
            ARMARX_FATAL << "Image provider does not provide a stereo calibration - " << imageProviderInfo.proxy->ice_ids();
            return;
        }

        stereoCalibration = visionx::tools::convert(calib->getStereoCalibration());
        calibration = stereoCalibration->GetLeftCalibration();
    }

    debugDrawerTopic = getTopic<armarx::DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopicName").getValue());
    debugDrawerTopic->clearLayer(layerName);
    if (ikPoseEstimationActive)
    {
        debugDrawerTopic->clearLayer(robotLayerName);

        // 3DPoseEstimation with IK
        robotScaling = 1.6;
        mmmRobotFilePath = getProperty<std::string>("MMM_ModelPath").getValue();
        mmmRobot = VirtualRobot::RobotIO::loadRobot(mmmRobotFilePath);
        mmmRobot = mmmRobot->clone("ScaledRobot", VirtualRobot::CollisionCheckerPtr(), robotScaling);
        if (!mmmRobot)
        {
            ARMARX_ERROR << "Failed to load robot: " << mmmRobotFilePath;
            return;
        }

        // test
        ikPoseEstimation.reset(new IKPoseEstimation(mmmRobot));
    }

    // Trying to access robotStateComponent if it is avaiable
    robotStateInterface = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue(), false, "", false);
    if (robotStateInterface)
    {
        localRobot = RemoteRobot::createLocalClone(robotStateInterface);
        ARMARX_IMPORTANT << "RobotStateComponent available";
    }

    if (getProperty<bool>("ActivateOnStartup").getValue())
    {
        running2D = true;
        task2DKeypoints = new RunningTask<OpenPoseEstimation>(this, &OpenPoseEstimation::run);
        task2DKeypoints->start();
        if (!stereo)
        {
            running3D = true;
            task3DKeypoints = new RunningTask<OpenPoseEstimation>(this, &OpenPoseEstimation::run3DKeypoints);
            task3DKeypoints->start();
        }
    }
    else
    {
        running2D = false;
        running3D = false;
    }
}

void OpenPoseEstimation::onDisconnectImageProcessor()
{
    running2D = false;
    task2DKeypoints->stop();
    running3D = false;
    task3DKeypoints->stop();

    delete rgbImageBuffer;
    delete depthImageBuffer;
    delete[] imageBuffer;
    delete[] openPoseResultImage;
    delete stereoCalibration;
}

void OpenPoseEstimation::onExitImageProcessor()
{
}

void OpenPoseEstimation::process()
{
    if (running2D)
    {
        if (!waitForImages(providerName))
        {
            ARMARX_WARNING << "Timeout or error in wait for images";
        }
        else
        {
            if (getImages(providerName, imageBuffer, imageMetaInfo) != numImages)
            {
                ARMARX_WARNING << "Unable to transfer or read images";
                return;
            }
            else
            {
                {
                    ScopedLock lock_rgb(rgbImageBufferMutex);
                    ::ImageProcessor::CopyImage(imageBuffer[0], rgbImageBuffer);
                }

                if (numImages == 2)
                {
                    {
                        ScopedLock lock_depth(depthImageBufferMutex);
                        ::ImageProcessor::CopyImage(imageBuffer[1], depthImageBuffer);
                    }
                }
                timeProvidedImage = imageMetaInfo->timeProvided;
                imageUpdated = true;
            }
        }
    }
}

void OpenPoseEstimation::run()
{
    setupOpenPoseEnvironment();
    ARMARX_IMPORTANT << "Openpose-Environment started -- " << running2D << " " << running3D;

    while (running2D && task2DKeypoints->isRunning())
    {
        if (!imageUpdated)
        {
            usleep(100);
            continue;
        }
        else
        {
            imageUpdated = false;
            //            ARMARX_INFO << deactivateSpam(0.5) << "processing image";
            if (!stereo)
            {
                {
                    ScopedLock lock_rgb(rgbImageBufferMutex);
                    // remove pixels that are far away
                    maskOutBasedOnDepth(*rgbImageBuffer, getProperty<int>("MaxDepth").getValue());
                    PoseKeypoints points = this->getPoseKeypoints(rgbImageBuffer);

                    // Topics
                    auto keypoints = generate2DKeypoints(points, *rgbImageBuffer);
                    last2DKeypoints_labeled = keypoints.first;
                    last2DKeypoints_ordered = keypoints.second;
                    this->keypoints2DUpdated = true;
                    if (!reportOnlyNearestPerson)
                    {
                        ARMARX_VERBOSE << deactivateSpam(0.5) << "Reporting 2Dkeypoints for " << last2DKeypoints_labeled.size() << " persons";
                        listener2DPrx->report2DKeypoints(last2DKeypoints_labeled, timeProvidedImage);
                        listener2DPrx->report2DKeypointsNormalized(normalizeKeypoints2D(keypoints.first), timeProvidedImage);
                    }
                    {
                        ScopedLock lock(resultImageBufferMutex);
                        render2DResultImage(*rgbImageBuffer, points, *openPoseResultImage[0]);
                    }
                }
                provideResultImages(openPoseResultImage);
            }
            else
            {
                Keypoint2DMatchedList matchedPersons;

                {
                    ScopedLock lock_rgb(rgbImageBufferMutex);
                    ScopedLock lock_depth(depthImageBufferMutex);
                    PoseKeypoints pointsLeft = getPoseKeypoints(rgbImageBuffer);
                    auto keypoints2DLeft = generate2DKeypoints(pointsLeft, *rgbImageBuffer);
                    PoseKeypoints pointsRight = getPoseKeypoints(depthImageBuffer);
                    auto keypoints2DRight = generate2DKeypoints(pointsRight, *depthImageBuffer);
                    {
                        ScopedLock lock(resultImageBufferMutex);
                        render2DResultImage(*rgbImageBuffer, pointsLeft, *openPoseResultImage[0]);
                    }

                    // Report 2DPoseKeypoints of one image
                    last2DKeypoints_labeled = keypoints2DLeft.first;
                    last2DKeypoints_ordered = keypoints2DLeft.second;
                    ARMARX_VERBOSE << deactivateSpam(0.5) << "Reporting 2Dkeypoints for " << last2DKeypoints_labeled.size() << " persons";
                    listener2DPrx->report2DKeypoints(last2DKeypoints_labeled, imageMetaInfo->timeProvided);
                    listener2DPrx->report2DKeypointsNormalized(normalizeKeypoints2D(keypoints2DLeft.first), timeProvidedImage);
                    // Match persons
                    matchedPersons = matchPersons(keypoints2DLeft.second, keypoints2DRight.second);
                }

                std::stringstream ssM;
                for (auto& pair : matchedPersons)
                {
                    Keypoint2DList left = pair.first;
                    Keypoint2DList right = pair.second;

                    ssM << "Person \n" << "Left ------------------ | Right ----------------\n";

                    for (unsigned int i = 0; i < left.size(); i++)
                    {
                        ssM << left.at(i).x << " " << left.at(i).y << " | " << right.at(i).x << " " << right.at(i).y << "\n";
                    }
                }
                ARMARX_DEBUG << ssM.str();


                // Calculate 3DPoseKeypoints
                generate3DKeypointsFromStereoImage(matchedPersons);
                // Report 3DPoseKeypoints
                listener3DPrx->report3DKeypoints(last3DKeypoints_labeled, timeProvidedImage);
                visualize3DKeypoints();

                std::stringstream ss2;
                for (Keypoint3DMap& p : last3DKeypoints_labeled)
                {
                    ss2 << "Person\n";
                    for (auto& iter : p)
                    {
                        ss2 << iter.first << " -- " << iter.second.x << " " << iter.second.y << " " << iter.second.z << " " << iter.second.confidence << "\n";
                    }
                }
                ARMARX_INFO << ss2.str();
                provideResultImages(openPoseResultImage);

            }
        }
    }
    ARMARX_INFO << "run finished";
}

void OpenPoseEstimation::run3DKeypoints()
{
    // setup
    if (lastGlobalPose.isZero())
    {
        lastGlobalPose = Eigen::Matrix4f::Identity();
    }

    ARMARX_DEBUG << "run3DKeypoints";

    while (running3D && task3DKeypoints->isRunning())
    {
        if (!keypoints2DUpdated)
        {
            usleep(100);
            continue;
        }
        else
        {
            if (robotStateInterface)
            {
                RemoteRobot::synchronizeLocalClone(localRobot, robotStateInterface->getSynchronizedRobot());
            }

            keypoints2DUpdated = false;

            generate3DKeypointsFromDepthImage(last2DKeypoints_ordered);
            ARMARX_VERBOSE << deactivateSpam(0.5) << "Running 3d estimation";

            if (reportOnlyNearestPerson)
            {
                filterToNearestPerson();
            }
            listener3DPrx->report3DKeypoints(last3DKeypoints_labeled, timeProvidedImage);
            visualize3DKeypoints();

            if (ikPoseEstimationActive)
            {
                generateRobotConfiguration();
            }
        }
    }

    ARMARX_DEBUG << "run3DKeypoints finished";
    // Remove alle Layer we might have used
    for (int i = 0; i < MAX_LAYER; i++)
    {
        debugDrawerTopic->removeLayer(layerName + std::to_string(i));
    }
}

void OpenPoseEstimation::setupOpenPoseEnvironment()
{
    TIMING_START(setupOpenPose);

    const op::Point<int> netInputSize = op::flagsToPoint(getProperty<std::string>("OP_net_resolution").getValue(), "-1x368");
    const op::Point<int> outputSize = op::flagsToPoint(getProperty<std::string>("OP_output_resolution").getValue(), "-1x-1");
    int scaleNumber = getProperty<int>("OP_scale_number").getValue();
    double scaleGap = getProperty<double>("OP_scale_gap").getValue();
    scaleAndSizeExtractor = boost::make_shared<op::ScaleAndSizeExtractor>(netInputSize, outputSize, scaleNumber, scaleGap);

    poseModel = op::flagsToPoseModel(getProperty<std::string>("OP_model_pose").getValue());
    cvMatToOpInput = boost::make_shared<op::CvMatToOpInput>(poseModel);
    cvMatToOpOutput.reset(new op::CvMatToOpOutput());
    opOutputToCvMat.reset(new op::OpOutputToCvMat());

    std::string modelFolder = getProperty<std::string>("OP_model_folder").getValue();
    int numGpuStart = getProperty<int>("OP_num_gpu_start").getValue();
    poseExtractorCaffe = boost::make_shared<op::PoseExtractorCaffe>(poseModel, modelFolder, numGpuStart);
    poseExtractorCaffe->initializationOnThread();

    TIMING_END(setupOpenPose);
}

OpenPoseEstimation::PoseKeypoints OpenPoseEstimation::getPoseKeypoints(CByteImage* imageBuffer)
{
    // Step 1 - Convert image to cv::Mat
    IplImage* iplImage = IplImageAdaptor::Adapt(imageBuffer);
    cv::Mat inputImage(iplImage);
    const op::Point<int> imageSize {inputImage.cols, inputImage.rows};

    // Step 2 - Get desired scale sizes
    std::vector<double> scaleInputToNetInputs;
    std::vector<op::Point<int>> netInputSizes;
    double scaleInputToOutput;
    op::Point<int> outputResolution;
    std::tie(scaleInputToNetInputs, netInputSizes, scaleInputToOutput, outputResolution) = scaleAndSizeExtractor->extract(imageSize);

    // Step 3 - Format input image to OpenPose input and output formats
    const auto netInputArray = cvMatToOpInput->createArray(inputImage, scaleInputToNetInputs, netInputSizes);

    // Step 4 - Estimate poseKeypoints
    poseExtractorCaffe->forwardPass(netInputArray, imageSize, scaleInputToNetInputs);
    const PoseKeypoints poseKeypoints = poseExtractorCaffe->getPoseKeypoints();

    return poseKeypoints;
}

void OpenPoseEstimation::render2DResultImage(const CByteImage& inputImage, OpenPoseEstimation::PoseKeypoints& keypoints, CByteImage& resultImage) const
{
    ARMARX_CHECK_EXPRESSION(inputImage.width == resultImage.width);
    ARMARX_CHECK_EXPRESSION(inputImage.height == resultImage.height);
    ::ImageProcessor::CopyImage(&inputImage, &resultImage);
    if (keypoints.getSize().empty())
    {
        return;
    }

    const std::vector<unsigned int>& posePartPairs = op::getPoseBodyPartPairsRender(poseModel);

    // Get colors for points and lines from openpose
    std::vector<float> keypointColors = op::getPoseColors(poseModel);

    const auto thicknessCircleRatio = 1.f / 75.f;
    const auto thicknessLineRatioWRTCircle = 0.75f;
    const auto area = inputImage.width * inputImage.height;
    const unsigned int numberKeypoints = keypoints.getSize(1);

    for (auto person = 0 ; person < keypoints.getSize(0) ; person++)
    {
        const auto personRectangle = op::getKeypointsRectangle(keypoints, person, 0.1f);
        if (personRectangle.area() > 0)
        {
            // define size-dependent variables
            const auto ratioAreas = op::fastMin(1.f, op::fastMax(personRectangle.width / (float)inputImage.width,
                                                personRectangle.height / (float)inputImage.height));
            const auto thicknessRatio = op::fastMax(op::intRound(std::sqrt(area)
                                                    * thicknessCircleRatio * ratioAreas), 2);
            // Negative thickness in ivt::drawCircle means that a filled circle is to be drawn.
            const auto thicknessCircle = op::fastMax(1, (ratioAreas > 0.05f ? thicknessRatio : -1));
            const auto thicknessLine = op::fastMax(1, op::intRound(thicknessRatio * thicknessLineRatioWRTCircle));
            const auto radius = thicknessRatio / 2;

            // Draw Lines
            for (unsigned int i = 0; i < posePartPairs.size(); i = i + 2)
            {
                const auto index1 = (person * numberKeypoints + posePartPairs[i]) * keypoints.getSize(2);
                const auto index2 = (person * numberKeypoints + posePartPairs[i + 1]) * keypoints.getSize(2);

                float x1 = keypoints[index1 + 0];
                float y1 = keypoints[index1 + 1];
                float x2 = keypoints[index2 + 0];
                float y2 = keypoints[index2 + 1];

                if (isKeypoint2DValid(x1, y1) && isKeypoint2DValid(x2, y2))
                {
                    if (keypoints[index1 + 2] > renderThreshold && keypoints[index2 + 2] > renderThreshold)
                    {
                        int colorIndex = posePartPairs[i + 1] * 3;
                        ::PrimitivesDrawer::DrawLine(&resultImage,
                                                     Vec2d({x1, y1}),
                                                     Vec2d({x2, y2}),
                                                     keypointColors[colorIndex + 2],
                                                     keypointColors[colorIndex + 1],
                                                     keypointColors[colorIndex + 0],
                                                     thicknessLine);
                    }
                }
            }

            // Draw points
            for (unsigned int i = 0; i < numberKeypoints; i++)
            {
                const auto index = (person * numberKeypoints + i) * keypoints.getSize(2);
                float x = keypoints[index + 0];
                float y = keypoints[index + 1];

                if (isKeypoint2DValid(x, y) && keypoints[index + 2] > renderThreshold)
                {
                    unsigned int colorIndex = i * 3;

                    ::PrimitivesDrawer::DrawCircle(&resultImage,
                                                   x,
                                                   y,
                                                   radius,
                                                   keypointColors[colorIndex + 2],
                                                   keypointColors[colorIndex + 1],
                                                   keypointColors[colorIndex + 0],
                                                   thicknessCircle);
                }
            }
        }
    }
}

OpenPoseEstimation::Keypoint2DListPair OpenPoseEstimation::generate2DKeypoints(PoseKeypoints& keypoints, const CByteImage& rgbImage) const
{
    if (keypoints.getSize().empty())
    {
        debugDrawerTopic->removeLayer(layerName + std::to_string(layerCounter)); // TODO find better way to clear if no persons are visible
        return std::pair<Keypoint2DMapList, Keypoint2DOrderedList>();
    }
    size_t entities = keypoints.getSize().at(0);
    size_t points = keypoints.getSize().at(1);

    const std::map<unsigned int, std::string>& poseBodyPartMapping = getPoseBodyPartMapping(poseModel);
    ARMARX_CHECK_EXPRESSION(points == poseBodyPartMapping.size() - 1);

    Keypoint2DMapList keypoints_labeled;
    Keypoint2DOrderedList keypoints_ordered;

    for (size_t i = 0; i < entities; i++)
    {
        Keypoint2DMap keypoints2D;
        std::vector<Keypoint2D> keypoints2D_ordered;

        int validKeypoints = 0;
        for (size_t j = 0; j < points; j++)
        {
            Keypoint2D p;
            p.label = poseBodyPartMapping.at(j);
            p.x = keypoints.at({(int)i, (int)j, 0});
            p.y = keypoints.at({(int)i, (int)j, 1});
            p.confidence = keypoints.at({(int)i, (int)j, 2});
            p.dominantColor = getDominantColorOfPatch(rgbImage, Vec2d {p.x, p.y}, rgbImage.width / 50);

            if (isKeypoint2DValid(p))
            {
                validKeypoints++;
            }
            keypoints2D[p.label] = p;
            keypoints2D_ordered.push_back(p);
        }

        if (validKeypoints >= minimalValidKeypoints)
        {
            keypoints_labeled.push_back(keypoints2D);
            keypoints_ordered.push_back(keypoints2D_ordered);
        }
    }

    return std::make_pair(keypoints_labeled, keypoints_ordered);
}

void OpenPoseEstimation::generate3DKeypointsFromDepthImage(Keypoint2DOrderedList& keypoints)
{
    ScopedLock lock(keypoints3DMutex);
    last3DKeypoints_labeled.clear();
    last3DKeypoints_ordered.clear();
    if (keypoints.empty())
    {
        ARMARX_VERBOSE << deactivateSpam(0.5) << "No keypoints";

        return;
    }
    const std::map<unsigned int, std::string>& poseBodyPartMapping = getPoseBodyPartMapping(poseModel);
    ARMARX_CHECK_EXPRESSION(keypoints[0].size() == poseBodyPartMapping.size() - 1);


    for (size_t i = 0; i < keypoints.size(); i++)
    {
        Keypoint3DMap map3D;
        Keypoint3DList keypoints3D_ordered;
        for (size_t j = 0; j < keypoints[i].size(); j++)
        {
            std::string label = poseBodyPartMapping.at(j);
            Keypoint2D keypoint = keypoints[i][j];

            Keypoint3D keypoint3d;
            keypoint3d.dominantColor = keypoint.dominantColor;
            keypoint3d.label = label;
            if (keypoint.confidence == 0.0)
            {
                keypoint3d.x = keypoint.x;
                keypoint3d.y = keypoint.y;
                keypoint3d.z = 0.0;
                keypoint3d.confidence = keypoint.confidence;
            }
            else if (!isKeypoint2DInImage(keypoint))
            {
                keypoint3d.x = 0.0;
                keypoint3d.y = 0.0;
                keypoint3d.z = 0.0;
                keypoint3d.confidence = 0.0;
            }
            else
            {
                // 1. Get corresponding depth value for key point (median around pixel)
                std::vector<int> depths;
                for (int xoffset = -depthMedianRadius; xoffset < depthMedianRadius; xoffset++)
                {
                    int x = ((int) std::round(keypoint.x)) + xoffset;
                    if (x < 0 || x > depthImageBuffer->width)
                    {
                        continue;
                    }
                    for (int yoffset = -depthMedianRadius; yoffset < depthMedianRadius; yoffset++)
                    {
                        int y = ((int) std::round(keypoint.y)) + yoffset;
                        if (y < 0 || y > depthImageBuffer->height)
                        {
                            continue;
                        }

                        // Check whether (x,y) is in circle:
                        if (xoffset * xoffset + yoffset * yoffset <= depthMedianRadius * depthMedianRadius)
                        {
                            size_t pixelPos = 3 * (y * depthImageBuffer->width + x);
                            unsigned int z_value = depthImageBuffer->pixels[pixelPos + 0]
                                                   + (depthImageBuffer->pixels[pixelPos + 1] << 8)
                                                   + (depthImageBuffer->pixels[pixelPos + 2] << 16);
                            if (z_value > 0)
                            {
                                depths.push_back(z_value);
                            }
                        }
                    }
                }

                std::sort(depths.begin(), depths.end());

                int depth = depths.empty() ? 0 : depths[depths.size() / 2];

                // keep in image coordinates for now so that we can remove outlier in depth afterwards
                keypoint3d.x = keypoint.x;
                keypoint3d.y = keypoint.y;
                keypoint3d.z = depth;
                keypoint3d.confidence = keypoint.confidence;
            }

            keypoints3D_ordered.push_back(keypoint3d);
        }

        Ice::IntSeq depths;
        //        depths.reserve(keypoints3D_ordered.size());
        for (auto& keypoint : keypoints3D_ordered)
        {
            if (keypoint.z != 0)
            {
                depths.push_back(keypoint.z);
            }
        }
        std::sort(depths.begin(), depths.end());
        if (!depths.empty())
        {
            const int medianDepth = depths.at(depths.size() / 2);
            //        float z_avg = z_sum / keypoints3D_ordered.size();
            const int depthThreshold = getProperty<int>("MaxDepthDifference").getValue();
            const int lowBoundary = medianDepth - depthThreshold;
            const int highBoundary = medianDepth + depthThreshold;
            auto limitZ = [&](Keypoint3D & keypoint)
            {
                if (keypoint.z > highBoundary)
                {
                    //                    ARMARX_INFO << VAROUT(medianDepth) << VAROUT(highBoundary) << VAROUT(keypoint.z) << VAROUT(depths);
                    keypoint.z = medianDepth;
                }
                else if (keypoint.z < lowBoundary)
                {
                    //                    ARMARX_INFO << VAROUT(medianDepth) << VAROUT(lowBoundary) << VAROUT(keypoint.z) << VAROUT(depths);
                    keypoint.z = medianDepth;
                }
            };
            for (auto& keypoint : keypoints3D_ordered)
            {
                if (!isKeypoint3DValid(keypoint))
                {
                    continue;
                }
                limitZ(keypoint);
            }
        }

        for (auto& keypoint : keypoints3D_ordered)
        {
            // 2. Transform pixel + depth value into world coordinates (functioncall from ivt ccalibration)
            if (!isKeypoint3DValid(keypoint))
            {
                continue;
            }

            Vec2d imagePoint;
            imagePoint.x = keypoint.x;
            imagePoint.y = keypoint.y;
            Vec3d result;
            calibration->ImageToWorldCoordinates(imagePoint, result, (float) keypoint.z, useDistortionParameters);
            keypoint.x = result.x;
            keypoint.y = result.y;
            keypoint.z = result.z;
        }

        filterKeypointsBasedOnWorkspacePolygon(keypoints3D_ordered);


        //        Eigen::MatrixXf keypoints3DMatrix(keypoints3D_ordered.size(), 3);
        //        i = 0;
        //        float z_sum = 0;
        //        for(auto& keypoint : keypoints3D_ordered)
        //        {
        //            z_sum += keypoint.z;
        ////            keypoints3DMatrix(i,0) = keypoint.x;
        ////            keypoints3DMatrix(i,1) = keypoint.y;
        ////            keypoints3DMatrix(i,2) = keypoint.z;
        ////            i++;
        //        }
        //        float z_avg = z_sum/keypoints3D_ordered.size();
        ////        Eigen::Vector3f mean = keypoints3DMatrix.colwise().mean();
        //
        //        float distanceThreshold = 1000.f;
        //        auto limitZ = [&](Keypoint3D &keypoint) {
        //
        //            float diff = std::abs(z_avg - keypoint.z);
        //            if (diff > distanceThreshold) {
        //                if (z_avg > keypoint.z)
        //                    keypoint.z = z_avg + distanceThreshold;
        //                else if (z_avg < keypoint.z)
        //                    keypoint.z = z_avg - distanceThreshold;
        //            }
        //
        //        };
        //        for (auto &keypoint : map3D) {
        //            limitZ(keypoint.second);
        //        }
        //        for (auto &keypoint : keypoints3D_ordered) {
        //            limitZ(keypoint);
        //        }


        for (auto& keypoint : keypoints3D_ordered)
        {
            map3D[keypoint.label] = keypoint;
        }

        // 3. Save 3D-Keypoints
        last3DKeypoints_labeled.push_back(map3D);
        last3DKeypoints_ordered.push_back(keypoints3D_ordered);

    }
    ARMARX_VERBOSE << deactivateSpam(1) << "ordered 3d persons: " << last3DKeypoints_ordered.size();
}

void OpenPoseEstimation::generateRobotConfiguration()
{
    if (last3DKeypoints_labeled.empty() || !ikPoseEstimation)
    {
        return;
    }

    Keypoint3DMap keypointHints = last3DKeypoints_labeled[0];

    //    this->ikPoseEstimation->estimate3DConfiguration(mmmRobot, keypointHints, generateGlobalPoseHint());

    ikPoseEstimation->estimateGlobalPose(mmmRobot, keypointHints, generateGlobalPoseHint());

    // Draw Robot
    //    debugDrawerTopic->setRobotVisuWithScaling(robotLayerName, "IKResult", this->mmmRobotFilePath, "RobotAPI", FullModel, this->robotScaling);

    auto robotConfigMap = mmmRobot->getConfig()->getRobotNodeJointValueMap();
    debugDrawerTopic->updateRobotConfig(robotLayerName, "IKResult", robotConfigMap);
    debugDrawerTopic->updateRobotPose(robotLayerName, "IKResult", new Pose(mmmRobot->getGlobalPose()));

    lastGlobalPose = mmmRobot->getGlobalPose();
}

Eigen::Matrix4f OpenPoseEstimation::generateGlobalPoseHint() const
{
    return this->lastGlobalPose;
}


OpenPoseEstimation::Keypoint2DMatchedList OpenPoseEstimation::matchPersons(OpenPoseEstimation::Keypoint2DOrderedList& leftPersons, OpenPoseEstimation::Keypoint2DOrderedList& rightPersons) const
{
    // For every person in the left image, we need to calculate the distance of all keypoints to their corresponding keypoints of every person in the right image
    // This results in a sum for each possible person-pair, which is the distance between two persons
    // The persons in both images then have to be paired in such a way that the sum of distances between the two persons in a pair is minimized.

    ARMARX_DEBUG << VAROUT(leftPersons.size()) << " " << VAROUT(rightPersons.size());

    if (leftPersons.size() == 0 || rightPersons.size() == 0)
    {
        return Keypoint2DMatchedList();
    }

    Keypoint2DOrderedList morePersons;
    Keypoint2DOrderedList lessPersons;
    if (leftPersons.size() > rightPersons.size())
    {
        morePersons = leftPersons;
        lessPersons = rightPersons;
    }
    else
    {
        morePersons = rightPersons;
        lessPersons = leftPersons;
    }

    Keypoint2DMatchedList result;
    if (leftPersons.size() == 1 && rightPersons.size() == 1)
    {
        result.push_back(std::make_pair(leftPersons.at(0), rightPersons.at(0)));
        return result;
    }

    Eigen::MatrixXf personDistances(lessPersons.size(), morePersons.size()); // rows = lessPersons; cols = morePersons
    Eigen::MatrixXf validKeypointsRatio(lessPersons.size(), morePersons.size()); // to prevent, that artifacts of the openpose algorithm are used in the pairs...

    for (unsigned int l = 0; l < lessPersons.size(); l++)
    {
        Keypoint2DList pl = lessPersons.at(l);
        for (unsigned int r = 0; r < morePersons.size(); r++)
        {
            Keypoint2DList pr = morePersons.at(r);
            float sum = 0.0;
            int addedDistances = 0;
            int validKeypointsPL = 0;
            int validKeypointsPR = 0;
            for (unsigned int i = 0; i < pl.size(); i++)
            {
                Keypoint2D kpl = pl.at(i);
                Keypoint2D kpr = pr.at(i);

                // Calculate ratio of valid keypoints
                if (isKeypoint2DValid(kpl))
                {
                    validKeypointsPL++;
                }
                if (isKeypoint2DValid(kpr))
                {
                    validKeypointsPR++;
                }

                if (isKeypoint2DValid(kpl) && isKeypoint2DValid(kpr))
                {
                    sum += std::pow(kpl.x - kpr.x, 2) + std::pow(kpl.y - kpr.y, 2); // squared euclidean distance
                    addedDistances++;
                }
            }
            personDistances(l, r) = (addedDistances == 0) ? std::numeric_limits<float>::max() : sum / (float) addedDistances;

            // Calculate ratio of valid keypoints
            if (validKeypointsPL == 0 || validKeypointsPR == 0)
            {
                validKeypointsRatio(l, r) = 0.000000001; // simply a very small number for a very bad ratio (ratio cannot be 0, because it is needed for a division later)
            }
            else if (validKeypointsPL > validKeypointsPR)
            {
                validKeypointsRatio(l, r) = (float) validKeypointsPR / (float) validKeypointsPL;
            }
            else
            {
                validKeypointsRatio(l, r) = (float) validKeypointsPL / (float) validKeypointsPR;
            }
        }
    }

    // Finding best pairs by generating permutations of the list with more persons and comparing the resulting sum of distances.

    std::vector<int> numbers(morePersons.size());
    std::iota(numbers.begin(), numbers.end(), 0);

    std::vector<int> bestPermutation;
    float currentMin = std::numeric_limits<float>::max();
    do
    {
        float sum = 0.0;
        for (unsigned int i = 0; i < lessPersons.size(); i++)
        {
            sum += personDistances(i, numbers.at(i)) / std::pow(validKeypointsRatio(i, numbers.at(i)), 3); // distances with bad ratios will be bigger than distances with good ratios
        }
        if (sum < currentMin)
        {
            currentMin = sum;
            bestPermutation = numbers;
        }
        std::reverse(numbers.begin() + lessPersons.size(), numbers.end()); // we only need permutations of the form (morePersons choose lessPersons)
    }
    while (std::next_permutation(numbers.begin(), numbers.end()));

    // Create pairs
    if (!bestPermutation.empty())
    {
        for (unsigned int i = 0; i < lessPersons.size(); i++)
        {
            Keypoint2DList left = (leftPersons.size() > rightPersons.size()) ? morePersons.at(bestPermutation.at(i)) : lessPersons.at(i);
            Keypoint2DList right = (leftPersons.size() > rightPersons.size()) ? lessPersons.at(i) : morePersons.at(bestPermutation.at(i));
            result.push_back(std::make_pair(left, right));
        }
    }
    return result;
}

void OpenPoseEstimation::generate3DKeypointsFromStereoImage(OpenPoseEstimation::Keypoint2DMatchedList& matchedKeypoints)
{
    ARMARX_CHECK_EXPRESSION(stereo);

    if (matchedKeypoints.empty())
    {
        return;
    }

    const std::map<unsigned int, std::string>& poseBodyPartMapping = getPoseBodyPartMapping(poseModel);
    ARMARX_CHECK_EXPRESSION(matchedKeypoints[0].first.size() == poseBodyPartMapping.size() - 1);

    ScopedLock lock(keypoints3DMutex);
    last3DKeypoints_labeled.clear();
    last3DKeypoints_ordered.clear();

    for (std::pair<Keypoint2DList, Keypoint2DList> pair : matchedKeypoints)
    {
        Keypoint2DList leftKeypoints = pair.first;
        Keypoint2DList rightKeypoints = pair.second;

        Keypoint3DMap map3D;
        Keypoint3DList keypoints3D_ordered;
        for (unsigned int i = 0; i < leftKeypoints.size(); i++)
        {
            std::string label = poseBodyPartMapping.at(i);

            Keypoint2D pointL = leftKeypoints.at(i);
            Keypoint2D pointR = rightKeypoints.at(i);

            Keypoint3D keypoint3d;
            keypoint3d.dominantColor = pointL.dominantColor;
            keypoint3d.label = label;
            if ((pointL.confidence == 0.0 || pointR.confidence == 0.0) || (!isKeypoint2DValid(pointL) || !isKeypoint2DValid(pointR)))
            {
                keypoint3d.x = 0.0;
                keypoint3d.y = 0.0;
                keypoint3d.z = 0.0;
                keypoint3d.confidence = 0.0;
            }
            else
            {
                const Vec2d imagePointL = {pointL.x, pointL.y};
                const Vec2d imagePointR = {pointR.x, pointR.y};
                ARMARX_DEBUG << "PointL: " << imagePointL.x << " " << imagePointL.y;
                ARMARX_DEBUG << "PointR: " << imagePointR.x << " " << imagePointR.y;
                Vec3d result;
                PointPair3d* rD = new PointPair3d();
                stereoCalibration->Calculate3DPoint(imagePointL, imagePointR, result, false, useDistortionParameters, rD);
                ARMARX_DEBUG << "IVT- ConnectionLine: " << rD->p1.x << " " << rD->p1.y << " " << rD->p1.z << " --- " << rD->p2.x << " " << rD->p2.y << " " << rD->p2.z;

                keypoint3d.x = result.x;
                keypoint3d.y = result.y;
                keypoint3d.z = result.z;
                keypoint3d.confidence = pointL.confidence / 2.0 + pointR.confidence / 2.0; // for now simply the average of the two confidences
                ARMARX_DEBUG << "3D-Keypoint: " << keypoint3d.x << " " << keypoint3d.y << " " << keypoint3d.z;
            }

            keypoints3D_ordered.push_back(keypoint3d);
        }

        filterKeypointsBasedOnWorkspacePolygon(keypoints3D_ordered);
        for (auto& keypoint : keypoints3D_ordered)
        {
            map3D[keypoint.label] = keypoint;
        }

        last3DKeypoints_labeled.push_back(map3D);
        last3DKeypoints_ordered.push_back(keypoints3D_ordered);
    }
}

void OpenPoseEstimation::filterToNearestPerson()
{
    ScopedLock lock(keypoints3DMutex);

    ARMARX_CHECK_EXPRESSION(last3DKeypoints_ordered.size() == last3DKeypoints_labeled.size());
    if (last3DKeypoints_ordered.size() < 1)
    {
        ARMARX_VERBOSE << deactivateSpam(0.5) << "No ordered 3d keypoints";
        return;
    }
    if (!localRobot)
    {
        ARMARX_VERBOSE << deactivateSpam(0.5) << "no robot";

        return;
    }

    float lowestDistance = std::numeric_limits<float>::max();
    unsigned int nearestPerson = 0;
    for (unsigned int i = 0; i < last3DKeypoints_ordered.size(); i++)
    {
        Keypoint3DList& points = last3DKeypoints_ordered.at(i);

        // Calculate average distance of current person:
        int validPoints = 0;
        float dist = 0.0;
        for (Keypoint3D& p : points)
        {
            if (isKeypoint3DValid(p))
            {
                validPoints++;

                Eigen::Vector3f f;
                f << p.x, p.y, p.z;
                dist += f.squaredNorm() / 1000000.0f; // to avoid overflow, conversion from mm to m
            }
        }
        dist /= (float) validPoints;

        if (dist < lowestDistance)
        {
            lowestDistance = dist;
            nearestPerson = i;
        }
    }

    // Delete every person except nearest in both last3DKeypoints_ordered AND last3DKeypoints_labeled
    // Is there a more elegant solution??
    Keypoint3DOrderedList newOrdered;
    newOrdered.push_back(last3DKeypoints_ordered.at(nearestPerson));
    Keypoint3DMapList newLabeled;
    newLabeled.push_back(last3DKeypoints_labeled.at(nearestPerson));
    last3DKeypoints_ordered = newOrdered;
    last3DKeypoints_labeled = newLabeled;

    // Report 2D keypoints for nearest person only
    {
        Keypoint2DMapList keypoints2D;
        // This relies on the indices in 2D and 3D being equal
        keypoints2D.push_back(last2DKeypoints_labeled.at(nearestPerson));
        ARMARX_INFO << deactivateSpam(0.5) << "Reporting 2Dkeypoints only nearest person";
        listener2DPrx->report2DKeypoints(keypoints2D, timeProvidedImage);
        listener2DPrx->report2DKeypointsNormalized(normalizeKeypoints2D(keypoints2D), timeProvidedImage);
    }
}

Keypoint3D OpenPoseEstimation::transform3DKeypointToGlobal(Keypoint3D& keypoint) const
{
    if (!localRobot || !isKeypoint3DValid(keypoint))
    {
        return keypoint;
    }

    Eigen::Vector3f pos;
    pos << keypoint.x, keypoint.y, keypoint.z;
    FramedPositionPtr keypointPosition = new FramedPosition(pos, cameraNodeName, localRobot->getName());
    keypointPosition->changeToGlobal(localRobot);

    Keypoint3D result = keypoint;
    result.x = keypointPosition->x;
    result.y = keypointPosition->y;
    result.z = keypointPosition->z;
    return result;
}

void OpenPoseEstimation::filterKeypointsBasedOnWorkspacePolygon(Keypoint3DList& list) const
{
    if (list.empty())
    {
        return;
    }
    for (Keypoint3D& p : list)
    {
        if (!workspacePolygon.isInside(transform3DKeypointToGlobal(p)))
        {
            ARMARX_VERBOSE << deactivateSpam(1) << "removing person due out of workspace";
            p.x = 0.0;
            p.y = 0.0;
            p.z = 0.0;
        }
    }
    ARMARX_VERBOSE << deactivateSpam(1) << "remaining persons: " << list.size();
}

void OpenPoseEstimation::visualize3DKeypoints()
{
    ScopedLock lock(keypoints3DMutex);
    debugDrawerTopic->removeLayer(layerName + std::to_string(layerCounter));

    if (last3DKeypoints_ordered.empty())
    {
        return;
    }
    const std::map<unsigned int, std::string>& poseBodyPartMapping = getPoseBodyPartMapping(poseModel);
    ARMARX_CHECK_EXPRESSION(last3DKeypoints_ordered[0].size() == poseBodyPartMapping.size() - 1);
    const std::vector<unsigned int>& posePartPairs = op::getPoseBodyPartPairsRender(poseModel);

    // Get colors for points and lines from openpose
    std::vector<float> keypointColors = op::getPoseColors(poseModel);

    layerCounter = (layerCounter == MAX_LAYER - 1) ? 0 : layerCounter + 1;

    int personIndex = 0;
    for (Keypoint3DList& person : last3DKeypoints_ordered)
    {
        // DrawLines
        for (unsigned int i = 0; i < posePartPairs.size(); i = i + 2)
        {
            Keypoint3D point1 = transform3DKeypointToGlobal(person[posePartPairs[i]]);
            Keypoint3D point2 = transform3DKeypointToGlobal(person[posePartPairs[i + 1]]);

            if (isKeypoint3DValid(point1) && isKeypoint3DValid(point2))
            {
                std::string name = poseBodyPartMapping.at(posePartPairs[i]) + " - " + poseBodyPartMapping.at(posePartPairs[i + 1]);
                int colorIndex = posePartPairs[i + 1] * 3;
                //                auto color = DrawColor {0, 1.0, 0, 1.0f};
                DrawColor color({keypointColors[colorIndex + 2] / 255.f, keypointColors[colorIndex + 1] / 255.f, keypointColors[colorIndex] / 255.f, 1.0f});
                debugDrawerTopic->setLineVisu(layerName + std::to_string(layerCounter), name + std::to_string(personIndex), new Vector3(point1.x, point1.y, point1.z), new Vector3(point2.x, point2.y, point2.z), 10, color);
            }
        }
        // Draw points
        for (unsigned int i = 0; i < person.size(); i++)
        {
            Keypoint3D point = transform3DKeypointToGlobal(person[i]);

            if (isKeypoint3DValid(point))
            {
                //                int colorIndex = i * 3;
                auto color24Bit = point.dominantColor;
                auto color = DrawColor {0.0039215f * color24Bit.b, 0.0039215f * color24Bit.g, 0.0039215f * color24Bit.r, 1.0f};
                //                                auto color = DrawColor {0, 0, 1, 1.0f};
                //                DrawColor color({keypointColors[colorIndex] / 255.f, keypointColors[colorIndex + 1] / 255.f, keypointColors[colorIndex + 2] / 255.f, 1.0f});
                debugDrawerTopic->setSphereVisu(layerName + std::to_string(layerCounter), poseBodyPartMapping.at(i) + std::to_string(personIndex), new Vector3(point.x, point.y, point.z), color, 20);
            }
        }
        personIndex++;
    }
}

Keypoint2DMapList OpenPoseEstimation::normalizeKeypoints2D(Keypoint2DMapList& keypoints) const
{
    Keypoint2DMapList newList = keypoints;
    int width = imageProviderInfo.imageFormat.dimension.width;
    int height = imageProviderInfo.imageFormat.dimension.height;

    for (Keypoint2DMap map : newList)
    {
        for (auto pair : map)
        {
            Keypoint2D point = pair.second;
            if (point.x < 0.0 || point.x > width || point.y < 0.0 || point.y > height)
            {
                point.x = 0.0;
                point.y = 0.0;
            }
            else
            {
                point.x = point.x / (float) width;
                point.y = point.y / (float) height;
            }
        }
    }
    return newList;
}

void OpenPoseEstimation::start(const Ice::Current&)
{
    if (running2D)
    {
        return;
    }
    else
    {
        ARMARX_DEBUG << "Starting OpenposeEstimation";
        running2D = true;
        task2DKeypoints = new RunningTask<OpenPoseEstimation>(this, &OpenPoseEstimation::run);
        task2DKeypoints->start();
    }
}

void OpenPoseEstimation::stop(const Ice::Current&)
{
    if (running2D)
    {
        ARMARX_DEBUG << "Stopping OpenposeEstimation";
        running2D = false;
        task2DKeypoints->stop(true);
        stop3DPoseEstimation();
    }
}

void OpenPoseEstimation::start3DPoseEstimation(const Ice::Current&)
{
    if (running3D)
    {
        return;
    }
    else
    {
        ARMARX_DEBUG << "Starting OpenposeEstimation -- 3D";

        if (!stereo)
        {
            running3D = true;
            task3DKeypoints = new RunningTask<OpenPoseEstimation>(this, &OpenPoseEstimation::run3DKeypoints);
            task3DKeypoints->start();
        }
        start();
    }
}

void OpenPoseEstimation::stop3DPoseEstimation(const Ice::Current&)
{
    if (running3D)
    {
        ARMARX_DEBUG << "Stopping OpenposeEstimation -- 3D";
        running3D = false;
        task3DKeypoints->stop(false);

        // Remove alle Layer we might have used
        for (int i = 0; i < MAX_LAYER; i++)
        {
            debugDrawerTopic->removeLayer(layerName + std::to_string(i));
        }
    }
}


void OpenPoseEstimation::maskOutBasedOnDepth(CByteImage& image, unsigned int maxDepth)
{
    ScopedLock lock(depthImageBufferMutex);

    if (maxDepth <= 0)
    {
        return;
    }
    int pixelCount = depthImageBuffer->width * depthImageBuffer->height;
    unsigned int depthThresholdmm = maxDepth;
    CByteImage maskImage(depthImageBuffer->width, depthImageBuffer->height, CByteImage::eGrayScale);
    for (int i = 0; i < pixelCount; i += 1)
    {
        unsigned int z_value = depthImageBuffer->pixels[i * 3 + 0]
                               + (depthImageBuffer->pixels[i * 3 + 1] << 8)
                               + (depthImageBuffer->pixels[i * 3 + 2] << 16);
        maskImage.pixels[i] = z_value > depthThresholdmm ||  z_value == 0 ? 0 : 255;

    }
    ::ImageProcessor::Erode(&maskImage, &maskImage, 5);
    ::ImageProcessor::Dilate(&maskImage, &maskImage, 20);
    for (int i = 0; i < pixelCount; i += 1)
    {
        if (maskImage.pixels[i] == 0)
        {
            image.pixels[i * 3] = 0;
            image.pixels[i * 3 + 1] = 255;
            image.pixels[i * 3 + 2] = 0;

        }
    }
}

DrawColor24Bit OpenPoseEstimation::getDominantColorOfPatch(const CByteImage& image, const Vec2d& point, int windowSize) const
{
    if (point.x < 0  || point.y < 0 || point.x >= image.width || point.y >= image.height)
        return DrawColor24Bit {0, 0, 0}; // Black
    int divisor = 256 / 3; // split into 3*3*3 bins
    typedef std::tuple<Ice::Byte, Ice::Byte, Ice::Byte> RGBTuple;
    std::map<RGBTuple, int> histogram;
    std::vector<int> histogramHSV(360, 0.0f);
    std::vector<float> histogramVoting(360, 0.0f);
    int halfWindowSize = windowSize * 0.5;
    int left = std::max<int>(0, point.x - halfWindowSize);
    int top = std::max<int>(0, point.y - halfWindowSize);
    int right = std::min<int>(image.width, point.x + halfWindowSize);
    int bottom = std::min<int>(image.height, point.y + halfWindowSize);

    for (int x = left; x < right; x++)
    {
        for (int y = top; y < bottom; y++)
        {
            int pixelPos = (y * image.width + x) * 3;
            //            int colorValue = image.pixels[pixelPos] / 32 +
            //                             (image.pixels[pixelPos + 1] / 32 << 3) +
            //                             (image.pixels[pixelPos + 2] / 32 << 6);
            //                        auto hsv = colorutils::RgbToHsv(DrawColor24Bit {image.pixels[pixelPos],
            //                                                        image.pixels[pixelPos + 1],
            //                                                        image.pixels[pixelPos + 2]
            //                                                                       });

            //            int hue = (int)std::round(hsv.h) % 360;
            //            histogramHSV.at(hue)++;
            auto tuple = std::make_tuple<Ice::Byte, Ice::Byte, Ice::Byte>(image.pixels[pixelPos] / divisor, image.pixels[pixelPos + 1] / divisor, image.pixels[pixelPos + 2] / divisor);
            if (histogram.count(tuple))
            {
                histogram.at(tuple)++;
            }
            else
            {
                histogram[tuple] = 1;
            }
        }
    }
    //
    //        double filterRadius = 10;
    //        double sigma = filterRadius / 2.5;
    //        double sigmaSquare = sigma * sigma;
    //        auto gaussianBellCurve = [&](double x)
    //        {
    //            if(x >= 360)
    //                x -= 360;
    //            return exp(-double((x) * (x)) / (sigmaSquare));
    //        };
    //        float maxSmoothedHistogramValue = 0;
    //        int dominantHue = 0;
    //        for (size_t i = 0; i < histogramVoting.size(); ++i)
    //        {
    //            for (size_t j = 0; j < histogramHSV.size(); ++j)
    //            {
    //                histogramVoting.at(i) += gaussianBellCurve(i - j) * histogramHSV.at(j);
    //            }
    //            if (histogramVoting.at(i) > maxSmoothedHistogramValue)
    //            {
    //                dominantHue = i;
    //                maxSmoothedHistogramValue = histogramVoting.at(i);
    //            }
    //        }


    //    double filterRadius = 10;
    //    double sigma = filterRadius / 2.5;
    //    double sigmaSquare = sigma * sigma;
    //    auto gaussianBellCurve = [&](double x)
    //    {
    //        return exp(-double((x) * (x)) / (sigmaSquare));
    //    };
    //    float maxSmoothedHistogramValue = 0;
    //    int dominantHue = 0;
    //    for (size_t i = 0; i < histogramVoting.size(); ++i)
    //    {
    //        for (size_t j = 0; j < histogram.size(); ++j)
    //        {
    //            histogramVoting.at(i) += gaussianBellCurve(i - j) * histogram.at(j);
    //        }
    //        if (histogramVoting.at(i) > maxSmoothedHistogramValue)
    //        {
    //            dominantHue = i;
    //            maxSmoothedHistogramValue = histogramVoting.at(i);
    //        }
    //    }
    float maxHistogramValue = 0;
    RGBTuple dominantColor;
    for (auto& pair : histogram)
    {
        if (pair.second > maxHistogramValue)
        {
            dominantColor = pair.first;
            maxHistogramValue = pair.second;
        }
    }
    //    auto rgb = colorutils::HsvToRgb(HsvColor{dominantHue, 1, 1});
    auto rgb = DrawColor24Bit {(Ice::Byte)(std::get<0>(dominantColor) * divisor) , (Ice::Byte)(std::get<1>(dominantColor) * divisor), (Ice::Byte)(std::get<2>(dominantColor) * divisor)};
    //    ::PrimitivesDrawer::DrawRectangle(openPoseResultImage[0],Rectangle2d{point, windowSize, windowSize, 0}, rgb.r, rgb.g, rgb.b);
    return rgb;

}
