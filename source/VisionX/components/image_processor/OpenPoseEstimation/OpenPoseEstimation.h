/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OpenPoseEstimation
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/Component.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/components/OpenPoseEstimationInterface.h>
#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/components/RobotState/RobotStateComponent.h>

#include <VirtualRobot/Robot.h>
#include "IKPoseEstimation.h"

#include <Calibration/Calibration.h>
#include <Calibration/StereoCalibration.h>

// OpenPose dependencies
#include <openpose/core/headers.hpp>
#include <openpose/filestream/headers.hpp>
#include <openpose/gui/headers.hpp>
#include <openpose/pose/headers.hpp>
#include <openpose/utilities/headers.hpp>

#ifdef _OPENPOSE_MODELS
#define _modelFolder _OPENPOSE_MODELS
#else
#define _modelFolder ""
#endif

namespace armarx
{
    /**
     * @class OpenPoseEstimationPropertyDefinitions
     * @brief
     */
    class OpenPoseEstimationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        OpenPoseEstimationPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("OpenPoseEstimation2DTopicName", "OpenPoseEstimation2D");
            defineOptionalProperty<std::string>("OpenPoseEstimation3DTopicName", "OpenPoseEstimation3D");
            defineOptionalProperty<int>("DepthMedianRadius", 10, "Radius of the circle around a target pixel in the depth image, which is used to calculate the median around that pixel.");
            defineOptionalProperty<bool>("UseDistortionParameters", false, "Whether to use distortion parameters when transforming image coordinates into world coordinates");

            defineRequiredProperty<std::string>("ImageProviderName", "Name of the ImageProviderComponent");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic that should be used");

            defineOptionalProperty<bool>("3DConstructionWithStereo", false, "If true the imageProcessor expects two images as a stereo input and the calculation of the 3D-values"
                                         "will be done without a depthimage.");

            defineOptionalProperty<bool>("IKPoseEstimationActive", false, "Whether the ikPoseEstimation with the mmm-model is active.");
            defineRequiredProperty<std::string>("MMM_ModelPath", "Path to xml-file of MMM-model");

            defineOptionalProperty<std::string>("OP_net_resolution", "-1x368", "Multiples of 16. If it is increased, the accuracy potentially increases. If it is "
                                                "decreased, the speed increases. For maximum speed-accuracy balance, it should keep the "
                                                "closest aspect ratio possible to the images or videos to be processed.\n Using `-1` in "
                                                "any of the dimensions, OP will choose the optimal aspect ratio depending on the user's "
                                                "input value.\n E.g. the default `-1x368` is equivalent to `656x368` in 16:9 resolutions, "
                                                "e.g. full HD (1980x1080) and HD (1280x720) resolutions.");
            defineOptionalProperty<std::string>("OP_output_resolution", "-1x-1", "The image resolution (display and output). Use \"-1x-1\" to force the program to use the"
                                                " input image resolution.");
            defineOptionalProperty<double>("OP_scale_gap", 0.3, "Scale gap between scales. No effect unless scale_number > 1. Initial scale is always 1. "
                                           "If you want to change the initial    calib->get scale, you actually want to multiply the "
                                           "`net_resolution` by your desired initial scale.");
            defineOptionalProperty<int>("OP_scale_number", 1, "Number of scales to average.");
            defineOptionalProperty<std::string>("OP_model_pose", "COCO", "Model to be used. E.g. `COCO` (18 keypoints), `MPI` (15 keypoints, ~10% faster), "
                                                "MPI_4_layers` (15 keypoints, even faster but less accurate).");
            defineOptionalProperty<std::string>("OP_model_folder", _modelFolder, "Folder path (absolute or relative) where the models (pose, face, ...) are located.");
            defineOptionalProperty<int>("OP_num_gpu_start", 0, "GPU device start number.");
            defineOptionalProperty<float>("OP_render_threshold", 0.05, "Only estimated keypoints whose score confidences are higher than this threshold will be"
                                          " rendered.\n Generally, a high threshold (> 0.5) will only render very clear body parts;"
                                          " while small thresholds (~0.1) will also output guessed and occluded keypoints, but also"
                                          " more false positives (i.e. wrong detections).");
            defineOptionalProperty<int>("MaxDepth", 3000, "Pixels with a distance higher than this value are masked out. Only for depth camera mode.", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<int>("MaxDepthDifference", 700, "Allowed difference of depth value for one keypoint to median of all keypoints.", PropertyDefinitionBase::eModifiable);

            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent");
            defineOptionalProperty<std::string>("CameraNodeName", "DepthCamera", "Name of the robot node for the input camera");
            defineOptionalProperty<std::string>("WorkspacePolygon", "-5000,-5000;5000,-5000;5000,5000;-5000,5000;-5000,-5000", "A string that describes a polygon which represents the workspace of the robot.\n"
                                                "Every keypoint outside of this polygon will be masked out.\n"
                                                "Every point is seperated by a ';' (semicolon) and every point is described as 'x-value, y-value' (comma-seperated).\n"
                                                "The last point must be identical to the first point.");
            defineOptionalProperty<int>("MinimalAmountKeypoints", 5, "Minimal amount of keypoints per person. Detected persons with less valid keypoints will be discarded.");
            defineOptionalProperty<bool>("ReportOnlyNearestPerson", false, "If true, only one person is reported in the 3DTopic. The reported person is the nearest person to the robot.");
            defineOptionalProperty<bool>("ActivateOnStartup", true, "If true, poseEstimation-tasks are started after starting the component. If false, the component idles.");
        }
    };

    /* ------------------------------------
     * Plan for calculating 3DKeypoints with inverse kinematic:
     *
     * - Load MMM-Model as a robot
     * - Add 3 translation and 3 orientation joints that together represent the 'robotpose'
     * - Setup constrainedIk with MMM-model
     * - Define positionConstraints for every valid 2DKeypoint with depth information (= pseudo 3D)
     * ---> Which points are valid?
     * ---> How to setup Constraints? (TSR vs. Position)
     * ---> Account for confidence?
     * - Set the configuration of the prevoius frame as seeds
     * - Find IK-solution and read joint positions as 3DKeypoints
     *
     *
     * TODO:
     *
     ------------------------------------ */

    /**
     * @defgroup Component-OpenPoseEstimation OpenPoseEstimation
     * @ingroup VisionX-Components
     * A description of the component OpenPoseEstimation.
     *
     * @class OpenPoseEstimation
     * @ingroup Component-OpenPoseEstimation
     * @brief Brief description of class OpenPoseEstimation.
     *
     * Detailed description of class OpenPoseEstimation.
     */
    class OpenPoseEstimation :
        public visionx::ImageProcessor,
        public OpenPoseEstimationInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "OpenPoseEstimation";
        }

        void start(const Ice::Current& = GlobalIceCurrent)  override;
        void stop(const Ice::Current& = GlobalIceCurrent)  override;
        void start3DPoseEstimation(const Ice::Current& = GlobalIceCurrent)  override;
        void stop3DPoseEstimation(const Ice::Current& = GlobalIceCurrent)  override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ImageProcessor interface
    protected:
        void onInitImageProcessor() override;
        void onConnectImageProcessor()  override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

        void maskOutBasedOnDepth(CByteImage& image, unsigned int maxDepth);
        DrawColor24Bit getDominantColorOfPatch(const CByteImage& image, const Vec2d& point, int windowSize = 10) const;

    private:
        struct Polygon2D
        {
            struct Point
            {
                float x;
                float y;

                bool operator==(const Point& p) const
                {
                    return this->x == p.x && this->y == p.y;
                }
            };

            typedef std::vector<Point> PointList;
            // ordered list of polygon points
            // assert(points[0] == points[n]
            std::vector<Point> points;

            Polygon2D() {}
            Polygon2D(PointList points)
            {
                ARMARX_CHECK_EXPRESSION(points[0] == points[points.size() - 1]);
                this->points = points;
            }

            // isLeft(): tests if a point is Left|On|Right of an infinite line.
            //    Input:  three points p0, p1, and p2
            //    Return: >0 for p2 left of the line through p0 and p1
            //            =0 for p2  on the line
            //            <0 for p2  right of the line
            int isLeft(Point p0, Point p1, Point p2) const
            {
                return ((p1.x - p0.x) * (p2.y - p0.y)
                        - (p2.x -  p0.x) * (p1.y - p0.y));
            }

            // Algorithm from http://geomalgorithms.com/a03-_inclusion.html#wn_PnPoly() (Date: 05/16/2018)
            bool isInside(Keypoint3D point) const
            {
                Point p;
                p.x = point.x;
                p.y = point.y;
                int    wn = 0;    // the  winding number counter
                // loop through all edges of the polygon
                for (unsigned int i = 0; i < points.size(); i++)  // edge from V[i] to  V[i+1]
                {
                    if (points[i].y <= p.y)            // start y <= P.y
                    {
                        if (points[i + 1].y  > p.y)    // an upward crossing
                            if (isLeft(points[i], points[i + 1], p) > 0) // P left of  edge
                            {
                                ++wn;    // have  a valid up intersect
                            }
                    }
                    else                          // start y > P.y (no test needed)
                    {
                        if (points[i + 1].y  <= p.y)   // a downward crossing
                            if (isLeft(points[i], points[i + 1], p) < 0) // P right of  edge
                            {
                                --wn;    // have  a valid down intersect
                            }
                    }
                }
                return wn == 1;
            }
        };


        typedef op::Array<float> PoseKeypoints;
        typedef std::vector<Keypoint2D> Keypoint2DList;
        typedef std::vector<Keypoint2DList> Keypoint2DOrderedList;
        typedef std::pair<Keypoint2DMapList, Keypoint2DOrderedList> Keypoint2DListPair;
        typedef std::vector<std::pair<Keypoint2DList, Keypoint2DList>> Keypoint2DMatchedList;
        typedef std::vector<Keypoint3D> Keypoint3DList;
        typedef std::vector<Keypoint3DList> Keypoint3DOrderedList;

        std::string providerName;
        visionx::ImageProviderInfo imageProviderInfo;
        CByteImage** imageBuffer;
        CByteImage* rgbImageBuffer;
        armarx::Mutex rgbImageBufferMutex;
        CByteImage* depthImageBuffer;
        armarx::Mutex depthImageBufferMutex;
        armarx::MetaInfoSizeBasePtr imageMetaInfo;
        int numImages;
        const CCalibration* calibration;
        CStereoCalibration* stereoCalibration;

        CByteImage** openPoseResultImage;
        armarx::Mutex resultImageBufferMutex;
        bool imageUpdated;
        bool keypoints2DUpdated;

        armarx::DebugDrawerInterfacePrx debugDrawerTopic;
        std::string layerName;
        int layerCounter = 0;
        std::string robotLayerName;

        RunningTask<OpenPoseEstimation>::pointer_type task2DKeypoints;
        RunningTask<OpenPoseEstimation>::pointer_type task3DKeypoints;
        bool running2D;
        bool running3D;
        void run();
        void run3DKeypoints();

        void setupOpenPoseEnvironment();
        boost::shared_ptr<op::ScaleAndSizeExtractor> scaleAndSizeExtractor;
        boost::shared_ptr<op::CvMatToOpInput> cvMatToOpInput;
        boost::shared_ptr<op::CvMatToOpOutput> cvMatToOpOutput;
        boost::shared_ptr<op::PoseExtractorCaffe> poseExtractorCaffe;
        boost::shared_ptr<op::OpOutputToCvMat> opOutputToCvMat;
        PoseKeypoints getPoseKeypoints(CByteImage* imageBuffer);

        op::PoseModel poseModel;

        Keypoint2DMapList last2DKeypoints_labeled;
        Keypoint3DMapList last3DKeypoints_labeled;
        Keypoint2DOrderedList last2DKeypoints_ordered;
        Keypoint3DOrderedList last3DKeypoints_ordered;
        armarx::Mutex keypoints2DMutex;
        armarx::Mutex keypoints3DMutex;

        std::pair<Keypoint2DMapList, Keypoint2DOrderedList> generate2DKeypoints(PoseKeypoints& keypoints, const CByteImage& rgbImage) const;
        int minimalValidKeypoints;
        void render2DResultImage(const CByteImage& inputImage, PoseKeypoints& keypoints, CByteImage& resultImage) const;
        float renderThreshold;
        long timeProvidedImage;
        // Depth
        void generate3DKeypointsFromDepthImage(Keypoint2DOrderedList& keypoints);
        int depthMedianRadius;
        bool useDistortionParameters;
        // 3DPoseEstimation with IK
        bool ikPoseEstimationActive;
        std::string mmmRobotFilePath;
        VirtualRobot::RobotPtr mmmRobot;
        float robotScaling;
        IKPoseEstimationPtr ikPoseEstimation;
        void generateRobotConfiguration();
        Eigen::Matrix4f lastGlobalPose;
        Eigen::Matrix4f generateGlobalPoseHint() const;

        // Stereo
        bool stereo;
        Keypoint2DMatchedList matchPersons(Keypoint2DOrderedList& leftPersons, Keypoint2DOrderedList& rightPersons) const;
        void generate3DKeypointsFromStereoImage(Keypoint2DMatchedList& matchedKeypoints);

        // Topics
        OpenPose2DListenerPrx listener2DPrx;
        OpenPose3DListenerPrx listener3DPrx;
        bool reportOnlyNearestPerson;
        void filterToNearestPerson();

        RobotStateComponentInterfacePrx robotStateInterface;
        VirtualRobot::RobotPtr localRobot;
        std::string cameraNodeName;
        Keypoint3D transform3DKeypointToGlobal(Keypoint3D& keypoint) const;
        Polygon2D workspacePolygon;
        void filterKeypointsBasedOnWorkspacePolygon(Keypoint3DList& list) const;

        // Helper
        inline bool isKeypoint3DValid(Keypoint3D& p) const
        {
            return !(p.x == 0.0 && p.y == 0.0 && p.z == 0.0);
        }

        inline bool isKeypoint2DInImage(Keypoint2D& p) const
        {
            return (p.x >= 0.0 && p.x <= depthImageBuffer->width && p.y >= 0.0 && p.y <= depthImageBuffer->height);
        }

        inline bool isKeypoint2DValid(Keypoint2D& p) const
        {
            return !(p.x == 0.0 && p.y == 0.0);
        }

        inline bool isKeypoint2DValid(float x, float y) const
        {
            return !(x == 0.0 && y == 0.0);
        }

        void visualize3DKeypoints();
        Keypoint2DMapList normalizeKeypoints2D(Keypoint2DMapList& keypoints) const;
    };
}
