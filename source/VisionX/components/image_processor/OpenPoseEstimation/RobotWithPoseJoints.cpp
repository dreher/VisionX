/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::RobotWithPoseJoints
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotWithPoseJoints.h"

#include <VirtualRobot/RobotFactory.h>
#include <VirtualRobot/Nodes/RobotNodeFixedFactory.h>
#include <VirtualRobot/Nodes/RobotNodePrismaticFactory.h>
#include <VirtualRobot/Nodes/RobotNodeRevoluteFactory.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/RobotConfig.h>

#include <ArmarXCore/core/logging/Logging.h>

using namespace armarx;
using namespace VirtualRobot;

static const std::map<std::string, std::string> POSE_JOINTS_MAPPING
{
    {"x", "GP_x"},
    {"y", "GP_y"},
    {"z", "GP_z"},
    {"roll", "GP_roll"},
    {"pitch", "GP_pitch"},
    {"yaw", "GP_yaw"},
    {"root", "GP_pseudoRoot"}
};

RobotWithPoseJoints::RobotWithPoseJoints(RobotPtr robot)
{
    // Clone given Robot
    RobotPtr tempRobot = RobotFactory::clone(robot, "Robot_temp");

    // Add pseudoRoot and globalPose joints
    RobotNodeFactoryPtr robotNodeFactoryRoot = RobotNodeFactory::fromName(RobotNodeFixedFactory::getName(), NULL);
    RobotNodePtr pseudoRoot = robotNodeFactoryRoot->createRobotNode(tempRobot, POSE_JOINTS_MAPPING.at("root"), VisualizationNodePtr(), CollisionModelPtr(),
                              0.0f, 0.0f, 0.0f,
                              Eigen::Matrix4f::Identity(), Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero());
    pseudoRoot->initialize();


    RobotNodeFactoryPtr robotNodeFactoryPrismatic = RobotNodeFactory::fromName(RobotNodePrismaticFactory::getName(), NULL);
    RobotNodePtr globalPoseJoint_x = robotNodeFactoryPrismatic->createRobotNode(tempRobot, POSE_JOINTS_MAPPING.at("x"), VisualizationNodePtr(), CollisionModelPtr(),
                                     -1500, 1500, 0.0f,
                                     Eigen::Matrix4f::Identity(), Eigen::Vector3f::Zero(), Eigen::Vector3f(1.0, 0.0, 0.0),
                                     SceneObject::Physics(), RobotNode::RobotNodeType::Joint);
    if (!globalPoseJoint_x->initialize(pseudoRoot))
    {
        ARMARX_ERROR << "Error while initializing robotNode " << globalPoseJoint_x->getName();
    }
    RobotNodePtr globalPoseJoint_y = robotNodeFactoryPrismatic->createRobotNode(tempRobot, POSE_JOINTS_MAPPING.at("y"), VisualizationNodePtr(), CollisionModelPtr(),
                                     -1500, 1500, 0.0f,
                                     Eigen::Matrix4f::Identity(), Eigen::Vector3f::Zero(), Eigen::Vector3f(0.0, 1.0, 0.0),
                                     SceneObject::Physics(), RobotNode::RobotNodeType::Joint);
    if (!globalPoseJoint_y->initialize(globalPoseJoint_x))
    {
        ARMARX_ERROR << "Error while initializing robotNode " << globalPoseJoint_y->getName();
    }
    RobotNodePtr globalPoseJoint_z = robotNodeFactoryPrismatic->createRobotNode(tempRobot, POSE_JOINTS_MAPPING.at("z"), VisualizationNodePtr(), CollisionModelPtr(),
                                     -1500, 1500, 0.0f,
                                     Eigen::Matrix4f::Identity(), Eigen::Vector3f::Zero(), Eigen::Vector3f(0.0, 0.0, 1.0),
                                     SceneObject::Physics(), RobotNode::RobotNodeType::Joint);
    if (!globalPoseJoint_z->initialize(globalPoseJoint_y))
    {
        ARMARX_ERROR << "Error while initializing robotNode " << globalPoseJoint_z->getName();
    }

    RobotNodeFactoryPtr robotNodeFactoryRevolute = RobotNodeFactory::fromName(RobotNodeRevoluteFactory::getName(), NULL);
    RobotNodePtr globalPoseJoint_roll = robotNodeFactoryRevolute->createRobotNode(tempRobot, POSE_JOINTS_MAPPING.at("roll"), VisualizationNodePtr(), CollisionModelPtr(),
                                        -M_PI, M_PI, 0.0f,
                                        Eigen::Matrix4f::Identity(), Eigen::Vector3f(1.0, 0.0, 0.0), Eigen::Vector3f::Zero(),
                                        SceneObject::Physics(), RobotNode::RobotNodeType::Joint);
    globalPoseJoint_roll->setLimitless(true);
    if (!globalPoseJoint_roll->initialize(globalPoseJoint_z))
    {
        ARMARX_ERROR << "Error while initializing robotNode " << globalPoseJoint_roll->getName();
    }
    RobotNodePtr globalPoseJoint_pitch = robotNodeFactoryRevolute->createRobotNode(tempRobot, POSE_JOINTS_MAPPING.at("pitch"), VisualizationNodePtr(), CollisionModelPtr(),
                                         -M_PI, M_PI, 0.0f,
                                         Eigen::Matrix4f::Identity(), Eigen::Vector3f(0.0, 1.0, 0.0), Eigen::Vector3f::Zero(),
                                         SceneObject::Physics(), RobotNode::RobotNodeType::Joint);
    globalPoseJoint_pitch->setLimitless(true);
    if (!globalPoseJoint_pitch->initialize(globalPoseJoint_roll))
    {
        ARMARX_ERROR << "Error while initializing robotNode " << globalPoseJoint_pitch->getName();
    }
    RobotNodePtr globalPoseJoint_yaw = robotNodeFactoryRevolute->createRobotNode(tempRobot, POSE_JOINTS_MAPPING.at("yaw"), VisualizationNodePtr(), CollisionModelPtr(),
                                       -M_PI, M_PI, 0.0f,
                                       Eigen::Matrix4f::Identity(), Eigen::Vector3f(0.0, 0.0, 1.0), Eigen::Vector3f::Zero(),
                                       SceneObject::Physics(), RobotNode::RobotNodeType::Joint);
    globalPoseJoint_yaw->setLimitless(true);
    std::vector<SceneObjectPtr> children;
    children.push_back(tempRobot->getRootNode());
    if (!globalPoseJoint_yaw->initialize(globalPoseJoint_pitch, children)) // important: set children to original rootNode

        //    if (!globalPoseJoint_yaw->initialize(globalPoseJoint_z, children)) // important: set children to original rootNode
    {
        ARMARX_ERROR << "Error while initializing robotNode " << globalPoseJoint_yaw->getName();
    }

    // cloneInverse to generate Robot with new pseudoRoot
    RobotPtr robotWithPoseJoints = RobotFactory::cloneInversed(tempRobot, POSE_JOINTS_MAPPING.at("root"), true, true);

    // Adapt every RobotNodeSet that contains the original root
    for (RobotNodeSetPtr nodeSet : robotWithPoseJoints->getRobotNodeSets())
    {
        if (nodeSet->hasRobotNode(robotWithPoseJoints->getRobotNode(tempRobot->getRootNode()->getName())))
        {
            std::vector<RobotNodePtr> nodes = nodeSet->getAllRobotNodes();
            nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("root")));
            nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("x")));
            nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("y")));
            nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("z")));
            nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("roll")));
            nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("pitch")));
            nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("yaw")));

            robotWithPoseJoints->deregisterRobotNodeSet(nodeSet);
            //                        robotWithPoseJoints->registerRobotNodeSet(RobotNodeSet::createRobotNodeSet(
            //                                    robotWithPoseJoints, nodeSet->getName(), nodes, robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("root"))));
            robotWithPoseJoints->registerRobotNodeSet(RobotNodeSet::createRobotNodeSet(
                        robotWithPoseJoints, nodeSet->getName(), nodes));
        }
    }

    // Define RobotNodeSet for global pose
    std::string name = "GlobalPoseNodeSet";
    std::vector<RobotNodePtr> nodes;
    nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("x")));
    nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("y")));
    nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("z")));
    nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("roll")));
    nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("pitch")));
    nodes.push_back(robotWithPoseJoints->getRobotNode(POSE_JOINTS_MAPPING.at("yaw")));

    nodes.push_back(robotWithPoseJoints->getRobotNode("BPSegment_joint"));
    nodes.push_back(robotWithPoseJoints->getRobotNode("BTSegment_joint"));
    nodes.push_back(robotWithPoseJoints->getRobotNode("LHsegment_joint"));
    nodes.push_back(robotWithPoseJoints->getRobotNode("RHsegment_joint"));
    nodes.push_back(robotWithPoseJoints->getRobotNode("LSCsegment_joint"));
    nodes.push_back(robotWithPoseJoints->getRobotNode("RSCsegment_joint"));


    globalPoseNodeSet = RobotNodeSet::createRobotNodeSet(robotWithPoseJoints, name, nodes);
    robotWithPoseJoints->registerRobotNodeSet(globalPoseNodeSet);
    this->robot = robotWithPoseJoints;
}

RobotWithPoseJoints::~RobotWithPoseJoints()
{
    this->robot.reset();
}

void RobotWithPoseJoints::setGlobalPose(const Eigen::Matrix4f& pose)
{
    //    robot->setGlobalPose(Eigen::Matrix4f::Identity());

    //    Eigen::Vector3f rpy = MathTools::eigen4f2rpy(pose);
    //    Eigen::Vector3f pos = pose.block<3, 1>(0, 3);

    //    robot->getRobotNode(POSE_JOINTS_MAPPING.at("x"))->setJointValue(pos.x());
    //    robot->getRobotNode(POSE_JOINTS_MAPPING.at("y"))->setJointValue(pos.y());
    //    robot->getRobotNode(POSE_JOINTS_MAPPING.at("z"))->setJointValue(pos.z());

    //    robot->getRobotNode(POSE_JOINTS_MAPPING.at("roll"))->setJointValue(rpy.z());
    //    robot->getRobotNode(POSE_JOINTS_MAPPING.at("pitch"))->setJointValue(rpy.y());
    //    robot->getRobotNode(POSE_JOINTS_MAPPING.at("yaw"))->setJointValue(rpy.x());

    robot->setGlobalPose(pose);

    robot->getRobotNode(POSE_JOINTS_MAPPING.at("x"))->setJointValue(0.0);
    robot->getRobotNode(POSE_JOINTS_MAPPING.at("y"))->setJointValue(0.0);
    robot->getRobotNode(POSE_JOINTS_MAPPING.at("z"))->setJointValue(0.0);

    robot->getRobotNode(POSE_JOINTS_MAPPING.at("roll"))->setJointValue(0.0);
    robot->getRobotNode(POSE_JOINTS_MAPPING.at("pitch"))->setJointValue(0.0);
    robot->getRobotNode(POSE_JOINTS_MAPPING.at("yaw"))->setJointValue(0.0);
}

Eigen::Matrix4f RobotWithPoseJoints::getGlobalPose() const
{
    float px = robot->getRobotNode(POSE_JOINTS_MAPPING.at("x"))->getJointValue();
    float py = robot->getRobotNode(POSE_JOINTS_MAPPING.at("y"))->getJointValue();
    float pz = robot->getRobotNode(POSE_JOINTS_MAPPING.at("z"))->getJointValue();

    //    float r = 0.0f, p = .0f;
    float r = robot->getRobotNode(POSE_JOINTS_MAPPING.at("roll"))->getJointValue();
    float p = robot->getRobotNode(POSE_JOINTS_MAPPING.at("pitch"))->getJointValue();
    float y = robot->getRobotNode(POSE_JOINTS_MAPPING.at("yaw"))->getJointValue();

    Eigen::Vector3f pos = {px, py, pz};
    Eigen::Matrix4f pose = MathTools::rpy2eigen4f(r, p, y);
    pose.block<3, 1>(0, 3) = pos;

    return robot->getGlobalPose() * pose;
}

void RobotWithPoseJoints::applyGlobalPose(VirtualRobot::RobotPtr robot) const
{
    robot->setGlobalPose(this->getGlobalPose());
}

void RobotWithPoseJoints::applyConfiguration(RobotPtr robot) const
{
    robot->setGlobalPose(this->getGlobalPose());
    RobotConfigPtr localConfig = this->robot->getConfig();

    // Remove entries of globalPoseJoints
    auto map = localConfig->getRobotNodeJointValueMap();
    map.erase(POSE_JOINTS_MAPPING.at("x"));
    map.erase(POSE_JOINTS_MAPPING.at("y"));
    map.erase(POSE_JOINTS_MAPPING.at("z"));
    map.erase(POSE_JOINTS_MAPPING.at("roll"));
    map.erase(POSE_JOINTS_MAPPING.at("pitch"));
    map.erase(POSE_JOINTS_MAPPING.at("yaw"));

    std::vector<RobotConfig::Configuration> configs;
    for (auto& iter : map)
    {
        configs.push_back({iter.first, iter.second});
    }

    RobotConfigPtr newConfig;
    newConfig.reset(new RobotConfig(robot, localConfig->getName(), configs));
    robot->setConfig(newConfig);
}

RobotPtr RobotWithPoseJoints::getRobot() const
{
    return robot;
}

VirtualRobot::RobotNodeSetPtr armarx::RobotWithPoseJoints::getGlobalPoseNodeSet() const
{
    return globalPoseNodeSet;
}
