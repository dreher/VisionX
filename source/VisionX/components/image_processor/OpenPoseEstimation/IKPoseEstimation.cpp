/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::IKPoseEstimation
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IKPoseEstimation.h"

#include <VirtualRobot/RobotNodeSet.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <VirtualRobot/IK/constraints/PositionConstraint.h>

// IK-testing
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

using namespace armarx;
using namespace VirtualRobot;

static const std::map<std::string, std::string> COCO_TO_MMM_MAPPING
{
    {"Neck", "collarSegment_joint"},
    {"RShoulder", "RSsegment_joint"},
    {"RElbow", "REsegment_joint"},
    {"RWrist", "RWsegment_joint"},
    {"LShoulder", "LSsegment_joint"},
    {"LElbow", "LEsegment_joint"},
    {"LWrist", "LWsegment_joint"},
    {"RHip", "RHsegment_joint"},
    {"RKnee", "RKsegment_joint"},
    {"RAnkle", "RAsegment_joint"},
    {"LHip", "LHsegment_joint"},
    {"LKnee", "LKsegment_joint"},
    {"LAnkle", "LAsegment_joint"},
    {"REye", "RightEyeSegmentY_joint"},
    {"LEye", "LeftEyeSegmentY_joint"}
};

static const std::map<std::string, std::string> MPI_TO_MMM_MAPPING
{

};


static const std::vector<std::string> GLOBAL_POSE_ESTIMATION_KEYPOINTS
{
    "Neck",
    "RShoulder",
    "LShoulder",
    "RHip",
    "LHip"
};


IKPoseEstimation::IKPoseEstimation(VirtualRobot::RobotPtr mmmRobot, const std::string& poseModelName, const std::string& robotNodeSet)
{
    this->robotWithPoseJoints.reset(new RobotWithPoseJoints(mmmRobot));
    this->poseModelName = poseModelName;
    const auto& nodeSetNames = this->robotWithPoseJoints->getRobot()->getRobotNodeSetNames();
    if (std::find(nodeSetNames.begin(), nodeSetNames.end(), robotNodeSet) != nodeSetNames.end())
    {
        this->robotNodeSet = this->robotWithPoseJoints->getRobot()->getRobotNodeSet(robotNodeSet);
    }
    else
    {
        ARMARX_ERROR << "Robot " << this->robotWithPoseJoints->getRobot()->getName() << " does not have a RobotNodeSet with the name " << robotNodeSet;
    }
}

IKPoseEstimation::IKPoseEstimation(RobotWithPoseJointsPtr mmmRobot, const std::string& poseModelName, const std::string& robotNodeSet)
{
    this->robotWithPoseJoints = mmmRobot;
    this->poseModelName = poseModelName;
    const auto& nodeSetNames = this->robotWithPoseJoints->getRobot()->getRobotNodeSetNames();
    if (std::find(nodeSetNames.begin(), nodeSetNames.end(), robotNodeSet) != nodeSetNames.end())
    {
        this->robotNodeSet = this->robotWithPoseJoints->getRobot()->getRobotNodeSet(robotNodeSet);
    }
    else
    {
        ARMARX_ERROR << "Robot " << this->robotWithPoseJoints->getRobot()->getName() << " does not have a RobotNodeSet with the name " << robotNodeSet;
    }
}

IKPoseEstimation::~IKPoseEstimation()
{
    this->robotWithPoseJoints.reset();
}

void IKPoseEstimation::setupTSRConstraints(TSRBoundsMap& tsrBounds)
{
    this->tsrConstraints.clear();

    RobotPtr robot = this->robotWithPoseJoints->getRobot();

    for (auto& iterBounds : tsrBounds)
    {
        TIMING_START(setupTSRConstraints);

        std::string name = iterBounds.first;
        std::string nodeName = getMMMNodeName(name);
        if (nodeName.empty())
        {
            continue;
        }

        RobotNodePtr node = robot->getRobotNode(nodeName);
        if (!node)
        {
            ARMARX_WARNING << "Could not find robotNode with name " << nodeName;
            continue;
        }

        TSRBounds b = iterBounds.second;

        Eigen::Matrix<float, 6, 2> bounds;
        bounds << b.lowerBounds.x(), b.upperBounds.x(),
               b.lowerBounds.y(), b.upperBounds.y(),
               b.lowerBounds.z(), b.upperBounds.z(),
               -M_PI, M_PI,
               -M_PI, M_PI,
               -M_PI, M_PI;

        TIMING_END_COMMENT(setupTSRConstraints, "calculations");
        //        PositionConstraintPtr xPositionConstraint(new PositionConstraint(robot,
        //                this->robotNodeSet,
        //                node,
        //                b.center,
        //                IKSolver::X));
        //        PositionConstraintPtr yPositionConstraint(new PositionConstraint(robot,
        //                this->robotNodeSet,
        //                node,
        //                b.center,
        //                IKSolver::Y));
        Eigen::Matrix4f trans = Eigen::Matrix4f::Identity();
        trans.block<3, 3>(0, 0) = b.rotation.toRotationMatrix();
        TSRConstraintPtr tsrConstraint(new TSRConstraint(
                                           robot,
                                           this->robotNodeSet,
                                           node,
                                           trans,
                                           Eigen::Matrix4f::Identity(),
                                           bounds,
                                           5.0f,
                                           M_PI));

        // More optimization nessecary
        tsrConstraint->setOptimizationFunctionFactor(1.0f);
        this->tsrConstraints.push_back(tsrConstraint);
        //        xPositionConstraint->setOptimizationFunctionFactor(1.0f);
        //        yPositionConstraint->setOptimizationFunctionFactor(1.0f);
        //        this->tsrConstraints.push_back(xPositionConstraint);
        //        this->tsrConstraints.push_back(yPositionConstraint);

        TIMING_END_COMMENT(setupTSRConstraints, "Constraint created");
    }

}

TSRBoundsMap IKPoseEstimation::convert3DHintsToBounds(const Keypoint3DMap& keypointHints) const
{
    float box_small = 20;
    float box_big = 300;

    // Here is a lot potential to optimize and improve the result of the constrainedOptimizationIK, by generating better tsrBounds
    TSRBoundsMap result;
    for (const auto& iter : keypointHints)
    {
        Keypoint3D point = iter.second;
        std::string name = iter.first;

        if (point.x == 0.0 && point.y == 0.0 && point.z == 0.0)
        {
            continue;
        }

        // for testing simply creating a small region in x-y-direction and a big region in z-direction:
        float x = point.x;
        float y = point.y;
        float z = point.z;

        TSRBounds b;
        float angle = VirtualRobot::MathTools::getAngle(Eigen::Vector3f(0, 0, z), Eigen::Vector3f(x, y, z));
        std::vector<Eigen::Vector3f> pts;
        pts.push_back(Eigen::Vector3f(0, 0, z));
        pts.push_back(Eigen::Vector3f(x, y, z));
        pts.push_back(Eigen::Vector3f(0, 0, 0));
        b.rotation = Eigen::AngleAxisf(0, Eigen::Vector3f(0, 0, 1));//Eigen::AngleAxisf(-angle, VirtualRobot::MathTools::findNormal(pts));
        b.center = Eigen::Vector3f(x, y, z);
        b.lowerBounds = Eigen::Vector3f({x - box_small, y - box_small, z - box_big});
        b.upperBounds = Eigen::Vector3f({x + box_small, y + box_small, z + box_big});
        result[name] = b;
    }
    return result;
}

void IKPoseEstimation::estimateGlobalPose(VirtualRobot::RobotPtr resultRobot, const Keypoint3DMap& keypointHints, const Eigen::Matrix4f& globalPoseHint)
{
    ARMARX_DEBUG << "IKPoseEstimation::estimateGlobalPose";

    RobotNodeSetPtr globalPoseNodeSet = robotWithPoseJoints->getGlobalPoseNodeSet();
    RobotPtr robot = robotWithPoseJoints->getRobot();

    robotWithPoseJoints->setGlobalPose(globalPoseHint);

    std::vector<RobotNodePtr> nodes;
    std::vector<Keypoint3D> points;
    for (std::string name : GLOBAL_POSE_ESTIMATION_KEYPOINTS)
    {
        auto iter = keypointHints.find(name);
        if (iter == keypointHints.end())
        {
            continue;
        }
        Keypoint3D p = iter->second;
        if (p.x == 0.0 && p.y == 0.0 && p.z == 0.0)
        {
            continue;
        }

        float confindenceThreshold = 0.1;
        if (p.confidence < confindenceThreshold)
        {
            continue;
        }

        std::string nodeName = getMMMNodeName(name);
        if (!nodeName.empty())
        {
            nodes.push_back(robot->getRobotNode(nodeName));
            points.push_back(p);
        }
    }

    // Setup Constraints
    std::vector<ConstraintPtr> constraints;
    float xRange = 5.0, yRange = 5.0, zRange = 200.0;
    for (unsigned int i = 0; i < nodes.size(); i++)
    {
        Keypoint3D point = points.at(i);
        RobotNodePtr node = nodes.at(i);

        Eigen::Matrix<float, 6, 2> bounds;
        bounds << point.x - xRange, point.x + xRange,
               point.y - yRange, point.y + yRange,
               point.z - zRange, point.z + zRange,
               -M_PI, M_PI,
               -M_PI, M_PI,
               -M_PI, M_PI;

        Eigen::Matrix4f trans = Eigen::Matrix4f::Identity();

        Eigen::Vector3f target(point.x, point.y, point.z);

        PositionConstraintPtr xPositionConstraint(new PositionConstraint(robot,
                globalPoseNodeSet,
                node,
                target,
                IKSolver::X));
        PositionConstraintPtr yPositionConstraint(new PositionConstraint(robot,
                globalPoseNodeSet,
                node,
                target,
                IKSolver::Y));
        xPositionConstraint->setOptimizationFunctionFactor(1.0f);
        yPositionConstraint->setOptimizationFunctionFactor(1.0f);
        constraints.push_back(xPositionConstraint);
        constraints.push_back(yPositionConstraint);

        //        TSRConstraintPtr tsrConstraint(new TSRConstraint(
        //                                           robot,
        //                                           globalPoseNodeSet,
        //                                           node,
        //                                           trans,
        //                                           Eigen::Matrix4f::Identity(),
        //                                           bounds,
        //                                           5.0f,
        //                                           M_PI));
        //        constraints.push_back(tsrConstraint);
    }
    ARMARX_INFO << VAROUT(constraints.size());


    // Setup ConstrainedIK
    constrainedIk.reset(new ConstrainedOptimizationIK(robot, globalPoseNodeSet, 1.f, 10));
    for (ConstraintPtr p : constraints)
    {
        constrainedIk->addConstraint(p);
    }

    constrainedIk->setMaxIterations(5);
    constrainedIk->initialize();

    TIMING_START(constrainedIkSolving);
    bool success = constrainedIk->solve();
    ARMARX_IMPORTANT << "Calculating constrained IK"
                     << " took: " << (IceUtil::Time::now() - constrainedIkSolving).toMilliSecondsDouble() << " ms";
    ARMARX_INFO << "ConstrainedIk was successful? -- " << success;

    this->robotWithPoseJoints->applyConfiguration(resultRobot);
}

std::string IKPoseEstimation::getMMMNodeName(const std::string& openPoseNodeName) const
{
    std::string nodeName;
    if (poseModelName == "COCO")
    {
        auto iter = COCO_TO_MMM_MAPPING.find(openPoseNodeName);
        if (iter != COCO_TO_MMM_MAPPING.end())
        {
            nodeName = iter->second;
        }
    }
    else if (poseModelName == "MPI")
    {
        auto iter = MPI_TO_MMM_MAPPING.find(openPoseNodeName);
        if (iter != MPI_TO_MMM_MAPPING.end())
        {
            nodeName = iter->second;
        }
    }
    return nodeName;
}

void IKPoseEstimation::estimate3DConfiguration(VirtualRobot::RobotPtr resultRobot, const Keypoint3DMap& keypointHints, const Eigen::Matrix4f& globalPoseHint)
{
    ARMARX_DEBUG << "IKPoseEstimation::estimate3DConfiguration";
    TIMING_START(setup);

    auto bounds = convert3DHintsToBounds(keypointHints);
    setupTSRConstraints(bounds);
    RobotPtr ikRobot = robotWithPoseJoints->getRobot();
    robotWithPoseJoints->setGlobalPose(globalPoseHint);

    // Setup constrainedIK
    constrainedIk.reset(new ConstrainedOptimizationIK(ikRobot, robotNodeSet, 1.f, 10));

    for (ConstraintPtr p : this->tsrConstraints)
    {
        constrainedIk->addConstraint(p);
    }

    ARMARX_INFO << VAROUT(tsrConstraints.size());
    TIMING_END(setup);

    // Solving IK
    constrainedIk->setMaxIterations(5);
    constrainedIk->initialize();

    TIMING_START(constrainedIkSolving);
    bool success = constrainedIk->solve();
    ARMARX_IMPORTANT << "Calculating constrained IK"
                     << " took: " << (IceUtil::Time::now() - constrainedIkSolving).toMilliSecondsDouble() << " ms";
    ARMARX_INFO << "ConstrainedIk was successful? -- " << success;

    this->robotWithPoseJoints->applyConfiguration(resultRobot);
}

void IKPoseEstimation::testConstraintIK() const
{
    std::string mmmRobotFilePath = "/common/homes/students/reither/home/armarx/RobotAPI/data/RobotAPI/robots/MMM/mmm.xml";
    std::string nodeSetName = "3DPoseEstimation";
    RobotPtr mmmRobot = RobotIO::loadRobot(mmmRobotFilePath);
    mmmRobot = mmmRobot->clone("ScaledRobot", CollisionCheckerPtr(), 1.6);
    if (!mmmRobot)
    {
        ARMARX_ERROR << "Failed to load robot: " << mmmRobotFilePath;
        return;
    }

    RobotNodeSetPtr nodeSet = mmmRobot->getRobotNodeSet(nodeSetName);

    // Random config for robot
    std::vector<float> randomConfig(nodeSet->getSize());
    Eigen::VectorXf initialConfig;
    nodeSet->getJointValues(initialConfig);

    for (unsigned int i = 0; i < nodeSet->getSize(); i++)
    {
        float t = (rand() % 1001) / 1000.0;
        randomConfig[i] = initialConfig(i) + 0.5 * (nodeSet->getNode(i)->getJointLimitLo() + t * (nodeSet->getNode(i)->getJointLimitHi() - nodeSet->getNode(i)->getJointLimitLo()) - initialConfig(i));
    }

    // ForwardKinematic to calculate EEF-Pose (or to calculate poses of all nodes in map)

    nodeSet->setJointValues(randomConfig);
    mmmRobot->applyJointValues();

    // Setup constraintIk with PositionConstraints for forwardKinematic results
    for (auto& iter : COCO_TO_MMM_MAPPING)
    {
        std::string nodeName = iter.second;



    }

    // Another random config for robot
    // Solve IK
}
