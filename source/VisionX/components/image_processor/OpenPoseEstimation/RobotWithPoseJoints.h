/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::RobotWithPoseJoints
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Robot.h>

namespace armarx
{
    /**
     * @class RobotWithPoseJoints
     * @brief A robot-representation which has 6 additional joints that store the global pose of the robot.
     *
     * Detailed Description
     */
    class RobotWithPoseJoints
    {
    public:
        /**
         * RobotWithPoseJoints Constructor
         */
        RobotWithPoseJoints(VirtualRobot::RobotPtr robot);

        /**
         * RobotWithPoseJoints Destructor
         */
        ~RobotWithPoseJoints();

        void setGlobalPose(const Eigen::Matrix4f& pose);
        Eigen::Matrix4f getGlobalPose() const;
        void applyGlobalPose(VirtualRobot::RobotPtr robot) const;
        void applyConfiguration(VirtualRobot::RobotPtr robot) const;

        VirtualRobot::RobotPtr getRobot() const;
        VirtualRobot::RobotNodeSetPtr getGlobalPoseNodeSet() const;
    private:
        VirtualRobot::RobotPtr robot;
        VirtualRobot::RobotNodeSetPtr globalPoseNodeSet;
    };

    typedef boost::shared_ptr<RobotWithPoseJoints> RobotWithPoseJointsPtr;
}
