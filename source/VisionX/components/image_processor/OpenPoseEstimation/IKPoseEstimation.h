/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    <PACKAGE_NAME>::<CATEGORY>::IKPoseEstimation
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/IK/constraints/TSRConstraint.h>
#include <VirtualRobot/IK/ConstrainedOptimizationIK.h>

#include "RobotWithPoseJoints.h"
#include <VisionX/interface/components/OpenPoseEstimationInterface.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <boost/shared_ptr.hpp>

namespace armarx
{
    struct TSRBounds
    {
        Eigen::Vector3f center;
        Eigen::Vector3f lowerBounds;
        Eigen::Vector3f upperBounds;
        Eigen::AngleAxisf rotation;
    };

    typedef std::map<std::string, TSRBounds> TSRBoundsMap;

    /**
     * @class IKPoseEstimation
     * @brief A brief description
     *
     * Detailed Description
     */
    class IKPoseEstimation
    {
    public:
        /**
         * IKPoseEstimation Constructor
         */
        IKPoseEstimation(VirtualRobot::RobotPtr robotWithPoseJoints, const std::string& poseModelName = "COCO", const std::string& robotNodeSet = "3DPoseEstimation");

        IKPoseEstimation(RobotWithPoseJointsPtr robotWithPoseJoints, const std::string& poseModelName = "COCO", const std::string& robotNodeSet = "3DPoseEstimation");

        /**
         * IKPoseEstimation Destructor
         */
        ~IKPoseEstimation();

        void estimate3DConfiguration(VirtualRobot::RobotPtr resultRobot, const Keypoint3DMap& keypointHints, const Eigen::Matrix4f& globalPoseHint);
        void estimateGlobalPose(VirtualRobot::RobotPtr resultRobot, const Keypoint3DMap& keypointHints, const Eigen::Matrix4f& globalPoseHint);

        void testConstraintIK() const;

    private:
        RobotWithPoseJointsPtr robotWithPoseJoints;
        VirtualRobot::RobotNodeSetPtr robotNodeSet;

        std::string poseModelName;
        VirtualRobot::ConstrainedOptimizationIKPtr constrainedIk;
        std::vector<VirtualRobot::ConstraintPtr> tsrConstraints;
        void setupTSRConstraints(TSRBoundsMap& tsrBounds);
        TSRBoundsMap convert3DHintsToBounds(const Keypoint3DMap& keypointHints) const;


        std::string getMMMNodeName(const std::string& openPoseNodeName) const;
    };

    typedef boost::shared_ptr<IKPoseEstimation> IKPoseEstimationPtr;
}
