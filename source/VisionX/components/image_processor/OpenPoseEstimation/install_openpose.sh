#!/bin/bash

DIR=${PWD}
ONLY_INSTALL=false
CUSTOM_PROTOBUF=true

PROTOBUF_VERSION="3.5.1"

# Process options
for i in "$@"
do
case $1 in
	-d=*|--dir=*)
	DIR="${i#*=}"
	DIR="${DIR/#\~/$HOME}"
	shift
	;;
	--only-install)
	ONLY_INSTALL=true
	shift
	;;
	--no-custom-protobuf)
	CUSTOM_PROTOBUF=false
	shift
	;;
	--help|-h)
	echo "--------------------------------"
	echo "Installation script for Openpose"
	echo "--------------------------------"
	echo "This script installs the real-time multi-person keypoint detection library 'Openpose' together with its dependencies 'Caffe' and 'protobuf'."
	echo "To be included in an external project, the file OpenPoseConfig.cmake can be found in the folder '../openpose/build/install/lib/Openpose/' in the installation directory."
	echo "Additionally the external project has to be linked against the custom 'Caffe'-library with link_directories(\${Caffe_LIBRARY_DIRS})"
	echo "--------------------------------"
	echo "Avaiable options:"
	echo "-d=/--dir= 	   -- specifies the installation directory, if not set, the current directory is used. (e.g. -dir=${HOME}/projects results in ${HOME}/projects/openpose"
	echo "--only-install       -- the script assumes Openpose is already present"
	echo "--no-custom-protobuf -- if set, the script assumes protobuf is already installed on the maschine"
	exit 0
	shift
	;;
	*)
		# default
	;;
esac
done

OPENPOSE_DIR=${DIR}"/openpose"

# change directory to specified DIR if necessary
if [ ! -d "$DIR" ]; then
	mkdir ${DIR}
fi
cd ${DIR}

# check if openpose-directory already exisits and is not empty
if [ ${ONLY_INSTALL} = false ] && [ -d "$OPENPOSE_DIR" ]; then
	if [ -n "$(ls -A $OPENPOSE_DIR)" ]; then
		echo ${OPENPOSE_DIR}" is not empty!"
		echo -n "Do you want to continue and overwrite (y/n)? "
		old_stty_cfg=$(stty -g)
		stty raw -echo
		answer=$( while ! head -c 1 | grep -i '[ny]' ;do true ;done )
		stty $old_stty_cfg
		if echo "$answer" | grep -iq "^n" ;then
			echo "No"
		    	exit 1
		else
			echo "Yes"
			rm -r -f ${OPENPOSE_DIR}
		fi
	fi
fi
if [[ ${ONLY_INSTALL} = true &&  ! -d "$OPENPOSE_DIR" ]] || [ ${ONLY_INSTALL} = false ] ; then
	# Clone openpose-repository
	echo "Cloning Openpose to "${OPENPOSE_DIR}
	git clone "https://github.com/CMU-Perceptual-Computing-Lab/openpose"
	echo "Cloning complete"
fi

# edit OpenposeConfig.cmake to correct OpenPose_INCLUDE_DIRS
cd "openpose/cmake/"
filename="OpenPoseConfig.cmake.in"
searchstring='set(OpenPose_INCLUDE_DIRS "${_prefix}/include/openpose")'
replacestring='set(OpenPose_INCLUDE_DIRS "${_prefix}/include")'
sed -i "s~${searchstring}~${replacestring}~" ${filename}
searchstring='# set(Caffe_LIBS "${_prefix}/lib/libcaffe.so")'
replacestring='set(Caffe_LIBRARY_DIRS "${_prefix}/lib/x86_64-linux-gnu")'
sed -i "s~${searchstring}~${replacestring}~" ${filename}
cd ".."

# Run cmake for openpose for the first time --> downloads caffe and models
if ! [ -d "build" ]; then
	mkdir "build"
fi
cd "build/"
cmake -DDOWNLOAD_MPI_MODEL=ON -DDOWNLOAD_HAND_MODEL=OFF -DDOWNLOAD_FACE_MODEL=OFF -DCMAKE_C_COMPILER=gcc-4.8 -DCMAKE_CXX_COMPILER=g++-4.8 ..
..
echo "Done cmake openpose"

# Installing custom protobuf
if [ ${CUSTOM_PROTOBUF} = true ]; then
	cd "../3rdparty"

	if [ ${ONLY_INSTALL} = false ] || [[ ${ONLY_INSTALL} = true && ! -d "protobuf-${PROTOBUF_VERSION}" ]]; then
		PROTOBUF_CONTAINER="protobuf-cpp-${PROTOBUF_VERSION}.tar.gz"
		if [ -f ${PROTOBUF_CONTAINER} ]; then
			rm ${PROTOBUF_CONTAINER}
		fi
		wget https://github.com/google/protobuf/releases/download/v${PROTOBUF_VERSION}/${PROTOBUF_CONTAINER}
		if [ -d "protobuf-${PROTOBUF_VERSION}" ]; then
			rm -r -f "protobuf-${PROTOBUF_VERSION}"
		fi
		tar -zxvf ${PROTOBUF_CONTAINER}

		cd "protobuf-${PROTOBUF_VERSION}"
		PROTOBUF_DIR=${PWD}
		PROTOBUF_LIBS=${PROTOBUF_DIR}/lib

		./configure --prefix=${PROTOBUF_DIR}
		make -j`nproc`
		make install
	else
		cd "protobuf-${PROTOBUF_VERSION}"
		PROTOBUF_DIR=${PWD}
		PROTOBUF_LIBS=${PROTOBUF_DIR}/lib
		make install		
	fi
	cd 
fi

# Run cmake for caffe
cd "${OPENPOSE_DIR}/build/caffe/src/openpose_caffe-build/"
if [ ${CUSTOM_PROTOBUF} = true ]; then
	cmake \
	-DPROTOBUF_INCLUDE_DIR=${PROTOBUF_DIR}/include \
	-DPROTOBUF_LIBRARY=${PROTOBUF_DIR}/lib/libprotobuf.so \
	-DPROTOBUF_LIBRARY_DEBUG=${PROTOBUF_DIR}/lib/libprotobuf.so \
	-DPROTOBUF_LITE_LIBRARY=${PROTOBUF_DIR}/lib/libprotobuf-lite.so \
	-DPROTOBUF_LITE_LIBRARY_DEBUG=${PROTOBUF_DIR}/lib/libprotobuf-lite.so \
	-DPROTOBUF_PROTOC_EXECUTABLE=${PROTOBUF_DIR}/bin/protoc \
	-DPROTOBUF_PROTOC_LIBRARY=${PROTOBUF_DIR}/lib/libprotoc.so \
	-DPROTOBUF_PROTOC_LIBRARY_DEBUG=${PROTOBUF_DIR}/lib/libprotoc.so \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_C_COMPILER=gcc-4.8 \
	-DCMAKE_CXX_COMPILER=g++-4.8 \
	${OPENPOSE_DIR}/3rdparty/caffe
else
	cmake ${OPENPOSE_DIR}/3rdparty/caffe
fi
echo "Done cmake caffe"

# Run cmake for openpose second time to set install dir and  "protobuf"-include
cd "${OPENPOSE_DIR}/build/"
cmake \
-DCaffe_INCLUDE_DIRS="${OPENPOSE_DIR}/build/caffe/include\;${PROTOBUF_DIR}/include" \
-DCMAKE_INSTALL_PREFIX="${OPENPOSE_DIR}/build/install" \
-DCMAKE_BUILD_TYPE=Release \
..

echo "Done cmake openpose"

make -j`nproc`
make install

echo "Done make"
