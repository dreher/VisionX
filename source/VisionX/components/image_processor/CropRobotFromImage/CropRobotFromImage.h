/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::CropRobotFromImage
 * @author     Julian Zimmer ( urdbu at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_VisionX_CropRobotFromImage_H
#define _ARMARX_COMPONENT_VisionX_CropRobotFromImage_H



#include "MaskRobotInImage.h"

#include <ArmarXCore/core/Component.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/core/ImageProvider.h>

#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/interface/core/RobotState.h>


#include <VirtualRobot/Robot.h>
//
#include <VirtualRobot/VirtualRobotCommon.h>


#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>
//#include <VisionX/interface/components/CropRobotFromImageInterface.h>
#include <VisionX/interface/components/Calibration.h>
#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>

#include <mutex>

#include <Eigen/Core>

namespace armarx
{
    /**
     * @class CropRobotFromImagePropertyDefinitions
     * @brief
     */
    class CropRobotFromImagePropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        CropRobotFromImagePropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("providerName", "ImageProvider", "Name of the image provider that should be used");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the robot state component that should be used");
            defineOptionalProperty<std::string>("cameraFrameName", "DepthCameraSim", "The source frame name");

            defineOptionalProperty<float>("filterColorR", 0.f, "The red value of the background color that has to be different from the robots color. Range 0.f to 255.f");
            defineOptionalProperty<float>("filterColorG", 177.f, "The green value of the background color that has to be different from the robots color. Range 0.f to 255.f");
            defineOptionalProperty<float>("filterColorB", 64.f, "The blue value of the background color that has to be different from the robots color. Range 0.f to 255.f");

            defineOptionalProperty<float>("collisionModelInflationMargin", 0.f, "When the collision model is used for the offscreen rendering, it will be inflated by this margin.");

            defineOptionalProperty<int>("dilationStrength", 0, "The strength of the dilation after the masking, which can thicken the part that is cut from the image.");

            defineOptionalProperty<bool>("flipImages", 1, "Boolean that indicates, if the rendered image should be flipped. This is needed e.g. when camera nodes ending on 'Sim' are used.");
            defineOptionalProperty<bool>("useFullModel", 1, "Boolean that indicates, if the full model should be used during the offscreen rendering. Otherwise the collision model will be used.");


            defineOptionalProperty<int>("numResultImages", 1, "The number of images processed and provided.");
        }
    };

    /**
     * @defgroup Component-CropRobotFromImage CropRobotFromImage
     * @ingroup VisionX-Components
     * A description of the component CropRobotFromImage.
     *
     * @class CropRobotFromImage
     * @ingroup Component-CropRobotFromImage
     * @brief Brief description of class CropRobotFromImage.
     *
     * Detailed description of class CropRobotFromImage.
     */
    class CropRobotFromImage :
        virtual public visionx::ImageProcessor,
        virtual public visionx::StereoCalibrationProcessorInterface
    {

    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "CropRobotFromImage";
        }


        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = ::Ice::Current()) override
        {
            return stereoCalibration;
        }

        bool getImagesAreUndistorted(const Ice::Current& c = ::Ice::Current()) override
        {
            return imagesAreUndistorted;
        }

        std::string getReferenceFrame(const Ice::Current& c = ::Ice::Current()) override
        {
            return referenceFrame;
        }

        /*
        void setStereoCalibration(visionx::StereoCalibration stereoCalibration, bool imagesAreUndistorted, const std::string& referenceFrame)
        {
            this->stereoCalibration = stereoCalibration;
            this->imagesAreUndistorted = imagesAreUndistorted;
            this->referenceFrame = referenceFrame;
            //calibrationPrx->reportStereoCalibrationChanged(this->stereoCalibration, this->imagesAreUndistorted, this->referenceFrame);
        }
        */


    protected:

        void process() override;

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();
    private:

        int width;
        int height;

        float fov;

        int dilationStrength;

        std::mutex mutex;

        bool flipImages;
        bool useFullModel;
        float collisionModelInflationMargin;

        visionx::ImageProviderInfo imageProviderInfo;



        std::string providerName;
        std::string cameraFrameName;

        int numImages, numResultImages;
        CByteImage** images;
        //CByteImage** result;

        visionx::MonocularCalibration depthCameraCalibration;

        VirtualRobot::RobotPtr localRobot;

        MaskRobotInImage* maskRobot;

        float backgroundR;
        float backgroundG;
        float backgroundB;

        RobotStateComponentInterfacePrx robotStateComponent;


        visionx::StereoCalibration stereoCalibration;
        bool imagesAreUndistorted;
        //StereoCalibrationInterfacePrx calibrationPrx;
        std::string referenceFrame;
    };
}

#endif
