/**
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    TabletTeleoperation::ArmarXObjects::StreamReceiver
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StreamReceiver.h"

#include <Image/ByteImage.h>
#include <VisionX/tools/ImageUtil.h>

extern "C" {
#include <gst/gst.h>
#include <gst/app/gstappsink.h>
#include <gst/app/gstappsrc.h>
#include <gst/app/gstappbuffer.h>
#include <glib.h>
#include <glib-object.h>
}

using namespace armarx;
using namespace Stream;


#define VP8DECODE_ELEMENT " vp8dec ! "
#define RTPVP8_ELEMENT  "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)VP8-DRAFT-IETF-01, payload=(int)96 ! " \
    "rtpvp8depay ! "
//    "gstrtpjitterbuffer latency=500 drop-on-latency=true ! "

#define H264DECODE_ELEMENT " ffdec_h264 ! "
#define RTPH264_ELEMENT "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96 ! " \
    "rtph264depay ! "
//    "gstrtpjitterbuffer latency=500 drop-on-latency=true ! "

void StreamReceiver::onInitComponent()
{
    pCombinedDecodedImage = NULL;
    usingProxy(getProperty<std::string>("UsedStreamProvider").getValue());
    usingTopic("ImageStream");
    numberImages = 0;
    gst_init(NULL, NULL);



    avcodec_register_all();
    av_init_packet(&m_packet);
    m_decoder = avcodec_find_decoder(AV_CODEC_ID_H264);
    if (!m_decoder)
    {
        ARMARX_ERROR << ("Can't find H264 decoder!");
    }
    m_decoderContext = avcodec_alloc_context3(m_decoder);

    if (m_decoder->capabilities & CODEC_CAP_TRUNCATED)
    {
        m_decoderContext->flags |= CODEC_FLAG_TRUNCATED;
    }


    //we can receive truncated frames
    m_decoderContext->flags2 |= CODEC_FLAG2_CHUNKS;
    m_decoderContext->thread_count = 4;//TODO: random value. May be changing can make decoding faster

    AVDictionary* dictionary = nullptr;
    if (avcodec_open2(m_decoderContext, m_decoder, &dictionary) < 0)
    {
        ARMARX_ERROR << "Could not open decoder";
    }
    ARMARX_INFO << "H264 Decoder successfully opened";
    m_picture = avcodec_alloc_frame();

}

//unused function
//static gboolean
//sink_event_handler(GstPad*    pad,
//                   GstEvent*  event)
//{
//    gboolean ret;

//    switch (GST_EVENT_TYPE(event))
//    {
//        case GST_EVENT_QOS:
//            ARMARX_DEBUG_S << "QOS event";
//            ret = gst_pad_event_default(pad, event);
//            break;

//        default:
//            /* just call the default handler */
//            ret = gst_pad_event_default(pad, event);
//            break;
//    }

//    return ret;
//}


void StreamReceiver::onConnectComponent()
{
    ScopedLock lock(decodedImageMutex);
    ppDecodedImages = new CByteImage*[numberImages];

    streamProvider = getProxy<Stream::StreamProviderPrx>(getProperty<std::string>("UsedStreamProvider").getValue());
    codec = streamProvider->getCodecType();
    numberImages = streamProvider->getNumberOfImages();
    int imgWidth, imgHeight, imgType;
    streamProvider->getImageInformation(imgWidth, imgHeight, imgType);
    ARMARX_INFO << "Images: " << numberImages;
    for (int i = 0; i < numberImages ; i++)
    {
        ppDecodedImages[i] = new CByteImage(imgWidth, imgHeight, CByteImage::eRGB24);
    }
    pCombinedDecodedImage = new CByteImage(imgWidth, imgHeight * numberImages, CByteImage::eRGB24);
    return;

    // put dummy string names
    //    for (int i = 0; i < numberImages ; i++)
    //    {
    //        std::stringstream ss;
    //        ss << "Camera" << i;
    //        ARMARX_WARNING << "Creating dummy stream  name for image  " << ss.str();
    //        streamSources[ss.str()].reset(new StreamElements(ss.str(), i, streamProvider));
    //        streamSources[ss.str()]->streamName = ss.str();

    //    }

    //    /*
    //    std::string sourcesStr = getProperty<std::string>("UsedSources").getValue();
    //    std::vector<std::string> sources;
    //    boost::split(sources,
    //                 sourcesStr,
    //                 boost::is_any_of("\t ,"),
    //                 boost::token_compress_on);
    //    for(unsigned int i=0; i< sources.size(); i++){
    //        if(sources[i].empty())
    //            continue;
    //        ARMARX_DEBUG << "Creating stream for " << sources[i];
    //        ARMARX_WARNING << "xxCreating stream for " << sources[i];
    //        streamSources[sources[i]].reset(new StreamElements( sources[i], streamProvider));
    //        streamSources[sources[i]]->streamName = sources[i];

    //    }
    //    */

    //    std::string camPipelineString = "appsrc name=dec ! ";

    //    switch (codec)
    //    {
    //        case Stream::eVP8:
    //            ARMARX_INFO << "Selected Codec: VP8";
    //            camPipelineString += RTPVP8_ELEMENT
    //                                 VP8DECODE_ELEMENT;
    //            break;

    //        case Stream::eH264:
    //            ARMARX_INFO << "Selected Codec: H264";
    //            camPipelineString +=  RTPH264_ELEMENT
    //                                  H264DECODE_ELEMENT;
    //            break;

    //        default:
    //            throw armarx::LocalException("Codec not yet supported");
    //    }

    //    camPipelineString +=
    //        //            " tee name=t ! "
    //        //            " queue !"
    //        //            " fpsdisplaysink t. ! "
    //        " queue !"
    //        " ffmpegcolorspace ! "
    //        " video/x-raw-rgb,bpp=24,depth=24,endianness=4321,red_mask=16711680,green_mask=65280,blue_mask=255 ! "
    //        " appsink name=raw drop=true max-buffers=100";


    //    start = IceUtil::Time::now();
    //    transferredBits = 0;


    //    StreamSourceMap::iterator it = streamSources.begin();

    //    for (; it != streamSources.end(); it++)
    //    {

    //        StreamElementsPtr elem = it->second;

    //        elem->pipeline =  gst_parse_launch(
    //                              camPipelineString.c_str()
    //                              , NULL
    //                          );

    //        if (!elem->pipeline)
    //        {
    //            ARMARX_ERROR << "pipeline is ZERO";
    //            terminate();
    //            return;
    //        }

    //        // get the appsrc
    //        elem->appsrc = gst_bin_get_by_name(GST_BIN(elem->pipeline), "dec");

    //        elem->appsink = gst_bin_get_by_name(GST_BIN(elem->pipeline), "raw");

    //        // set the pipeline to playing state.
    //        gst_element_set_state(elem->pipeline, GST_STATE_PLAYING);

    //        elem->taskReceive = new PeriodicTask<StreamElements>(elem.get(), &StreamReceiver::StreamElements::receive, 1);
    ////        elem->taskReceive->start();
    //        ARMARX_INFO << it->first << " pipeline created";

    //    }

    //    taskSave = new PeriodicTask<StreamReceiver>(this, &StreamReceiver::store, 5, false, "StreamConverterToIVT");
    //    taskSave->start();

}


void StreamReceiver::onDisconnectComponent()
{
    //    ARMARX_IMPORTANT << "onDisconnectComponent";
    StreamSourceMap::iterator it = streamSources.begin();

    for (; it != streamSources.end(); it++)
    {
        gst_element_send_event(it->second->pipeline, gst_event_new_eos());
        gst_element_send_event(it->second->appsink, gst_event_new_eos());
        gst_element_set_state(it->second->appsink, GST_STATE_NULL);

        if (it->second->taskReceive)
        {
            it->second->taskReceive->stop();
        }
    }

    //    ScopedLock lock(pipelineMutex);
    //    ARMARX_IMPORTANT << "stopping tasks";
    if (taskSave)
    {
        taskSave->stop();
    }

    //    ARMARX_IMPORTANT << "taskSave stopped";

    it = streamSources.begin();

    for (; it != streamSources.end(); it++)
    {

        //        ARMARX_INFO << "Stopping : " << it->first;
        gst_element_set_state(it->second->pipeline, GST_STATE_NULL);
        gst_element_set_state(it->second->appsink, GST_STATE_NULL);
        g_object_unref(it->second->pipeline);
        it->second->pipeline = NULL;
        //        ARMARX_INFO << it->first << " stopped ";
    }

    it = streamSources.begin();

    for (; it != streamSources.end(); it++)
    {

        try
        {
            if (streamProvider)
            {
                streamProvider->stopCapture();
            }

        }
        catch (Ice::NotRegisteredException& e)
        {
            ARMARX_DEBUG << "no proxy available";
            continue;
        }
        catch (Ice::ConnectionRefusedException& e)
        {
            ARMARX_DEBUG << "no proxy available";
            continue;
        }

        //        ARMARX_INFO << it->first << "remote  stopped ";


    }
    delete pCombinedDecodedImage;
    pCombinedDecodedImage = nullptr;
    if (ppDecodedImages)
    {
        for (int i = 0; i < numberImages ; i++)
        {
            delete ppDecodedImages[i];
        }

        delete[] ppDecodedImages;
        ppDecodedImages = nullptr;
    }


    ARMARX_IMPORTANT << "onDisconnectComponent DONE";
}


void StreamReceiver::onExitComponent()
{

}

StreamReceiver::StreamElementsPtr StreamReceiver::getStreamElements(std::string streamName)
{
    StreamSourceMap::iterator it = streamSources.find(streamName);

    if (it == streamSources.end())
    {
        throw armarx::LocalException("No Stream registered under the name: '") << streamName << "'";
    }

    return it->second;
}

void StreamReceiver::getImageFormat(StreamElementsPtr elem, int& height, int& width)
{
    GstPad* pad = gst_element_get_static_pad(elem->appsink, "sink");

    if (!pad)
    {
        ARMARX_WARNING << "Could not get pad";
        return;
    }

    GstCaps* caps = gst_pad_get_negotiated_caps(pad);

    if (!caps)
    {
        ARMARX_WARNING << "Could not get caps";
        return;
    }

    GstStructure* s = gst_caps_get_structure(caps, 0);
    gst_structure_get_int(s, "width", &width);
    gst_structure_get_int(s, "height", &height);

    gst_caps_unref(caps);
    gst_object_unref(pad);
}

void StreamReceiver::reportNewStreamData(const DataChunk& chunk, const Ice::Current&)
{
    ScopedLock lock(streamDecodeMutex);
    m_packet.size = chunk.size();
    m_packet.data = const_cast<Ice::Byte*>(chunk.data());
    //            ARMARX_INFO << "H264Decoder: received encoded frame with framesize " << enc_frame.size();

    while (m_packet.size > 0)
    {
        int got_picture;
        int len = avcodec_decode_video2(m_decoderContext, m_picture, &got_picture, &m_packet);
        if (len < 0)
        {
            std::string err("Decoding error");
            ARMARX_INFO << err;
            return;
        }
        if (got_picture)
        {
            ARMARX_INFO << deactivateSpam(1) << "H264Decoder: frame decoded!";
            //                    std::vector<unsigned char> result;
            //                    this->storePicture(result);

            if (m_picture->format == AV_PIX_FMT_YUV420P)
            {
                static SwsContext* m_swsCtx = NULL;
                //                    QImage frame_img = QImage(width, height, QImage::Format_RGB888);
                m_swsCtx = sws_getCachedContext(m_swsCtx,  m_picture->width,
                                                m_picture->height, AV_PIX_FMT_YUV420P,
                                                m_picture->width,  m_picture->height,
                                                AV_PIX_FMT_RGB24, SWS_GAUSS,
                                                NULL, NULL, NULL);
                ScopedLock lock(decodedImageMutex);

                uint8_t* dstSlice[] = { pCombinedDecodedImage->pixels };
                int dstStride =  m_picture->width * 3;
                if (sws_scale(m_swsCtx, m_picture->data, m_picture->linesize,
                              0, m_picture->height, dstSlice, &dstStride) != m_picture->height)
                {
                    ARMARX_INFO << "SCALING FAILED!";
                    return;
                }
                for (int i = 0; i < numberImages; ++i)
                {
                    size_t imageByteSize = ppDecodedImages[i]->width * ppDecodedImages[i]->height * ppDecodedImages[i]->bytesPerPixel;
                    //            if(ppInputImages[i]->type != CByteImage::eRGB24)
                    //            {
                    //                ::ImageProcessor::ConvertImage(imageProviderInfo, eRgb,)
                    //            }
                    memcpy(ppDecodedImages[i]->pixels, pCombinedDecodedImage->pixels + i * imageByteSize, imageByteSize);
                }
                //                ARMARX_INFO << "New decoded image!";
            }
            else
            {
                ARMARX_INFO << "Other format: " << m_picture->format;
            }

            //                        emit newDecodedFrame(frame_img);
            //                    }
            //                    else if (m_picture->format == PIX_FMT_RGB32)
            //                    {
            //                        QImage img = QImage(result.data(), m_picture->width, m_picture->height, QImage::Format_RGB32);
            //                        ARMARX_INFO << "New decoded image!";
            //                        emit newDecodedFrame(img);
            //                    }
            //                    else if (m_picture->format == AV_PIX_FMT_RGB24)
            //                    {
            //                        QImage img = QImage(result.data(), m_picture->width, m_picture->height, QImage::Format_RGB888);
            //                        ARMARX_INFO << "New decoded image!";
            //                        emit newDecodedFrame(img);
            //                    }
            //                    else
            //                    {
            //                        std::string err = std::string( "Unsupported pixel format! Can't create QImage!");
            //                        ARMARX_INFO << err;
            //                        emit criticalError( err );
            //                        return false;
            //                    }
        }
        m_packet.size -= len;
        m_packet.data += len;
    }

}




void StreamReceiver::getImages(std::vector<CByteImage*>& imagesOut)
{


    ARMARX_DEBUG << deactivateSpam(3)  << "FPS: " << 1 / (IceUtil::Time::now() - lastReceiveTimestamp).toSecondsDouble();
    lastReceiveTimestamp  = IceUtil::Time::now();

    if (getState() < eManagedIceObjectStarted)
    {
        return;
    }
    ScopedLock lock2(decodedImageMutex);
    for (int i = 0; i < numberImages && i < (int)imagesOut.size(); ++i)
    {

        memcpy(imagesOut.at(i)->pixels, ppDecodedImages[i]->pixels, ppDecodedImages[i]->width * ppDecodedImages[i]->height * ppDecodedImages[i]->bytesPerPixel);
    }
    return;

    ScopedLock lock(pipelineMutex);
    int i = 0;
    StreamSourceMap::iterator it = streamSources.begin();

    for (; it != streamSources.end(); it++, i++)
    {
        //    ARMARX_IMPORTANT << "Waiting for Buffer";

        CByteImage* image = imagesOut.at(i);

        if (!image)
        {
            continue;
        }

        if (!image->m_bOwnMemory)
        {
            throw armarx::LocalException("Output images need to have there own memory");
        }

        StreamElementsPtr elem = it->second;


        if (!elem->appsink || !elem->pipeline)
        {
            continue;
        }

        GstState state;

        if (gst_element_get_state(elem->appsink, &state, NULL, 1000000000) == GST_STATE_CHANGE_FAILURE || state != GST_STATE_PLAYING)
        {
            ARMARX_ERROR  << deactivateSpam(5) << "state of appsink for "  << elem->streamName << " not playing state: " << state  << " - skipping" << std::endl;
            continue;
        }

        GstBuffer* buffer =  gst_app_sink_pull_buffer(GST_APP_SINK(elem->appsink));

        if (!buffer)
        {
            ARMARX_DEBUG << "Received Buffer is empty";
            return;
        }

        //    ARMARX_IMPORTANT << "got Buffer";

        int width;
        int height;
        getImageFormat(elem, height, width);

        //        g_object_get(caps, "width", &width);
        //        g_object_get(caps, "height", &height);
        //        ARMARX_INFO_S << "width: " << width;
        //        CByteImage image(width, height, CByteImage::eRGB24);
        int buf_size = GST_BUFFER_SIZE(buffer);

        if (width * height * 3 != buf_size)
        {
            ARMARX_WARNING << "Invalid buffer size: actual " << buf_size << " vs. expected " << width* height * 3;
        }
        else
        {
            memcpy(image->pixels, GST_BUFFER_DATA(buffer), width * height * 3);
        }

        //                std::stringstream str;
        //                str << "/tmp/images/snap" << std::setfill('0') << std::setw(3) << imageNumber++ << ".bmp";
        //                //    ARMARX_DEBUG << "Storing image";
        //                image.SaveToFile(str.str().c_str());

        gst_buffer_unref(buffer);

    }

}

int StreamReceiver::getNumberOfImages()
{
    // return streamSources.size();
    return numberImages;
}

void StreamReceiver::getImageInformation(int& imageWidth, int& imageHeight, int& imageType)
{
    streamProvider->getImageInformation(imageWidth, imageHeight, imageType);
}

void StreamReceiver::receive()
{

    //    ScopedLock lock(pipelineMutex);

    //    Stream::DataChunk c ;
    //    try{
    //        c = streamProvider->getChunk("Camera");
    //    }
    //    catch(Ice::NotRegisteredException &e)
    //    {
    //        ARMARX_DEBUG << "no proxy available";
    //        return;
    //    }
    //    catch(Ice::ConnectionRefusedException &e)
    //    {
    //        ARMARX_DEBUG << "no proxy available";
    //        return;
    //    }
    //    unsigned char* dataChunk = new unsigned char[c.size()];
    //    memcpy(dataChunk, &c[0], c.size());

    //    //std::cout << "\r\033[K" << std::flush;

    //    if(c.size() == 0)
    //    {
    //        ARMARX_DEBUG << "No Data received";
    //        return;
    //    }
    //    ARMARX_DEBUG << "Feeding chunk of size " << c.size() << std::endl;
    //    GstFlowReturn ret;
    //    GstBuffer *buffer;
    //    buffer = gst_app_buffer_new(dataChunk, c.size(), g_free, dataChunk);
    //    GST_BUFFER_TIMESTAMP (buffer) = (IceUtil::Time::now()- start).toMilliSeconds();
    //    ret = gst_app_src_push_buffer(GST_APP_SRC(appsrc), buffer);

    //    if (ret == GST_FLOW_UNEXPECTED) {
    //        ARMARX_DEBUG << std::endl << "Stream ended (received EOS)" ;
    //        return;
    //    } else if (ret != GST_FLOW_OK) {
    //        ARMARX_DEBUG << std::endl << "error while feeding chunk of size: " << gst_flow_get_name(ret) ;
    //        return;
    //    }



}


StreamReceiver::StreamElements::StreamElements(std::string streamName, int streamID, StreamProviderPrx streamProvider)
{
    this->streamName = streamName;
    this-> streamProvider = streamProvider;
    receivedIndex = 0;
    realPosition = 0;
    this->streamID = streamID;

}

void StreamReceiver::StreamElements::receive()
{

    //    //    ScopedLock lock(pipelineMutex);

    //    Stream::GstBufferWrapper wrap;

    //    try
    //    {
    //        //wrap = streamProvider->getGstBuffer(streamName);
    //        wrap = streamProvider->getEncodedImagesFromBuffer(streamID, receivedIndex, realPosition);
    //        receivedIndex = realPosition;
    //        receivedIndex++;
    //        //ARMARX_DEBUG_S << " r " << receivedIndex << " p: " << realPosition << " s: " << wrap.data.size();

    //    }
    //    catch (Ice::NotRegisteredException& e)
    //    {
    //        ARMARX_DEBUG_S << "no proxy available";
    //        return;
    //    }
    //    catch (Ice::ConnectionRefusedException& e)
    //    {
    //        ARMARX_DEBUG_S << "no proxy available";
    //        return;
    //    }

    //    unsigned char* dataChunk = new unsigned char[wrap.data.size()];
    //    memcpy(dataChunk, &wrap.data[0], wrap.data.size());

    //    //std::cout << "\r\033[K" << std::flush;

    //    if (wrap.data.size() == 0)
    //    {
    //        ARMARX_DEBUG_S << "No Data received";
    //        return;
    //    }

    //    //    ARMARX_DEBUG << "Feeding chunk of size " << wrap.data.size() << std::endl;
    //    GstFlowReturn ret;
    //    GstBuffer* buffer;
    //    buffer = gst_app_buffer_new(dataChunk, wrap.data.size(), g_free, dataChunk);
    //    GST_BUFFER_TIMESTAMP(buffer) = wrap.timestamp;
    //    GST_BUFFER_DURATION(buffer) = wrap.duration;
    //    ret = gst_app_src_push_buffer(GST_APP_SRC(appsrc), buffer);

    //    if (ret == GST_FLOW_UNEXPECTED)
    //    {
    //        ARMARX_DEBUG_S << std::endl << "Stream ended (received EOS)" ;
    //        return;
    //    }
    //    else if (ret != GST_FLOW_OK)
    //    {
    //        ARMARX_DEBUG_S << std::endl << "error while feeding chunk of size: " << gst_flow_get_name(ret) ;
    //        return;
    //    }

    //    //    bandwidth_kbps = 0.001 * wrap.data.size() * 24 * 8;
    //    //    ARMARX_VERBOSE << "kbps: " << bandwidth_kbps;
    //    //    transferredBits += wrap.data.size() * 8;
    //    //    ARMARX_INFO << "Avg bandwidth: " << 0.001*transferredBits/(IceUtil::Time::now() - start).toSecondsDouble() << " kbps";
}


void StreamReceiver::store()
{
    //    ImageMap images;
    //    CByteImage image(640,480, CByteImage::eRGB24);
    //    CByteImage image2(640,480, CByteImage::eRGB24);
    //    images["Camera"] =  &image;
    //    images["Camera2"] =  &image2;
    //    getImages(images);

    //    //    ARMARX_DEBUG << "FPS: " << 1/(IceUtil::Time::now() -lastReceiveTimestamp).toSecondsDouble();
    //    lastReceiveTimestamp  = IceUtil::Time::now();

    //    ScopedLock lock(pipelineMutex);

    //    //    ARMARX_IMPORTANT << "Waiting for Buffer";
    //    GstBuffer* buffer =  gst_app_sink_pull_buffer(GST_APP_SINK(this->appsink));
    //    if(!buffer)
    //    {
    //        ARMARX_DEBUG << "Received Buffer is empty";
    //        return;
    //    }
    //    //    ARMARX_IMPORTANT << "got Buffer";

    //    int width = 640;
    //    int height = 480;
    //    CByteImage image(width, height, CByteImage::eRGB24);
    //    size_t buf_size = GST_BUFFER_SIZE(buffer);
    //    if( width*height*3 != buf_size){
    //        ARMARX_WARNING << "Invalid buffer size: actual " << buf_size << " vs. expected " << width*height*3;
    //        gst_buffer_unref (buffer);
    //        return;
    //    }
    //    //    ARMARX_DEBUG << "copying data";
    //    memcpy(image.pixels, GST_BUFFER_DATA(buffer), width*height*3);

    //    std::stringstream str;
    //    str << "/tmp/images/snap" << std::setfill('0') << std::setw(3) << imageNumber++ << ".bmp";
    //    //    ARMARX_DEBUG << "Storing image";
    //    image.SaveToFile(str.str().c_str());

    //    gst_buffer_unref (buffer);
}
