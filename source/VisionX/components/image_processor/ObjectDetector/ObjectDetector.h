/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ObjectDetector
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/components/EarlyVisionGraph/GraphPyramidLookupTable.h>
#include <RobotAPI/components/EarlyVisionGraph/IntensityGraph.h>
#include <RobotAPI/components/EarlyVisionGraph/MathTools.h>


#include <VisionX/core/ImageProcessor.h>

#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>


#include <opencv2/opencv.hpp>
#include <Image/IplImageAdaptor.h>
#include <mutex>

namespace armarx
{
    /**
     * @class ObjectDetectorPropertyDefinitions
     * @brief
     */
    class ObjectDetectorPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ObjectDetectorPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("providerName", "ImageProvider", "Name of the image provider that should be used");
            defineOptionalProperty<std::string>("darkNetConfigPath", "VisionX/darknet", "Path to the folder that contains darknet weights");
        }
    };

    /**
     * @defgroup Component-ObjectDetector ObjectDetector
     * @ingroup VisionX-Components
     * A description of the component ObjectDetector.
     *
     * @class ObjectDetector
     * @ingroup Component-ObjectDetector
     * @brief Brief description of class ObjectDetector.
     *
     * Detailed description of class ObjectDetector.
     */
    class ObjectDetector :
        virtual public visionx::ImageProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ObjectDetector";
        }

    protected:


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void process() override;

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onDisconnectImageProcessor() override;
        void onExitImageProcessor() override;

    private:

        std::mutex mutex;

        std::string providerName;
        int numImages, numResultImages;
        CByteImage** images;
        CByteImage** result;

        std::string darkNetConfigPath;
        void darkNetObjectDetector();
        void createColorVector(int VectorSize);
        int maxObjectSize;


        struct rgbValues
        {
            uint8_t rVal;
            uint8_t gVal;
            uint8_t bVal;
        };
        std::vector<rgbValues > colorVector;


    };
}

