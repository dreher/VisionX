/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ObjectDetector
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectDetector.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <Image/ImageProcessor.h>

#ifdef USE_DARKNET
#include "DarkNet.h"
#endif

using namespace armarx;
using namespace visionx;


armarx::PropertyDefinitionsPtr ObjectDetector::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ObjectDetectorPropertyDefinitions(
            getConfigIdentifier()));
}

void armarx::ObjectDetector::onInitImageProcessor()
{
    providerName = getProperty<std::string>("providerName").getValue();
    usingImageProvider(providerName);

    // Get DarkNet path
    darkNetConfigPath = getProperty<std::string>("darkNetConfigPath").getValue();
    if (!ArmarXDataPath::getAbsolutePath(darkNetConfigPath, darkNetConfigPath))
    {
        ARMARX_ERROR << "Could not find dark net config file in ArmarXDataPath: " << darkNetConfigPath;
    }
    ARMARX_INFO << "darkNet config path: " << darkNetConfigPath << flush;

    // create a color vector
    maxObjectSize = 1000;
    createColorVector(maxObjectSize);
}

void armarx::ObjectDetector::onConnectImageProcessor()
{
    ImageType imageDisplayType = visionx::tools::typeNameToImageType("rgb");

    ImageProviderInfo imageProviderInfo = getImageProvider(providerName, imageDisplayType);

    numImages = imageProviderInfo.numberImages;

    images = new CByteImage*[numImages];
    for (int i = 0 ; i < numImages; i++)
    {
        images[i] = visionx::tools::createByteImage(imageProviderInfo);
    }

    numResultImages = 1;
    result = new CByteImage*[numResultImages];
    result[0] = images[0];

    ImageDimension dimension(images[0]->width, images[0]->height);
    enableResultImages(numResultImages, dimension, visionx::tools::typeNameToImageType("rgb"));

}

void armarx::ObjectDetector::onDisconnectImageProcessor()
{

}

void armarx::ObjectDetector::onExitImageProcessor()
{
    for (int i = 0; i < numImages; i++)
    {
        delete images[i];
    }
    delete [] images;

    for (int i = 0; i < numResultImages; i++)
    {
        delete result[i];
    }
    delete [] result;
}

void armarx::ObjectDetector::process()
{
    ARMARX_IMPORTANT << "ObjectDetector is processing data!";

    std::lock_guard<std::mutex> lock(mutex);

    ARMARX_VERBOSE << "waiting for images";
    if (!waitForImages(providerName))
    {
        ARMARX_WARNING << "Timeout while waiting for images";
        return;
    }

    ARMARX_VERBOSE << "getting images";
    if (getImages(images) != numImages)
    {
        ARMARX_WARNING << "Unable to transfer or read images";
        return;
    }

    darkNetObjectDetector();
    provideResultImages(result);

    ARMARX_IMPORTANT << "ObjectDetector process finished";
}

void ObjectDetector::darkNetObjectDetector()
{
#ifndef USE_DARKNET
    return;
#endif

    ARMARX_INFO << " DarkNet received image with width: " << images[0]->width << " height: " << images[0]->height ;
    IplImage* iplImage;
    iplImage = cvCreateImage(cvSize(images[0]->width, images[0]->height), 8, 3);

    // convert from CByteImage to iplImage
    #pragma omp parallel for
    for (int i = 0; i < images[0]->height; i++)
    {
        for (int j = 0; j < images[0]->width; j++)
        {
            int idx = i * images[0]->width + j;

            iplImage->imageData[i * iplImage->widthStep + j * iplImage->nChannels + 0] = images[0]->pixels[3 * idx + 0];
            iplImage->imageData[i * iplImage->widthStep + j * iplImage->nChannels + 1] = images[0]->pixels[3 * idx + 1];
            iplImage->imageData[i * iplImage->widthStep + j * iplImage->nChannels + 2] = images[0]->pixels[3 * idx + 2];
        }
    }

    // run the darknet
    int objectNumber = 0;
    int initSize = 1000;
    char** objectNames = (char**) malloc(initSize * sizeof(char*));
    int** objectBB = (int**) malloc(initSize * sizeof(int*));

    if (objectNames && objectBB)
    {
        for (int val = 0; val < initSize; val++)
        {
            objectNames[val] = (char*) malloc(sizeof * objectNames[val] * 100);
            objectBB[val] = (int*) malloc(sizeof * objectBB[val] * 4);
        }
    }

#ifdef USE_DARKNET
    char* baseFolderPath = &darkNetConfigPath[0];
    processIplImage(iplImage, objectNames, objectBB, &objectNumber, baseFolderPath);
#endif

    // just update the color vector
    if (objectNumber > maxObjectSize)
    {
        createColorVector(objectNumber);
    }

    if (true)
    {
        ARMARX_INFO << " DarkNet detected " <<  objectNumber << " objects!...";
        for (int objectIndex = 0; objectIndex < objectNumber; ++objectIndex)
            ARMARX_INFO << objectNames[objectIndex] << " ( " << objectBB[objectIndex][0] << " , " << objectBB[objectIndex][1]
                        << " , " << objectBB[objectIndex][2] << " , " << objectBB[objectIndex][3] << " ) " ;
    }

    // drawing results
    CvFont font;
    cvInitFont(&font, 5, 1, 1, 0, 1);
    for (int objectIndex = 0; objectIndex < objectNumber; ++objectIndex)
    {
        int xStart = objectBB[objectIndex][0];
        int yStart = objectBB[objectIndex][2];
        int xEnd = objectBB[objectIndex][1];
        int yEnd = objectBB[objectIndex][3];
        cvRectangle(iplImage, cvPoint(xStart, yStart), cvPoint(xEnd, yEnd), CV_RGB(colorVector[objectIndex].rVal, colorVector[objectIndex].gVal,  colorVector[objectIndex].bVal), 3);
        cvRectangle(iplImage, cvPoint(xStart, yStart), cvPoint(xEnd, yStart + 10), CV_RGB(colorVector[objectIndex].rVal, colorVector[objectIndex].gVal,  colorVector[objectIndex].bVal), 10);
        cvPutText(iplImage, objectNames[objectIndex], cvPoint(xStart + 20, yStart + 10), &font, cvScalar(0, 0, 0));
    }

    // convert from iplImage to CByteImage
    #pragma omp parallel for
    for (int i = 0; i < images[0]->height; i++)
    {
        for (int j = 0; j < images[0]->width; j++)
        {
            int idx = i * images[0]->width + j;

            result[0]->pixels[3 * idx + 0] = iplImage->imageData[i * iplImage->widthStep + j * iplImage->nChannels + 0];
            result[0]->pixels[3 * idx + 1] = iplImage->imageData[i * iplImage->widthStep + j * iplImage->nChannels + 1];
            result[0]->pixels[3 * idx + 2] = iplImage->imageData[i * iplImage->widthStep + j * iplImage->nChannels + 2];
        }
    }
}

void ObjectDetector::createColorVector(int VectorSize)
{
    int currSize = colorVector.size();
    // creating for the first time
    if (currSize == 0)
    {
        for (int i = 0; i < VectorSize; i++)
        {
            rgbValues currRGB;
            currRGB.rVal = rand() % 255;
            currRGB.gVal = rand() % 255;
            currRGB.bVal = rand() % 255;

            colorVector.push_back(currRGB);
        }
    }
    else
    {
        // updating the vector
        for (int i = currSize; i < VectorSize; i++)
        {
            rgbValues currRGB;
            currRGB.rVal = rand() % 255;
            currRGB.gVal = rand() % 255;
            currRGB.bVal = rand() % 255;

            colorVector.push_back(currRGB);
        }
    }
}
