/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ImageSourceSelection
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ImageSourceSelection.h"


#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>

#include <VisionX/tools/ImageUtil.h>
#include <VisionX/tools/TypeMapping.h>

#include <Image/ImageProcessor.h>

using namespace armarx;
using namespace visionx;



void ImageSourceSelection::onInitImageProcessor()
{
    providers = getProperty<std::vector<std::string>>("providerName").getValue();

    for (std::string name : providers)
    {
        ARMARX_LOG << "name" << name;
        usingImageProvider(name);
    }
}

void ImageSourceSelection::onConnectImageProcessor()
{
    ImageType imageDisplayType = visionx::tools::typeNameToImageType("rgb");
    ImageProviderInfo imageProviderInfo;
    imageProviderInfo.numberImages = 0;

    for (std::string name : providers)
    {
        if (imageProviderInfo.numberImages)
        {
            ImageProviderInfo otherImageProviderInfo = getImageProvider(name, imageDisplayType);

            if (imageProviderInfo.imageFormat != otherImageProviderInfo.imageFormat)
            {
                ARMARX_ERROR << "image format does not match.";
            }
            else if (numImages != otherImageProviderInfo.numberImages)
            {
                ARMARX_ERROR << "number of images do not match";
            }

            StereoCalibrationInterfacePrx calibrationProvider = StereoCalibrationInterfacePrx::checkedCast(imageProviderInfo.proxy);
            if (!calibrationProvider)
            {
                ARMARX_WARNING << "image provider does not have a stereo calibration interface";
            }
        }
        else
        {
            imageProviderInfo = getImageProvider(name, imageDisplayType);
            numImages = imageProviderInfo.numberImages;
        }

    }

    cameraImages = new CByteImage*[numImages];
    for (int i = 0 ; i < numImages; i++)
    {
        cameraImages[i] = visionx::tools::createByteImage(imageProviderInfo);
    }


    visionx::ImageDimension targetDimension = getProperty<visionx::ImageDimension>("TargetDimension").getValue();

    if (targetDimension.width != imageProviderInfo.imageFormat.dimension.width ||
        targetDimension.height != imageProviderInfo.imageFormat.dimension.height)
    {

        scaleFactorX = (float) imageProviderInfo.imageFormat.dimension.width / (float) targetDimension.width;
        imageProviderInfo.imageFormat.dimension = targetDimension;

        scaledCameraImages = new CByteImage*[numImages];
        for (int i = 0 ; i < numImages; i++)
        {
            scaledCameraImages[i] = visionx::tools::createByteImage(imageProviderInfo);
        }

        ARMARX_INFO << "image scale factor is " << scaleFactorX;

    }
    else
    {
        scaleFactorX = 0.0;
    }


    enableStereoResultImages(imageProviderInfo.imageFormat.dimension, imageDisplayType);

    setImageSource(providers[0]);
}

void ImageSourceSelection::onExitImageProcessor()
{
    for (int i = 0; i < numImages; i++)
    {
        delete cameraImages[i];
    }

    delete [] cameraImages;

    if (scaleFactorX > 0.0)
    {
        for (int i = 0; i < numImages; i++)
        {
            delete scaledCameraImages[i];
        }
    }
    delete [] scaledCameraImages;
}

void ImageSourceSelection::process()
{
    std::lock_guard<std::mutex> lock(mutex);

    if (!waitForImages(activeProvider))
    {
        ARMARX_WARNING << "Timeout while waiting for images";
        return;
    }
    armarx::MetaInfoSizeBasePtr info;
    if (getImages(activeProvider, cameraImages, info) != numImages)
    {
        ARMARX_WARNING << "Unable to transfer or read images";
        return;
    }

    if (scaleFactorX > 0.0)
    {
        info->size /= (2.0 * numImages * scaleFactorX);
        for (int i = 0; i < numImages; i++)
        {
            ::ImageProcessor::Resize(cameraImages[i], scaledCameraImages[i]);
        }
        provideResultImages(scaledCameraImages, info);
    }
    else
    {
        provideResultImages(cameraImages, info);
    }
}

void ImageSourceSelection::setImageSource(const std::string& imageSource, const Ice::Current& c)
{

    if (imageSource == activeProvider)
    {
        return;
    }
    if (!imageProviderInfoMap.count(imageSource))
    {
        ARMARX_WARNING << "invalid image source";
        return;
    }

    StereoCalibrationInterfacePrx calibrationProvider = StereoCalibrationInterfacePrx::checkedCast(imageProviderInfoMap[imageSource].proxy);

    std::lock_guard<std::mutex> lock(mutex);

    activeProvider = imageSource;

    if (calibrationProvider)
    {

        visionx::StereoCalibration stereoCalibration = calibrationProvider->getStereoCalibration();


        if (scaleFactorX)
        {

            stereoCalibration.calibrationLeft.cameraParam.focalLength[0] /= scaleFactorX;
            stereoCalibration.calibrationLeft.cameraParam.focalLength[1] /= scaleFactorX;

            stereoCalibration.calibrationLeft.cameraParam.principalPoint[0] /= scaleFactorX;
            stereoCalibration.calibrationLeft.cameraParam.principalPoint[1] /= scaleFactorX;

            stereoCalibration.calibrationLeft.cameraParam.width /= scaleFactorX;
            stereoCalibration.calibrationLeft.cameraParam.height /= scaleFactorX;

            stereoCalibration.calibrationRight.cameraParam.focalLength[0] /= scaleFactorX;
            stereoCalibration.calibrationRight.cameraParam.focalLength[1] /= scaleFactorX;


            stereoCalibration.calibrationRight.cameraParam.principalPoint[0] /= scaleFactorX;
            stereoCalibration.calibrationRight.cameraParam.principalPoint[1] /= scaleFactorX;



            stereoCalibration.calibrationRight.cameraParam.width = ((float) stereoCalibration.calibrationRight.cameraParam.width) / scaleFactorX;
            stereoCalibration.calibrationRight.cameraParam.height = ((float) stereoCalibration.calibrationRight.cameraParam.height) / scaleFactorX;

        }


        StereoResultImageProviderPtr stereoResultImageProvider = StereoResultImageProviderPtr::dynamicCast(resultImageProvider);
        stereoResultImageProvider->setStereoCalibration(stereoCalibration, calibrationProvider->getImagesAreUndistorted(), calibrationProvider->getReferenceFrame());

    }
}


void ImageSourceSelection::enableStereoResultImages(ImageDimension imageDimension, ImageType imageType)
{
    if (!resultImageProvider)
    {
        ARMARX_INFO << "Enabling StereoResultImageProvider";

        StereoResultImageProviderPtr stereoResultImageProvider = Component::create<StereoResultImageProvider>();
        stereoResultImageProvider->setName(getProperty<std::string>("resultProviderName"));
        resultImageProvider = IceInternal::Handle<ResultImageProvider>::dynamicCast(stereoResultImageProvider);

        getArmarXManager()->addObject(resultImageProvider);

        resultImageProvider->setNumberResultImages(2);
        resultImageProvider->setResultImageFormat(imageDimension, imageType);

        // wait for resultImageProvider
        resultImageProvider->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted);
    }
}


armarx::PropertyDefinitionsPtr ImageSourceSelection::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ImageSourceSelectionPropertyDefinitions(
            getConfigIdentifier()));
}
