/**
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    TabletTeleoperation::ArmarXObjects::StreamImageProvider
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <VisionX/core/ImageProvider.h>
#include <VisionX/components/image_processor/StreamReceiver/StreamReceiver.h>
namespace armarx
{
    /**
     * @class StreamImageProvider
     * @brief StreamImageProvider implements a ImageProvider interfaces
     * and retrieves images of a StreamReceiver, which in turn decodes
     * an image stream into raw images.
     *
     * This class is the interface to
     * from a compressed video stream to the normal visionx image provider
     * format.
     *
     *
     */
    class StreamImageProvider :
        virtual public visionx::ImageProvider

    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "StreamImageProvider";
        }

        void setReceiver(StreamReceiverPtr receiver);
    protected:


        // ImageProvider interface
    protected:
        void onConnectComponent();
        void onInitImageProvider();
        void onConnectImageProvider();
        void onExitImageProvider();

        // ManagedIceObject interface
    private:

        void pullImages();
        StreamReceiverPtr streamReceiver;
        std::vector<CByteImage*> images;
        PeriodicTask<StreamImageProvider>::pointer_type imagePullTask;


    };
    typedef IceInternal::Handle<StreamImageProvider> StreamImageProviderPtr;
}

