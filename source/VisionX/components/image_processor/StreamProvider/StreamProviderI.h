/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <boost/thread.hpp>
#include <Image/ByteImage.h>
#include <string>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/Synchronization.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/components/StreamProvider.h>

// boost
#include <boost/thread.hpp>
#include <boost/circular_buffer.hpp>
extern "C" {
#include <x264.h>
#include <libswscale/swscale.h>
}




#define NUMBER_OF_COMPRESSIONRATES 3 //should hold the number of compression rates (the compression rates  are defined in the enum ::Stream::CompressionRate)


namespace Stream
{
    /*
     * This class realizes the StreamProviderImageProcessorInterface-SliceInterface and is a subclass of visionx::ImageProcessor.
     * On tablet-side the methods of this class can be called through an Ice-Proxy.
    */
    class StreamProviderI:
        public StreamProviderImageProcessorInterface,
        public visionx::ImageProcessor
    {

    public:
        class StreamProviderPropertyDefinitions:
            public armarx::ComponentPropertyDefinitions
        {
        public:
            StreamProviderPropertyDefinitions(std::string prefix):
                ComponentPropertyDefinitions(prefix)
            {
                defineOptionalProperty<std::string>("imageProviderProxyName", "TestImageProvider", "Name of the image provider");
                defineOptionalProperty<std::string>("imageStreamTopicName", "ImageStream", "Name of the image streaming topic");
                defineOptionalProperty<std::string>("h264Preset", "veryfast", "Preset for the h264 codec");
                defineOptionalProperty<std::string>("h264Profile", "Main", "profile for the h264 codec");
                //            defineOptionalProperty<std::string>("OfferedSources", "Camera,Test", "Names of sources that are to be offered. For Identification on Receiver Side. seperated by comma, space or tab");
                //            defineOptionalProperty<int>("ImageWidth", 640, "Image width");
                //            defineOptionalProperty<int>("ImageHeight", 480, "Image height");
                defineOptionalProperty<float>("CompressionRate", 25, "QRC value of h264 codec");
                defineOptionalProperty<CodecType>("Codec", eH264, "Codec used for compression")
                .setCaseInsensitive(true)
                .map("h264", eH264);
                defineOptionalProperty<float>("Framerate", 25, "Framerate at which the input images are encoded. Slower or fasting pushing of input is possible. Frames will be skipped then. ");
            }
        };
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new StreamProviderPropertyDefinitions(
                           getConfigIdentifier()));
        }
        StreamProviderI();

        void stopCapture(const ::Ice::Current& = ::Ice::Current()) override;
        /** starts the capture for the given source */
        bool startCapture(const ::Ice::Current& = ::Ice::Current()) override;

        void getImageInformation(int& imageWidth, int& imageHeight, int& imageType,  const Ice::Current& c = Ice::Current()) override;
        int getNumberOfImages(const ::Ice::Current& = ::Ice::Current()) override;
        void setCompressionRate(::Stream::CompressionRate = COMPRESSIONHIGH, const ::Ice::Current& = ::Ice::Current()) override;
        CodecType getCodecType(const Ice::Current&) override;


        /* Inherited from ImageProcessor. */
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        virtual void onDisConnectImageProcessor();
        void onExitImageProcessor() override;
        void process() override;

        /**
         * Executes a single cycle of the encoding process.
         */
        void step(const Ice::Current& c = ::Ice::Current());


        std::string getDefaultName() const override;

        void onStartRecording(const ::Ice::Current& = ::Ice::Current()) override;



    private:

        //Pushes the given image into the given appsrc, helper method for pushNewImages()
        void calculateFps(); //calcuates the current fps for the streamSources (as (80% old:20% new) weighted medium)
        armarx::PeriodicTask<StreamProviderI>::pointer_type fpsCalculator; //task, which calcualtes the fps


        bool capturing = false;
        std::string imageProviderProxyName;
        visionx::ImageProviderInterfacePrx imageProviderProxy;
        visionx::ImageFormatInfo imageFormat;
        int numberImages;
        int imgWidth;
        int imgHeight;
        int imgType;
        int encodedImgHeight;

        CByteImage** ppInputImages, * pImageForEncoder;
        typedef boost::shared_ptr<CByteImage> CByteImagePtr;
        typedef std::vector<CByteImagePtr> ImageContainer;

        x264_param_t param;
        float fps;
        x264_t* encoder;
        x264_picture_t pic_in, pic_out;
        SwsContext* convertCtx;
        int frameCounter = 0;
        Stream::StreamListenerInterfacePrx listener;
    };
    typedef IceInternal::Handle<StreamProviderI> StreamProviderIPtr;
}
