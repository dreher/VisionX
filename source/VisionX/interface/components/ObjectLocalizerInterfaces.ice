/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     David Schiebener <schiebener at kit dot edu>
* @copyright  2015 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <VisionX/interface/core/PointCloudProcessorInterface.ice>

#include <VisionX/interface/components/ImageSourceSelectionInterface.ice>

#include <MemoryX/interface/workingmemory/WorkingMemoryUpdaterBase.ice>

/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{
    interface ObjectLocalizerImageInterface extends ImageProcessorInterface, memoryx::ObjectLocalizerInterface, armarx::StereoCalibrationUpdateInterface
    {
    };

    interface ObjectLocalizerPointCloudInterface extends PointCloudProcessorInterface, memoryx::ObjectLocalizerInterface
    {
    };

    interface ObjectLocalizerPointCloudAndImageInterface extends PointCloudAndImageProcessorInterface, memoryx::ObjectLocalizerInterface
    {
    };
};

