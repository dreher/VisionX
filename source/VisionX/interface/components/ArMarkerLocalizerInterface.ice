/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     David Schiebener <schiebener at kit dot edu>
* @copyright  2015 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>

/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{
    struct ArMarkerLocalizationResult
    {
        int id;
        armarx::FramedPoseBase pose;
    };

    sequence<ArMarkerLocalizationResult> ArMarkerLocalizationResultList;

    interface ArMarkerLocalizerInterface extends ImageProcessorInterface
    {
        ArMarkerLocalizationResultList LocalizeAllMarkersNow();
        ArMarkerLocalizationResultList GetLatestLocalizationResult();
    };
};

