/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::ObjectMemory
* @author     David Schiebener <david dot schiebener at kit dot edu>
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>

#include <RobotAPI/interface/units/UnitInterface.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{
    /**
     * The ObjectMemory contains all available information about the 
     * location of known objects that have been requested to be
     * localized continuously.
     */
    interface ObjectMemoryInterface extends armarx::SensorActorUnitInterface
    {
        /**
         * Add an object to the memory that has to be localized and observed
         *
         * @param objectName the name of the object
         * @param initialPose an initial guess of the pose of the object that may significantly speed up the search for it
         * @param initialPriority the priority of the object
         */
        void addObject(string objectName, armarx::PoseBase initialPose, int initialPriority);


        /**
         * Change the prioriy of an object that is being observed
         *
         * @param objectName name of the object
         * @param newPriority the priority of the object
         */
        void changeObjectPriority(string objectName, int newPriority);


        /**
         * Receive the result from a localizer
         * Maybe we don't need this function in the interface.
         *
         * @param objectName name of the object that has been localized
         * @param newPose the pose as determined by the localizer
         * @param recognitionCertainty the certainty that the recognition was successfull and correct
         */
        void reportLocalizationResult(string objectName, armarx::PoseBase newPose, float recognitionCertainty);
    };


    /**
     * An ObjectMemoryListener is an observer that watches the changes of the ObjectMemory
     */
    interface ObjectMemoryListenerInterface
    {
        /**
         * Initiates object recognition for a specific object.
         *
         * @param objectInfo the information about the object that is being reported
         */
        void reportObjectInformationUpdate(types::ObjectInformation objectInfo);
    };

};

