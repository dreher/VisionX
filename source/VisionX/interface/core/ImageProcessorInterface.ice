/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     Kai Welke <welke at kit dot edu>
* @copyright  2010 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>
#include <VisionX/interface/core/ImageProviderInterface.ice>

/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{
    /**
     * An image processor component listener interface. A 
     * listener is able to receive image update notifications
     * from multiple image providers. This supports image
     * transfer via shared memory if the listener and provider
     * reside on the same machine. 
     */     
    interface ImageProcessorInterface 
    {
        /**
         * Image update notification callback invoked by the 
         * respective registerd provider.
         *
         * @param providerName name of the notifying provider.
         *        In case of multiple provider this can be used
         *        to discriminate the image source.
         */
        void reportImageAvailable(string providerName);
    };
};

