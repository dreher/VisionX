# interfaces mus come first
add_subdirectory(interface)
add_subdirectory(tools)
add_subdirectory(core) 
add_subdirectory(libraries)
add_subdirectory(components)
add_subdirectory(statecharts)
add_subdirectory(vision_algorithms)
# gui-plugins and applications must come last
add_subdirectory(gui-plugins)
add_subdirectory(applications)

