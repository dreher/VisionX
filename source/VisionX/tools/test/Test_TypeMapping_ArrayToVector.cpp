/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools::Tests
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::tools::TypeMapping::ArrayToVector
#define ARMARX_BOOST_TEST
#include <VisionX/Test.h>

// STL
#include <vector>

// VisionXTools
#include <VisionX/tools/TypeMapping.h>

// Fixture use
struct VectorArrayTestFixture
{
    VectorArrayTestFixture() :
        intVector(), intArray(0)
    {
        BOOST_TEST_MESSAGE("setup VectorTestFixture");

        int arraySize = 3;
        intArray = new int[arraySize];

        intArray[0] = 2;
        intArray[1] = 1;
        intArray[2] = 0;

        visionx::tools::arrayToVector(intArray, 3, intVector);
    }

    ~VectorArrayTestFixture()
    {
        BOOST_TEST_MESSAGE("teardown VectorTestFixture");

        if (intArray)
        {
            delete intArray;
        }
    }

    std::vector<int> intVector;
    int* intArray;
};


BOOST_FIXTURE_TEST_CASE(VectorContentTest, VectorArrayTestFixture)
{
    BOOST_CHECK(intVector[0] == 2);
    BOOST_CHECK(intVector[1] == 1);
    BOOST_CHECK(intVector[2] == 0);
}


BOOST_FIXTURE_TEST_CASE(ArrayVectorEqualityTest, VectorArrayTestFixture)
{
    BOOST_CHECK(intVector[0] == intArray[0]);
    BOOST_CHECK(intVector[1] == intArray[1]);
    BOOST_CHECK(intVector[2] == intArray[2]);
}


BOOST_FIXTURE_TEST_CASE(VectorSizeTest, VectorArrayTestFixture)
{
    BOOST_CHECK(intVector.size() == 3);
}
