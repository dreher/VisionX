/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ImageViewerArea.h"

#include <qpainter.h>
#include <qimage.h>

#include <IceUtil/Time.h>


namespace visionx
{
    ImageViewerArea::ImageViewerArea(QWidget* parent)
        : QWidget(parent)
    {
        inputWidth = -1;
        inputHeight = -1;
        numberImages = 0;

        buffer = 0;
    }

    ImageViewerArea::~ImageViewerArea()
    {
        if (buffer)
        {
            delete [] buffer;
        }
    }

    void ImageViewerArea::paintEvent(QPaintEvent* e)
    {
        if (numberImages == 0)
        {
            return;
        }

        QPainter painter(this);
        boost::mutex::scoped_lock lock(imageMutex);
        painter.drawImage(0, 0, scaledImage);
    }

    int ImageViewerArea::getNumberImages()
    {
        return numberImages;
    }

    void ImageViewerArea::setImages(int numberImages, CByteImage** images)
    {
        boost::mutex::scoped_lock lock(bufferMutex);


        if (inputWidth != images[0]->width || inputHeight != images[0]->height || this->numberImages != numberImages)
        {
            inputWidth = images[0]->width;
            inputHeight = images[0]->height;
            this->numberImages = numberImages;

            if (buffer)
            {
                delete [] buffer;
            }

            buffer = new unsigned char[inputWidth * inputHeight * 4 * numberImages];
        }


        for (int i = 0 ; i < numberImages ; i++)
        {
            if (images[i]->type == CByteImage::eRGB24)
            {
                unsigned char* pixels = images[i]->pixels;
                int* output = (int*) buffer;
                int inputOffset = 0;

                for (int y = 0 ; y < inputHeight ; y++)
                {
                    int baseOffset =  y * inputWidth * numberImages + i * inputWidth;
                    for (int x = 0 ; x < inputWidth ; x++)
                    {
                        output[x + baseOffset] = 255 << 24 | pixels[inputOffset] << 16 | pixels[inputOffset + 1] << 8 | pixels[inputOffset + 2];
                        inputOffset += 3;
                    }
                }

            }
        }

        QImage image(buffer, inputWidth * this->numberImages, inputHeight, QImage::Format_RGB32);
        //scaledImage = image.scaled (width(), height(),  Qt::KeepAspectRatio, Qt::FastTransformation);
        boost::mutex::scoped_lock lock2(imageMutex);
        scaledImage = image.scaled(width(), height(),  Qt::KeepAspectRatio, Qt::SmoothTransformation);


        QMetaObject::invokeMethod(this, "updateImage");
    }

    void ImageViewerArea::updateImage()
    {
        update(0, 0, width(), height());
    }

    Vec2d ImageViewerArea::getScaledImageDimensions()
    {
        return {(float) scaledImage.width(), (float) scaledImage.height()};
    }

}

