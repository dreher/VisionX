/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "Manager.h"

namespace visionx
{

    Manager::Manager()
    {

    }

    Manager::~Manager()
    {

    }

    void Manager::registerPointCloud(const std::string& name)
    {
        armarx::ScopedLock lock(mutex);
        for (auto cloud : clouds)
        {
            if (cloud.first.compare(name) == 0)
            {
                return;
            }
        }
        this->clouds.push_back(std::pair<std::string, CoinPointCloud*>(name, NULL));
    }

    void Manager::unregisterPointCloud(const std::string& name)
    {
        armarx::ScopedLock lock(mutex);
        for (auto& cloud : clouds)
        {
            if (cloud.first.compare(name) == 0)
            {
                if (cloud.second != NULL)
                {
                    int position = this->findChild(cloud.second);
                    if (position != -1)
                    {
                        this->removeChild(position);
                    }
                }
                clouds.erase(std::remove(clouds.begin(), clouds.end(), cloud), clouds.end());
            }
        }
    }

    std::vector<std::string> Manager::getRegisteredPointClouds()
    {
        armarx::ScopedLock lock(mutex);

        std::vector<std::string> cloud_list;

        for (auto cloud : clouds)
        {
            cloud_list.push_back(cloud.first);
        }

        return cloud_list;
    }

    CoinPointCloud* Manager::getCloudByName(std::string& name)
    {
        armarx::ScopedLock lock(mutex);
        for (auto& cloud_item : clouds)
        {
            if (cloud_item.first.compare(name) == 0)
            {
                return cloud_item.second;
            }
        }
        return NULL;
    }

    void Manager::updatePointCloud(const std::string& name, CoinPointCloud& cloud)
    {
        armarx::ScopedLock lock(mutex);

        for (auto& cloud_item : clouds)
        {
            if (cloud_item.first.compare(name) == 0)
            {
                if (cloud_item.second != NULL)
                {
                    //This will also bring ref counter of cloud to zero
                    //and free memory
                    int position = this->findChild(cloud_item.second);
                    if (position != -1)
                    {
                        this->removeChild(position);
                    }
                }
                cloud_item.second = &cloud;
                this->addChild(&cloud);
            }
        }
    }
}
