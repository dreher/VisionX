/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    VisionX::gui-plugins::WidgetController
 * \author     Philipp Schmidt ( ufedv at student dot kit dot edu )
 * \date       2015
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WidgetController.h"

#include <algorithm>

//Ice
#include <Ice/Ice.h>

//VisionX
#include <VisionX/interface/core/PointCloudProviderInterface.h>
#include <VisionX/components/pointcloud_core/PointCloudProvider.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/CoinViewer.h>

// PCL tools
#include <VisionX/components/pointcloud_core/PCLUtilities.h>

#include "CoinPointCloud.h"

namespace visionx
{
    WidgetController::WidgetController() : recenterCamera(false)
    {
        qRegisterMetaType<IceGrid::ObjectInfoSeq>("IceGrid::ObjectInfoSeq");
    }

    WidgetController::~WidgetController()
    {
    }

    void WidgetController::loadSettings(QSettings* settings)
    {

    }

    void WidgetController::saveSettings(QSettings* settings)
    {

    }

    void WidgetController::onInitPointCloudProcessor()
    {
        ARMARX_INFO << "onInitPointCloudProcessor" << std::flush;
    }

    void WidgetController::onConnectPointCloudProcessor()
    {
        ARMARX_INFO << "onConnectPointCloudProcessor" << std::flush;

        //Refresh list once
        this->refreshProviderList();

        //Ice is ready, allow refreshing of list
        QMetaObject::invokeMethod(widget.refreshButton, "setEnabled", Q_ARG(bool, true));
    }

    void WidgetController::onDisconnectPointCloudProcessor()
    {
        ARMARX_INFO << "onDisconnectPointCloudProcessor" << std::flush;
        QMetaObject::invokeMethod(this, "disconnectQt");

    }

    void WidgetController::disconnectQt()
    {
        auto icemanager = this->getIceManager();
        if (!icemanager)
        {
            return;
        }
        //Let's check which providers just went offline
        for (int row = 0; row < widget.providerList->count(); row++)
        {
            QListWidgetItem* item = widget.providerList->item(row);
            std::string subscriber = item->text().toStdString();

            ARMARX_LOG << subscriber << " has state " << icemanager->getIceGridSession()->getComponentState(subscriber) << std::endl;

            //Check component status
            if (icemanager->getIceGridSession()->getComponentState(subscriber) != armarx::IceGridAdmin::eActivated)
            {
                //Unregister cloud
                manager->unregisterPointCloud(subscriber);

                //Remove item from provider list (and free memory) and clear table if it was selected
                if (item->isSelected())
                {
                    this->widget.tableWidget->setRowCount(0);
                }
                delete this->widget.providerList->takeItem(this->widget.providerList->row(item));
                //Indices got reduced by one
                row--;

                //Release point cloud provider
                this->releasePointCloudProvider(subscriber);
            }
        }
    }

    void WidgetController::onExitPointCloudProcessor()
    {
        ARMARX_INFO << "onExitPointCloudProcessor" << std::flush;
    }

    void WidgetController::process()
    {
        std::lock_guard<std::mutex> lock(mutex);

        //Fetch all point cloud providers for updates and save in buffer
        for (std::string providerName : providerNames)
        {
            if (!this->pointCloudHasNewData(providerName))
            {
                //Skip this one, no new data available
                continue;
            }

            //Save in this buffer
            pcl::PointCloud<PointT>::Ptr providerBuffer(new pcl::PointCloud<PointT>());

            //Check point cloud type
            visionx::MetaPointCloudFormatPtr info = this->getPointCloudFormat(providerName);
            visionx::PointContentType pointCloudContent = info->type;

            if (pointCloudContent == visionx::ePoints)
            {
                pcl::PointCloud<pcl::PointXYZ>::Ptr newPointCloudPtr(new pcl::PointCloud<pcl::PointXYZ>());

                if (getPointClouds<pcl::PointXYZ>(providerName, newPointCloudPtr))
                {
                    pcl::copyPointCloud(*newPointCloudPtr, *providerBuffer);
                }
            }
            else if (pointCloudContent == visionx::eColoredPoints)
            {
                pcl::PointCloud<pcl::PointXYZRGBA>::Ptr newPointCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBA>());

                if (getPointClouds<pcl::PointXYZRGBA>(providerName, newPointCloudPtr))
                {
                    pcl::copyPointCloud(*newPointCloudPtr, *providerBuffer);
                }
            }
            else if (pointCloudContent == visionx::eColoredLabeledPoints)
            {
                pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloudPtr(new pcl::PointCloud<pcl::PointXYZRGBA>());
                pcl::PointCloud<pcl::PointXYZRGBL>::Ptr newPointCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBL>());
                pcl::PointCloud<pcl::PointXYZL>::Ptr labeledCloudPtr(new pcl::PointCloud<pcl::PointXYZL>());

                if (getPointClouds<pcl::PointXYZRGBL>(providerName, newPointCloudPtr))
                {
                    pcl::copyPointCloud(*newPointCloudPtr, *labeledCloudPtr);
                    tools::colorizeLabeledPointCloud(labeledCloudPtr, cloudPtr);
                    pcl::copyPointCloud(*cloudPtr, *providerBuffer);
                }
            }
            else if (pointCloudContent == visionx::eLabeledPoints)
            {
                pcl::PointCloud<pcl::PointXYZRGBA>::Ptr newPointCloudPtr(new pcl::PointCloud<pcl::PointXYZRGBA>());
                pcl::PointCloud<pcl::PointXYZL>::Ptr labeledCloudPtr(new pcl::PointCloud<pcl::PointXYZL>());

                if (getPointClouds<pcl::PointXYZL>(providerName, labeledCloudPtr))
                {
                    tools::colorizeLabeledPointCloud(labeledCloudPtr, newPointCloudPtr);
                    pcl::copyPointCloud(*newPointCloudPtr, *providerBuffer);
                }
            }
            else
            {
                ARMARX_WARNING << deactivateSpam() << "unable to handle point cloud content";
                return;
            }


            CoinPointCloud* cloud = new CoinPointCloud(providerBuffer);
            emit updatePointCloud(QString::fromStdString(providerName), cloud);
        }
    }

    void WidgetController::processPointCloud(QString providerName, CoinPointCloud* cloud)
    {
        manager->updatePointCloud(providerName.toStdString(), *cloud);
        if (recenterCamera)
        {
            widget.pointCloudDisplay->getDisplay()->cameraViewAll();
            recenterCamera = false;
        }
    }

    void WidgetController::updateProviderList(IceGrid::ObjectInfoSeq objects)
    {
        ARMARX_INFO_S << "updating cloudlist";
        IceGrid::ObjectInfoSeq::iterator iter = objects.begin();

        //Signal user we are fetching providers
        widget.loadingStatusLabel->setText(QString::fromStdString("Loading..."));

        //Count how many providers are online
        int counter = 0;

        //Iterate over all objects
        while (iter != objects.end())
        {
            Ice::ObjectPrx current = iter->proxy;
            std::string name = this->getIceManager()->getCommunicator()->proxyToString(current);

            int pos = name.find(' ');
            std::string shortName = name.substr(0, pos);

            //Check if we didn't connect to this provider yet
            //Invariancy: Every item in list widget is a provider we already connected in the past
            if (widget.providerList->findItems(QString::fromStdString(shortName), Qt::MatchExactly).count() == 0)
            {
                //Add new item and make it checkable
                QListWidgetItem* item = new QListWidgetItem(QString::fromStdString(shortName), widget.providerList);
                item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
                item->setCheckState(Qt::Unchecked);

                //Subscribe to topic, provider is fetchable after that
                this->usingPointCloudProvider(shortName);
                this->getPointCloudProvider(shortName);
            }

            counter++;
            iter++;
        }

        //Signal user we are done and show how many providers are online
        widget.loadingStatusLabel->setText(QString::fromStdString("Loading complete: " + std::to_string(counter) + " provider found"));
    }

    void WidgetController::refreshProviderList()
    {
        ARMARX_INFO_S << "get cloud providers from ice";
        //Get ice grid admin
        armarx::IceGridAdminPtr admin = this->getIceManager()->getIceGridSession();
        IceGrid::ObjectInfoSeq objects = admin->getRegisteredObjects<PointCloudProviderInterfacePrx>(widget.lineEditPointCloudFilter->text().toStdString());
        QMetaObject::invokeMethod(this, "updateProviderList", Q_ARG(IceGrid::ObjectInfoSeq, objects));
    }


    void WidgetController::showPointCloudInfo(QListWidgetItem* item)
    {
        visionx::MetaPointCloudFormatPtr info =  this->getPointCloudFormat(item->text().toStdString());

        widget.tableWidget->setRowCount(8);
        widget.tableWidget->setColumnCount(2);

        //Name
        widget.tableWidget->setItem(0, 0, new QTableWidgetItem("Name"));
        widget.tableWidget->setItem(0, 1, new QTableWidgetItem(item->text()));

        //Type
        widget.tableWidget->setItem(1, 0, new QTableWidgetItem("Type"));
        widget.tableWidget->setItem(1, 1, new QTableWidgetItem(QString::fromStdString(conversionMap[info->type])));
        widget.tableWidget->item(1, 0)->setBackgroundColor(Qt::gray);
        widget.tableWidget->item(1, 1)->setBackgroundColor(Qt::gray);

        //Width
        widget.tableWidget->setItem(2, 0, new QTableWidgetItem("Width"));
        widget.tableWidget->setItem(2, 1, new QTableWidgetItem(QString::fromStdString(std::to_string(info->width))));

        //Height
        widget.tableWidget->setItem(3, 0, new QTableWidgetItem("Height"));
        widget.tableWidget->setItem(3, 1, new QTableWidgetItem(QString::fromStdString(std::to_string(info->height))));
        widget.tableWidget->item(3, 0)->setBackgroundColor(Qt::gray);
        widget.tableWidget->item(3, 1)->setBackgroundColor(Qt::gray);

        //Size
        widget.tableWidget->setItem(4, 0, new QTableWidgetItem("Size"));
        widget.tableWidget->setItem(4, 1, new QTableWidgetItem(QString::fromStdString(std::to_string(info->size))));

        //Capacity
        widget.tableWidget->setItem(5, 0, new QTableWidgetItem("Capacity"));
        widget.tableWidget->setItem(5, 1, new QTableWidgetItem(QString::fromStdString(std::to_string(info->capacity))));
        widget.tableWidget->item(5, 0)->setBackgroundColor(Qt::gray);
        widget.tableWidget->item(5, 1)->setBackgroundColor(Qt::gray);

        //Sequence
        widget.tableWidget->setItem(6, 0, new QTableWidgetItem("Sequence"));
        widget.tableWidget->setItem(6, 1, new QTableWidgetItem(QString::fromStdString(std::to_string(info->seq))));

        //Time provided
        widget.tableWidget->setItem(7, 0, new QTableWidgetItem("TimeProvided"));
        widget.tableWidget->setItem(7, 1, new QTableWidgetItem(QString::fromStdString(std::to_string(info->timeProvided))));
        widget.tableWidget->item(7, 0)->setBackgroundColor(Qt::gray);
        widget.tableWidget->item(7, 1)->setBackgroundColor(Qt::gray);
    }

    void WidgetController::pointCloudChecked(QListWidgetItem* item)
    {

        std::lock_guard<std::mutex> lock(mutex);

        std::string providerName = item->text().toStdString();

        if (item->checkState())
        {
            providerNames.push_back(providerName);
            manager->registerPointCloud(providerName);
        }
        else
        {
            auto it = std::find(providerNames.begin(), providerNames.end(), providerName);
            if (it != providerNames.end())
            {
                providerNames.erase(it);
            }
            manager->unregisterPointCloud(providerName);
        }
        recenterCamera = true;
    }

    void WidgetController::selectColor()
    {
        QColor selectedColor = this->colorDialog.getColor();

        widget.pointCloudDisplay->getDisplay()->setBackgroundColor(SbColor(selectedColor.red() / 255.0f, selectedColor.green() / 255.0f, selectedColor.blue() / 255.0f));
    }

    void WidgetController::savePointCloud()
    {
        if (widget.providerList->selectedItems().size() == 1)
        {
            SaveDialog saveDialog(this->getMainWindow());
            std::string providerID = widget.providerList->selectedItems()[0]->text().toStdString();
            std::vector<std::string> clouds = manager->getRegisteredPointClouds();
            if (std::find(clouds.begin(), clouds.end(), providerID) != clouds.end())
            {
                saveDialog.setPointCloudToSave(manager->getCloudByName(providerID));
                int result = saveDialog.exec();

                if (result == QDialog::Accepted)
                {
                    pcl::io::savePCDFile(saveDialog.getResultFileName(), *manager->getCloudByName(providerID)->getPCLCloud());
                }

                saveDialog.releaseBuffer();
            }
        }
    }

    QPointer<QWidget> WidgetController::getWidget()
    {
        if (!m_widget)
        {
            m_widget = new QWidget();
            initUI();
        }

        return m_widget;
    }

    void WidgetController::initUI()
    {
        //Setup user interface
        widget.setupUi(getWidget());

        //qRegisterMetaType<visionx::CoinPointCloud*>("visionx::CoinPointCloud*");

        //Connect update calls with process calls
        connect(this, SIGNAL(updatePointCloud(QString, CoinPointCloud*)), this, SLOT(processPointCloud(QString, CoinPointCloud*)));

        //Add manager
        manager = new Manager();
        widget.pointCloudDisplay->getDisplay()->getRootNode()->addChild(manager);

        //Manually fill conversion map
        conversionMap[visionx::PointContentType::ePoints] = "ePfxPfyPfz";
        conversionMap[visionx::PointContentType::eColoredPoints] = "eCbrCbgCbbCba_PfxPfyPfz";
        conversionMap[visionx::PointContentType::eColoredLabeledPoints] = "eColoredLabeledPoints";
        conversionMap[visionx::PointContentType::eLabeledPoints] = "eLabeledPoints";

        //Remove line counter in table
        widget.tableWidget->verticalHeader()->setVisible(false);

        //Disable editing of table
        widget.tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

        //Button for manual refreshing
        connect(widget.refreshButton, SIGNAL(clicked()), this, SLOT(refreshProviderList()));

        //Do not allow refreshing until component connected and ice is ready
        //        widget.refreshButton->setEnabled(false);

        //Allow to chose background color
        connect(widget.backgroundButton, SIGNAL(clicked()), this, SLOT(selectColor()));

        //Items get selected and deselected in list, show infos for pointcloud
        connect(widget.providerList, SIGNAL(itemPressed(QListWidgetItem*)), this, SLOT(showPointCloudInfo(QListWidgetItem*)));

        //Items get checked and unchecked in list, add to viewport
        connect(widget.providerList, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(pointCloudChecked(QListWidgetItem*)));

        //Connect save point cloud button
        connect(widget.saveButton, SIGNAL(clicked()), this, SLOT(savePointCloud()));
    }
}
