/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Peter Kaiser (peter dot kaiser at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "PointCloudVisualizationConfigDialog.h"
#include "ui_PointCloudVisualizationConfigDialog.h"

#include <QTimer>
#include <QPushButton>
#include <QMessageBox>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <VisionX/interface/components/PointCloudVisualization.h>

#include <IceUtil/UUID.h>

using namespace armarx;

PointCloudVisualizationConfigDialog::PointCloudVisualizationConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::PointCloudVisualizationConfigDialog),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);

    connect(this->ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfig()));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);

    pointCloudVisualizationProxyFinder = new IceProxyFinder<visionx::PointCloudVisualizationInterfacePrx>(this);

    pointCloudVisualizationProxyFinder->setSearchMask("*");

    ui->gridLayout->addWidget(pointCloudVisualizationProxyFinder, 0, 1, 1, 2);
}

PointCloudVisualizationConfigDialog::~PointCloudVisualizationConfigDialog()
{
    delete ui;
}

void PointCloudVisualizationConfigDialog::onInitComponent()
{
    pointCloudVisualizationProxyFinder->setIceManager(getIceManager());
}

void PointCloudVisualizationConfigDialog::onConnectComponent()
{
}

void PointCloudVisualizationConfigDialog::onExitComponent()
{
    QObject::disconnect();
}

void PointCloudVisualizationConfigDialog::verifyConfig()
{
    if (!pointCloudVisualizationProxyFinder->getSelectedProxyName().trimmed().length())
    {
        QMessageBox::critical(this, "Invalid Configuration", "The proxy name must not be empty");
        return;
    }

    this->accept();
}
