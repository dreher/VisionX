/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::PointCloudVisualizationWidgetController
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ui_PointCloudVisualizationWidget.h"
#include "PointCloudVisualizationConfigDialog.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <VisionX/interface/components/PointCloudVisualization.h>

namespace armarx
{
    /**
    \page VisionX-GuiPlugins-PointCloudVisualization PointCloudVisualization
    \brief The PointCloudVisualization allows visualizing point clouds provided by configured PointCloudProviders using a DebugDrawer topic

    API Documentation \ref PointCloudVisualizationWidgetController

    \see PointCloudVisualizationGuiPlugin
    */

    /**
     * \class PointCloudVisualizationWidgetController
     * \brief PointCloudVisualizationWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        PointCloudVisualizationWidgetController:
        public ArmarXComponentWidgetControllerTemplate<PointCloudVisualizationWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit PointCloudVisualizationWidgetController();

        /**
         * Controller destructor
         */
        virtual ~PointCloudVisualizationWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;

        void configured() override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "VisionX.PointCloudVisualization";
        }

        static QIcon GetWidgetIcon()
        {
            return QIcon {"://icons/point_cloud_visu.svg"};
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        void onDisconnectComponent() override;

    protected:
        void updateProviderList();

    public slots:
        void providerSelectionChanged();
        void providerVisualizationTypeChanged(int x);
        void updateProviderTable();

    signals:
        void providerListChanged();

    private:
        /**
         * Widget Form
         */
        Ui::PointCloudVisualizationWidget widget;
        QPointer<PointCloudVisualizationConfigDialog> configDialog;

        std::string visualizerName;
        visionx::PointCloudVisualizationInterfacePrx visualizer;

        visionx::PointCloudProviderVisualizationInfoList providers;
    };
}

