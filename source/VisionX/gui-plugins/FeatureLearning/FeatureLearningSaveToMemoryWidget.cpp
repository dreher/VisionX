/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Julian Zimmer (urdbu at student dot kit dot edu)
* @date       2018
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "FeatureLearningSaveToMemoryWidget.h"
#include "VisionX/tools/TypeMapping.h"
#include <VisionX/gui-plugins/FeatureLearning/ui_FeatureLearningSaveToMemoryWidget.h>
#include <QPointer>

using namespace armarx;

namespace visionx
{
    FeatureLearningSaveToMemoryWidget::FeatureLearningSaveToMemoryWidget()
    {
        QDialog* dialogPointer = this;
        widget.setupUi(dialogPointer);


        connect(widget.okayButtonBox, SIGNAL(accepted()), this, SLOT(accept()));
        connect(widget.okayButtonBox, SIGNAL(rejected()), this, SLOT(close()));
        connect(widget.objectComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(refreshOverwriteWarningLabel()));

        //widget.overwriteWarningLabel->setStyleSheet("Qlabel { color : red; }");
    }

    FeatureLearningSaveToMemoryWidget::~FeatureLearningSaveToMemoryWidget()
    {

    }

    Ui::FeatureLearningSaveToMemoryWidget* FeatureLearningSaveToMemoryWidget::getWidget()
    {
        return &widget;
    }

    //this method is triggered, when a different item is chosen in the combobox
    //it displays a warning if the user is about to overwrite the feature file present in the memory
    void FeatureLearningSaveToMemoryWidget::refreshOverwriteWarningLabel()
    {
        QString objectId = widget.objectComboBox->itemData(widget.objectComboBox->currentIndex()).toString();
        if (overwriteWarningMap[objectId] == true)
        {
            widget.overwriteWarningLabel->setStyleSheet("font-weight: bold; color: red");
            widget.overwriteWarningLabel->setText("Warning: This Object has Features in the Memory that will be overwritten!");
            widget.overwriteWarningLabel->setFrameShape(QFrame::Shape::StyledPanel);
        }
        else
        {
            widget.overwriteWarningLabel->setText("");
            widget.overwriteWarningLabel->setFrameShape(QFrame::Shape::NoFrame);
        }
    }

    void FeatureLearningSaveToMemoryWidget::setOverwriteWarningMap(QMap<QString, bool> newMap)
    {
        overwriteWarningMap = newMap;
    }

}
