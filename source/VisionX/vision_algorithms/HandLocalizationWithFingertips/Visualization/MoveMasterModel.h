// *****************************************************************
// Filename:    MoveMasterModel.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        25.09.2008
// *****************************************************************

#pragma once

// *****************************************************************
// includes
// *****************************************************************
#include <string>
#include "Math/Math3d.h"
#include "OIFwdKinematicsInterface.h"

#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoCylinder.h>

using namespace std;

#define NO_KEYPOINTS 4
#define NO_JOINTS 26

// *****************************************************************
// Definition of CMoveMasterModel
// *****************************************************************

namespace visionx
{
    class CMoveMasterModel
    {
    public:
        // construction / destruction
        CMoveMasterModel();
        ~CMoveMasterModel();

        // initialize once before use
        bool init(string sFilename, float fFocalLengthY);
        // call update freuently for gui update
        void update();

        // forward kinematics
        void setJointAngle(int nJoint, float fAngleRad);
        Vec3d getPositionOfKeypoint(int nKeyPoint);

        OIFwdKinematicsInterface* m_pOIFwdKinematicsInterface;


        // hand pose (David)
        SoTransform* hand_rot;
        SoTranslation* hand_pos;
        void setHandRotation(double alpha, double beta, double gamma);
        void setHandPosition(Vec3d pos);

        // Object Cylinder (David)
        SoTransform* m_pObjCylRot;
        SoTranslation* m_pObjCylPos;
        SoCylinder* m_pObjectCylinder;
        void setObjCylConfig(Vec3d pos, double alpha, double beta, double gamma, float height, float radius);

        // test sphere (David)
        SoTranslation* m_pTestSpherePos;
        void setTestSpherePosition(Vec3d pos);

    private:
        // names of keypoints and joints (see .cpp file)
        const static string keypoint[NO_KEYPOINTS];
        const static string jointname[NO_JOINTS];

        // members for OI interaction
        SoPath* keypath[NO_KEYPOINTS];
        SbVec3f iposition[NO_KEYPOINTS];
        SoPath* jointpath[NO_JOINTS];

    };
}

