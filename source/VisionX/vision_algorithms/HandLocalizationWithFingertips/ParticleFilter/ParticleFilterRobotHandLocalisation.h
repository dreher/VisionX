/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include "../HandLocalisationConstants.h"
#include "ParticleFilterFrameworkParallelized.h"
#include "../HandModel/HandModelV2.h"
#include "../HandModel/HandModeliCub.h"
#include "Polygon.h"

// IVT
#include "Math/Math3d.h"
#include "Helpers/helpers.h"
#include "Image/ByteImage.h"
#include "Calibration/StereoCalibration.h"

#include <cstdlib>
#include <cmath>

#define NUMBER_OF_CUES 7



namespace visionx
{

    struct DetailedPFRating
    {
        // config that was evaluated
        double dResultConfig[DSHT_NUM_PARAMETERS];

        // number of found correct pixels
        int nNumberOfWhitePixelsInImage;

        int nOverallNumberOfFoundPixels;
        double dOverallFoundPercentageOfExpectedPixels;

        int nNumberOfFoundPixelsOfFinger[DSHT_NUM_FINGERS];
        double dFoundPercentageOfExpectedPixelsOfFinger[DSHT_NUM_FINGERS];

        // results of edge detection
        int  nEdgeSum;
        double dEdgePercentage;
        double dEdgeDirection;

        // rating
        double dPFCueRating;
        double dRating;
        double dConfidenceOfRating;

    };


    class CParticleFilterRobotHandLocalisation : public CParticleFilterFrameworkParallelized
    {
    public:
        // constructor
        CParticleFilterRobotHandLocalisation(int nParticles, int nDimension, int nNumParallelThreads, CStereoCalibration* pCalibration, std::string sHandModelFileName);

        // destructor
        virtual ~CParticleFilterRobotHandLocalisation();


        // public methods

        void SetImages(CByteImage* pRegionImageLeft, CByteImage* pRegionImageRight, CByteImage* pSobelImageLeft, CByteImage* pSobelImageRight,
                       CByteImage* pSobelXImageLeft, CByteImage* pSobelXImageRight, CByteImage* pSobelYImageLeft, CByteImage* pSobelYImageRight);

        void UpdateModel(int nParticleIndex, int nModelIndex) override;
        void UpdateModel(int nParticleIndex) override
        {
            UpdateModel(nParticleIndex, 0);
        }
        void PredictNewBases(double dSigmaFactor) override;
        double CalculateProbability(int nParticleIndex, int nModelIndex) override;
        double CalculateProbability(bool bSeparateCall = true) override
        {
            return CalculateProbability(0, 0);
        }
        void CalculateFinalProbabilities() override;

        // set the configuration of the particles
        void SetParticleConfig(double* pConfig);
        // set the configuration of the first half of the particles, the rest is not changed
        void SetParticleConfigHalf(double* pConfig);
        // set the configuration of ten percent of the particles, the rest is not changed
        void SetConfigOfATenthOfTheParticles(int nTenthIndex, double* pConfig);
        // set the array containing the sensor values
        void SetSensorConfig(double* sconfig);
        // set the allowed deviation of the particle configs from the sensor values
        void SetAllowedDeviation(double* adeviation);

        // get a detailed rating for a config
        void GetDetailedRating(double* pConf, DetailedPFRating* pRating);

        // calc distance between two configs
        double DistanceBetweenConfigs(double* pOldConf, double* pNewConf);

        // set the tracking ball position and size
        void SetTrackingBallPositions(double* dPosX, double* dPosY, double* dRadius, int nNumTrBallRegions, bool bLeftCamImage);

    private:
    public: //TODO: make private again

        // private methods

        void CalculateRegionCue(int& region_sum, int& region_length, int nModelIndex, bool bLeftCamImage);
        void CalculateEdgeCue(int& edge_sum, int& edge_length, double& angle_diffs, int nModelIndex, bool bLeftCamImage);

        // sum up all pixel values of camImage that are inside the polygon pol, and count the number
        // of pixels inside the polygon
        inline void CalculatePixelSumInPolygon(ConvexPolygonCalculations::Polygon* pol, int& region_sum, int& region_length, bool bLeftCamImage);

        inline void CalculatePixelSumOnLineSequence(Vec3d linePoints[], int nPoints, int& edge_sum, int& edge_length, double& angle_diffs, bool bLeftCamImage);

        // calculate error distance to tracking ball
        void CalculateTrackingBallCue(double& dDistanceXY, double& dDistanceZ, int nModelIndex);


        // private attributes

        CHandModelV2** m_pHandModels;
        CByteImage* m_pRegionImageLeft, *m_pRegionImageRight, *m_pSobelImageLeft, *m_pSobelImageRight,
                    *m_pSobelXImageLeft, *m_pSobelXImageRight, *m_pSobelYImageLeft, *m_pSobelYImageRight;

        double* sensor_config;
        double* allowed_deviation;


        double** m_ppProbabilities;
        //int m_nParticleIndex;     // index of the particle which is to be handled next

        ConvexPolygonCalculations::Polygon** m_pTempIntersectionPolygons;
        Vec3d** m_pTempVecArrays;
        Vec3d** m_pTempClockwiseHullPolys1;
        Vec3d** m_pTempClockwiseHullPolys2;
        bool** m_pTempBoolArrays;

        double m_pProbMin[NUMBER_OF_CUES];
        double m_pProbMax[NUMBER_OF_CUES];

        //float m_fFocalLengthX;
        //float m_fFocalLengthY;
        //float m_fPrincipalPointX;
        //float m_fPrincipalPointY;

        double* m_pdTrackingBallPosXLeft, *m_pdTrackingBallPosXRight;
        double* m_pdTrackingBallPosYLeft, *m_pdTrackingBallPosYRight;
        double* m_pdTrackingBallRadiusLeft, *m_pdTrackingBallRadiusRight;
        int m_nNumTrackingBallRegionsLeft, m_nNumTrackingBallRegionsRight;
    };


}





