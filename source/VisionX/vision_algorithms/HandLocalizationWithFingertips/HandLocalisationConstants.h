/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <math.h>
#include "Math/Math3d.h"


namespace visionx
{

    // define the used hand model

    //#define DSHT_USE_ARMAR3A
#define DSHT_USE_ARMAR3B
    //#define DSHT_USE_ICUB

#define DSHT_NUM_FINGERS 5

#ifdef DSHT_USE_ICUB
#define DSHT_MAX_POLYGON_CORNERS 12
#elif DSHT_USE_ARMAR3A
#define DSHT_MAX_POLYGON_CORNERS 22
#else
#define DSHT_MAX_POLYGON_CORNERS 18
#endif

#define DSHT_USE_EDGE_DIRECTION

    //#define DSHT_USE_OLD_HAND_MODEL

#define DSHT_NUM_PARAMETERS 12

#define DSHT_OI_FILE_PATH "/home/SMBAD/schieben/home/MeinSVN/RobotHandTracking/OI-files/rightHand-David.iv"

#ifdef DSHT_USE_ICUB
#define DSHT_HAND_MODEL_PATH "/home/SMBAD/schieben/home/armarx/VisionX/source/VisionX/vision_algorithms/HandLocalizationWithFingertips/HandModel/HandModeliCubRight.txt"
#elif DSHT_USE_ARMAR3A
#define DSHT_HAND_MODEL_PATH "/home/SMBAD/schieben/home/armarx/VisionX/source/VisionX/vision_algorithms/HandLocalizationWithFingertips/HandModel/HandModelArmar3aRight.txt"
#else
#define DSHT_HAND_MODEL_PATH "/home/SMBAD/schieben/home/armarx/VisionX/source/VisionX/vision_algorithms/HandLocalizationWithFingertips/HandModel/HandModelArmar3bRight-2015.txt"
#endif

    // size of the camera image
#define DSHT_IMAGE_WIDTH 640
#define DSHT_IMAGE_HEIGHT 480


    // colors to be segmented

#ifdef DSHT_USE_ARMAR3A
    static const unsigned char default_fingertip_hue = 113;
    static const unsigned char default_fingertip_hue_tolerance = 20;
    static const unsigned char default_fingertip_sat_min = 75;
    static const unsigned char default_fingertip_sat_max = 255;
    static const unsigned char default_fingertip_val_min = 10;
    static const unsigned char default_fingertip_val_max = 150;

    static const unsigned char default_trball_hue = 0;
    static const unsigned char default_trball_hue_tolerance = 10;
    static const unsigned char default_trball_sat_min = 135;
    static const unsigned char default_trball_sat_max = 255;
    static const unsigned char default_trball_val_min = 35;
    static const unsigned char default_trball_val_max = 220;
#else
    static const unsigned char default_fingertip_hue = 127;
    static const unsigned char default_fingertip_hue_tolerance = 50;
    static const unsigned char default_fingertip_sat_min = 60;
    static const unsigned char default_fingertip_sat_max = 255;
    static const unsigned char default_fingertip_val_min = 0;
    static const unsigned char default_fingertip_val_max = 63;

    // green3 59 16 65 164 6 109
    static const unsigned char default_trball_hue = 59;
    static const unsigned char default_trball_hue_tolerance = 16;
    static const unsigned char default_trball_sat_min = 65;
    static const unsigned char default_trball_sat_max = 164;
    static const unsigned char default_trball_val_min = 6;
    static const unsigned char default_trball_val_max = 109;
#endif


    // focal length for x/y-direction
    //const float default_focal_length_x = 896.8084716797;
    //const float default_focal_length_y = 893.5639648438;
    //const float fallback_default_focal_length_x = 530.8067016602;
    //const float fallback_default_focal_length_y = 528.9429321289;


    // constant for pixel/mm conversion 48mm breit, 36mm hoch
    // seems to be included in focal length
    //const float default_pixel_per_mm_factor = 1.0;//40.0/3.0;

    // principal point of the image
    //const float default_principal_point_x = 350.6317443848;
    //const float default_principal_point_y = 257.4656372070;
    //const float fallback_default_principal_point_x = 355.1824035645;
    //const float fallback_default_principal_point_y = 226.5831146240;


    // dimensions of the OI-rendering window
#define DSHT_OI_RENDERSIZE_X 760
#define DSHT_OI_RENDERSIZE_Y 570

    // allowed deviations from the model to the sensor values
    //const double DSHT_default_allowed_deviation[12] = {   100, 100, 100,
    //                                                M_PI/180*20, M_PI/180*20, M_PI/180*20,
    //                                                M_PI/180*5,   M_PI/180*5, M_PI/180*5,
    //                                                M_PI/180*5,  M_PI/180*5,  M_PI/180*5 };
    const double DSHT_default_allowed_deviation[12] = { 120, 120, 120,
                                                        M_PI / 180 * 30, M_PI / 180 * 30, M_PI / 180 * 30,
                                                        M_PI / 180 * 12,   M_PI / 180 * 12, M_PI / 180 * 12,
                                                        M_PI / 180 * 12,  M_PI / 180 * 12,  M_PI / 180 * 12
                                                      };
}


