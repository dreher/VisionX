armarx_component_set_name("ObjectLocalizationExampleApp")

find_package(Eigen3 QUIET)
find_package(Simox 2.1.4 QUIET)

armarx_build_if(Eigen3_FOUND "Eigen3 not available")
armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")
armarx_build_if(MemoryX_FOUND "MemoryX not available")


if (Eigen3_FOUND AND Simox_FOUND AND MemoryX_FOUND)
    include_directories(${MemoryX_INCLUDE_DIRS})
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
    include_directories(${Simox_INCLUDE_DIRS})
endif()

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore MemoryXInterfaces ArmarXCoreObservers RobotAPICore ${Simox_LIBRARIES})

set(EXE_SOURCES
    ObjectLocalizationExampleApp.cpp
    RequestClass.cpp
    main.cpp)

armarx_add_component_executable("${EXE_SOURCES}")

