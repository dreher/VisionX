armarx_component_set_name("PointCloudUtilityApp")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")
if(Eigen3_FOUND)
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox not available")
if(Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
endif()

find_package(PCL 1.8 QUIET)
armarx_build_if(PCL_FOUND "PCL not available")
if(PCL_FOUND)
    include_directories(SYSTEM ${PCL_INCLUDE_DIRS})
    add_definitions(${PCL_DEFINITIONS})
endif()

set(COMPONENT_LIBS
    PointCloudUtility
    MemoryXCore
    VisionXInterfaces
    VisionXPointCloud
    VisionXCore
    ArmarXCoreInterfaces
    ArmarXCore
    RobotAPICore
    ${Simox_LIBRARIES}
)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
