armarx_component_set_name("MultiSensePointCloudProviderApp")

find_package(multisense_lib QUIET)
armarx_build_if(multisense_lib_FOUND "LibMultiSense not available")
if(multisense_lib_FOUND)
    include_directories(SYSTEM ${multisense_lib_INCLUDE_DIRS})
endif()

find_package(OpenCV QUIET)
armarx_build_if(OpenCV_FOUND "OpenCV not available")
if(PCL_FOUND AND OpenCV_FOUND)
    include_directories(SYSTEM ${OpenCV_INCLUDE_DIRS})
endif()

find_package(IVT QUIET)
armarx_build_if(IVT_FOUND "IVT not available")
if(IVT_FOUND)
    include_directories(${IVT_INCLUDE_DIRS})
endif()

find_package(PCL 1.7 QUIET)
armarx_build_if(PCL_FOUND "PCL not available")
if(PCL_FOUND)
    include_directories(SYSTEM ${PCL_INCLUDE_DIRS})
    link_directories(${PCL_LIBRARY_DIRS})
    add_definitions(${PCL_DEFINITIONS})
endif()

set(COMPONENT_LIBS MultiSensePointCloudProvider VisionXInterfaces VisionXPointCloud VisionXCore ArmarXCoreInterfaces ArmarXCore ${multisense_lib_LIBRARIES} ${OpenCV_LIBS})
set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
