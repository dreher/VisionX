armarx_component_set_name(ObjectLearningByPushing)


find_package(IVT COMPONENTS ivt ivtopencv QUIET)
find_package(Eigen3 QUIET)
find_package(Simox QUIET)
find_package(OpenMP QUIET)
find_package(OpenCV QUIET)
armarx_find_qt(QtCore QtGui QtOpenGL)
find_package(PCL 1.8 QUIET)

armarx_build_if(IVT_FOUND "ivt library not found")
armarx_build_if(IVT_ivtopencv_FOUND "ivt opencv bridge not available")
armarx_build_if(Eigen3_FOUND "Eigen3 not available")
armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")
armarx_build_if(OPENMP_FOUND "OpenMP not available")
armarx_build_if(OpenCV_FOUND "OpenCV not available")
armarx_build_if(PCL_FOUND "PCL not available")

if (IVT_FOUND AND Eigen3_FOUND AND Simox_FOUND AND OpenCV_FOUND AND PCL_FOUND)
    include_directories(${IVT_INCLUDE_DIRS})
    include_directories(${Simox_INCLUDE_DIRS})
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
    include_directories(SYSTEM ${OpenCV_INCLUDE_DIRS})
    include_directories(SYSTEM ${PCL_INCLUDE_DIRS})
    link_directories(${PCL_LIBRARY_DIRS})
    add_definitions(${PCL_DEFINITIONS})
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
set(LDFLAGS "${LDFLAGS} ${OpenMP_CXX_FLAGS}")

set(COMPONENT_LIBS VisionXObjectLearningByPushing
                   VisionXInterfaces
                   VisionXCore
                   VisionXTools
                   ArmarXCoreInterfaces
                   ArmarXCore
                   RobotAPICore
                   ArmarXCoreObservers
                   RobotAPIRobotStateComponent
                   )

set(EXE_SOURCE main.cpp)
armarx_add_component_executable("${EXE_SOURCE}")
