/**
\page VisionX-HowTos VisionX HowTos
 \li Implementing an ImageProvider: \subpage VisionX-HowTos-implement-imageprovider
 \li Implementing an ImageProcessor: \subpage VisionX-HowTos-HowTos-implement-imageprocessor
 \li Implementing a PointCloudProvider: \subpage VisionX-HowTos-implement-pointcloudprovider
 \li Implementing a generic PointCloudProcessor: \subpage VisionX-HowTos-HowTos-implement-pointcloudprocessor
 \li Implementing an RGB ObjectLocalizer: \subpage VisionX-HowTos-implement-objectlocalizationprocessor
 \li Setting up the Armar4 seperated head: \subpage VisionX-HowTos-Setting-up-Armar4-head
 \li Calibrating the cameras of the Armar4 head: \subpage VisionX-HowTos-Calibrating-Armar4-Cameras
 \li Calculating point clouds with Armar4 stereo vision: \subpage VisionX-HowTos-Calculating-point-clouds-with-stereo-vision
 \li Creating a new object model: \subpage VisionX-HowTos-Learning-new-Object-Models
 \li Localizing textured objects with Armar4: \subpage VisionX-HowTos-Localizing-textured-objects-with-Armar4
 \li Running the VisionX Pipeline: \subpage VisionX-HowTos-Running-VisionX-Pipeline

\section VisionX-HowTos-objectlocalization How to implement an object localizer

If you object localizer is purely image based, there is a framework prepared that you can use and fill with your own functionality.
Follow these instructions on implementing an ObjectLocalizationProcessor: \ref VisionX-HowTos-implement-objectlocalizationprocessor

Otherwise, you need to implement the Ice interface memoryx::ObjectLocalizerInterface.
Depending on the input you use you should also inherit from the visionx::PointCloudProcessor or visionx::PointCloudAndImageProcessor.
If you encounter compilation problems due to multiple inheritance, follow the instructions here: \ref ArmarXCore-FAQ-Multiple-Inheritance "I have compilation problems due to multiple inheritance".

Essentially, you have to implement the function ObjectLocalizationResultList localizeObjectClasses(ObjectClassNameList objectClassNames) from the interface.
The input is a list of (usually just one) object names, and the output is a list of localization results.
You can potentially return several results for the same object.

This function is automatically called by MemoryX when somebody requested the localization of an object.
The appropriate localizer for the object is saved in the object class in the memory (usually either in PriorKnowledge or LongtermMemory).
When the object is in the PriorKowledge, the localizer can be set using the PriorKnowledgeEditor gui plugin.
Otherwise, the localizer for an object class can be set like this:
\code
#include <MemoryX/libraries/helpers/ObjectRecognitionHelpers/ObjectRecognitionWrapper.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

...

// create object class (or get it from somewhere)
memoryx::ObjectClassPtr newObjectClass = memoryx::ObjectClassPtr(new memoryx::ObjectClass());
newObjectClass->setName(objectName);

// set localizer and default motion model
memoryx::ObjectRecognitionWrapperPtr recognitionWrapper = newObjectClass->addWrapper(new memoryx::ObjectRecognitionWrapper());
recognitionWrapper->setDefaultMotionModel("Static");
recognitionWrapper->setRecognitionMethod("MyGloriousLocalizer");
\endcode
*/
