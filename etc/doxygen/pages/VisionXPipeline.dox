/*!
\page VisionX-HowTos-Running-VisionX-Pipeline Running the VisionX Pipeline



\section VisionX-HowTos-Running-VisionX-Pipeline-Provider Getting point clouds from a provider
The VisionX pipeline is a chain of components that process point clouds. Therefore, point clouds have to be provided in the first component.

\subsection VisionX-HowTos-Running-VisionX-Pipeline-Provider-Scenario Creating the scenario

Create a new scenario in the VisionX package using the \a ScenarioManager and add the \a FakePointCloudProvider component. As the provider depends on the \a RobotStateComponent, it has to be added as well.

Using the standard configuration, the \a FakePointCloudProvider alternatingly provides the three point clouds located in VisionX/data/VisionX/examples/point_clouds/. These point clouds are all relatively similar and show a scene of chair in front of a table. 

Editing the \a pointCloudFileName variable in the \a FakePointCloudProvider config allows you to use another point cloud. In this tutorial the default point clouds will be used.

The \a RobotStateComponent needs information about the robot you are using. In its configuration, the \a AgentName, \a RobotFileName and the \a RobotNodeSetName have to be entered. If you don't have a specific robot in mind, just use the data for the Armar3 robot:

\code
AgentName = Armar3
RobotFileName = RobotAPI/robots/Armar3/ArmarIII.xml
RobotNodeSetName = Robot

\endcode



\image html VisionXPipeline-RobotStateComponentSetting.png "The standard setup using the \a FakePointCloudProvider"

\subsection VisionX-HowTos-Running-VisionX-Pipeline-Provider-Executing Executing the scenario

To test the provider, add the \a PointCloudViewer widget to the gui and start the scenario in the \a ScenarioManager. After refreshing and selecting the provider, the result should look similar to the image below.

\warning Be sure to uncheck the point cloud you were viewing before stopping the scenario to avoid crashes of the ArmarX GUI!

If you want to avoid the rapid changes between the different scenes, you can change the file path to one the three scenes:
\code
pointCloudFileName = VisionX/examples/point_clouds/test03.pcd
\endcode

\image html VisionXPipeline-StandardScene.png "The resulting point cloud"


\subsection VisionX-HowTos-Running-VisionX-Pipeline-Provider-Other Other potential providers

Instead of using the stored point clouds, you can of course use live images. 

If you are working with the Armar4 head, you can use the how to on \ref VisionX-HowTos-Calculating-point-clouds-with-stereo-vision which explains how point clouds can be generated using stereoscopy and the Armar4 head cameras.

If you want to work with a kinect-like camera like the Xtion PRO, the \a OpenNIPointCloudProvider will be able to provide these point clouds.

\section VisionX-HowTos-Running-VisionX-Pipeline-Segmenter Segmenting the point clouds

Now that the point clouds are available, they can be segmented. To do this, add the \a PointCloudSegmenter to the scenario. It will continously try to find segments in the point clouds provided. Change the \a providerName, if you didn't use the \a FakePointCloudProvider in the previous step.

The quality of the segmentation highly depends on the segmentation parameters. Instead of \a EC, the \a LCCP method as the \a segmentationMethod is recommended. Regarding the rest of the variables, the \a lccpSeedResolution and the \a lccpVoxelResolution are the most important.

For the default scene, these parameters create decent results (as can be seen in the image below):

\code
lccpSeedResolution = 40
lccpVoxelResolution = 15
\endcode

\image html VisionXPipeline-Segmentation.png "Segmentation result of the default scene"

Of course you can try different parameters to see what works best for your scene (maybe also varying the other lccp variables). 

\section VisionX-HowTos-Running-VisionX-Pipeline-Extractor Extracting primitives from the point clouds

The next component in the VisionX pipeline is the \a PrimitiveExtractor. It receives the segmented point cloud and tries to estimate shape primitives in the scene (planes, spheres, cylinders). Since the \a PrimitiveExtractor writes the estimated primitives to the memory, these memory components have to be added to the scenario as well:

\a WorkingMemory, \a PriorKnowledge, \a LongTermMemory and \a CommonStorage.

Here are the changes that need to be applied to the respective configurations:

\a WorkingMemory:
no changes

\a PriorKnowledge:
\code
ClassCollections = memdb.Prior_KitchenKK,memdb.Prior_KitchenKKObjects
\endcode

\a LongTermMemory:
\code
DatabaseName = memdb
\endcode

\a CommonStorage
\code
MongoAuth = 0
MongoUser = 1
MongoPassword = 1
\endcode

The configuration of the \a PrimitiveExtractor includes many parameters which influence the performance of the estimation. Generally, all the parameters ending on ...DistanceThreshold or ...NormalDistance can be altered.

But for the scene in this tutorial, the default parameters actually produce an okay result:


\image html VisionXPipeline-Primitives-2.png "The primitives calculated from the default scene"

The point cloud viewer can of course only show the inlier of the respective primitives that were estimated. To get an overview over the estimation in the scene, you can take a look at the textual output of the \a PrimitiveExtractor:

\code
[29417][15:01:34.729][PrimitiveExtractor][PrimitiveExtractor]: ********* Detected Plane number: 24
[29417][15:01:34.729][PrimitiveExtractor][PrimitiveExtractor]: ********* Detected Cylinder number: 0
[29417][15:01:34.729][PrimitiveExtractor][PrimitiveExtractor]: ********* Detected Sphere number: 0
[29417][15:01:34.729][PrimitiveExtractor][PrimitiveExtractor]: ********* Detected Circle Probabilities: 24
...
[29417][15:01:34.733][PrimitiveExtractor][PrimitiveExtractor]: Primitive Extraction took 1.85691 secs
...
[29417][15:01:35.012][PrimitiveExtractor][PrimitiveExtractor]: Point cloud received (timestamp: 15:01:34.570)
[29417][15:01:35.014][PrimitiveExtractor][PrimitiveExtractor]: --------- Segments -----------  19 pointsize: 76800
[29417][15:01:35.014][PrimitiveExtractor][PrimitiveExtractor]: --------------------
\endcode

Additionally, the estimated primitives are written to the memory and can be inspected in the \a WorkingMemoryGui widget under the \a environmentalPrimitives tab.

\image html VisionXPipeline-Primitives-in-Memory.png "The estimated primitives in the WorkingMemory"



\section VisionX-HowTos-Running-VisionX-Pipeline-Visualizer Visualization of the primitives

To get a better visualization of the primitives, apart from their point cloud inliers, the \a PrimitiveVisualization can be used. When the according application is added, the default configuration should be sufficient to view the primitives. You need to have added the \a WorkingMemoryGui widget and are then able to see a representation in the \a 3DViewer that opened together with the \a WoringMemoryGui.


\image html VisionXPipeline-Primitives-Visualization.png "A visualization of the detected primitives."



*/
