# This file contains macros for projects depending on VisionX


macro(armarx_build_if_not_opencv3)
    find_package(OpenCV QUIET)
    if(${OpenCV_VERSION} GREATER 3)
        armarx_build_if(dummy_variable_to_disable "opencv3 was found but opencv is not supported yet - path: ${OpenCV_INCLUDE_DIRS}")
    endif()
endmacro()
