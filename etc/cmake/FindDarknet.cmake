# This module defines the following variables:
# Darknet_FOUND   : 1 if Darknet was found, 0 otherwise
# Darknet_LIBRARIES : Darknet location
# Darknet_INCLUDE_DIRS: directory where the headers can be found
cmake_policy(SET CMP0015 NEW)
include(FindPackageHandleStandardArgs)

# to set the relative path
set(DARKNET_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../../../../../lib/)

find_library(Darknet_LIBRARIES Names darknet HINTS ${Darknet_LIBRARY_DIRS} ${DARKNET_PATH} PATHS ${DARKNET_PATH} PATH_SUFFIXES lib lib64 NO_DEFAULT_PATH)

find_package_handle_standard_args(Darknet DEFAULT_MSG Darknet_LIBRARIES)

# Hack: since the macro makes the package name uppercase
set(Darknet_FOUND ${DARKNET_FOUND})

mark_as_advanced(Darknet_LIBRARIES Darknet_LIBRARIES)
